// Copyright (c) 2018 Doyub Kim
//
// I am making my contributions/submissions to this project solely in my
// personal capacity and am not conveying any rights to any intellectual
// property of any third parties.

#ifndef INCLUDE_JET_FLIP_SOLVER2_H_
#define INCLUDE_JET_FLIP_SOLVER2_H_

#include "pic_solver2.h"

namespace vox {

//!
//! \brief 2-D Fluid-Implicit Particle (FLIP) implementation.
//!
//! This class implements 2-D Fluid-Implicit Particle (FLIP) solver from the
//! SIGGRAPH paper, Zhu and Bridson 2005. By transferring delta-velocity field
//! from grid to particles, the FLIP solver achieves less viscous fluid flow
//! compared to the original PIC method.
//!
//! \see Zhu, Yongning, and Robert Bridson. "Animating sand as a fluid."
//!     ACM Transactions on Graphics (TOG). Vol. 24. No. 3. ACM, 2005.
//!
class FlipSolver2 : public PicSolver2 {
public:
  class Builder;

  //! Default constructor.
  FlipSolver2();

  //! Constructs solver with initial grid size.
  FlipSolver2(const Vector2UZ &resolution, const Vector2D &gridSpacing, const Vector2D &gridOrigin);

  //! Deleted copy constructor.
  FlipSolver2(const FlipSolver2 &) = delete;

  //! Deleted move constructor.
  FlipSolver2(FlipSolver2 &&) noexcept = delete;

  //! Default virtual destructor.
  ~FlipSolver2() override = default;

  //! Deleted copy assignment operator.
  FlipSolver2 &operator=(const FlipSolver2 &) = delete;

  //! Deleted move assignment operator.
  FlipSolver2 &operator=(FlipSolver2 &&) noexcept = delete;

  //! Returns the PIC blending factor.
  [[nodiscard]] double picBlendingFactor() const;

  //!
  //! \brief  Sets the PIC blending factor.
  //!
  //! This function sets the PIC blending factor which mixes FLIP and PIC
  //! results when transferring velocity from grids to particles in order to
  //! reduce the noise. The factor can be a value between 0 and 1, where 0
  //! means no blending and 1 means full PIC. Default is 0.
  //!
  //! \param[in]  factor The blending factor.
  //!
  void setPicBlendingFactor(double factor);

  //! Returns builder fox FlipSolver2.
  static Builder builder();

protected:
  //! Transfers velocity field from particles to grids.
  void transferFromParticlesToGrids() override;

  //! Transfers velocity field from grids to particles.
  void transferFromGridsToParticles() override;

private:
  double _picBlendingFactor = 0.0;
  Array2<float> _uDelta;
  Array2<float> _vDelta;
};

//! Shared pointer type for the FlipSolver2.
using FlipSolver2Ptr = std::shared_ptr<FlipSolver2>;

//!
//! \brief Front-end to create FlipSolver2 objects step by step.
//!
class FlipSolver2::Builder final : public GridFluidSolverBuilderBase2<FlipSolver2::Builder> {
public:
  //! Builds FlipSolver2.
  [[nodiscard]] FlipSolver2 build() const;

  //! Builds shared pointer of FlipSolver2 instance.
  [[nodiscard]] FlipSolver2Ptr makeShared() const;
};

} // namespace  vox

#endif // INCLUDE_JET_FLIP_SOLVER2_H_
