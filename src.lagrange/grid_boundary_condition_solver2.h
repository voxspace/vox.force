// Copyright (c) 2018 Doyub Kim
//
// I am making my contributions/submissions to this project solely in my
// personal capacity and am not conveying any rights to any intellectual
// property of any third parties.

#ifndef INCLUDE_JET_GRID_BOUNDARY_CONDITION_SOLVER2_H_
#define INCLUDE_JET_GRID_BOUNDARY_CONDITION_SOLVER2_H_

#include "../src.common/collider.h"
#include "../src.common/constants.h"
#include "../src.common/fields/scalar_field.h"
#include "../src.common/grids/face_centered_grid.h"

#include <memory>

namespace vox {

//!
//! \brief Abstract base class for 2-D boundary condition solver for grids.
//!
//! This is a helper class to constrain the 2-D velocity field with given
//! collider object. It also determines whether to open any domain boundaries.
//! To control the friction level, tune the collider parameter.
//!
class GridBoundaryConditionSolver2 {
public:
  //! Default constructor.
  GridBoundaryConditionSolver2() = default;

  //! Deleted copy constructor.
  GridBoundaryConditionSolver2(const GridBoundaryConditionSolver2 &) = delete;

  //! Deleted move constructor.
  GridBoundaryConditionSolver2(GridBoundaryConditionSolver2 &&) noexcept = delete;

  //! Default virtual destructor.
  virtual ~GridBoundaryConditionSolver2() = default;

  //! Deleted copy assignment operator.
  GridBoundaryConditionSolver2 &operator=(const GridBoundaryConditionSolver2 &) = delete;

  //! Deleted move assignment operator.
  GridBoundaryConditionSolver2 &operator=(GridBoundaryConditionSolver2 &&) noexcept = delete;

  //! Returns associated collider.
  [[nodiscard]] const Collider2Ptr &collider() const;

  //!
  //! \brief Applies new collider and build the internals.
  //!
  //! This function is called to apply new collider and build the internal
  //! cache. To provide a hint to the cache, info for the expected velocity
  //! grid that will be constrained is provided.
  //!
  //! \param newCollider New collider to apply.
  //! \param gridSize Size of the velocity grid to be constrained.
  //! \param gridSpacing Grid spacing of the velocity grid to be constrained.
  //! \param gridOrigin Origin of the velocity grid to be constrained.
  //!
  void updateCollider(const Collider2Ptr &newCollider, const Vector2UZ &gridSize, const Vector2D &gridSpacing,
                      const Vector2D &gridOrigin);

  //! Returns the closed domain boundary flag.
  [[nodiscard]] int closedDomainBoundaryFlag() const;

  //! Sets the closed domain boundary flag.
  void setClosedDomainBoundaryFlag(int flag);

  //!
  //! Constrains the velocity field to conform the collider boundary.
  //!
  //! \param velocity Input and output velocity grid.
  //! \param extrapolationDepth Number of inner-collider grid cells that
  //!     velocity will get extrapolated.
  //!
  virtual void constrainVelocity(FaceCenteredGrid2 *velocity, unsigned int extrapolationDepth = 5) = 0;

  //! Returns the signed distance field of the collider.
  [[nodiscard]] virtual ScalarField2Ptr colliderSdf() const = 0;

  //! Returns the velocity field of the collider.
  [[nodiscard]] virtual VectorField2Ptr colliderVelocityField() const = 0;

protected:
  //! Invoked when a new collider is set.
  virtual void onColliderUpdated(const Vector2UZ &gridSize, const Vector2D &gridSpacing,
                                 const Vector2D &gridOrigin) = 0;

  //! Returns the size of the velocity grid to be constrained.
  [[nodiscard]] const Vector2UZ &gridSize() const;

  //! Returns the spacing of the velocity grid to be constrained.
  [[nodiscard]] const Vector2D &gridSpacing() const;

  //! Returns the origin of the velocity grid to be constrained.
  [[nodiscard]] const Vector2D &gridOrigin() const;

private:
  Collider2Ptr _collider;
  Vector2UZ _gridSize;
  Vector2D _gridSpacing;
  Vector2D _gridOrigin;
  int _closedDomainBoundaryFlag = kDirectionAll;
};

//! Shared pointer type for the GridBoundaryConditionSolver2.
using GridBoundaryConditionSolver2Ptr = std::shared_ptr<GridBoundaryConditionSolver2>;

} // namespace  vox

#endif // INCLUDE_JET_GRID_BOUNDARY_CONDITION_SOLVER2_H_
