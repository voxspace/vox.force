//
//  grid_data_weno.hpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef grid_data_weno_hpp
#define grid_data_weno_hpp

#include "grid_data.h"
#include "weno_auxiliary.h"

namespace vox {

template <int dim, int order> class GridDataWENO : public GridData<dim> {
public:
  class Builder;
  using point_t = typename Mesh<dim>::point_t;

  //! Constructs grid data with given grid.
  GridDataWENO(size_t idx, MeshPtr<dim> grid, WENOAuxiliaryPtr<dim, order> aux);
  //! Copy constructor.
  GridDataWENO(const GridDataWENO &other);
  //! Copies from other grid data.
  GridDataWENO &operator=(const GridDataWENO &other);
  //! Destructor.
  virtual ~GridDataWENO();
  //! Returns the copy of the grid instance.
  std::shared_ptr<GridData<dim>> deepCopy() override;

public:
  void calculateBasisFunction() override;

private:
  void calculateBasisFunction(Vector<size_t, dim> i, double epsilon);

  double weight(typename PolyInfo<dim, order>::Vec &slope, const Vector<size_t, dim>& i);

  void calculateOnePatch(Vector<size_t, dim> i, size_t j);

  void solve(Vector<size_t, dim> i, size_t j, const double &UU, const std::vector<double> &bary_info,
             typename PolyInfo<dim, order>::Vec &result);

public:
  //! return value of specific point
  double value(const point_t &pt, Vector<size_t, dim> i) override;

  //! return value of specific point
  std::array<double, dim> gradient(const point_t &pt, Vector<size_t, dim> i) override;

private:
  const WENOAuxiliaryPtr<dim, order> wenoAux;

  struct ReconCache {
    std::vector<typename PolyInfo<dim, order>::Vec> grad_choice;

    typename PolyInfo<dim, order>::Vec Slope;
  };
  Array<ReconCache, dim> recon_cache;

  using GridData<dim>::_grid;
  using GridDataBase<dim>::_gridData;

  friend class GridData1DWENOTest;
  friend class GridData2DWENOTest;
  friend class GridData2DAdaptWENOTest;
  friend class GridData3DWENOTest;
};
template <int dim, int order> using GridDataWENOPtr = std::shared_ptr<GridDataWENO<dim, order>>;

//----------------------------------------------------------------------------------------------------------------------
// MARK:- Builder class
template <int dim, int order> class GridDataWENO<dim, order>::Builder : public GridData<dim>::Builder {
public:
  void setWENOAuxiliary(WENOAuxiliaryPtr<dim, order> aux);

  GridDataPtr<dim> build(size_t idx, MeshPtr<dim> grid) override;

private:
  WENOAuxiliaryPtr<dim, order> wenoAux;
};
template <int dim, int order>
using GridDataWENOBuilderPtr = std::shared_ptr<typename GridDataWENO<dim, order>::Builder>;

} // namespace vox
#endif /* grid_data_weno_hpp */
