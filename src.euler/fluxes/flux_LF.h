//
//  flux_LF.hpp
//  Flames
//
//  Created by 杨丰 on 2020/5/1.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef flux_LF_hpp
#define flux_LF_hpp

#include "../flux_base.h"

namespace vox {
template <int dim, int dos> class FluxLF : public FluxBase<dim, dos> {
public:
  void NumericalFlux(const Vector<double, dos> &ul, const Vector<double, dos> &ur,
                     const std::array<double, dim> &outNormal, double para, const ConvEquationPtr<dim, dos> &EqInfo,
                     Vector<double, dos> &flux) override {
    double c1 = EqInfo->MaxCharacteristicSpeed(ul);
    double c2 = EqInfo->MaxCharacteristicSpeed(ur); // just norm max_speed can't work
    Vector<double, dos> flux_l, flux_r;
    EqInfo->TFlux(ul, outNormal, flux_l);
    EqInfo->TFlux(ur, outNormal, flux_r);
    flux_l *= 0.5;
    flux_r *= 0.5;

    flux = ul;
    flux -= ur;
    flux *= std::max(c1, c2);
    flux *= 0.5;
    flux += flux_l;
    flux += flux_r;
  }
};
template <int dim, int dos> using FluxLFPtr = std::shared_ptr<FluxLF<dim, dos>>;
} // namespace vox
#endif /* flux_LF_hpp */
