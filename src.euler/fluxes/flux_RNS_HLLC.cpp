//
//  flux_RNS_HLLC.cpp
//  Flames
//
//  Created by 杨丰 on 2020/5/12.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "flux_RNS_HLLC.h"
#include "../../src.common/math_utils.h"
#include "../../src.common/private_helpers.h"
#include "../equations/equation_reactingNS.h"

namespace vox {

#define GAMMA 1.4

#define ROE_AVERAGE(U_l, U_r, sqrt_rho_l, sqrt_rho_r)                                                                  \
  (((sqrt_rho_l) * (U_l) + (sqrt_rho_r) * (U_r)) / (sqrt_rho_l + sqrt_rho_r))

// MARK:- Internal Namespace
namespace {
template <int dim> struct SolutionCache {
  std::array<double, dim> v;
  double p;
  double H;
  double c;
  double entropy;
  double f1; /// reaction term
};

template <int dim>
void getAuxiliaryVariables(const Vector<double, EquationReactingNS<dim>::DOS> &u, EquationReactingNSPtr<dim> Eq,
                           SolutionCache<dim> &sc) {
  for (size_t i = 0; i < sc.v.size(); ++i) {
    sc.v[i] = u[i + 1] / u[0];
  }

  sc.p = Eq->pressure(u);
  sc.H = (u[dim + 1] + sc.p) / u[0];
  sc.c = std::sqrt(sc.p / u[0] * Eq->Gamma);
  sc.entropy = sc.p / pow(u[0], Eq->Gamma);
  sc.f1 = u[EquationReactingNS<dim>::DOS - 1] / u[0];
}

template <class C> typename C::value_type innerProduct(const C &c0, const C &c1) {
  typename C::value_type v = 0;
  auto the_v0 = c0.begin(), end_v0 = c0.end(), the_v1 = c1.begin();
  for (; the_v0 != end_v0; ++the_v0, ++the_v1) {
    v += (*the_v0) * (*the_v1);
  }
  return v;
}
} // namespace

// MARK:- Flux
template <int dim>
void FluxRNSHLLC<dim>::NumericalFlux(const Vector<double, dos> &ul, const Vector<double, dos> &ur,
                                     const std::array<double, dim> &outNormal, double para,
                                     const ConvEquationPtr<dim, dos> &EqInfo, Vector<double, dos> &flux) {
  UNUSED_VARIABLE(para);

  EquationReactingNSPtr<dim> eqInfo = std::static_pointer_cast<EquationReactingNS<dim>>(EqInfo);
  SolutionCache<dim> U_l, U_r;
  getAuxiliaryVariables(ul, eqInfo, U_l);
  getAuxiliaryVariables(ur, eqInfo, U_r);

  // HLLC
  const double &rho_l = ul[0];
  const double &rho_r = ur[0];

  const double &e_l = ul[EquationReactingNS<dim>::DOS - 2];
  const double &e_r = ur[EquationReactingNS<dim>::DOS - 2];

  const std::array<double, dim> &u_l = U_l.v;
  const std::array<double, dim> &u_r = U_r.v;

  const double &p_l = U_l.p;
  const double &p_r = U_r.p;

  const double &c_l = U_l.c;
  const double &c_r = U_r.c;

  const double &H_l = U_l.H;
  const double &H_r = U_r.H;

  const double &rhor_l = ul[EquationReactingNS<dim>::DOS - 1];
  const double &rhor_r = ur[EquationReactingNS<dim>::DOS - 1];

  double q_l = 0;
  double q_r = 0;
  for (int i = 0; i < dim; i++) {
    q_l += u_l[i] * outNormal[i];
    q_r += u_r[i] * outNormal[i];
  }

  double lambda_1l = q_l - c_l;
  double lambda_mr = q_r + c_r;

  double sqrt_rho_l = sqrt(rho_l);
  double sqrt_rho_r = sqrt(rho_r);
  std::array<double, dim> u_roe{};
  for (int i = 0; i < dim; i++) {
    u_roe[i] = ROE_AVERAGE(u_l[i], u_r[i], sqrt_rho_l, sqrt_rho_r);
  }
  double H_roe = ROE_AVERAGE(H_l, H_r, sqrt_rho_l, sqrt_rho_r);
  double c_roe = sqrt((GAMMA - 1) * (H_roe - 0.5 * innerProduct(u_roe, u_roe)));
  double q_roe = innerProduct(u_roe, outNormal);

  double lambda_1roe = q_roe - c_roe;
  double lambda_mroe = q_roe + c_roe;

  double S_l = std::min(lambda_1l, lambda_1roe);
  double S_r = std::max(lambda_mroe, lambda_mr);

  double S_m =
      (rho_r * q_r * (S_r - q_r) - rho_l * q_l * (S_l - q_l) + p_l - p_r) / (rho_r * (S_r - q_r) - rho_l * (S_l - q_l));

  if (S_l > 0) {
    flux[0] = rho_l * q_l;
    for (int i = 0; i < dim; i++) {
      flux[i + 1] = ul[i + 1] * q_l + p_l * outNormal[i];
    }
    flux[EquationReactingNS<dim>::DOS - 2] = (ul[EquationReactingNS<dim>::DOS - 2] + p_l) * q_l;
    flux[EquationReactingNS<dim>::DOS - 1] = rhor_l * q_l;
  } else if (S_m > 0) {
    std::vector<double> U_ast(EquationReactingNS<dim>::DOS);
    double Omega_l = 1.0 / (S_l - S_m);
    double p_ast = rho_l * (q_l - S_l) * (q_l - S_m) + p_l;

    U_ast[0] = Omega_l * ((S_l - q_l) * rho_l);
    for (int i = 0; i < dim; i++) {
      U_ast[i + 1] = Omega_l * ((S_l - q_l) * ul[i + 1] + (p_ast - p_l) * outNormal[i]);
    }
    U_ast[EquationReactingNS<dim>::DOS - 2] = Omega_l * ((S_l - q_l) * e_l - p_l * q_l + p_ast * S_m);
    U_ast[EquationReactingNS<dim>::DOS - 1] = Omega_l * ((S_l - q_l) * rhor_l);

    flux[0] = U_ast[0] * S_m;
    for (int i = 0; i < dim; i++) {
      flux[i + 1] = U_ast[i + 1] * S_m + p_ast * outNormal[i];
    }
    flux[EquationReactingNS<dim>::DOS - 2] = (U_ast[EquationReactingNS<dim>::DOS - 2] + p_ast) * S_m;
    flux[EquationReactingNS<dim>::DOS - 1] = U_ast[EquationReactingNS<dim>::DOS - 1] * S_m;
  } else if (S_r > 0) {
    std::vector<double> U_ast(EquationReactingNS<dim>::DOS);
    double Omega_r = 1.0 / (S_r - S_m);
    double p_ast = rho_r * (q_r - S_r) * (q_r - S_m) + p_r;

    U_ast[0] = Omega_r * ((S_r - q_r) * rho_r);
    for (int i = 0; i < dim; i++) {
      U_ast[i + 1] = Omega_r * ((S_r - q_r) * ur[i + 1] + (p_ast - p_r) * outNormal[i]);
    }
    U_ast[EquationReactingNS<dim>::DOS - 2] = Omega_r * ((S_r - q_r) * e_r - p_r * q_r + p_ast * S_m);
    U_ast[EquationReactingNS<dim>::DOS - 1] = Omega_r * ((S_r - q_r) * rhor_r);

    flux[0] = U_ast[0] * S_m;
    for (int i = 0; i < dim; i++) {
      flux[i + 1] = U_ast[i + 1] * S_m + p_ast * outNormal[i];
    }
    flux[EquationReactingNS<dim>::DOS - 2] = (U_ast[EquationReactingNS<dim>::DOS - 2] + p_ast) * S_m;
    flux[EquationReactingNS<dim>::DOS - 1] = U_ast[EquationReactingNS<dim>::DOS - 1] * S_m;
  } else {
    flux[0] = rho_r * q_r;
    for (int i = 0; i < dim; i++) {
      flux[i + 1] = ur[i + 1] * q_r + p_r * outNormal[i];
    }
    flux[EquationReactingNS<dim>::DOS - 2] = (ur[EquationReactingNS<dim>::DOS - 2] + p_r) * q_r;
    flux[EquationReactingNS<dim>::DOS - 1] = rhor_r * q_r;
  }
}

#undef GAMMA

template class FluxRNSHLLC<1>;
template class FluxRNSHLLC<2>;
template class FluxRNSHLLC<3>;
} // namespace vox
