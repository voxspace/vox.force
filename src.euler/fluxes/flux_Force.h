//
//  flux_Force.hpp
//  Flames
//
//  Created by 杨丰 on 2020/5/12.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef flux_Force_hpp
#define flux_Force_hpp

#include "../flux_base.h"

namespace vox {
template <int dim, int dos> class FluxForce : public FluxBase<dim, dos> {
public:
  void NumericalFlux(const Vector<double, dos> &ul, const Vector<double, dos> &ur,
                     const std::array<double, dim> &outNormal, double para, const ConvEquationPtr<dim, dos> &EqInfo,
                     Vector<double, dos> &flux) override {
    // FORCE
    Vector<double, dos> fl, fr;
    EqInfo->TFlux(ul, outNormal, fl);
    EqInfo->TFlux(ur, outNormal, fl);

    fl *= para;
    fl += ul;
    fr *= -para;
    fr += ur;
    Vector<double, dos> u_star = fl;
    u_star += fr;
    u_star *= 0.5;
    Vector<double, dos> flux_lw;
    EqInfo->TFlux(u_star, outNormal, flux_lw); // calculate flux_lw
    flux = fl;
    flux -= fr;
    flux /= 2. * para; // calculate flux_lf
    flux += flux_lw;
    flux *= 0.5;
  }
};
template <int dim, int dos> using FluxForcePtr = std::shared_ptr<FluxForce<dim, dos>>;
} // namespace vox
#endif /* flux_Force_hpp */
