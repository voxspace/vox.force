//
//  flux_NS_Positive.cpp
//  Flames
//
//  Created by 杨丰 on 2020/5/12.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "flux_NS_Positive.h"
#include "../../src.common/private_helpers.h"
#include "../equations/equation_NS.h"

namespace vox {
// MARK:- Flux
template <int dim>
void FluxNSPositive<dim>::NumericalFlux(const Vector<double, dos> &ul, const Vector<double, dos> &ur,
                                        const std::array<double, dim> &outNormal, double para,
                                        const ConvEquationPtr<dim, dos> &EqInfo, Vector<double, dos> &flux) {
  UNUSED_VARIABLE(para);
  const double eps = 1.0e-10;

  EquationNSPtr<dim> eqInfo = std::static_pointer_cast<EquationNS<dim>>(EqInfo);
  double gamma = eqInfo->Gamma;

  double vel1, c1;
  double vel2, c2;
  eqInfo->CharacteristicSpeed(ul, vel1, c1);
  eqInfo->CharacteristicSpeed(ur, vel2, c2);

  double alpha_l = vel1 + c1 * std::sqrt((gamma - 1) / gamma / 2.0);
  double alpha_r = vel2 + c2 * std::sqrt((gamma - 1) / gamma / 2.0);

  Vector<double, dos> flux_l, flux_r;
  EqInfo->TFlux(ul, outNormal, flux_l);
  EqInfo->TFlux(ur, outNormal, flux_r);
  flux_l *= 0.5;
  flux_r *= 0.5;

  flux = ul;
  flux -= ur;
  flux *= std::max(alpha_l, alpha_r) + eps;
  flux *= 0.5;
  flux += flux_l;
  flux += flux_r;
}

template <int dim>
void FluxNSPositive<dim>::NumericalFlux(const Vector<double, dos> &ul, const Vector<double, dos> &ur,
                                        const Vector<Vector<double, dim>, dos> &gl,
                                        const Vector<Vector<double, dim>, dos> &gr,
                                        const std::array<double, dim> &outNormal, double para,
                                        const ConvEquationPtr<dim, dos> &EqInfo, Vector<double, dos> &flux,
                                        bool isOnlyViscous) {
  UNUSED_VARIABLE(para);
  const double eps = 1.0e-10;

  EquationNSPtr<dim> eqInfo = std::static_pointer_cast<EquationNS<dim>>(EqInfo);
  double beta_l = eqInfo->PositiveFLuxPara(ul, gl, outNormal, isOnlyViscous);
  double beta_r = eqInfo->PositiveFLuxPara(ur, gr, outNormal, isOnlyViscous);

  Vector<double, dos> flux_l, flux_r;
  EqInfo->TFlux(ul, gl, outNormal, flux_l, isOnlyViscous);
  EqInfo->TFlux(ur, gr, outNormal, flux_r, isOnlyViscous);
  flux_l *= 0.5;
  flux_r *= 0.5;

  flux = ul;
  flux -= ur;
  flux *= std::max(beta_l, beta_r) + eps;
  flux *= 0.5;
  flux += flux_l;
  flux += flux_r;
}

template class FluxNSPositive<1>;
template class FluxNSPositive<2>;
template class FluxNSPositive<3>;

} // namespace vox
