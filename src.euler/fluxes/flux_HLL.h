//
//  flux_HLL.hpp
//  Flames
//
//  Created by 杨丰 on 2020/5/12.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef flux_HLL_hpp
#define flux_HLL_hpp

#include "../flux_base.h"

namespace vox {
template <int dim, int dos> class FluxHLL : public FluxBase<dim, dos> {
public:
  void NumericalFlux(const Vector<double, dos> &ul, const Vector<double, dos> &ur,
                     const std::array<double, dim> &outNormal, double para, const ConvEquationPtr<dim, dos> &EqInfo,
                     Vector<double, dos> &flux) override {
    // HLL
    double c_l, c_r, c_l2, c_r2;
    EqInfo->MaxSpeeds(ul, outNormal, c_l, c_r);
    EqInfo->MaxSpeeds(ur, outNormal, c_l2, c_r2);
    c_l = std::min(c_l, c_l2);
    c_r = std::max(c_r, c_r2);
    if (c_l > 0) {
      EqInfo->TFlux(ul, outNormal, flux);
      return;
    }
    if (c_r < 0) {
      EqInfo->TFlux(ur, outNormal, flux);
      return;
    }
    Vector<double, dos> flux_l, flux_r;
    EqInfo->TFlux(ul, outNormal, flux_l);
    EqInfo->TFlux(ur, outNormal, flux_r);
    flux_l *= c_r;
    flux_r *= c_l;
    flux = ur;
    flux -= ul;
    flux *= c_l * c_r;
    flux += flux_l;
    flux -= flux_r;
    flux /= c_r - c_l;
  }
};
template <int dim, int dos> using FluxHLLPtr = std::shared_ptr<FluxHLL<dim, dos>>;
} // namespace vox

#endif /* flux_HLL_hpp */
