//
//  flux_NS_Positive.hpp
//  Flames
//
//  Created by 杨丰 on 2020/5/12.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef flux_NS_Positive_hpp
#define flux_NS_Positive_hpp

#include "../flux_base.h"

namespace vox {
template <int dim> class FluxNSPositive : public FluxBase<dim, dim + 2> {
public:
  static constexpr int dos = dim + 2;

  void NumericalFlux(const Vector<double, dos> &ul, const Vector<double, dos> &ur,
                     const std::array<double, dim> &outNormal, double para, const ConvEquationPtr<dim, dos> &EqInfo,
                     Vector<double, dos> &flux) override;

  void NumericalFlux(const Vector<double, dos> &ul, const Vector<double, dos> &ur,
                     const Vector<Vector<double, dim>, dos> &gl, const Vector<Vector<double, dim>, dos> &gr,
                     const std::array<double, dim> &outNormal, double para, const ConvEquationPtr<dim, dos> &EqInfo,
                     Vector<double, dos> &flux, bool isOnlyViscous) override;
};

template <int dim> using FluxNSPositivePtr = std::shared_ptr<FluxNSPositive<dim>>;

} // namespace vox

#endif /* flux_NS_Positive_hpp */
