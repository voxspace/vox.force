//
//  flux_RNS_HLLC.hpp
//  Flames
//
//  Created by 杨丰 on 2020/5/12.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef flux_RNS_HLLC_hpp
#define flux_RNS_HLLC_hpp

#include "../flux_base.h"

namespace vox {
template <int dim> class FluxRNSHLLC : public FluxBase<dim, dim + 3> {
public:
  static constexpr int dos = dim + 3;

  void NumericalFlux(const Vector<double, dos> &ul, const Vector<double, dos> &ur,
                     const std::array<double, dim> &outNormal, double para, const ConvEquationPtr<dim, dos> &EqInfo,
                     Vector<double, dos> &flux) override;
};

template <int dim> using FluxRNSHLLCPtr = std::shared_ptr<FluxRNSHLLC<dim>>;

} // namespace vox

#endif /* flux_RNS_HLLC_hpp */
