//
//  grid_data.hpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef grid_data_hpp
#define grid_data_hpp

#include "grid_data_base.h"
#include "mesh.h"

namespace vox {
template <int dim> class GridData : public GridDataBase<dim> {
public:
  class Builder;
  using point_t = typename Mesh<dim>::point_t;

public:
  //! Default constructor.
  GridData(size_t idx, MeshPtr<dim> grid);
  //! Copy constructor.
  GridData(const GridData &other);
  //! Copies from other grid data.
  GridData &operator=(const GridData &other);
  //! Destructor.
  ~GridData() override;

  //! Returns the copy of the grid instance.
  virtual std::shared_ptr<GridData> deepCopy();

  //! return the reference grid to access data;
  MeshPtr<dim> getGrid();

public:
  //! calculate high order reconstruction
  virtual void calculateBasisFunction() {}

public:
  //! return value of specific point
  virtual double value(const point_t &pt, Vector<size_t, dim> idx);
  //! return values of many points
  std::vector<double> value(const std::vector<point_t> &pt, Vector<size_t, dim> idx);

  //! return value of specific point
  virtual std::array<double, dim> gradient(const point_t &pt, Vector<size_t, dim> idx);
  //! return values of many points
  std::vector<std::array<double, dim>> gradient(const std::vector<point_t> &pt, Vector<size_t, dim> idx);

protected:
  const MeshPtr<dim> _grid;
  using GridDataBase<dim>::_gridData;
};
template <int dim> using GridDataPtr = std::shared_ptr<GridData<dim>>;

//----------------------------------------------------------------------------------------------------------------------
// MARK:- Grid Data Builder
template <int dim> class GridData<dim>::Builder {
public:
  virtual ~Builder() = default;

  virtual GridDataPtr<dim> build(size_t idx, MeshPtr<dim> grid);
};
template <int dim> using GridDataBuilderPtr = std::shared_ptr<typename GridData<dim>::Builder>;

//----------------------------------------------------------------------------------------------------------------------
// MARK:- Helper Functions
template <int dim> void add(GridDataPtr<dim> data1, GridDataPtr<dim> data2);

template <int dim> void scale(GridDataPtr<dim> data, double scale);

} // namespace vox
#endif /* grid_data_hpp */
