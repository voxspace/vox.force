//
//  grid_system_data_chars.hpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef grid_system_data_chars_hpp
#define grid_system_data_chars_hpp

#include "equations/equation_NS.h"
#include "grid_system_data.h"

namespace vox {

template <int dim> class GridSysDataNSCharacter : public GridSystemData<dim, EquationNS<dim>::DOS> {
public:
  static constexpr int dos = EquationNS<dim>::DOS;
  using point_t = typename Mesh<dim>::point_t;

public:
  //! Constructs empty grid system.
  explicit GridSysDataNSCharacter(MeshPtr<dim> grid);
  //! Copy constructor.
  GridSysDataNSCharacter(const GridSysDataNSCharacter &other);
  //! Copy operator
  GridSysDataNSCharacter &operator=(const GridSysDataNSCharacter &other);

  //! Destructor.
  virtual ~GridSysDataNSCharacter();

  //! Returns the copy of the grid instance.
  std::shared_ptr<GridSystemData<dim, EquationNS<dim>::DOS>> deepCopy() override;

  class Builder;

  static Builder builder();

public:
  //! return value of specific point
  Vector<double, EquationNS<dim>::DOS> value(const point_t &pt, Vector<size_t, dim> var_idx) override;

  //! return value of specific point
  Vector<Vector<double, dim>, EquationNS<dim>::DOS> gradient(const point_t &pt, Vector<size_t, dim> var_idx) override;

public:
  void calculateBasisFunction() override;

private:
  Vector<double, EquationNS<dim>::DOS> cons2char(const Vector<double, dos> &cons,
                                                 const Matrix<double, dos, dos> &Trans);
  Vector<double, EquationNS<dim>::DOS> char2cons(const Vector<double, dos> &prim,
                                                 const Matrix<double, dos, dos> &Trans);

  void RightEigenVec(const Vector<double, dos> &ul, const Vector<double, dos> &ur, Matrix<double, dos, dos> &eigenVec);

private:
  std::array<GridDataPtr<dim>, dos> _PrimitiveData;
  Array<Matrix<double, dos, dos>, dim> _TransMats;

  void setEquation(EquationNSPtr<dim> eq);

  EquationNSPtr<dim> eqInfo;

  using GridSystemData<dim, dos>::_grid;
  using GridSystemData<dim, dos>::_scalarDataList;

  friend class GridSysData1DNSCharacterTest;
};
template <int dim> using GridSysDataNSCharacterPtr = std::shared_ptr<GridSysDataNSCharacter<dim>>;

template <int dim> class GridSysDataNSCharacter<dim>::Builder {
public:
  GridSysDataNSCharacterPtr<dim> build(GridDataBuilderPtr<dim> builder, MeshPtr<dim> grid, EquationNSPtr<dim> eq_info);

  GridSysDataNSCharacterPtr<dim> build(MeshPtr<dim> grid, EquationNSPtr<dim> eq_info);
};
template <int dim>
using GridSysDataNSCharacterBuilderPtr = std::shared_ptr<typename GridSysDataNSCharacter<dim>::Builder>;

} // namespace vox
#endif /* grid_system_data_chars_hpp */
