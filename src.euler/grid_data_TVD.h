//
//  grid_data_TVD.hpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef grid_data_TVD_hpp
#define grid_data_TVD_hpp

#include "grid_data.h"

namespace vox {

class GridDataTVD : public GridData<1> {
public:
  class Builder;
  //! point from AFEPack
  using point_t = typename Mesh<1>::point_t;

  enum LimiterType { MIN_MODE = 0, MC, ENO };

  //! Constructs grid data with given grid1.
  GridDataTVD(size_t idx, const MeshPtr<1> &grid, LimiterType limiter);
  //! Copy constructor.
  GridDataTVD(const GridDataTVD &other);
  //! Copies from other grid data.
  GridDataTVD &operator=(const GridDataTVD &other);
  //! Destructor.
  ~GridDataTVD() override;
  //! Returns the copy of the grid instance.
  std::shared_ptr<GridData<1>> deepCopy() override;

public:
  void calculateBasisFunction() override;

private:
  void calculateBasisFunction(Vector<size_t, 1> idx);

public:
  //! return value of specific point
  double value(const point_t &pt, Vector<size_t, 1> idx) override;

  //! return value of specific point
  std::array<double, 1> gradient(const point_t &pt, Vector<size_t, 1> idx) override;

private:
  struct ReconCache {
    double gradient;
  };
  Array<ReconCache, 1> recon_cache;

  LimiterType limiter;

  friend class GridDataTVDTest;
};
using GridDataTVDPtr = std::shared_ptr<GridDataTVD>;

//----------------------------------------------------------------------------------------------------------------------
class GridDataTVD::Builder : public GridData<1>::Builder {
public:
  void setLimiterType(GridDataTVD::LimiterType type);

  GridDataPtr<1> build(size_t idx, MeshPtr<1> grid) override;

private:
  GridDataTVD::LimiterType limiter;
};
using GridDataTVDBuilderPtr = std::shared_ptr<GridDataTVD::Builder>;
} // namespace vox
#endif /* grid_data_TVD_hpp */
