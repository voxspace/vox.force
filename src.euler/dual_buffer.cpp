//
//  dual_buffer.cpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "dual_buffer.h"

namespace vox {

template <int dim, int dos> DualBuffer<dim, dos>::DualBuffer() : new_idx(0) {}

template <int dim, int dos> void DualBuffer<dim, dos>::swapBuffer() { new_idx = 1 - new_idx; }

template <int dim, int dos> GridSystemDataPtr<dim, dos> DualBuffer<dim, dos>::newBuffer() { return _buffer[new_idx]; }

template <int dim, int dos> GridSystemDataPtr<dim, dos> DualBuffer<dim, dos>::oldBuffer() {
  return _buffer[1 - new_idx];
}

template <int dim, int dos> void DualBuffer<dim, dos>::setNewBuffer(GridSystemDataPtr<dim, dos> buffer) {
  _buffer[new_idx] = buffer;
}

template <int dim, int dos> void DualBuffer<dim, dos>::setOldBuffer(GridSystemDataPtr<dim, dos> buffer) {
  _buffer[1 - new_idx] = buffer;
}

#define Implement(dim)                                                                                                 \
  template class DualBuffer<dim, 1>;                                                                                   \
  template class DualBuffer<dim, 2>;                                                                                   \
  template class DualBuffer<dim, 3>;                                                                                   \
  template class DualBuffer<dim, 4>;                                                                                   \
  template class DualBuffer<dim, 5>;                                                                                   \
  template class DualBuffer<dim, 6>;

Implement(1);
Implement(2);
Implement(3);
#undef Implement
} // namespace vox
