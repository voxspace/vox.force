//
//  grid_data_limiter.hpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef grid_data_limiter_hpp
#define grid_data_limiter_hpp

#include "grid_data_unlimited.h"

namespace vox {
template <int dim> class GridDataLimiter : public GridDataUnlimited<dim, 1> {
public:
  using LimiterAuxiliaryPtr = ReconAuxiliaryPtr<dim, 1>;

  enum LimitersType { SCALAR = 0, R_SCALAR, BARTH, R_BARTH };

  enum PntType { BARYCENTER, BOUNDARY };

  class Builder;

  //! Constructs grid data with given grid1.
  GridDataLimiter(size_t idx, MeshPtr<dim> grid, LimiterAuxiliaryPtr aux, LimitersType t);
  //! Copy constructor.
  GridDataLimiter(const GridDataLimiter &other);
  //! Copies from other grid data.
  GridDataLimiter &operator=(const GridDataLimiter &other);
  //! Destructor.
  virtual ~GridDataLimiter();
  //! Returns the copy of the grid instance.
  std::shared_ptr<GridData<dim>> deepCopy() override;

public:
  void setLimiterType(LimitersType t);

protected:
  void solve(Vector<size_t, dim> i, const double &UU, const std::vector<double> &bary_info,
             typename PolyInfo<dim, 1>::Vec &result) override;

  void limiter(Vector<size_t, dim> idx, typename PolyInfo<dim, 1>::Vec &grad, PntType type, bool relaxed);

private:
  LimitersType type;

  using GridData<dim>::_grid;
  using GridDataBase<dim>::_gridData;
  using GridDataUnlimited<dim, 1>::reconAux;

  friend class GridData1DLimiterTest;
  friend class GridData2DLimiterTest;
  friend class GridData2DAdaptLimiterTest;
  friend class GridData3DLimiterTest;
};
template <int dim> using GridDataLimiterPtr = std::shared_ptr<GridDataLimiter<dim>>;

//----------------------------------------------------------------------------------------------------------------------
// MARK:- Builder class
template <int dim> class GridDataLimiter<dim>::Builder : public GridData<dim>::Builder {
public:
  void setLimiterAuxiliary(LimiterAuxiliaryPtr aux);

  void setLimiterType(LimitersType t);

  GridDataPtr<dim> build(size_t idx, MeshPtr<dim> grid) override;

private:
  LimiterAuxiliaryPtr limiterAux;

  LimitersType type;
};
template <int dim> using GridDataLimiterBuilderPtr = std::shared_ptr<typename GridDataLimiter<dim>::Builder>;

} // namespace vox

#endif /* grid_data_limiter_hpp */
