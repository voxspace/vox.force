//
//  polyInfo3d.hpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef polyInfo3d_hpp
#define polyInfo3d_hpp

#include "mesh.h"
#include "polyInfo.h"

namespace vox {

template <int order> class PolyInfo<3, order> {
public:
  static constexpr int n_unknown = (order + 3) * (order + 2) * (order + 1) / (3 * 2) - 1;
  static constexpr int dim = 3;
  using point_t = typename Mesh<dim>::point_t;

  using Mat = Matrix<double, n_unknown, n_unknown>;

  using Vec = Matrix<double, n_unknown, 1>;

  explicit PolyInfo(const MeshPtr<dim> &g);

  void resetGrid(MeshPtr<dim> g);

public:
  void loadBasisFunc();

  /// Element average of basisFunc
  /// @param basisIdx the loc of basisFunc
  /// @param quadIdx the loc of quadrature area
  /// @param result result
  void averageBasisFunc(Vector<size_t, dim> basisIdx, Vector<size_t, dim> quadIdx,
                        std::array<double, n_unknown> &result);

  void averageBasisFunc(Vector<size_t, dim> basisIdx, ArrayView1<Vector<size_t, dim>> patch,
                        ArrayView1<std::array<double, n_unknown>> result);

  void updateLSMatrix(Vector<size_t, dim> basisIdx, ArrayView1<Vector<size_t, dim>> patch,
                      ArrayView1<std::array<double, n_unknown>> poly_avgs, Mat &G);

public:
  void basisFuncValue(Vector<size_t, dim> idx, const point_t &coord, std::array<double, n_unknown> &result);

  void basisFuncGradient(const Vector<size_t, dim>& idx, const point_t &coord,
                         std::array<std::array<double, n_unknown>, dim> &result);

  double funcValue(Vector<size_t, dim> idx, const point_t &coord, const double &avg, const Vec &para);

  std::array<double, 3> funcGradient(Vector<size_t, dim> idx, const point_t &coord, const Vec &para);

private:
  Array<std::array<double, n_unknown>, dim> element_cache;

  MeshPtr<dim> grid;
};

} // namespace vox
#endif /* polyInfo3d_hpp */
