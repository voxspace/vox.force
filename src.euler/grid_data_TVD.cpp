//
//  grid_data_TVD.cpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "grid_data_TVD.h"
#include "../src.common/private_helpers.h"

namespace vox {
namespace {
double min_mod(double a, double b) {
  if (a > 0) {
    if (b > 0) {
      return std::min(a, b);
    }
    return 0;
  } else {
    if (b < 0) {
      return std::max(a, b);
    }
    return 0;
  }
}

double MCLimiter(double a, double b, double c) { return min_mod(min_mod(a, b), c); }

double ENOLimiter(double a, double b) {
  if (std::abs(a) < std::abs(b)) {
    return a;
  } else {
    return b;
  }
}

} // namespace

GridDataTVD::GridDataTVD(size_t idx, const MeshPtr<1> &grid, LimiterType limiter)
    : GridData<1>(idx, grid), limiter(limiter) {
  recon_cache.resize(grid->dataSize());
}

GridDataTVD::GridDataTVD(const GridDataTVD &other) : GridData<1>(other) {
  limiter = other.limiter;

  recon_cache = other.recon_cache;
}

GridDataTVD &GridDataTVD::operator=(const GridDataTVD &other) {
  GridData<1>::operator=(other);

  limiter = other.limiter;

  recon_cache = other.recon_cache;

  return *this;
}

GridDataTVD::~GridDataTVD() = default;

std::shared_ptr<GridData<1>> GridDataTVD::deepCopy() { return CLONE_W_CUSTOM_DELETER(GridDataTVD); }

// MARK:- Cell Reconstructed Function API
double GridDataTVD::value(const point_t &pt, Vector<size_t, 1> idx) {
  point_t bc = _grid->BaryCenter(idx);
  return _gridData(idx) + recon_cache(idx).gradient * (pt[0] - bc[0]);
}

std::array<double, 1> GridDataTVD::gradient(const point_t &pt, Vector<size_t, 1> idx) {
  UNUSED_VARIABLE(pt);

  return std::array<double, 1>{recon_cache(idx).gradient};
}

// MARK:- Reconstruction Solvers
void GridDataTVD::calculateBasisFunction() {
  parallelFor(kZeroSize, _grid->dataSize()[0], [&](size_t ele_idx) -> void { calculateBasisFunction({ele_idx}); });
}

void GridDataTVD::calculateBasisFunction(Vector<size_t, 1> i) {
  std::array<int, 3> neigh{};
  neigh[1] = i[0];
  neigh[0] = neigh[1] - 1;
  neigh[2] = neigh[1] + 1;

  bool isBoundary = false;
  auto result = _grid->periodicMap(neigh[0], left);
  if (result == static_cast<size_t>(-1)) {
    isBoundary = true;
  } else {
    neigh[0] = result;
  }
  result = _grid->periodicMap(neigh[2], right);
  if (result == static_cast<size_t>(-1)) {
    isBoundary = true;
  } else {
    neigh[2] = result;
  }

  if (isBoundary) {
    recon_cache(i).gradient = 0.0;
  } else {
    double left_gradient =
        (GridDataBase::value(i) - GridDataBase::value({static_cast<unsigned long>(neigh[0])})) / (_grid->SizeOfEle());
    double right_gradient =
        (GridDataBase::value({static_cast<unsigned long>(neigh[2])}) - GridDataBase::value(i)) / (_grid->SizeOfEle());
    switch (limiter) {
    case LimiterType::MIN_MODE:
      recon_cache(i).gradient = min_mod(left_gradient, right_gradient);
      break;
    case LimiterType::MC:
      recon_cache(i).gradient =
          MCLimiter(2 * left_gradient, 2 * right_gradient, (left_gradient + right_gradient) / 2.0);
      break;
    case LimiterType::ENO:
      recon_cache(i).gradient = ENOLimiter(left_gradient, right_gradient);
      break;

    default:
      JET_THROW_INVALID_ARG_WITH_MESSAGE_IF(true, "Undefined Limiter Type");
    }
  }
}

// MARK:- Builder
void GridDataTVD::Builder::setLimiterType(const GridDataTVD::LimiterType type) { limiter = type; }

GridDataPtr<1> GridDataTVD::Builder::build(size_t idx, MeshPtr<1> grid) {
  return std::make_shared<GridDataTVD>(idx, grid, limiter);
}

} // namespace vox
