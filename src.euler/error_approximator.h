//
//  error_approximator.hpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef error_approximator_hpp
#define error_approximator_hpp

#include "equation_prototype.h"
#include "grid_system_data.h"

namespace vox {

template <int dim, int dos> class ErrorApproximator {
public:
  ErrorApproximator(MeshPtr<dim> grid) : grid(grid), integrator(grid->origin(), grid->gridSpacing()) {}

  void l1Error(GridSystemDataPtr<dim, dos> data,
               std::array<std::function<double(const typename Mesh<dim>::point_t &pt)>, dos> realFunc,
               ConvEquationPtr<dim, dos> eqInfo, Vector<double, dos> &error, double &dx);

  void linftyError(GridSystemDataPtr<dim, dos> data,
                   std::array<std::function<double(const typename Mesh<dim>::point_t &pt)>, dos> realFunc,
                   ConvEquationPtr<dim, dos> eqInfo, Vector<double, dos> &error, double &dx);

private:
  MeshPtr<dim> grid;
  VolumeIntegrator<dim, 2> integrator;
};

} // namespace vox
#endif /* error_approximator_hpp */
