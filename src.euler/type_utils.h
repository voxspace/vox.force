//
// Created by yangfengzzz on 2021/4/22.
//

#ifndef DIGITALFLEX3_TYPE_UTILS_H
#define DIGITALFLEX3_TYPE_UTILS_H

#include "../src.common/matrix.h"

namespace vox {
template <int dim> using Point = Vector<double, dim>;
using Point1D = Vector1D;
using Point1D = Vector1D;
using Point3D = Vector3D;

enum Direction { left, right, up, bottom, front, back };

} // namespace vox

#endif // DIGITALFLEX3_TYPE_UTILS_H
