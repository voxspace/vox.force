//
// Created by yangfengzzz on 2021/4/22.
//

#ifndef DIGITALFLEX3_QUADRATURE_INFOS_H
#define DIGITALFLEX3_QUADRATURE_INFOS_H

#include "type_utils.h"

namespace vox {
template <int order> struct QuadratureInfo {};
// ---------------------------------------------------------------------------------------------------------------------
// MARK:- Order 1
template <> struct QuadratureInfo<1> {
  static constexpr int n_quad = 1;
  QuadratureInfo();

  const std::pair<double, double> &quad_info(int ele) { return quad_infos[ele]; }

  const double &pnt(int ele) { return quad_infos[ele].first; }

  const double &weight(int ele) { return quad_infos[ele].second; }

private:
  std::array<std::pair<double, double>, n_quad> quad_infos;
};

// ---------------------------------------------------------------------------------------------------------------------
// MARK:- Order 2
template <> struct QuadratureInfo<2> {
  static constexpr int n_quad = 2;
  QuadratureInfo();

  const std::pair<double, double> &quad_info(int ele) { return quad_infos[ele]; }

  const double &pnt(int ele) { return quad_infos[ele].first; }

  const double &weight(int ele) { return quad_infos[ele].second; }

private:
  std::array<std::pair<double, double>, n_quad> quad_infos;
};

// ---------------------------------------------------------------------------------------------------------------------
// MARK:- Order 3
template <> struct QuadratureInfo<3> {
  static constexpr int n_quad = 3;
  QuadratureInfo();

  const std::pair<double, double> &quad_info(int ele) { return quad_infos[ele]; }

  const double &pnt(int ele) { return quad_infos[ele].first; }

  const double &weight(int ele) { return quad_infos[ele].second; }

private:
  std::array<std::pair<double, double>, n_quad> quad_infos;
};

// ---------------------------------------------------------------------------------------------------------------------
// MARK:- Order 4
template <> struct QuadratureInfo<4> {
  static constexpr int n_quad = 4;
  QuadratureInfo();

  const std::pair<double, double> &quad_info(int ele) { return quad_infos[ele]; }

  const double &pnt(int ele) { return quad_infos[ele].first; }

  const double &weight(int ele) { return quad_infos[ele].second; }

private:
  std::array<std::pair<double, double>, n_quad> quad_infos;
};

// ---------------------------------------------------------------------------------------------------------------------
// MARK:- Order 5
template <> struct QuadratureInfo<5> {
  static constexpr int n_quad = 5;
  QuadratureInfo();

  const std::pair<double, double> &quad_info(int ele) { return quad_infos[ele]; }

  const double &pnt(int ele) { return quad_infos[ele].first; }

  const double &weight(int ele) { return quad_infos[ele].second; }

private:
  std::array<std::pair<double, double>, n_quad> quad_infos;
};

// ---------------------------------------------------------------------------------------------------------------------
// MARK:- Order 6
template <> struct QuadratureInfo<6> {
  static constexpr int n_quad = 6;
  QuadratureInfo();

  const std::pair<double, double> &quad_info(int ele) { return quad_infos[ele]; }

  const double &pnt(int ele) { return quad_infos[ele].first; }

  const double &weight(int ele) { return quad_infos[ele].second; }

private:
  std::array<std::pair<double, double>, n_quad> quad_infos;
};

// ---------------------------------------------------------------------------------------------------------------------
// MARK:- Order 7
template <> struct QuadratureInfo<7> {
  static constexpr int n_quad = 7;
  QuadratureInfo();

  const std::pair<double, double> &quad_info(int ele) { return quad_infos[ele]; }

  const double &pnt(int ele) { return quad_infos[ele].first; }

  const double &weight(int ele) { return quad_infos[ele].second; }

private:
  std::array<std::pair<double, double>, n_quad> quad_infos;
};

// ---------------------------------------------------------------------------------------------------------------------
// MARK:- Order 8
template <> struct QuadratureInfo<8> {
  static constexpr int n_quad = 8;
  QuadratureInfo();

  const std::pair<double, double> &quad_info(int ele) { return quad_infos[ele]; }

  const double &pnt(int ele) { return quad_infos[ele].first; }

  const double &weight(int ele) { return quad_infos[ele].second; }

private:
  std::array<std::pair<double, double>, n_quad> quad_infos;
};

// ---------------------------------------------------------------------------------------------------------------------
// MARK:- Order 9
template <> struct QuadratureInfo<9> {
  static constexpr int n_quad = 9;
  QuadratureInfo();

  const std::pair<double, double> &quad_info(int ele) { return quad_infos[ele]; }

  const double &pnt(int ele) { return quad_infos[ele].first; }

  const double &weight(int ele) { return quad_infos[ele].second; }

private:
  std::array<std::pair<double, double>, n_quad> quad_infos;
};

// ---------------------------------------------------------------------------------------------------------------------
// MARK:- Order 10
template <> struct QuadratureInfo<10> {
  static constexpr int n_quad = 10;
  QuadratureInfo();

  const std::pair<double, double> &quad_info(int ele) { return quad_infos[ele]; }

  const double &pnt(int ele) { return quad_infos[ele].first; }

  const double &weight(int ele) { return quad_infos[ele].second; }

private:
  std::array<std::pair<double, double>, n_quad> quad_infos;
};

// ---------------------------------------------------------------------------------------------------------------------
// MARK:- Order 11
template <> struct QuadratureInfo<11> {
  static constexpr int n_quad = 11;
  QuadratureInfo();

  const std::pair<double, double> &quad_info(int ele) { return quad_infos[ele]; }

  const double &pnt(int ele) { return quad_infos[ele].first; }

  const double &weight(int ele) { return quad_infos[ele].second; }

private:
  std::array<std::pair<double, double>, n_quad> quad_infos;
};

// ---------------------------------------------------------------------------------------------------------------------
// MARK:- Order 12
template <> struct QuadratureInfo<12> {
  static constexpr int n_quad = 12;
  QuadratureInfo();

  const std::pair<double, double> &quad_info(int ele) { return quad_infos[ele]; }

  const double &pnt(int ele) { return quad_infos[ele].first; }

  const double &weight(int ele) { return quad_infos[ele].second; }

private:
  std::array<std::pair<double, double>, n_quad> quad_infos;
};

// ---------------------------------------------------------------------------------------------------------------------
// MARK:- Order 13
template <> struct QuadratureInfo<13> {
  static constexpr int n_quad = 13;
  QuadratureInfo();

  const std::pair<double, double> &quad_info(int ele) { return quad_infos[ele]; }

  const double &pnt(int ele) { return quad_infos[ele].first; }

  const double &weight(int ele) { return quad_infos[ele].second; }

private:
  std::array<std::pair<double, double>, n_quad> quad_infos;
};

// ---------------------------------------------------------------------------------------------------------------------
// MARK:- Order 14
template <> struct QuadratureInfo<14> {
  static constexpr int n_quad = 14;
  QuadratureInfo();

  const std::pair<double, double> &quad_info(int ele) { return quad_infos[ele]; }

  const double &pnt(int ele) { return quad_infos[ele].first; }

  const double &weight(int ele) { return quad_infos[ele].second; }

private:
  std::array<std::pair<double, double>, n_quad> quad_infos;
};

// ---------------------------------------------------------------------------------------------------------------------
// MARK:- Order 15
template <> struct QuadratureInfo<15> {
  static constexpr int n_quad = 15;
  QuadratureInfo();

  const std::pair<double, double> &quad_info(int ele) { return quad_infos[ele]; }

  const double &pnt(int ele) { return quad_infos[ele].first; }

  const double &weight(int ele) { return quad_infos[ele].second; }

private:
  std::array<std::pair<double, double>, n_quad> quad_infos;
};

// ---------------------------------------------------------------------------------------------------------------------
// MARK:- Order 16
template <> struct QuadratureInfo<16> {
  static constexpr int n_quad = 16;
  QuadratureInfo();

  const std::pair<double, double> &quad_info(int ele) { return quad_infos[ele]; }

  const double &pnt(int ele) { return quad_infos[ele].first; }

  const double &weight(int ele) { return quad_infos[ele].second; }

private:
  std::array<std::pair<double, double>, n_quad> quad_infos;
};

// ---------------------------------------------------------------------------------------------------------------------
// MARK:- Order 17
template <> struct QuadratureInfo<17> {
  static constexpr int n_quad = 17;
  QuadratureInfo();

  const std::pair<double, double> &quad_info(int ele) { return quad_infos[ele]; }

  const double &pnt(int ele) { return quad_infos[ele].first; }

  const double &weight(int ele) { return quad_infos[ele].second; }

private:
  std::array<std::pair<double, double>, n_quad> quad_infos;
};

// ---------------------------------------------------------------------------------------------------------------------
// MARK:- Order 18
template <> struct QuadratureInfo<18> {
  static constexpr int n_quad = 18;
  QuadratureInfo();

  const std::pair<double, double> &quad_info(int ele) { return quad_infos[ele]; }

  const double &pnt(int ele) { return quad_infos[ele].first; }

  const double &weight(int ele) { return quad_infos[ele].second; }

private:
  std::array<std::pair<double, double>, n_quad> quad_infos;
};

// ---------------------------------------------------------------------------------------------------------------------
// MARK:- Order 19
template <> struct QuadratureInfo<19> {
  static constexpr int n_quad = 19;
  QuadratureInfo();

  const std::pair<double, double> &quad_info(int ele) { return quad_infos[ele]; }

  const double &pnt(int ele) { return quad_infos[ele].first; }

  const double &weight(int ele) { return quad_infos[ele].second; }

private:
  std::array<std::pair<double, double>, n_quad> quad_infos;
};

// ---------------------------------------------------------------------------------------------------------------------
// MARK:- Order 20
template <> struct QuadratureInfo<20> {
  static constexpr int n_quad = 20;
  QuadratureInfo();

  const std::pair<double, double> &quad_info(int ele) { return quad_infos[ele]; }

  const double &pnt(int ele) { return quad_infos[ele].first; }

  const double &weight(int ele) { return quad_infos[ele].second; }

private:
  std::array<std::pair<double, double>, n_quad> quad_infos;
};

} // namespace vox

class quadrature_infos {};

#endif // DIGITALFLEX3_QUADRATURE_INFOS_H
