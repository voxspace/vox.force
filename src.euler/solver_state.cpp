//
//  solver_state.cpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "solver_state.h"

namespace vox {

template <int dim, int dos> void SolverState<dim, dos>::_notifyPhysicsSystem(PhysicsSolver<dim, dos> *system) {
  mPhysicsSystem = system;

  buffer = mPhysicsSystem->getDualBuffer();
}

#define Implement(dim)                                                                                                 \
  template class SolverState<dim, 1>;                                                                                  \
  template class SolverState<dim, 2>;                                                                                  \
  template class SolverState<dim, 3>;                                                                                  \
  template class SolverState<dim, 4>;                                                                                  \
  template class SolverState<dim, 5>;                                                                                  \
  template class SolverState<dim, 6>;

Implement(1);
Implement(2);
Implement(3);
#undef Implement
} // namespace vox
