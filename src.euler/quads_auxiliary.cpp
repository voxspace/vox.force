//
//  quads_auxiliary.cpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "quads_auxiliary.h"
#include "../src.common/parallel.h"
#include "../src.common/private_helpers.h"

namespace vox {
template <int dim, int order>
void QuadsAuxiliary<dim, order>::getEleQuadPoint(MeshPtr<dim> g, Vector<size_t, dim> ele_idx,
                                                 std::vector<typename Mesh<dim>::point_t> &quad_pts) {
  getEleInnerQuadPoint(g, ele_idx, quad_pts);
  std::vector<Point<dim>> tmp;
  getBryQuadPoint(g, ele_idx, tmp);
  quad_pts.insert(quad_pts.end(), tmp.begin(), tmp.end());
}

// MARK:- getEleInnerQuadPoint
template <>
void QuadsAuxiliary<1, 1>::getEleInnerQuadPoint(const MeshPtr<1> &g, const Vector<size_t, 1> &ele_idx,
                                                std::vector<Mesh<1>::point_t> &quad_pts) {
  UNUSED_VARIABLE(g);
  UNUSED_VARIABLE(ele_idx);
  quad_pts.clear();
}

template <>
void QuadsAuxiliary<1, 2>::getEleInnerQuadPoint(const MeshPtr<1> &g, const Vector<size_t, 1> &ele_idx,
                                                std::vector<Mesh<1>::point_t> &quad_pts) {
  quad_pts.resize(1);
  quad_pts[0] = g->BaryCenter(ele_idx);
}

template <>
void QuadsAuxiliary<2, 1>::getEleInnerQuadPoint(const MeshPtr<2> &g, const Vector<size_t, 2> &ele_idx,
                                                std::vector<Mesh<2>::point_t> &quad_pts) {
  UNUSED_VARIABLE(g);
  UNUSED_VARIABLE(ele_idx);
  quad_pts.clear();
}

template <>
void QuadsAuxiliary<2, 2>::getEleInnerQuadPoint(const MeshPtr<2> &g, const Vector<size_t, 2> &ele_idx,
                                                std::vector<Mesh<2>::point_t> &quad_pts) {
  quad_pts.resize(1);
  quad_pts[0] = {0.0, 0.0};
  VolumeIntegrator<2, 2> integrator(g->origin(), g->gridSpacing());
  quad_pts = integrator.transform(quad_pts, ele_idx);
}

template <>
void QuadsAuxiliary<3, 1>::getEleInnerQuadPoint(const MeshPtr<3> &g, const Vector3UZ &ele_idx,
                                                std::vector<Mesh<3>::point_t> &quad_pts) {
  UNUSED_VARIABLE(g);
  UNUSED_VARIABLE(ele_idx);
  quad_pts.clear();
}

template <>
void QuadsAuxiliary<3, 2>::getEleInnerQuadPoint(const MeshPtr<3> &g, const Vector3UZ &ele_idx,
                                                std::vector<Mesh<3>::point_t> &quad_pts) {
  quad_pts.resize(1);
  quad_pts[0] = {0.0, 0.0, 0.0};
  VolumeIntegrator<3, 2> integrator(g->origin(), g->gridSpacing());
  quad_pts = integrator.transform(quad_pts, ele_idx);
}

// MARK:- getBryQuadPoint
template <int dim, int order>
void QuadsAuxiliary<dim, order>::getBryQuadPoint(MeshPtr<dim> g, Vector<size_t, dim> ele_idx,
                                                 std::vector<typename Mesh<dim>::point_t> &quad_pts) {
  SurfaceIntegrator<dim, order> integrator(g->origin(), g->gridSpacing());
  for (int i = 0; i < dim; ++i) {
    if (i == 0) {
      integrator.template surfaceIntegral<double>(
          [&](const Point<dim> &pt) -> double {
            quad_pts.push_back(pt);

            return 0.0;
          },
          ele_idx, left);

      integrator.template surfaceIntegral<double>(
          [&](const Point<dim> &pt) -> double {
            quad_pts.push_back(pt);

            return 0.0;
          },
          ele_idx, right);
    }

    if (i == 1) {
      integrator.template surfaceIntegral<double>(
          [&](const Point<dim> &pt) -> double {
            quad_pts.push_back(pt);

            return 0.0;
          },
          ele_idx, bottom);

      integrator.template surfaceIntegral<double>(
          [&](const Point<dim> &pt) -> double {
            quad_pts.push_back(pt);

            return 0.0;
          },
          ele_idx, up);
    }

    if (i == 2) {
      integrator.template surfaceIntegral<double>(
          [&](const Point<dim> &pt) -> double {
            quad_pts.push_back(pt);

            return 0.0;
          },
          ele_idx, front);

      integrator.template surfaceIntegral<double>(
          [&](const Point<dim> &pt) -> double {
            quad_pts.push_back(pt);

            return 0.0;
          },
          ele_idx, back);
    }
  }
}

template class QuadsAuxiliary<1, 1>;
template class QuadsAuxiliary<1, 2>;

template class QuadsAuxiliary<2, 1>;
template class QuadsAuxiliary<2, 2>;

template class QuadsAuxiliary<3, 1>;
template class QuadsAuxiliary<3, 2>;
} // namespace vox
