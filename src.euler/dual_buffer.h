//
//  dual_buffer.hpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef dual_buffer_hpp
#define dual_buffer_hpp

#include "grid_system_data.h"

namespace vox {

template <int dim, int dos> class DualBuffer {
public:
  DualBuffer();

  GridSystemDataPtr<dim, dos> newBuffer();

  GridSystemDataPtr<dim, dos> oldBuffer();

  void setNewBuffer(GridSystemDataPtr<dim, dos> buffer);

  void setOldBuffer(GridSystemDataPtr<dim, dos> buffer);

  void swapBuffer();

private:
  // dual buffer
  GridSystemDataPtr<dim, dos> _buffer[2];

  size_t new_idx;
};

} // namespace vox
#endif /* dual_buffer_hpp */
