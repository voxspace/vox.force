//
//  polyInfo3d.cpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "polyInfo3d.h"

#include "../src.common/parallel.h"
#include <utility>

namespace vox {

template <int order> PolyInfo<3, order>::PolyInfo(const MeshPtr<dim> &g) : grid(g) {
  if (grid != nullptr) {
    element_cache.resize(grid->dataSize());
    loadBasisFunc();
  }
}

template <int order> void PolyInfo<3, order>::resetGrid(MeshPtr<dim> g) {
  grid = std::move(g);
  element_cache.resize(grid->dataSize());
  loadBasisFunc();
}

template <int order> void PolyInfo<3, order>::loadBasisFunc() {
  VolumeIntegrator<dim, order> integrator(grid->origin(), grid->gridSpacing());
  parallelFor(kZeroSize, grid->dataSize().x, kZeroSize, grid->dataSize().y, kZeroSize, grid->dataSize().z,
              [&](size_t idx, size_t idy, size_t idz) -> void {
                // Get momentum
                int index = 0;
                double J0 = 0;
                for (int m = 1; m <= order; ++m) {
                  for (int j = 0; j <= m; ++j) {
                    for (int t = 0; t <= m - j; ++t) {
                      int k = m - j - t;
                      J0 = integrator.template volumeIntegral<double>(
                          [&](const point_t &pt) -> double {
                            point_t bc = grid->BaryCenter({idx, idy, idz});
                            return std::pow(pt[0] - bc[0], j) * std::pow(pt[1] - bc[1], t) * std::pow(pt[2] - bc[2], k);
                          },
                          {idx, idy, idz});
                      J0 /= grid->AreaOfEle();
                      element_cache({idx, idy, idz})[index] = J0;
                      index++;
                    }
                  }
                }
              });
}

// MARK:- Average Basis Funcs
template <int order>
void PolyInfo<3, order>::averageBasisFunc(const Vector<size_t, dim> basisIdx, const Vector<size_t, dim> quadIdx,
                                          std::array<double, n_unknown> &result) {
  VolumeIntegrator<dim, order> integrator(grid->origin(), grid->gridSpacing());

  int index = 0;
  double J_i = 0;
  for (int m = 1; m <= order; ++m) {
    for (int i = 0; i <= m; ++i) {
      for (int j = 0; j <= m - i; ++j) {
        int k = m - i - j;
        J_i = integrator.template volumeIntegral<double>(
            [&](const point_t &pt) -> double {
              point_t bc = grid->BaryCenter(basisIdx);
              point_t p = grid->periodicLocalize(basisIdx, pt);
              return std::pow(p[0] - bc[0], i) * std::pow(p[1] - bc[1], j) * std::pow(p[2] - bc[2], k);
            },
            quadIdx);
        J_i /= grid->AreaOfEle();
        J_i -= element_cache(basisIdx)[index];
        J_i *= std::pow(grid->SizeOfEle(), 1 - m);
        result[index] = J_i;
        index++;
      }
    }
  }
}

template <int order>
void PolyInfo<3, order>::averageBasisFunc(const Vector<size_t, dim> basisIdx, ArrayView1<Vector<size_t, dim>> patch,
                                          ArrayView1<std::array<double, n_unknown>> result) {
  for (size_t j = 0; j < patch.length(); ++j) {
    std::array<double, n_unknown> s{};
    averageBasisFunc(basisIdx, patch[j], s);
    result[j] = s;
  }
}

template <int order>
void PolyInfo<3, order>::updateLSMatrix(Vector<size_t, dim> basisIdx, ArrayView1<Vector<size_t, dim>> patch,
                                        ArrayView1<std::array<double, n_unknown>> poly_avgs, Mat &G) {
  averageBasisFunc(basisIdx, patch, poly_avgs);

  G.fill(0.0);
  for (size_t j = 0; j < patch.length(); ++j) {
    for (int t1 = 0; t1 < n_unknown; ++t1) {
      for (int t2 = 0; t2 < n_unknown; ++t2) {
        G(t1, t2) += poly_avgs[j][t1] * poly_avgs[j][t2];
      }
    }
  }
}

// MARK:- Basis Funcs
template <int order>
void PolyInfo<3, order>::basisFuncValue(const Vector<size_t, dim> idx, const point_t &coord,
                                        std::array<double, n_unknown> &result) {
  point_t bc = grid->BaryCenter(idx);
  point_t cr = coord - bc;
  int index = 0;
  double J0 = 0;
  for (int m = 1; m <= order; ++m) {
    for (int i = 0; i <= m; ++i) {
      for (int k = 0; k <= m - i; ++k) {
        int j = m - i - k;
        J0 = std::pow(cr[0], i) * std::pow(cr[1], k) * std::pow(cr[2], j);
        J0 -= element_cache(idx)[index];
        J0 *= std::pow(grid->SizeOfEle(), 1 - m);
        result[index] = J0;
        index++;
      }
    }
  }
}

template <int order>
void PolyInfo<3, order>::basisFuncGradient(const Vector<size_t, dim>& idx, const point_t &coord,
                                           std::array<std::array<double, n_unknown>, dim> &result) {
  point_t cr = coord - grid->BaryCenter(idx);

  // coordinate x
  int index = 0;
  for (int m = 1; m <= order; ++m) {
    std::vector<double> As((m + 2) * (m + 1) / 2);
    int ind = 0;
    for (int i = 0; i <= m; ++i) {
      for (int k = 0; k <= m - i; ++k) {
        int j = m - i - k;
        if (i == 0) {
          As[ind] = 0.0;
        } else {
          As[ind] = i * std::pow(cr[0], i - 1) * std::pow(cr[1], k) * std::pow(cr[2], j);
        }
        ind++;
      }
    }

    for (int i = 0; i < (m + 2) * (m + 1) / 2; ++i) {
      result[0][index] = As[i];
      result[0][index] *= std::pow(grid->SizeOfEle(), 1 - m);
      index++;
    }
  }

  // coordinate y
  index = 0;
  for (int m = 1; m <= order; ++m) {
    std::vector<double> As((m + 2) * (m + 1) / 2);
    int ind = 0;
    for (int i = 0; i <= m; ++i) {
      for (int k = 0; k <= m - i; ++k) {
        int j = m - i - k;
        if (k == 0) {
          As[ind] = 0.0;
        } else {
          As[ind] = std::pow(cr[0], i) * k * std::pow(cr[1], k - 1) * std::pow(cr[2], j);
        }
        ind++;
      }
    }

    for (int i = 0; i < (m + 2) * (m + 1) / 2; ++i) {
      result[1][index] = As[i];
      result[1][index] *= std::pow(grid->SizeOfEle(), 1 - m);
      index++;
    }
  }

  // coordinate z
  index = 0;
  for (int m = 1; m <= order; ++m) {
    std::vector<double> As((m + 2) * (m + 1) / 2);
    int ind = 0;
    for (int i = 0; i <= m; ++i) {
      for (int k = 0; k <= m - i; ++k) {
        int j = m - i - k;
        if (j == 0) {
          As[ind] = 0.0;
        } else {
          As[ind] = std::pow(cr[0], i) * std::pow(cr[1], k) * j * std::pow(cr[2], j - 1);
        }
        ind++;
      }
    }

    for (int i = 0; i < (m + 2) * (m + 1) / 2; ++i) {
      result[2][index] = As[i];
      result[2][index] *= std::pow(grid->SizeOfEle(), 1 - m);
      index++;
    }
  }
}

template <int order>
double PolyInfo<3, order>::funcValue(const Vector<size_t, dim> idx, const point_t &coord, const double &avg,
                                     const Vec &para) {
  std::array<double, n_unknown> aa{};
  basisFuncValue(idx, coord, aa);

  double temp;
  double result = avg;
  for (int t = 0; t < n_unknown; ++t) {
    temp = para[t] * aa[t];
    result += temp;
  }

  return result;
}

template <int order>
std::array<double, 3> PolyInfo<3, order>::funcGradient(const Vector<size_t, dim> idx, const point_t &coord,
                                                       const Vec &para) {
  std::array<std::array<double, n_unknown>, dim> aa{};
  basisFuncGradient(idx, coord, aa);

  double temp;
  std::array<double, dim> result{};
  for (uint i = 0; i < dim; ++i) {
    for (int t = 0; t < n_unknown; ++t) {
      temp = para[t] * aa[i][t];
      result[i] += temp;
    }
  }

  return result;
}

template class PolyInfo<3, 1>;
template class PolyInfo<3, 2>;
template class PolyInfo<3, 3>;
} // namespace vox
