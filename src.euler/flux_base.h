//
//  flux_base.hpp
//  Flames
//
//  Created by 杨丰 on 2020/4/29.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef flux_base_hpp
#define flux_base_hpp

#include <memory>
#include <valarray>
#include <vector>

#include "equation_prototype.h"

namespace vox {
template <int dim, int dos> class FluxBase {
public:
  virtual void NumericalFlux(const Vector<double, dos> &ul, const Vector<double, dos> &ur,
                             const std::array<double, dim> &outNormal, double para,
                             const ConvEquationPtr<dim, dos> &EqInfo, Vector<double, dos> &flux) = 0;

  virtual void NumericalFlux(const Vector<double, dos> &ul, const Vector<double, dos> &ur,
                             const Vector<Vector<double, dim>, dos> &gl, const Vector<Vector<double, dim>, dos> &gr,
                             const std::array<double, dim> &outNormal, double para,
                             const ConvEquationPtr<dim, dos> &EqInfo, Vector<double, dos> &flux, bool isOnlyViscous) {
    // use simple average to get gradient flux
    Vector<Vector<double, dim>, dos> g_average;
    for (size_t i = 0; i < dos; ++i) {
      for (int j = 0; j < dim; ++j) {
        g_average[i][j] = 0.5 * (gl[i][j] + gr[i][j]);
      }
    }
    EqInfo->TFlux((ul + ur) / 2.0, g_average, outNormal, flux, true);

    if (!isOnlyViscous) {
      Vector<double, dos> inviscidFlux;
      NumericalFlux(ul, ur, outNormal, para, EqInfo, inviscidFlux);
      flux *= -1.0;
      flux += inviscidFlux;
    }
  }
};
template <int dim, int dos> using FluxPtr = std::shared_ptr<FluxBase<dim, dos>>;

} // namespace vox

#endif /* flux_base_hpp */
