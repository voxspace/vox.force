//
//  grid_data_base.cpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "grid_data_base.h"
#include <fstream>
#include <iomanip>

namespace vox {
template <int dim> GridDataBase<dim>::GridDataBase(size_t idx) : gridData_idx(idx) {}

template <int dim> GridDataBase<dim>::GridDataBase(const GridDataBase &other) : gridData_idx(other.gridData_idx) {
  _gridData = other._gridData;
}

template <int dim> GridDataBase<dim> &GridDataBase<dim>::operator=(const GridDataBase &other) {
  _gridData = other._gridData;
  return *this;
}

template <int dim> GridDataBase<dim>::~GridDataBase() = default;

template <int dim> void GridDataBase<dim>::swapGridData(GridDataBase *other) { _gridData.swap(other->_gridData); }

template <int dim> void GridDataBase<dim>::setGridData(const GridDataBase &other) { _gridData = other._gridData; }

// MARK:- GETTER Method
template <int dim> void GridDataBase<dim>::resize(Vector<size_t, dim> numberOfCell, double initialValue) {
  _gridData.resize(numberOfCell, initialValue);
}

template <int dim> Vector<size_t, dim> GridDataBase<dim>::numberOfData() const { return _gridData.size(); }

template <int dim> ArrayView<const double, dim> GridDataBase<dim>::scalarDataAt() const { return _gridData.view(); }

template <int dim> ArrayView<double, dim> GridDataBase<dim>::scalarDataAt() { return _gridData.view(); }

template <int dim> double &GridDataBase<dim>::operator[](size_t i) { return _gridData[i]; }

template <int dim> const double &GridDataBase<dim>::operator[](size_t i) const { return _gridData[i]; }

template <int dim>
const double& GridDataBase<dim>::operator()(const Vector<size_t, dim> &idx) const {
  return _gridData(idx);
}

template <int dim>
double& GridDataBase<dim>::operator()(const Vector<size_t, dim> &idx) {
  return _gridData(idx);
}

template <int dim> double GridDataBase<dim>::value(Vector<size_t, dim> i) { return _gridData(i); }

template <int dim> void GridDataBase<dim>::getData(std::vector<double> *data) const {
  data->resize(_gridData.length());
  std::copy(_gridData.begin(), _gridData.end(), data->begin());
}

template <int dim> void GridDataBase<dim>::save_cache(const std::string &header) const {
  std::string filename = header + ".dat";
  std::ofstream output(filename.c_str());
  for (size_t i = 0; i < _gridData.length(); ++i) {
    output << std::setprecision(20) << _gridData[i] << std::endl;
  }
}

template <int dim> void GridDataBase<dim>::load_cache(const std::string &header) {
  std::string filename = header + ".dat";
  std::ifstream input(filename.c_str());
  for (size_t i = 0; i < _gridData.length(); ++i) {
    double data;
    input >> data;
    _gridData[i] = data;
  }
}

// MARK:- SETTER Method
template <int dim> void GridDataBase<dim>::forEachDataPointIndex(const std::function<void(size_t)> &func) const {
  forEachIndex(_gridData.length(), func);
}

template <int dim>
void GridDataBase<dim>::parallelForEachDataPointIndex(const std::function<void(size_t)> &func) const {
  parallelForEachIndex(_gridData.length(), func);
}

template <int dim> void GridDataBase<dim>::setData(const std::vector<double> &data) {
  JET_ASSERT(_gridData.size() == data.size());

  std::copy(data.begin(), data.end(), _gridData.begin());
}

template <int dim> void GridDataBase<dim>::setData(double value) { _gridData.fill(value); }
//-----------------------------------------------------------------------
template class GridDataBase<1>;
template class GridDataBase<2>;
template class GridDataBase<3>;
} // namespace vox
