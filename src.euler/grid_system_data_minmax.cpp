//
//  grid_system_data_minmax.cpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "grid_system_data_minmax.h"
#include "quads_auxiliary.h"

namespace vox {

template <int dim, int order>
GridSysDataMaxPrinciple<dim, order>::GridSysDataMaxPrinciple(MeshPtr<dim> grid) : GridSystemData<dim, 1>(grid) {
  updateQuadInfo();
}

template <int dim, int order>
GridSysDataMaxPrinciple<dim, order>::GridSysDataMaxPrinciple(const GridSysDataMaxPrinciple &other)
    : GridSystemData<dim, 1>(other) {
  quad_pts = other.quad_pts;
  thetas = other.thetas;

  maximum = other.maximum;
  minimum = other.minimum;
}

template <int dim, int order>
GridSysDataMaxPrinciple<dim, order> &
GridSysDataMaxPrinciple<dim, order>::operator=(const GridSysDataMaxPrinciple &other) {
  GridSystemData<dim, 1>::operator=(other);
  quad_pts = other.quad_pts;
  thetas = other.thetas;

  maximum = other.maximum;
  minimum = other.minimum;

  return *this;
}

template <int dim, int order> GridSysDataMaxPrinciple<dim, order>::~GridSysDataMaxPrinciple() = default;

template <int dim, int order> std::shared_ptr<GridSystemData<dim, 1>> GridSysDataMaxPrinciple<dim, order>::deepCopy() {
  return std::shared_ptr<GridSysDataMaxPrinciple<dim, order>>(
      new GridSysDataMaxPrinciple<dim, order>(*this), [](GridSysDataMaxPrinciple<dim, order> *obj) { delete obj; });
}

// MARK:- Getter
template <int dim, int order>
Vector<double, 1> GridSysDataMaxPrinciple<dim, order>::value(const point_t &pt, Vector<size_t, dim> var_idx) {
  // make maximum preserving
  Vector<double, 1> tmp = GridSystemData<dim, 1>::value(pt, var_idx);
  Vector<double, 1> tmp_avg = GridSystemData<dim, 1>::value(var_idx);
  tmp = thetas(var_idx) * (tmp - tmp_avg) + tmp_avg;

  return tmp;
}

template <int dim, int order>
Vector<Vector<double, dim>, 1> GridSysDataMaxPrinciple<dim, order>::gradient(const point_t &pt,
                                                                             Vector<size_t, dim> var_idx) {
  Vector<Vector<double, dim>, 1> tmp = GridSystemData<dim, 1>::gradient(pt, var_idx);
  tmp[0] *= thetas(var_idx);
  return tmp;
}

// MARK:- Max preserving modifier
template <int dim, int order> void GridSysDataMaxPrinciple<dim, order>::calculateBasisFunction() {
  GridSystemData<dim, 1>::calculateBasisFunction();
  MaxPreserving();
}

template <int dim, int order> void GridSysDataMaxPrinciple<dim, order>::MaxPreserving() {
  if constexpr (dim == 1) {
    parallelFor(kZeroSize, _grid->dataSize()[0], [&](size_t ele_idx) -> void { MaxPreserving({ele_idx}); });
  } else if constexpr (dim == 2) {
    parallelFor(kZeroSize, _grid->dataSize()[0], kZeroSize, _grid->dataSize()[1],
                [&](size_t ele_idx, size_t ele_idy) -> void {
                  MaxPreserving({ele_idx, ele_idy});
                });
  } else if constexpr (dim == 3) {
    parallelFor(kZeroSize, _grid->dataSize()[0], kZeroSize, _grid->dataSize()[1], kZeroSize, _grid->dataSize()[2],
                [&](size_t ele_idx, size_t ele_idy, size_t ele_idz) -> void {
                  MaxPreserving({ele_idx, ele_idy, ele_idz});
                });
  }
}

template <int dim, int order> void GridSysDataMaxPrinciple<dim, order>::MaxPreserving(Vector<size_t, dim> var_idx) {
  Vector<double, 1> var_min = GridSystemData<dim, 1>::value(quad_pts(var_idx)[0], var_idx);
  Vector<double, 1> var_max = GridSystemData<dim, 1>::value(quad_pts(var_idx)[0], var_idx);

  for (size_t i = 0; i < quad_pts(var_idx).size(); ++i) {
    Vector<double, 1> tmp = GridSystemData<dim, 1>::value(quad_pts(var_idx)[i], var_idx);
    for (size_t j = 0; j < 1; ++j) {
      var_min[j] = std::min(var_min[j], tmp[j]);
      var_max[j] = std::max(var_max[j], tmp[j]);
    }
  }

  Vector<double, 1> vals = GridSystemData<dim, 1>::value(var_idx);

  double tmp_min = std::abs((minimum - vals[0]) / (var_min[0] - vals[0]));
  double tmp_max = std::abs((maximum - vals[0]) / (var_max[0] - vals[0]));
  thetas(var_idx) = std::min(1.0, std::min(tmp_min, tmp_max));
}

template <int dim, int order> void GridSysDataMaxPrinciple<dim, order>::updateQuadInfo() {
  thetas.resize(_grid->dataSize());
  quad_pts.resize(_grid->dataSize());
  if constexpr (dim == 1) {
    for (size_t i = 0; i < quad_pts.size()[0]; ++i) {
      QuadsAuxiliary<dim, order>::getEleQuadPoint(_grid, {i}, quad_pts({i}));
    }
  } else if constexpr (dim == 2) {
    for (size_t i = 0; i < quad_pts.size()[0]; ++i) {
      for (size_t j = 0; j < quad_pts.size()[1]; ++j) {
        QuadsAuxiliary<dim, order>::getEleQuadPoint(_grid, {i, j}, quad_pts({i, j}));
      }
    }
  } else if constexpr (dim == 3) {
    for (size_t i = 0; i < quad_pts.size()[0]; ++i) {
      for (size_t j = 0; j < quad_pts.size()[1]; ++j) {
        for (size_t k = 0; k < quad_pts.size()[2]; ++k) {
          QuadsAuxiliary<dim, order>::getEleQuadPoint(_grid, {i, j, k}, quad_pts({i, j, k}));
        }
      }
    }
  }
}

template <int dim, int order> void GridSysDataMaxPrinciple<dim, order>::setBounds(double min, double max) {
  minimum = min;
  maximum = max;
}

// MARK:- Builder
template <int dim, int order>
GridSysDataMaxPrinciplePtr<dim, order> GridSysDataMaxPrinciple<dim, order>::Builder::build(MeshPtr<dim> grid,
                                                                                           double min, double max) {

  GridSysDataMaxPrinciplePtr<dim, order> dataSys = std::make_shared<GridSysDataMaxPrinciple<dim, order>>(grid);

  dataSys->setScalarBuilder(std::make_shared<typename GridData<dim>::Builder>());
  for (size_t i = 0; i < 1; i++) {
    dataSys->loadScalarData(std::make_shared<typename GridData<dim>::Builder>());
  }

  dataSys->setBounds(min, max);

  return dataSys;
}

template <int dim, int order>
GridSysDataMaxPrinciplePtr<dim, order>
GridSysDataMaxPrinciple<dim, order>::Builder::build(GridDataBuilderPtr<dim> builder, MeshPtr<dim> grid, double min,
                                                    double max) {
  GridSysDataMaxPrinciplePtr<dim, order> dataSys = std::make_shared<GridSysDataMaxPrinciple<dim, order>>(grid);

  dataSys->setScalarBuilder(builder);
  for (size_t i = 0; i < 1; i++) {
    dataSys->loadScalarData(builder);
  }

  dataSys->setBounds(min, max);

  return dataSys;
}

template <int dim, int order>
typename GridSysDataMaxPrinciple<dim, order>::Builder GridSysDataMaxPrinciple<dim, order>::builder() {
  return Builder();
}

template class GridSysDataMaxPrinciple<1, 1>;
template class GridSysDataMaxPrinciple<1, 2>;

template class GridSysDataMaxPrinciple<2, 1>;
template class GridSysDataMaxPrinciple<2, 2>;

template class GridSysDataMaxPrinciple<3, 1>;
template class GridSysDataMaxPrinciple<3, 2>;
} // namespace vox
