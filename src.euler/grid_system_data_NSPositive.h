//
//  grid_system_data_NSPositive.hpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef grid_system_data_NSPositive_hpp
#define grid_system_data_NSPositive_hpp

#include "equations/equation_NS.h"
#include "grid_system_data.h"

namespace vox {

template <int dim, int order> class GridSysDataNSPositive : public GridSystemData<dim, EquationNS<dim>::DOS> {
public:
  static constexpr int dos = EquationNS<dim>::DOS;
  using point_t = typename Mesh<dim>::point_t;

public:
  //! Constructs empty grid system.
  explicit GridSysDataNSPositive(MeshPtr<dim> grid);
  //! Copy constructor.
  GridSysDataNSPositive(const GridSysDataNSPositive &other);
  //! Copy operator
  GridSysDataNSPositive &operator=(const GridSysDataNSPositive &other);

  //! Destructor.
  virtual ~GridSysDataNSPositive();

  //! Returns the copy of the grid instance.
  std::shared_ptr<GridSystemData<dim, EquationNS<dim>::DOS>> deepCopy() override;

  class Builder;

  static Builder builder();

public:
  //! return value of specific point
  Vector<double, EquationNS<dim>::DOS> value(const point_t &pt, Vector<size_t, dim> var_idx) override;

  //! return value of specific point
  Vector<Vector<double, dim>, EquationNS<dim>::DOS> gradient(const point_t &pt, Vector<size_t, dim> var_idx) override;

public:
  void calculateBasisFunction() override;

private:
  void PositivityPreserving();
  void PositivityRho(Vector<size_t, dim> var_idx, double eps, double rho);
  void PositivityPressure(Vector<size_t, dim> var_idx, double eps);

  void updateQuadInfo();

  double Pressure(const point_t &pt, Vector<size_t, dim> var_idx);

private:
  struct PositivePara {
    double theta0;
    double theta1;

    std::vector<point_t> quad_pts;
  };
  Array<PositivePara, dim> PositiveParas;

  void setEquation(EquationNSPtr<dim> eq);

  EquationNSPtr<dim> eqInfo;

  using GridSystemData<dim, dos>::_grid;

  friend class GridSysData1DNSPositiveTest;
  friend class GridSysData2DNSPositiveTest;
  friend class GridSysData3DNSPositiveTest;
};
template <int dim, int order> using GridSysDataNSPositivePtr = std::shared_ptr<GridSysDataNSPositive<dim, order>>;

//----------------------------------------------------------------------------------------------------------------------
template <int dim, int order> class GridSysDataNSPositive<dim, order>::Builder {
public:
  GridSysDataNSPositivePtr<dim, order> build(GridDataBuilderPtr<dim> builder, MeshPtr<dim> grid,
                                             EquationNSPtr<dim> eq_info);

  GridSysDataNSPositivePtr<dim, order> build(MeshPtr<dim> grid, EquationNSPtr<dim> eq_info);
};
template <int dim, int order>
using GridSysDataNSPositiveBuilderPtr = std::shared_ptr<typename GridSysDataNSPositive<dim, order>::Builder>;
} // namespace vox

#endif /* grid_system_data_NSPositive_hpp */
