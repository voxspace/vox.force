//
//  grid_data_limiter.cpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "grid_data_limiter.h"
#include "../src.common/private_helpers.h"

namespace vox {
template <int dim>
GridDataLimiter<dim>::GridDataLimiter(size_t idx, MeshPtr<dim> grid, LimiterAuxiliaryPtr aux, LimitersType t)
    : GridDataUnlimited<dim, 1>(idx, grid, aux), type(t) {}

template <int dim>
GridDataLimiter<dim>::GridDataLimiter(const GridDataLimiter &other)
    : GridDataUnlimited<dim, 1>(other), type(other.type) {}

template <int dim> GridDataLimiter<dim> &GridDataLimiter<dim>::operator=(const GridDataLimiter<dim> &other) {
  GridDataUnlimited<dim, 1>::operator=(other);

  type = other.type;

  return *this;
}

template <int dim> GridDataLimiter<dim>::~GridDataLimiter() = default;

template <int dim> std::shared_ptr<GridData<dim>> GridDataLimiter<dim>::deepCopy() {
  return CLONE_W_CUSTOM_DELETER(GridDataLimiter<dim>);
}

// MARK:- Reconstruction Solvers
template <int dim> void GridDataLimiter<dim>::setLimiterType(LimitersType t) { type = t; }

template <int dim>
void GridDataLimiter<dim>::solve(Vector<size_t, dim> i, const double &UU, const std::vector<double> &bary_info,
                                 typename PolyInfo<dim, 1>::Vec &result) {
  GridDataUnlimited<dim, 1>::solve(i, UU, bary_info, result);

  switch (type) {
  case SCALAR:
    limiter(i, result, BARYCENTER, false);
    break;

  case R_SCALAR:
    limiter(i, result, BARYCENTER, true);
    break;

  case BARTH:
    limiter(i, result, BOUNDARY, false);
    break;

  case R_BARTH:
    limiter(i, result, BOUNDARY, true);
    break;

  default:
    break;
  }
}

template <int dim>
void GridDataLimiter<dim>::limiter(Vector<size_t, dim> idx, typename PolyInfo<dim, 1>::Vec &grad, PntType tt,
                                   bool relaxed) {
  double dup = 0.0, dum = 0.0;
  if (relaxed) {
    for (int i = 0; i < dim; ++i) {
      size_t result;
      for (const auto &shift : {-1, 1}) {
        int ele_idx = idx[i] + shift;
        if (i == 0) {
          result = _grid->periodicMap(ele_idx, left);
        } else if (i == 1) {
          result = _grid->periodicMap(ele_idx, bottom);
        } else if (i == 2) {
          result = _grid->periodicMap(ele_idx, front);
        }

        if (result == static_cast<size_t>(-1)) {
          continue;
        } else {
          Vector<size_t, dim> nei_idx = idx;
          nei_idx[i] = result;
          double dv = _gridData(nei_idx) - _gridData(idx);
          dup = std::max(dup, dv);
          dum = std::min(dum, dv);
        }
      }
    }
  }

  double phi = 1.0;
  for (int i = 0; i < dim; ++i) {
    size_t result;
    for (const auto &shift : {-1, 1}) {
      int ele_idx = idx[i] + shift;
      if (i == 0) {
        result = _grid->periodicMap(ele_idx, left);
      } else if (i == 1) {
        result = _grid->periodicMap(ele_idx, bottom);
      } else if (i == 2) {
        result = _grid->periodicMap(ele_idx, front);
      }

      if (result == static_cast<size_t>(-1)) {
        continue;
      } else {
        Vector<size_t, dim> nei_idx = idx;
        nei_idx[i] = result;

        double du = _gridData(nei_idx) - _gridData(idx);

        std::array<double, PolyInfo<dim, 1>::n_unknown> aa{};
        if (tt == BARYCENTER) {
          reconAux->getPolyInfo()->basisFuncValue(idx, _grid->periodicLocalize(idx, _grid->BaryCenter(nei_idx)), aa);
        } else if (tt == BOUNDARY) {
          Direction d;
          if (i == 0 && shift == -1) {
            d = left;
          } else if (i == 0 && shift == 1) {
            d = right;
          } else if (i == 1 && shift == -1) {
            d = bottom;
          } else if (i == 1 && shift == 1) {
            d = up;
          } else if (i == 2 && shift == -1) {
            d = front;
          } else {
            d = back;
          }
          reconAux->getPolyInfo()->basisFuncValue(idx, _grid->BoundaryCenter(idx, d), aa);
        }
        double temp;
        double gr = 0.0;
        for (int t = 0; t < PolyInfo<dim, 1>::n_unknown; ++t) {
          temp = grad[t];
          temp *= aa[t];
          gr += temp;
        }

        if (relaxed) {
          if (phi * gr > dup)
            phi = dup / gr;
          else if (phi * gr < dum)
            phi = dum / gr;
        } else {
          if (phi * gr > std::max(du, 0.0))
            phi = std::max(du, 0.0) / gr;
          else if (phi * gr < std::min(du, 0.0))
            phi = std::min(du, 0.0) / gr;
        }
      }
    }
  }

  grad *= phi;
}

// MARK:- Grid Data Builder
template <int dim> void GridDataLimiter<dim>::Builder::setLimiterAuxiliary(LimiterAuxiliaryPtr aux) {
  limiterAux = aux;
}

template <int dim> void GridDataLimiter<dim>::Builder::setLimiterType(LimitersType t) { type = t; }

template <int dim> GridDataPtr<dim> GridDataLimiter<dim>::Builder::build(size_t idx, MeshPtr<dim> grid) {
  return std::make_shared<GridDataLimiter<dim>>(idx, grid, limiterAux, type);
}

template class GridDataLimiter<1>;
template class GridDataLimiter<2>;
template class GridDataLimiter<3>;
} // namespace vox
