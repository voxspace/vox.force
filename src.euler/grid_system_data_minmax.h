//
//  grid_system_data_minmax.hpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef grid_system_data_minmax_hpp
#define grid_system_data_minmax_hpp

#include "grid_system_data.h"

namespace vox {

template <int dim, int order> class GridSysDataMaxPrinciple : public GridSystemData<dim, 1> {
public:
  using point_t = typename Mesh<dim>::point_t;

public:
  //! Constructs empty grid system.
  explicit GridSysDataMaxPrinciple(MeshPtr<dim> grid);
  //! Copy constructor.
  GridSysDataMaxPrinciple(const GridSysDataMaxPrinciple &other);
  //! Copy operator
  GridSysDataMaxPrinciple &operator=(const GridSysDataMaxPrinciple &other);

  //! Destructor.
  virtual ~GridSysDataMaxPrinciple();

  //! Returns the copy of the grid instance.
  std::shared_ptr<GridSystemData<dim, 1>> deepCopy() override;

  class Builder;

  static Builder builder();

public:
  //! return value of specific point
  Vector<double, 1> value(const point_t &pt, Vector<size_t, dim> var_idx) override;

  //! return value of specific point
  Vector<Vector<double, dim>, 1> gradient(const point_t &pt, Vector<size_t, dim> var_idx) override;

public:
  void calculateBasisFunction() override;

private:
  void MaxPreserving();
  void MaxPreserving(Vector<size_t, dim> var_idx);

  void updateQuadInfo();
  void setBounds(double min, double max);

private:
  Array<double, dim> thetas;
  Array<std::vector<point_t>, dim> quad_pts;

  double maximum{};
  double minimum{};

  using GridSystemData<dim, 1>::_grid;

  friend class GridSysData1DMaxPrincipleTest;
  friend class GridSysData2DMaxPrincipleTest;
  friend class GridSysData3DMaxPrincipleTest;
};
template <int dim, int order> using GridSysDataMaxPrinciplePtr = std::shared_ptr<GridSysDataMaxPrinciple<dim, order>>;

//----------------------------------------------------------------------------------------------------------------------
template <int dim, int order> class GridSysDataMaxPrinciple<dim, order>::Builder {
public:
  virtual GridSysDataMaxPrinciplePtr<dim, order> build(GridDataBuilderPtr<dim> builder, MeshPtr<dim> grid, double min,
                                                       double max);

  virtual GridSysDataMaxPrinciplePtr<dim, order> build(MeshPtr<dim> grid, double min, double max);
};
template <int dim, int order>
using GridSysDataMaxPrincipleBuilderPtr = std::shared_ptr<typename GridSysDataMaxPrinciple<dim, order>::Builder>;
} // namespace vox
#endif /* grid_system_data_minmax_hpp */
