//
//  equation_scalar.cpp
//  Flames
//
//  Created by 杨丰 on 2020/5/1.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "equation_scalar.h"
#include "../../src.common/private_helpers.h"

namespace vox {

template <int dim> double EquationScalar<dim>::MaxCharacteristicSpeed(const Vector<double, DOS> &u) {
  double s = 0.;
  for (int i = 0; i < dim; i++) {
    s += std::pow(fluxDerFuncs[i](u[0]), 2);
  }
  return std::sqrt(s);
}

template <int dim>
void EquationScalar<dim>::TFlux(const Vector<double, DOS> &u, const std::array<double, dim> &normal,
                                Vector<double, DOS> &flux) {
  for (int i = 0; i < dim; i++) {
    flux[0] += fluxFuncs[i](u[0]) * normal[i];
  }
}

template <int dim>
void EquationScalar<dim>::TFlux(const Vector<double, DOS> &u, const Vector<Vector<double, dim>, DOS> &gu,
                                const std::array<double, dim> &normal, Vector<double, DOS> &flux, bool isOnlyViscous) {
  for (int i = 0; i < dim; ++i) {
    flux[0] += viscousFuncs[i](u[0], gu[0][i]) * normal[i];
  }

  if (!isOnlyViscous) {
    Vector<double, DOS> inviscidFlux;
    TFlux(u, normal, inviscidFlux);
    flux *= -1.0;
    flux += inviscidFlux;
  }
}

template class EquationScalar<1>;
template class EquationScalar<2>;
template class EquationScalar<3>;

// MARK:- ScalarEquationDataBase
namespace ScalarEquationDataBase {
// MARK:- Advection
namespace Advection {
std::function<double(double u)> flux(double a) {
  return [a](double u) -> double { return a * u; };
}

std::function<double(double u)> fluxDer(double a) {
  return [a](double u) -> double {
    UNUSED_VARIABLE(u);
    return a;
  };
}
} // namespace Advection
// MARK:- Burgers
namespace Burgers {
std::function<double(double u)> flux() {
  return [](double u) -> double { return u * u / 2.0; };
}

std::function<double(double u)> fluxDer() {
  return [](double u) -> double { return u; };
}
} // namespace Burgers
// MARK:- ViscousTerm
namespace ViscousTerm {
std::function<double(double u, double gu)> constVal(double eps) {
  return [eps](double u, double gu) -> double {
    UNUSED_VARIABLE(u);
    return eps * gu;
  };
}

std::function<double(double u, double gu)> deGenerate(double eps) {
  return [eps](double u, double gu) -> double {
    if (std::abs(u) > 0.25) {
      return eps * gu;
    } else {
      return 0.0;
    }
  };
}
} // namespace ViscousTerm
//-----------------------------------------
} // namespace ScalarEquationDataBase

} // namespace vox
