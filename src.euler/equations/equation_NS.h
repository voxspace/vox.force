//
//  equation_NS.hpp
//  Flames
//
//  Created by 杨丰 on 2020/5/1.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef equation_NS_hpp
#define equation_NS_hpp

#include "../equation_prototype.h"

namespace vox {

template <int dim> class EquationNS final : public ConvEquationType<dim, dim + 2> {
public:
  using ConvEquationType<dim, dim + 2>::DIM;
  using ConvEquationType<dim, dim + 2>::DOS;

  explicit EquationNS(double gamma, double mu = 0.0, double kappa = 0.0) : Gamma(gamma), Mu(mu), Kappa(kappa) {}

  virtual ~EquationNS() = default;

  const double Gamma;
  const double Mu;
  const double Kappa;

public:
  // MARK:- Flux Functions
  double MaxCharacteristicSpeed(const Vector<double, DOS> &u) override;

  void MaxSpeeds(const Vector<double, DOS> &u, const std::array<double, dim> &normal, double &lambda_l,
                 double &lambda_r) override;

  void TFlux(const Vector<double, DOS> &u, const std::array<double, dim> &normal, Vector<double, DOS> &flux) override;

  void TFlux(const Vector<double, DOS> &u, const Vector<Vector<double, dim>, DOS> &gu,
             const std::array<double, dim> &normal, Vector<double, DOS> &flux, bool isOnlyViscous) override;

  double PositiveFLuxPara(const Vector<double, DOS> &u, const Vector<Vector<double, dim>, DOS> &gu,
                          const std::array<double, dim> &normal, bool isOnlyViscous);

  // result have dim+1 elements, which is \nabla v, \nabla T,
  void ViscousDer(const Vector<double, DOS> &u, const Vector<Vector<double, dim>, DOS> &gu,
                  Vector<Vector<double, dim>, dim + 1> &result);

public:
  // MARK:- Helper functions
  double pressure(const Vector<double, DOS> &u);

  void CharacteristicSpeed(const Vector<double, DOS> &u, double &vel, double &speed);

  double MaxCharacteristicSpeed(const Vector<double, DOS> &u, const std::array<double, dim> &normal);

  Vector<double, dim + 2> prim2cons(const Vector<double, DOS> &u);
  Vector<double, dim + 2> cons2prim(const Vector<double, DOS> &u);

protected:
  void CalVelTheta(const Vector<double, DOS> &u, std::array<double, dim> &vel, double &theta);
  void CalVelTheta(const Vector<double, DOS> &u, std::array<double, dim> &vel, double &vn, double &theta,
                   const std::array<double, dim> &normal);
};
template <int dim> using EquationNSPtr = std::shared_ptr<EquationNS<dim>>;

} // namespace vox

#endif /* equation_NS_hpp */
