//
//  equation_scalar.hpp
//  Flames
//
//  Created by 杨丰 on 2020/5/1.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef equation_scalar_hpp
#define equation_scalar_hpp

#include <utility>

#include "../equation_prototype.h"

namespace vox {

template <int dim> class EquationScalar final : public ConvEquationType<dim, 1> {
public:
  using fluxFunc = std::function<double(double u)>;
  using viscousFunc = std::function<double(double u, double gu)>;
  using ConvEquationType<dim, 1>::DIM;
  using ConvEquationType<dim, 1>::DOS;

  EquationScalar(std::array<fluxFunc, dim> funcs, std::array<fluxFunc, dim> derFuncs,
                 std::array<viscousFunc, dim> vFuncs = {})
      : fluxFuncs(std::move(funcs)), fluxDerFuncs(std::move(derFuncs)), viscousFuncs(std::move(vFuncs)) {}

  std::array<fluxFunc, dim> getFluxFunc() { return fluxFuncs; }

  fluxFunc getFluxFunc(int direction) {
    JET_ASSERT(direction < dim);
    return fluxFuncs[direction];
  }

  std::array<fluxFunc, dim> getFluxDerFunc() { return fluxDerFuncs; }

  fluxFunc getFluxDerFunc(int direction) {
    JET_ASSERT(direction < dim);
    return fluxDerFuncs[direction];
  }

  std::array<viscousFunc, dim> getViscousFuncs() { return viscousFuncs; }

  viscousFunc getViscousFuncs(int direction) {
    JET_ASSERT(direction < dim);
    return viscousFuncs[direction];
  }

public:
  double MaxCharacteristicSpeed(const Vector<double, DOS> &u) override;

  void TFlux(const Vector<double, DOS> &u, const std::array<double, dim> &normal, Vector<double, DOS> &flux) override;

  void TFlux(const Vector<double, DOS> &u, const Vector<Vector<double, dim>, DOS> &gu,
             const std::array<double, dim> &normal, Vector<double, DOS> &flux, bool isOnlyViscous) override;

private:
  const std::array<fluxFunc, dim> fluxFuncs;
  const std::array<fluxFunc, dim> fluxDerFuncs;
  const std::array<viscousFunc, dim> viscousFuncs;
};
template <int dim> using EquationScalarPtr = std::shared_ptr<EquationScalar<dim>>;

// MARK:- ScalarEquationDataBase
namespace ScalarEquationDataBase {
// MARK:- Advection
namespace Advection {
std::function<double(double u)> flux(double a);

std::function<double(double u)> fluxDer(double a);
} // namespace Advection
// MARK:- Burgers
namespace Burgers {
std::function<double(double u)> flux();

std::function<double(double u)> fluxDer();
} // namespace Burgers
// MARK:- ViscousTerm
namespace ViscousTerm {
std::function<double(double u, double gu)> constVal(double eps);

std::function<double(double u, double gu)> deGenerate(double eps);
} // namespace ViscousTerm
//-----------------------------------------
} // namespace ScalarEquationDataBase

} // namespace vox

#endif /* equation_scalar_hpp */
