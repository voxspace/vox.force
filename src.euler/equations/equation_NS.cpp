//
//  equation_NS.cpp
//  Flames
//
//  Created by 杨丰 on 2020/5/1.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "equation_NS.h"
#include <glog/logging.h>

namespace vox {
template <int dim> double EquationNS<dim>::MaxCharacteristicSpeed(const Vector<double, DOS> &u) {
  std::array<double, dim> vel{};
  double theta;
  CalVelTheta(u, vel, theta);
  double vel2 = 0.;
  for (int d = 0; d < dim; ++d)
    vel2 += vel[d] * vel[d];
  return std::sqrt(vel2) + std::sqrt(Gamma * theta);
}

template <int dim>
void EquationNS<dim>::MaxSpeeds(const Vector<double, DOS> &u, const std::array<double, dim> &normal, double &lambda_l,
                                double &lambda_r) {
  std::array<double, dim> vel{};
  double theta, vn;
  CalVelTheta(u, vel, vn, theta, normal);
  theta = std::sqrt(Gamma * theta);
  lambda_l = vn - theta;
  lambda_r = vn + theta;
} // max and min Eigenvalue

template <int dim>
void EquationNS<dim>::TFlux(const Vector<double, DOS> &u, const std::array<double, dim> &normal,
                            Vector<double, DOS> &flux) {
  std::array<double, dim> vel{};
  double theta, vn;
  CalVelTheta(u, vel, vn, theta, normal);
  double p = u[0] * theta;

  flux[0] = u[0] * vn;
  for (int d = 0; d < dim; ++d)
    flux[d + 1] = vn * u[d + 1];
  for (int d = 0; d < dim; ++d)
    flux[d + 1] += normal[d] * p;
  flux[dim + 1] = vn * (u[dim + 1] + p);
}

template <int dim>
void EquationNS<dim>::TFlux(const Vector<double, DOS> &u, const Vector<Vector<double, dim>, DOS> &gu,
                            const std::array<double, dim> &normal, Vector<double, DOS> &flux, bool isOnlyViscous) {
  Vector<Vector<double, dim>, dim + 1> viscousDer;
  ViscousDer(u, gu, viscousDer);

  double P = 0.0;
  for (int i = 0; i < dim; ++i) {
    P += viscousDer[i][i];
  }
  P /= 3.0;

  Matrix<double, dim, dim> strain_rate;
  for (int i = 0; i < dim; ++i) {
    for (int j = 0; j < dim; ++j) {
      strain_rate(i, j) = 0.5 * (viscousDer[i][j] + viscousDer[j][i]);
    }
  }

  Matrix<double, dim, dim> stress;
  for (int i = 0; i < dim; ++i) {
    for (int j = 0; j < dim; ++j) {
      if (i == j) {
        stress(i, j) = 2 * Mu * (strain_rate(i, j) - P);
      } else {
        stress(i, j) = 2 * Mu * strain_rate(i, j);
      }
    }
  }

  std::array<double, dim> q{};
  for (int i = 0; i < dim; ++i) {
    q[i] = -Kappa * viscousDer[dim][i];
  }

  flux[0] = 0.0;
  for (int i = 0; i < dim; ++i) {
    double e = 0.0;
    for (int j = 0; j < dim; ++j) {
      flux[i + 1] += stress(i, j) * normal[j];
      e += u[j + 1] / u[0] * stress(i, j);
    }
    flux[dim + 1] += (e - q[i]) * normal[i];
  }

  if (!isOnlyViscous) {
    Vector<double, DOS> inviscidFlux;
    TFlux(u, normal, inviscidFlux);
    flux *= -1.0;
    flux += inviscidFlux;
  }
}
//-------------------------------------------------------------------------------
template <int dim>
double EquationNS<dim>::PositiveFLuxPara(const Vector<double, DOS> &u, const Vector<Vector<double, dim>, DOS> &gu,
                                         const std::array<double, dim> &normal, bool isOnlyViscous) {
  std::array<double, dim> vel{};
  double theta, vn;
  CalVelTheta(u, vel, vn, theta, normal);
  double P = theta * u[0];
  double e = theta / (Gamma - 1.0);

  Vector<Vector<double, dim>, dim + 1> viscousDer;
  ViscousDer(u, gu, viscousDer);
  double q = 0.0;
  double tau = 0.0;
  if (isOnlyViscous) {
    for (int i = 0; i < dim; ++i) {
      q += Kappa * viscousDer[dim][i] * normal[i];
      std::valarray<double> tau_vec(0.0, dim);
      for (int j = 0; j < dim; ++j) {
        tau_vec[i] += Mu * viscousDer[i][j] * normal[j];
      }
      tau = std::pow(tau_vec, 2.0).sum();
    }
  } else {
    for (int i = 0; i < dim; ++i) {
      q += Kappa * viscousDer[dim][i] * normal[i];
      std::valarray<double> tau_vec(0.0, dim);
      for (int j = 0; j < dim; ++j) {
        tau_vec[i] += Mu * viscousDer[i][j] * normal[j];
      }
      tau = std::pow(tau_vec - P * normal[i], 2.0).sum();
    }
  }
  double para =
      (std::sqrt(u[0] * u[0] * q * q + 2 * u[0] * u[0] * e * tau) + u[0] * std::abs(q)) / (2 * u[0] * u[0] * e);

  para = std::abs(para);
  if (!isOnlyViscous) {
    para += vn;
  }
  return para;
}
//-------------------------------------------------------------------------------
template <int dim>
void EquationNS<dim>::ViscousDer(const Vector<double, DOS> &u, const Vector<Vector<double, dim>, DOS> &gu,
                                 Vector<Vector<double, dim>, dim + 1> &result) {
  //\nabla u, v, w
  for (int i = 0; i < dim; ++i) {
    for (int j = 0; j < dim; ++j) {
      result[i][j] = (gu[i + 1][j] - gu[0][j] * u[i + 1] / u[0]) / u[0];
    }
  }

  //\nabla e
  for (int i = 0; i < dim; ++i) {
    result[dim][i] = gu[dim + 1][i] * u[0] - gu[0][i] * u[dim + 1];
    result[dim][i] /= std::pow(u[0], 2);
    for (int j = 0; j < dim; ++j) {
      result[dim][i] -= result[j][i] * u[j + 1] / u[0];
    }
  }
}

// MARK:- Helper functions
template <int dim> double EquationNS<dim>::pressure(const Vector<double, DOS> &u) {
  std::array<double, dim> vel{};
  double theta;
  CalVelTheta(u, vel, theta);
  return theta * u[0];
}
//-------------------------------------------------------------------------------
template <int dim> void EquationNS<dim>::CharacteristicSpeed(const Vector<double, DOS> &u, double &vel, double &speed) {
  std::array<double, dim> velocity{};
  double theta;
  CalVelTheta(u, velocity, theta);
  vel = 0.;
  for (int d = 0; d < dim; ++d)
    vel += velocity[d] * velocity[d];

  vel = std::sqrt(vel);
  speed = std::sqrt(Gamma * theta);
}

//-------------------------------------------------------------------------------
template <int dim>
double EquationNS<dim>::MaxCharacteristicSpeed(const Vector<double, DOS> &u, const std::array<double, dim> &normal) {
  std::array<double, dim> vel{};
  double theta, vn;
  CalVelTheta(u, vel, vn, theta, normal);

  return std::fabs(vn) + std::sqrt(Gamma * theta);
} // Max Eigenvalue
//-------------------------------------------------------------------------------
template <int dim> Vector<double, dim + 2> EquationNS<dim>::prim2cons(const Vector<double, DOS> &u) {
  Vector<double, dim + 2> u_out;

  u_out[0] = u[0];
  double v_l2 = 0.0;
  for (int i = 0; i < dim; ++i) {
    u_out[i + 1] = u[i + 1] * u[0];
    v_l2 += u[i + 1] * u[i + 1];
  }
  u_out[dim + 1] = u[dim + 1] / (Gamma - 1.0) + 0.5 * u[0] * v_l2;

  return u_out;
}
//-------------------------------------------------------------------------------
template <int dim> Vector<double, dim + 2> EquationNS<dim>::cons2prim(const Vector<double, DOS> &u) {
  std::array<double, dim> vel{};
  double theta;
  CalVelTheta(u, vel, theta);

  Vector<double, dim + 2> u_out;
  u_out[0] = u[0]; // rho
  for (int i = 0; i < dim; ++i) {
    u_out[i + 1] = vel[i]; // u
  }
  u_out[dim + 1] = theta * u[0]; // P

  return u_out;
}
// MARK:- Private functions
template <int dim>
void EquationNS<dim>::CalVelTheta(const Vector<double, DOS> &u, std::array<double, dim> &vel, double &theta) {
  double vel2 = 0.;
  for (int d = 0; d < dim; ++d) {
    vel[d] = u[d + 1] / u[0];
    vel2 += vel[d] * vel[d];
  }
  theta = (Gamma - 1) * (u[dim + 1] / u[0] - 0.5 * vel2); // P/rho

  if (theta < 0.0) {
    LOG(INFO) << "Negative Pressure!" + std::to_string(theta * u[0]);
    theta = 0.0;
  }
}
//-------------------------------------------------------------------------------
template <int dim>
void EquationNS<dim>::CalVelTheta(const Vector<double, DOS> &u, std::array<double, dim> &vel, double &vn, double &theta,
                                  const std::array<double, dim> &normal) {
  double vel2 = 0.;
  for (int d = 0; d < dim; ++d) {
    vel[d] = u[d + 1] / u[0];
    vel2 += vel[d] * vel[d];
  }
  vn = 0.;
  for (int d = 0; d < dim; ++d)
    vn += vel[d] * normal[d];
  theta = (Gamma - 1) * (u[dim + 1] / u[0] - 0.5 * vel2); // P/rho

  if (theta < 0.0) {
    LOG(INFO) << "Negative Pressure!" + std::to_string(theta * u[0]);
    theta = 0.0;
  }
}
//-------------------------------------------------------------------------------
template class EquationNS<1>;
template class EquationNS<2>;
template class EquationNS<3>;
} // namespace vox
