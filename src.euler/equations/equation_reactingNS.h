//
//  equation_reactingNS.hpp
//  Flames
//
//  Created by 杨丰 on 2020/5/1.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef equation_reactingNS_hpp
#define equation_reactingNS_hpp

#include "../equation_prototype.h"

namespace vox {
template <int dim> class EquationReactingNS final : public ConvEquationType<dim, dim + 3> {
public:
  using ConvEquationType<dim, dim + 3>::DIM;
  using ConvEquationType<dim, dim + 3>::DOS;

  EquationReactingNS(double gamma, double q, double mu = 0.0, double kappa = 0.0, double d = 0.0)
      : Gamma(gamma), Mu(mu), Kappa(kappa), q0(q), D(d) {}

  virtual ~EquationReactingNS() = default;

  const double Gamma;
  const double Mu;
  const double Kappa;
  const double q0;
  const double D;

public:
  // MARK:- Flux Functions
  double MaxCharacteristicSpeed(const Vector<double, DOS> &u) override;

  void MaxSpeeds(const Vector<double, DOS> &u, const std::array<double, dim> &normal, double &lambda_l,
                 double &lambda_r) override;

  void TFlux(const Vector<double, DOS> &u, const std::array<double, dim> &normal, Vector<double, DOS> &flux) override;

  void TFlux(const Vector<double, DOS> &u, const Vector<Vector<double, dim>, DOS> &gu,
             const std::array<double, dim> &normal, Vector<double, DOS> &flux, bool isOnlyViscous) override;

  double PositiveFLuxPara(const Vector<double, DOS> &u, const Vector<Vector<double, dim>, DOS> &gu,
                          const std::array<double, dim> &normal, bool isOnlyViscous);

  // result have dim+2 elements, which is \nabla v, \nabla T, \nabla\lambda,
  void ViscousDer(const Vector<double, DOS> &u, const Vector<Vector<double, dim>, DOS> &gu,
                  Vector<Vector<double, dim>, dim + 2> &result);

public:
  // MARK:- Helper functions
  double pressure(const Vector<double, DOS> &u);

  void CharacteristicSpeed(const Vector<double, DOS> &u, double &vel, double &speed);

  double MaxCharacteristicSpeed(const Vector<double, DOS> &u, const std::array<double, dim> &normal);

  Vector<double, dim + 3> prim2cons(const Vector<double, DOS> &u);
  Vector<double, dim + 3> cons2prim(const Vector<double, DOS> &u);

private:
  void CalVelTheta(const Vector<double, DOS> &u, std::array<double, dim> &vel, double &theta);
  void CalVelTheta(const Vector<double, DOS> &u, std::array<double, dim> &vel, double &vn, double &theta,
                   const std::array<double, dim> &normal);
};
template <int dim> using EquationReactingNSPtr = std::shared_ptr<EquationReactingNS<dim>>;

} // namespace vox

#endif /* equation_reactingNS_hpp */
