//
//  polyInfo1d.cpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "polyInfo1d.h"

#include "../src.common/parallel.h"
#include <utility>

namespace vox {

template <int order> PolyInfo<1, order>::PolyInfo(const MeshPtr<dim> &g) : grid(g) {
  if (grid != nullptr) {
    element_cache.resize(grid->dataSize());
    loadBasisFunc();
  }
}

template <int order> void PolyInfo<1, order>::resetGrid(MeshPtr<dim> g) {
  grid = std::move(g);
  element_cache.resize(grid->dataSize());
  loadBasisFunc();
}

template <int order> void PolyInfo<1, order>::loadBasisFunc() {
  parallelFor(kZeroSize, grid->dataSize().x, [&](size_t ele_idx) -> void {
    for (int m = 1; m <= order; ++m) {
      double J0 = 0.0;

      J0 = std::pow(grid->SizeOfEle() / 2.0, m + 1);
      J0 -= std::pow(-grid->SizeOfEle() / 2.0, m + 1);
      J0 /= (m + 1);

      J0 /= grid->SizeOfEle();
      element_cache[ele_idx][m - 1] = J0;
    }
  });
}

// MARK:- Average Basis Funcs
template <int order>
void PolyInfo<1, order>::averageBasisFunc(const Vector<size_t, dim> basisIdx, const Vector<size_t, dim> &quadIdx,
                                          std::array<double, n_unknown> &result) {
  point_t pl = grid->periodicLocalize(basisIdx, grid->BoundaryCenter(quadIdx, left));
  point_t pr = grid->periodicLocalize(basisIdx, grid->BoundaryCenter(quadIdx, right));
  for (int m = 1; m <= order; ++m) {
    double J_i = 0;

    J_i = std::pow(pr[0] - grid->BaryCenter(basisIdx)[0], m + 1);
    J_i -= std::pow(pl[0] - grid->BaryCenter(basisIdx)[0], m + 1);
    J_i /= (m + 1);
    J_i /= grid->SizeOfEle();

    result[m - 1] = (J_i - element_cache(basisIdx)[m - 1]);
    result[m - 1] *= std::pow(grid->SizeOfEle(), 1 - m);
  }
}

template <int order>
void PolyInfo<1, order>::averageBasisFunc(const Vector<size_t, dim> basisIdx, ArrayView1<Vector<size_t, dim>> patch,
                                          ArrayView1<std::array<double, n_unknown>> result) {
  for (size_t j = 0; j < patch.length(); ++j) {
    std::array<double, n_unknown> s{};
    averageBasisFunc(basisIdx, patch[j], s);
    result[j] = s;
  }
}

template <int order>
void PolyInfo<1, order>::updateLSMatrix(Vector<size_t, dim> basisIdx, ArrayView1<Vector<size_t, dim>> patch,
                                        ArrayView1<std::array<double, n_unknown>> poly_avgs, Mat &G) {
  averageBasisFunc(basisIdx, patch, poly_avgs);

  G.fill(0.0);
  for (size_t j = 0; j < patch.length(); ++j) {
    for (int t1 = 0; t1 < n_unknown; ++t1) {
      for (int t2 = 0; t2 < n_unknown; ++t2) {
        G(t1, t2) += poly_avgs[j][t1] * poly_avgs[j][t2];
      }
    }
  }
}

// MARK:- Basis Funcs
template <int order>
void PolyInfo<1, order>::basisFuncValue(const Vector<size_t, dim> idx, const point_t &coord,
                                        std::array<double, n_unknown> &result) {
  point_t cr = coord - grid->BaryCenter(idx);
  for (int m = 1; m <= order; ++m) {
    result[m - 1] = std::pow(cr[0], m) - element_cache(idx)[m - 1];
    result[m - 1] *= std::pow(grid->SizeOfEle(), 1 - m);
  }
}

template <int order>
void PolyInfo<1, order>::basisFuncGradient(const Vector<size_t, dim>& idx, const point_t &coord,
                                           std::array<std::array<double, n_unknown>, dim> &result) {
  point_t cr = coord - grid->BaryCenter(idx);
  for (int m = 1; m <= order; ++m) {
    result[0][m - 1] = m * std::pow(cr[0], m - 1);
    result[0][m - 1] *= std::pow(grid->SizeOfEle(), 1 - m);
  }
}

template <int order>
double PolyInfo<1, order>::funcValue(const Vector<size_t, dim> idx, const point_t &coord, const double &avg,
                                     const Vec &para) {
  std::array<double, n_unknown> aa{};
  basisFuncValue(idx, coord, aa);

  double temp;
  double result = avg;
  for (int t = 0; t < n_unknown; ++t) {
    temp = para[t] * aa[t];
    result += temp;
  }

  return result;
}

template <int order>
std::array<double, 1> PolyInfo<1, order>::funcGradient(const Vector<size_t, dim> idx, const point_t &coord,
                                                       const Vec &para) {
  std::array<std::array<double, n_unknown>, dim> aa{};
  basisFuncGradient(idx, coord, aa);

  double temp;
  std::array<double, dim> result{};
  for (uint i = 0; i < dim; ++i) {
    for (int t = 0; t < n_unknown; ++t) {
      temp = para[t] * aa[i][t];
      result[i] += temp;
    }
  }

  return result;
}

template class PolyInfo<1, 1>;
template class PolyInfo<1, 2>;
template class PolyInfo<1, 3>;
} // namespace vox
