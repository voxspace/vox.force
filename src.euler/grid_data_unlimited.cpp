//
//  grid_data_unlimited.cpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "grid_data_unlimited.h"

namespace vox {

template <int dim, int order>
GridDataUnlimited<dim, order>::GridDataUnlimited(size_t idx, MeshPtr<dim> grid, ReconAuxiliaryPtr<dim, order> aux)
    : GridData<dim>(idx, grid), reconAux(aux) {
  recon_cache.resize(_grid->dataSize());
}

template <int dim, int order>
GridDataUnlimited<dim, order>::GridDataUnlimited(const GridDataUnlimited<dim, order> &other)
    : GridData<dim>(other), reconAux(other.reconAux) {
  recon_cache = other.recon_cache;
}

template <int dim, int order>
GridDataUnlimited<dim, order> &GridDataUnlimited<dim, order>::operator=(const GridDataUnlimited<dim, order> &other) {
  GridData<dim>::operator=(other);

  recon_cache = other.recon_cache;

  return *this;
}

template <int dim, int order> GridDataUnlimited<dim, order>::~GridDataUnlimited() = default;

template <int dim, int order> std::shared_ptr<GridData<dim>> GridDataUnlimited<dim, order>::deepCopy() {
  return std::shared_ptr<GridDataUnlimited<dim, order>>(new GridDataUnlimited<dim, order>(*this),
                                                        [](GridDataUnlimited<dim, order> *obj) { delete obj; });
}

// MARK:- Cell Reconstructed Function API
template <int dim, int order> double GridDataUnlimited<dim, order>::value(const point_t &pt, Vector<size_t, dim> idx) {
  return reconAux->getPolyInfo()->funcValue(idx, pt, _gridData(idx), recon_cache(idx).Slope);
}

template <int dim, int order>
std::array<double, dim> GridDataUnlimited<dim, order>::gradient(const point_t &pt, Vector<size_t, dim> idx) {
  return reconAux->getPolyInfo()->funcGradient(idx, pt, recon_cache(idx).Slope);
}

// MARK:- Reconstruction Solvers

template <int dim, int order> void GridDataUnlimited<dim, order>::calculateBasisFunction() {
  if constexpr (dim == 1) {
    parallelFor(kZeroSize, _grid->dataSize()[0], [&](size_t ele_idx) -> void { calculateBasisFunction({ele_idx}); });
  } else if constexpr (dim == 2) {
    parallelFor(kZeroSize, _grid->dataSize()[0], kZeroSize, _grid->dataSize()[1],
                [&](size_t ele_idx, size_t ele_idy) -> void {
                  calculateBasisFunction({ele_idx, ele_idy});
                });
  } else if constexpr (dim == 3) {
    parallelFor(kZeroSize, _grid->dataSize()[0], kZeroSize, _grid->dataSize()[1], kZeroSize, _grid->dataSize()[2],
                [&](size_t ele_idx, size_t ele_idy, size_t ele_idz) -> void {
                  calculateBasisFunction({ele_idx, ele_idy, ele_idz});
                });
  }
}

template <int dim, int order> void GridDataUnlimited<dim, order>::calculateBasisFunction(Vector<size_t, dim> i) {
  // prepare for stencil neighbor
  const ArrayView1<Vector<size_t, dim>> &ind = reconAux->getPatch(recon_cache.index(i));
  // prepare for stencil bary
  std::vector<double> stencil_bary(ind.length());
  for (size_t j = 0; j < ind.length(); ++j) {
    stencil_bary[j] = _gridData(ind[j]);
  }

  // solver
  solve(i, _gridData(i), stencil_bary, recon_cache(i).Slope);
}

template <int dim, int order>
void GridDataUnlimited<dim, order>::solve(Vector<size_t, dim> i, const double &UU, const std::vector<double> &bary_info,
                                          typename PolyInfo<dim, order>::Vec &result) {
  // prepare c
  typename PolyInfo<dim, order>::Vec cc;
  cc.fill(0.0);
  ArrayView1<std::array<double, PolyInfo<dim, order>::n_unknown>> s = reconAux->getPolyAvgs(recon_cache.index(i));
  for (size_t j = 0; j < bary_info.size(); ++j) {
    for (int w = 0; w < PolyInfo<dim, order>::n_unknown; ++w) {
      cc[w] += -(bary_info[j] - UU) * s[j][w];
    }
  }

  // solve
  result = -*reconAux->getGInv(i) * cc;
}

//----------------------------------------------------------------------------------------------------------------------
// MARK:- Grid Data Builder
template <int dim, int order>
void GridDataUnlimited<dim, order>::Builder::setReconAuxiliary(ReconAuxiliaryPtr<dim, order> aux) {
  reconAux = aux;
}

template <int dim, int order>
GridDataPtr<dim> GridDataUnlimited<dim, order>::Builder::build(size_t idx, MeshPtr<dim> grid) {
  return std::make_shared<GridDataUnlimited<dim, order>>(idx, grid, reconAux);
}

template class GridDataUnlimited<1, 1>;
template class GridDataUnlimited<1, 2>;
template class GridDataUnlimited<1, 3>;

template class GridDataUnlimited<2, 1>;
template class GridDataUnlimited<2, 2>;
template class GridDataUnlimited<2, 3>;

template class GridDataUnlimited<3, 1>;
template class GridDataUnlimited<3, 2>;
template class GridDataUnlimited<3, 3>;
} // namespace vox
