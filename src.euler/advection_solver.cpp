//
//  advection_solver.cpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "advection_solver.h"

namespace vox {

template <int dim, int dos>
AdvectionSolver<dim, dos>::AdvectionSolver(MeshPtr<dim> grid, FluxPtr<dim, dos> flux)
    : grid(grid), fluxPtr(flux), integrator(grid->origin(), grid->gridSpacing()) {}

template <int dim, int dos> AdvectionSolver<dim, dos>::~AdvectionSolver() { fluxPtr = nullptr; }

template <int dim, int dos> void AdvectionSolver<dim, dos>::setFlux(FluxPtr<dim, dos> flux) { fluxPtr = flux; }

template <int dim, int dos> FluxPtr<dim, dos> AdvectionSolver<dim, dos>::getFlux() { return fluxPtr; }

// MARK:- advect functions
template <int dim, int dos>
void AdvectionSolver<dim, dos>::fluxOneSide(const GridSystemDataPtr<dim, dos> &input,
                                            const ProblemDefPtr<dim, dos> &def,
                                            const std::array<double, dim> &out_normal,
                                            const Vector<size_t, dim> &ele_idx, const Vector<size_t, dim> &nei_ele_idx,
                                            Direction d, double dt, AdvectionType type,
                                            GridSystemDataPtr<dim, dos> output) {
  size_t result;
  Direction opposite_d;
  switch (d) {
  case left: {
    result = nei_ele_idx[0];
    opposite_d = right;
    break;
  }
  case right: {
    result = nei_ele_idx[0];
    opposite_d = left;
    break;
  }
  case up: {
    result = nei_ele_idx[1];
    opposite_d = bottom;
    break;
  }
  case bottom: {
    result = nei_ele_idx[1];
    opposite_d = up;
    break;
  }
  case front: {
    result = nei_ele_idx[2];
    opposite_d = back;
    break;
  }
  case back: {
    result = nei_ele_idx[2];
    opposite_d = front;
    break;
  }
  }

  if (result != static_cast<size_t>(-1)) {
    Vector<double, dos> flux;
    flux = integrator.template surfaceIntegral<Vector<double, dos>>(
        [&](const typename Mesh<dim>::point_t &pt) -> Vector<double, dos> {
          Vector<double, dos> v0 = input->value(pt, ele_idx);

          typename Mesh<dim>::point_t bary_dist = grid->BoundaryCenter(nei_ele_idx, opposite_d);
          bary_dist -= grid->BoundaryCenter(ele_idx, d);
          Vector<double, dos> v1 = input->value(pt + bary_dist, nei_ele_idx);

          Vector<double, dos> flux_quad;
          switch (type) {
          case inviscid_only:
            fluxPtr->NumericalFlux(v0, v1, out_normal, dt / grid->AreaOfEle() * grid->LengthOfBry(), def->eqInfo,
                                   flux_quad);
            break;

          case Viscous_only: {
            Vector<Vector<double, dim>, dos> g0 = input->gradient(pt, ele_idx);
            Vector<Vector<double, dim>, dos> g1 = input->gradient(pt + bary_dist, nei_ele_idx);
            fluxPtr->NumericalFlux(v0, v1, g0, g1, out_normal, dt / grid->AreaOfEle() * grid->LengthOfBry(),
                                   def->eqInfo, flux_quad, true);
            break;
          }

          case Whole_flux: {
            Vector<Vector<double, dim>, dos> g0 = input->gradient(pt, ele_idx);
            Vector<Vector<double, dim>, dos> g1 = input->gradient(pt + bary_dist, nei_ele_idx);
            fluxPtr->NumericalFlux(v0, v1, g0, g1, out_normal, dt / grid->AreaOfEle() * grid->LengthOfBry(),
                                   def->eqInfo, flux_quad, false);
            break;
          }

          default:
            break;
          }

          return flux_quad;
        },
        ele_idx, d);

    flux /= -grid->AreaOfEle();

    output->addVectorData(ele_idx, flux);
  } else {
    Vector<double, dos> flux;
    flux = integrator.template surfaceIntegral<Vector<double, dos>>(
        [&](const typename Mesh<dim>::point_t &pt) -> Vector<double, dos> {
          Vector<double, dos> v0 = input->value(pt, ele_idx);

          Vector<double, dos> bVals;
          def->boundaryCondition(v0, bVals, out_normal, pt, d);

          Vector<double, dos> flux_quad;
          switch (type) {
          case inviscid_only:
            fluxPtr->NumericalFlux(v0, bVals, out_normal, dt / grid->AreaOfEle() * grid->LengthOfBry(), def->eqInfo,
                                   flux_quad);
            break;

          case Viscous_only: {
            Vector<Vector<double, dim>, dos> g0 = input->gradient(pt, ele_idx);
            Vector<Vector<double, dim>, dos> bgVals;
            def->visBoundaryCondition(g0, bgVals, out_normal, pt, d);

            fluxPtr->NumericalFlux(v0, bVals, g0, bgVals, out_normal, dt / grid->AreaOfEle() * grid->LengthOfBry(),
                                   def->eqInfo, flux_quad, true);
            break;
          }

          case Whole_flux: {
            Vector<Vector<double, dim>, dos> g0 = input->gradient(pt, ele_idx);
            Vector<Vector<double, dim>, dos> bgVals;
            def->visBoundaryCondition(g0, bgVals, out_normal, pt, d);

            fluxPtr->NumericalFlux(v0, bVals, g0, bgVals, out_normal, dt / grid->AreaOfEle() * grid->LengthOfBry(),
                                   def->eqInfo, flux_quad, false);
            break;
          }

          default:
            break;
          }
          return flux_quad;
        },
        ele_idx, d);

    flux /= -grid->AreaOfEle();

    output->addVectorData(ele_idx, flux);
  }
}

template <int dim, int dos>
void AdvectionSolver<dim, dos>::advectForElement(Vector<size_t, dim> ele_idx, const GridSystemDataPtr<dim, dos> input,
                                                 double dt, ProblemDefPtr<dim, dos> def, AdvectionType type,
                                                 GridSystemDataPtr<dim, dos> output) {
  int neigh;
  size_t result;
  Vector<size_t, dim> nei_ele_idx;
  std::array<double, dim> out{};
  for (int i = 0; i < dim; ++i) {
    if (i == 0) {
      // left
      {
        neigh = ele_idx[i];
        neigh -= 1;
        result = grid->periodicMap(neigh, left);
        nei_ele_idx = ele_idx;
        nei_ele_idx[i] = result;
        out.fill(0.0);
        out[i] = -1;
        fluxOneSide(input, def, out, ele_idx, nei_ele_idx, left, dt, type, output);
      }

      // right
      {
        neigh = ele_idx[i];
        neigh += 1;
        result = grid->periodicMap(neigh, right);
        nei_ele_idx[i] = result;
        out[i] = 1;
        fluxOneSide(input, def, out, ele_idx, nei_ele_idx, right, dt, type, output);
      }
    } else if (i == 1) {
      // bottom
      {
        neigh = ele_idx[i];
        neigh -= 1;
        result = grid->periodicMap(neigh, bottom);
        nei_ele_idx = ele_idx;
        nei_ele_idx[i] = result;
        out.fill(0.0);
        out[i] = -1;
        fluxOneSide(input, def, out, ele_idx, nei_ele_idx, bottom, dt, type, output);
      }

      // up
      {
        neigh = ele_idx[i];
        neigh += 1;
        result = grid->periodicMap(neigh, up);
        nei_ele_idx[i] = result;
        out[i] = 1;
        fluxOneSide(input, def, out, ele_idx, nei_ele_idx, up, dt, type, output);
      }
    } else if (i == 2) {
      // front
      {
        neigh = ele_idx[i];
        neigh -= 1;
        result = grid->periodicMap(neigh, front);
        nei_ele_idx = ele_idx;
        nei_ele_idx[i] = result;
        out.fill(0.0);
        out[i] = -1;
        fluxOneSide(input, def, out, ele_idx, nei_ele_idx, front, dt, type, output);
      }

      // back
      {
        neigh = ele_idx[i];
        neigh += 1;
        result = grid->periodicMap(neigh, back);
        nei_ele_idx[i] = result;
        out[i] = 1;
        fluxOneSide(input, def, out, ele_idx, nei_ele_idx, back, dt, type, output);
      }
    }
  }
}

template <int dim, int dos>
void AdvectionSolver<dim, dos>::advect(const GridSystemDataPtr<dim, dos> input, double dt, ProblemDefPtr<dim, dos> def,
                                       AdvectionType type, GridSystemDataPtr<dim, dos> output) {
  output->clearSystemData();
  if constexpr (dim == 1) {
    parallelFor(kZeroSize, grid->dataSize()[0],
                [&](size_t ele_idx) -> void { advectForElement({ele_idx}, input, dt, def, type, output); });
  } else if constexpr (dim == 2) {
    parallelFor(kZeroSize, grid->dataSize()[0], kZeroSize, grid->dataSize()[1],
                [&](size_t ele_idx, size_t ele_idy) -> void {
                  advectForElement({ele_idx, ele_idy}, input, dt, def, type, output);
                });
  } else if constexpr (dim == 3) {
    parallelFor(kZeroSize, grid->dataSize()[0], kZeroSize, grid->dataSize()[1], kZeroSize, grid->dataSize()[2],
                [&](size_t ele_idx, size_t ele_idy, size_t ele_idz) -> void {
                  advectForElement({ele_idx, ele_idy, ele_idz}, input, dt, def, type, output);
                });
  }
}

#define Implement(dim)                                                                                                 \
  template class AdvectionSolver<dim, 1>;                                                                              \
  template class AdvectionSolver<dim, 2>;                                                                              \
  template class AdvectionSolver<dim, 3>;                                                                              \
  template class AdvectionSolver<dim, 4>;                                                                              \
  template class AdvectionSolver<dim, 5>;                                                                              \
  template class AdvectionSolver<dim, 6>;

Implement(1);
Implement(2);
Implement(3);
#undef Implement

} // namespace vox
