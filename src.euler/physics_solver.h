//
//  physics_solver.hpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef physics_solver_hpp
#define physics_solver_hpp

#include "dual_buffer.h"
#include "global_clock.h"

namespace vox {
template <int dim, int dos> class SolverState;

template <int dim, int dos> class PhysicsSolver final {
public:
  //! Default constructor.
  explicit PhysicsSolver(SolverState<dim, dos> *state);

  //! Destructor.
  ~PhysicsSolver();

  void Run(double t_end);
  void StepRun(int step);

  void initialize();

  void createScene();

  void preProcess();

  void postProcess();

  DualBuffer<dim, dos> *getDualBuffer();

private:
  double dt;
  GlobalClock *mGlobalClock;

  DualBuffer<dim, dos> buffer;

  SolverState<dim, dos> *mSolverState;
};

} // namespace vox

#endif /* physics_solver_hpp */
