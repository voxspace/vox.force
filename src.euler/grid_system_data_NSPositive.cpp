//
//  grid_system_data_NSPositive.cpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "grid_system_data_NSPositive.h"
#include "quads_auxiliary.h"

namespace vox {

template <int dim, int order>
GridSysDataNSPositive<dim, order>::GridSysDataNSPositive(MeshPtr<dim> grid) : GridSystemData<dim, dos>(grid) {
  updateQuadInfo();
}

template <int dim, int order>
GridSysDataNSPositive<dim, order>::GridSysDataNSPositive(const GridSysDataNSPositive<dim, order> &other)
    : GridSystemData<dim, dos>(other), eqInfo(other.eqInfo) {
  PositiveParas = other.PositiveParas;
}

template <int dim, int order>
GridSysDataNSPositive<dim, order> &
GridSysDataNSPositive<dim, order>::operator=(const GridSysDataNSPositive<dim, order> &other) {
  GridSystemData<dim, dos>::operator=(other);

  PositiveParas = other.PositiveParas;

  eqInfo = other.eqInfo;

  return *this;
}

template <int dim, int order> GridSysDataNSPositive<dim, order>::~GridSysDataNSPositive() = default;

template <int dim, int order>
std::shared_ptr<GridSystemData<dim, EquationNS<dim>::DOS>> GridSysDataNSPositive<dim, order>::deepCopy() {
  return std::shared_ptr<GridSysDataNSPositive<dim, order>>(new GridSysDataNSPositive<dim, order>(*this),
                                                            [](GridSysDataNSPositive<dim, order> *obj) { delete obj; });
}

// MARK:- Getter
template <int dim, int order>
Vector<double, EquationNS<dim>::DOS> GridSysDataNSPositive<dim, order>::value(const point_t &pt,
                                                                              Vector<size_t, dim> var_idx) {
  // make positivity
  Vector<double, dos> tmp = GridSystemData<dim, dos>::value(pt, var_idx);
  Vector<double, dos> tmp_avg = GridSystemData<dim, dos>::value(var_idx);

  tmp[0] = PositiveParas(var_idx).theta0 * (tmp[0] - tmp_avg[0]) + tmp_avg[0];
  tmp = PositiveParas(var_idx).theta1 * (tmp - tmp_avg) + tmp_avg;

  return tmp;
}

template <int dim, int order>
Vector<Vector<double, dim>, EquationNS<dim>::DOS>
GridSysDataNSPositive<dim, order>::gradient(const point_t &pt, Vector<size_t, dim> var_idx) {
  Vector<Vector<double, dim>, dos> tmp = GridSystemData<dim, dos>::gradient(pt, var_idx);

  tmp[0] *= PositiveParas(var_idx).theta0;
  for (size_t i = 0; i < dos; ++i) {
    tmp[i] *= PositiveParas(var_idx).theta1;
  }
  return tmp;
}

// MARK:- Positivity modifier
template <int dim, int order> void GridSysDataNSPositive<dim, order>::calculateBasisFunction() {
  GridSystemData<dim, dos>::calculateBasisFunction();
  PositivityPreserving();
}

template <int dim, int order> void GridSysDataNSPositive<dim, order>::PositivityPreserving() {
  const double eps = 1.0e-13;
  if constexpr (dim == 1) {
    parallelFor(kZeroSize, _grid->dataSize()[0], [&](size_t ele_idx) -> void {
      Vector<double, dos> avg = GridSystemData<dim, dos>::value({ele_idx});
      PositivityRho({ele_idx}, eps, avg[0]);
      PositivityPressure({ele_idx}, eps);
    });
  } else if constexpr (dim == 2) {
    parallelFor(kZeroSize, _grid->dataSize()[0], kZeroSize, _grid->dataSize()[1],
                [&](size_t ele_idx, size_t ele_idy) -> void {
                  Vector<double, dos> avg = GridSystemData<dim, dos>::value({ele_idx, ele_idy});
                  PositivityRho({ele_idx, ele_idy}, eps, avg[0]);
                  PositivityPressure({ele_idx, ele_idy}, eps);
                });
  } else if constexpr (dim == 3) {
    parallelFor(kZeroSize, _grid->dataSize()[0], kZeroSize, _grid->dataSize()[1], kZeroSize, _grid->dataSize()[2],
                [&](size_t ele_idx, size_t ele_idy, size_t ele_idz) -> void {
                  Vector<double, dos> avg = GridSystemData<dim, dos>::value({ele_idx, ele_idy, ele_idz});
                  PositivityRho({ele_idx, ele_idy, ele_idz}, eps, avg[0]);
                  PositivityPressure({ele_idx, ele_idy, ele_idz}, eps);
                });
  }
}

template <int dim, int order>
void GridSysDataNSPositive<dim, order>::PositivityRho(Vector<size_t, dim> var_idx, double eps, double rho) {
  double rho_min = GridSystemData<dim, dos>::value(PositiveParas(var_idx).quad_pts[0], var_idx)[0];
  for (size_t i = 0; i < PositiveParas(var_idx).quad_pts.size(); ++i) {
    rho_min = std::min(rho_min, GridSystemData<dim, dos>::value(PositiveParas(var_idx).quad_pts[i], var_idx)[0]);
  }
  PositiveParas(var_idx).theta0 = std::min((rho - eps) / (rho - rho_min), 1.0);
}

template <int dim, int order>
void GridSysDataNSPositive<dim, order>::PositivityPressure(Vector<size_t, dim> var_idx, double eps) {
  double p_avgs = eqInfo->pressure(GridSystemData<dim, dos>::value(var_idx));

  PositiveParas(var_idx).theta1 = 1.0;
  for (size_t i = 0; i < PositiveParas(var_idx).quad_pts.size(); ++i) {
    double p = Pressure(PositiveParas(var_idx).quad_pts[i], var_idx);
    if (p < eps) {
      double t0 = std::abs((p_avgs - eps) / (p_avgs - p));
      PositiveParas(var_idx).theta1 = std::min(PositiveParas(var_idx).theta1, t0);
    }
  }
}

template <int dim, int order>
double GridSysDataNSPositive<dim, order>::Pressure(const point_t &pt, Vector<size_t, dim> var_idx) {
  Vector<double, dos> vals = GridSystemData<dim, dos>::value(pt, var_idx);
  Vector<double, dos> val_avgs = GridSystemData<dim, dos>::value(var_idx);
  vals[0] = PositiveParas(var_idx).theta0 * (vals[0] - val_avgs[0]) + val_avgs[0];

  // can't use pressure in EquationNS which will do correction
  double vel = 0.0;
  for (int i = 1; i <= dim; i++) {
    vel += vals[i] * vals[i];
  }

  return (eqInfo->Gamma - 1) * (vals[dim + 1] - 0.5 * vel / vals[0]);
}

template <int dim, int order> void GridSysDataNSPositive<dim, order>::updateQuadInfo() {
  PositiveParas.resize(_grid->dataSize());
  if constexpr (dim == 1) {
    for (size_t i = 0; i < PositiveParas.size()[0]; ++i) {
      QuadsAuxiliary<dim, order>::getEleQuadPoint(_grid, {i}, PositiveParas({i}).quad_pts);
    }
  } else if constexpr (dim == 2) {
    for (size_t i = 0; i < PositiveParas.size()[0]; ++i) {
      for (size_t j = 0; j < PositiveParas.size()[1]; ++j) {
        QuadsAuxiliary<dim, order>::getEleQuadPoint(_grid, {i, j}, PositiveParas({i, j}).quad_pts);
      }
    }
  } else if constexpr (dim == 3) {
    for (size_t i = 0; i < PositiveParas.size()[0]; ++i) {
      for (size_t j = 0; j < PositiveParas.size()[1]; ++j) {
        for (size_t k = 0; k < PositiveParas.size()[2]; ++k) {
          QuadsAuxiliary<dim, order>::getEleQuadPoint(_grid, {i, j, k}, PositiveParas({i, j, k}).quad_pts);
        }
      }
    }
  }
}

template <int dim, int order> void GridSysDataNSPositive<dim, order>::setEquation(EquationNSPtr<dim> eq) {
  eqInfo = eq;
}

// MARK:- Builder
template <int dim, int order>
GridSysDataNSPositivePtr<dim, order> GridSysDataNSPositive<dim, order>::Builder::build(MeshPtr<dim> grid,
                                                                                       EquationNSPtr<dim> eq_info) {
  GridSysDataNSPositivePtr<dim, order> dataSys = std::make_shared<GridSysDataNSPositive<dim, order>>(grid);

  dataSys->setScalarBuilder(std::make_shared<typename GridData<dim>::Builder>());
  for (size_t i = 0; i < dos; i++) {
    dataSys->loadScalarData(std::make_shared<typename GridData<dim>::Builder>());
  }

  dataSys->setEquation(eq_info);

  return dataSys;
}

template <int dim, int order>
GridSysDataNSPositivePtr<dim, order> GridSysDataNSPositive<dim, order>::Builder::build(GridDataBuilderPtr<dim> builder,
                                                                                       MeshPtr<dim> grid,
                                                                                       EquationNSPtr<dim> eq_info) {
  GridSysDataNSPositivePtr<dim, order> dataSys = std::make_shared<GridSysDataNSPositive<dim, order>>(grid);

  dataSys->setScalarBuilder(builder);
  for (size_t i = 0; i < dos; i++) {
    dataSys->loadScalarData(builder);
  }

  dataSys->setEquation(eq_info);

  return dataSys;
}

template <int dim, int order>
typename GridSysDataNSPositive<dim, order>::Builder GridSysDataNSPositive<dim, order>::builder() {
  return Builder();
}

template class GridSysDataNSPositive<1, 1>;
template class GridSysDataNSPositive<1, 2>;

template class GridSysDataNSPositive<2, 1>;
template class GridSysDataNSPositive<2, 2>;

template class GridSysDataNSPositive<3, 1>;
template class GridSysDataNSPositive<3, 2>;
} // namespace vox
