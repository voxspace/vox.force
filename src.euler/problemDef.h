//
//  problemDef.hpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef problemDef_hpp
#define problemDef_hpp

#include <utility>

#include "equation_prototype.h"
#include "mesh.h"

namespace vox {

template <int dim, int dos> class ProblemDef final {
public:
  using bcFunc =
      std::function<void(const Vector<double, dos> &val, Vector<double, dos> &result,
                         const std::array<double, dim> &outNormal, const typename Mesh<dim>::point_t &pt, Direction flag)>;

  using viscousbcFunc =
      std::function<void(const Vector<Vector<double, dim>, dos> &val, Vector<Vector<double, dim>, dos> &result,
                         const std::array<double, dim> &outNormal, const typename Mesh<dim>::point_t &pt, Direction flag)>;

  using initFunc = std::function<void(const typename Mesh<dim>::point_t &pt, Vector<double, dos> &value)>;

public:
  ProblemDef(ConvEquationPtr<dim, dos> e, MeshPtr<dim> g, bcFunc bd, viscousbcFunc vbd, initFunc init, int order,
             std::string id)
      : solverOrder(order), grid(g), eqInfo(e), boundaryCondition(bd), visBoundaryCondition(vbd),
        initialCondition(init), identity(std::move(id)) {}

public:
  const int solverOrder;

  const MeshPtr<dim> grid;
  const ConvEquationPtr<dim, dos> eqInfo;

  const bcFunc boundaryCondition;
  const viscousbcFunc visBoundaryCondition;
  const initFunc initialCondition;

  const std::string identity;
};

template <int dim, int dos> using ProblemDefPtr = std::shared_ptr<ProblemDef<dim, dos>>;

} // namespace vox
#endif /* problemDef_hpp */
