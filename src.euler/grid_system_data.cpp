//
//  grid_system_data.cpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "grid_system_data.h"
#include "global_clock.h"
#include <fstream>
#include <iomanip>

namespace vox {
// MARK:- Constructor & Destructor
template <int dim, int dos> GridSystemData<dim, dos>::GridSystemData(MeshPtr<dim> grid) : _grid(grid) {}

template <int dim, int dos>
GridSystemData<dim, dos>::GridSystemData(const GridSystemData<dim, dos> &other) : _grid(other._grid) {
  for (int i = 0; i < dos; i++) {
    _scalarDataList[i] = other._scalarDataList[i]->deepCopy();
  }

  scalarBuilder = other.scalarBuilder;
}

template <int dim, int dos>
GridSystemData<dim, dos> &GridSystemData<dim, dos>::operator=(const GridSystemData<dim, dos> &other) {
  JET_ASSERT(other.numberOfScalarData() == this->numberOfScalarData());
  _scalarDataList = other._scalarDataList;

  scalarBuilder = other.scalarBuilder;

  return *this;
}

template <int dim, int dos> GridSystemData<dim, dos>::~GridSystemData() = default;

template <int dim, int dos> std::shared_ptr<GridSystemData<dim, dos>> GridSystemData<dim, dos>::deepCopy() {
  return std::shared_ptr<GridSystemData<dim, dos>>(new GridSystemData<dim, dos>(*this),
                                                   [](GridSystemData<dim, dos> *obj) { delete obj; });
}

// MARK:- Scalar Data Methods
template <int dim, int dos> const GridDataPtr<dim> GridSystemData<dim, dos>::scalarDataAt(size_t idx) const {
  return _scalarDataList[idx];
}

template <int dim, int dos> size_t GridSystemData<dim, dos>::numberOfScalarData() const { return dos; }

template <int dim, int dos> void GridSystemData<dim, dos>::loadScalarData(const GridDataBuilderPtr<dim> &builder) {
  for (int idx = 0; idx < dos; idx++) {
    _scalarDataList[idx] = builder->build(idx, _grid);
  }
}

// MARK:- whole Grid System Data Methods
template <int dim, int dos> void GridSystemData<dim, dos>::addSystemData(const GridSystemData<dim, dos> &data) {
  // check the size
  JET_ASSERT(dos == data.numberOfScalarData());
  for (size_t i = 0; i < dos; i++) {
    add(_scalarDataList[i], data.scalarDataAt(i));
  }
}

template <int dim, int dos> void GridSystemData<dim, dos>::scaleSystemData(double val) {
  for (size_t i = 0; i < dos; i++) {
    scale(_scalarDataList[i], val);
  }
}

template <int dim, int dos> void GridSystemData<dim, dos>::setSystemData(const GridSystemData<dim, dos> &data) {
  for (size_t i = 0; i < dos; i++) {
    _scalarDataList[i]->setGridData(*(data.scalarDataAt(i)));
  }
}

template <int dim, int dos> void GridSystemData<dim, dos>::clearSystemData() {
  for (size_t i = 0; i < dos; i++) {
    _scalarDataList[i]->setData(0.0);
  }
}

template <int dos> void DataSaver<1, dos>::save_data(GridSystemData<1, dos> *data, const std::string &filename) {
  MeshPtr<1> grid = data->getGrid();

  std::ofstream output(filename.c_str());
  for (size_t i = 0; i < data->numberOfVectorData()[0]; i++) {
    const Mesh1D::point_t &bary_center = grid->BaryCenter({i});
    output << bary_center[0] << "\t";
    output << grid->SizeOfEle() << "\t";

    Vector<double, dos> vecData = data->value({i});
    for (size_t t = 0; t < dos; ++t)
      output << vecData[t] << "\t";

    output << std::endl;
  }
}

template <int dos> void DataSaver<2, dos>::save_data(GridSystemData<2, dos> *data, const std::string &filename) {
  MeshPtr<2> grid = data->getGrid();
  Vector<size_t, 2> size = data->numberOfVectorData();

  std::ofstream output(filename.c_str());
  for (size_t i = 0; i < size[0]; i++) {
    for (size_t j = 0; j < size[1]; ++j) {
      const Mesh2D::point_t &bary_center = grid->BaryCenter({i, j});
      output << bary_center[0] << "\t";
      output << bary_center[1] << "\t";
      output << grid->SizeOfEle() << "\t";

      Vector<double, dos> vecData = data->value({i, j});
      for (size_t t = 0; t < dos; ++t)
        output << vecData[t] << "\t";

      output << std::endl;
    }
  }
}

template <int dos> void DataSaver<3, dos>::save_data(GridSystemData<3, dos> *data, const std::string &filename) {
  MeshPtr<3> grid = data->getGrid();
  Vector3UZ size = data->numberOfVectorData();

  std::ofstream output(filename.c_str());
  for (size_t i = 0; i < size[0]; i++) {
    for (size_t j = 0; j < size[1]; ++j) {
      for (size_t k = 0; k < size[2]; ++k) {
        const Mesh3D::point_t &bary_center = grid->BaryCenter({i, j, k});
        output << bary_center[0] << "\t";
        output << bary_center[1] << "\t";
        output << bary_center[2] << "\t";
        output << grid->SizeOfEle() << "\t";

        Vector<double, dos> vecData = data->value({i, j, k});
        for (size_t t = 0; t < dos; ++t)
          output << vecData[t] << "\t";

        output << std::endl;
      }
    }
  }
}

template <int dim, int dos> void GridSystemData<dim, dos>::save_data(const std::string &filename) {
  DataSaver<dim, dos> saver;
  saver.save_data(this, filename);
}

template <int dim, int dos> void GridSystemData<dim, dos>::save_cache(const std::string &header) {
  std::string filename = header + "_Aux.dat";
  std::ofstream output(filename.c_str());
  output << std::setprecision(20) << GlobalClock::getSingleton().getTime() << std::endl;

  for (size_t i = 0; i < numberOfScalarData(); ++i) {
    _scalarDataList[i]->save_cache(header + "_" + std::to_string(i));
  }
}

template <int dim, int dos> void GridSystemData<dim, dos>::load_cache(const std::string &header) {
  std::string filename = header + "_Aux.dat";
  std::ifstream input(filename.c_str());
  double begin_t;
  input >> begin_t;
  GlobalClock::getSingleton().start(begin_t);

  for (size_t i = 0; i < numberOfScalarData(); ++i) {
    _scalarDataList[i]->load_cache(header + "_" + std::to_string(i));
  }
}

template <int dim, int dos> void GridSystemData<dim, dos>::calculateBasisFunction() {
  for (size_t i = 0; i < dos; ++i) {
    _scalarDataList[i]->calculateBasisFunction();
  }
}

// MARK:- Vector Data Methods
template <int dim, int dos> Vector<size_t, dim> GridSystemData<dim, dos>::numberOfVectorData() const {
  JET_ASSERT(dos != 0);
  return _scalarDataList[0]->numberOfData();
}

template <int dim, int dos>
void GridSystemData<dim, dos>::setVectorData(Vector<size_t, dim> var_idx, const Vector<double, dos> &data) {
  for (size_t i = 0; i < dos; i++) {
    _scalarDataList[i]->operator()(var_idx) = data[i];
  }
}

template <int dim, int dos>
void GridSystemData<dim, dos>::addVectorData(Vector<size_t, dim> var_idx, const Vector<double, dos> &data) {
  for (size_t i = 0; i < dos; i++) {
    _scalarDataList[i]->operator()(var_idx) += data[i];
  }
}

template <int dim, int dos> Vector<double, dos> GridSystemData<dim, dos>::value(Vector<size_t, dim> var_idx) {
  Vector<double, dos> tmp;
  for (size_t i = 0; i < dos; i++) {
    tmp[i] = _scalarDataList[i]->GridDataBase<dim>::value(var_idx);
  }

  return tmp;
}

template <int dim, int dos>
Vector<double, dos> GridSystemData<dim, dos>::value(const point_t &pt, Vector<size_t, dim> var_idx) {
  Vector<double, dos> tmp;
  for (size_t i = 0; i < dos; i++) {
    tmp[i] = _scalarDataList[i]->value(pt, var_idx);
  }

  return tmp;
}

template <int dim, int dos>
std::vector<Vector<double, dos>> GridSystemData<dim, dos>::value(const std::vector<point_t> &pt,
                                                                 Vector<size_t, dim> var_idx) {
  std::vector<Vector<double, dos>> tmp(pt.size());
  for (size_t i = 0; i < pt.size(); i++) {
    tmp[i] = value(pt[i], var_idx);
  }

  return tmp;
}

template <int dim, int dos>
Vector<Vector<double, dim>, dos> GridSystemData<dim, dos>::gradient(const point_t &pt, Vector<size_t, dim> var_idx) {
  Vector<Vector<double, dim>, dos> tmp;

  for (size_t j = 0; j < dos; j++) {
    std::array<double, dim> g = _scalarDataList[j]->gradient(pt, var_idx);
    for (size_t k = 0; k < dim; ++k) {
      tmp[j][k] = g[k];
    }
  }

  return tmp;
}

template <int dim, int dos>
std::vector<Vector<Vector<double, dim>, dos>> GridSystemData<dim, dos>::gradient(const std::vector<point_t> &pt,
                                                                                 Vector<size_t, dim> var_idx) {
  std::vector<Vector<Vector<double, dim>, dos>> tmp(pt.size());
  for (size_t i = 0; i < pt.size(); i++) {
    tmp[i] = gradient(pt[i], var_idx);
  }

  return tmp;
}

// MARK:- Builder functions
template <int dim, int dos> void GridSystemData<dim, dos>::setScalarBuilder(GridDataBuilderPtr<dim> builder) {
  scalarBuilder = builder;
}

template <int dim, int dos> GridSystemDataPtr<dim, dos> GridSystemData<dim, dos>::Builder::build(MeshPtr<dim> grid) {
  GridSystemDataPtr<dim, dos> dataSys = std::make_shared<GridSystemData<dim, dos>>(grid);

  dataSys->setScalarBuilder(std::make_shared<typename GridData<dim>::Builder>());
  for (size_t i = 0; i < dos; i++) {
    dataSys->loadScalarData(std::make_shared<typename GridData<dim>::Builder>());
  }

  return dataSys;
}

template <int dim, int dos>
GridSystemDataPtr<dim, dos> GridSystemData<dim, dos>::Builder::build(GridDataBuilderPtr<dim> builder,
                                                                     MeshPtr<dim> grid) {
  GridSystemDataPtr<dim, dos> dataSys = std::make_shared<GridSystemData<dim, dos>>(grid);

  dataSys->setScalarBuilder(builder);
  for (size_t i = 0; i < dos; i++) {
    dataSys->loadScalarData(builder);
  }

  return dataSys;
}

template <int dim, int dos> typename GridSystemData<dim, dos>::Builder GridSystemData<dim, dos>::builder() {
  return Builder();
}

#define Implement(dim)                                                                                                 \
  template class GridSystemData<dim, 1>;                                                                               \
  template class GridSystemData<dim, 2>;                                                                               \
  template class GridSystemData<dim, 3>;                                                                               \
  template class GridSystemData<dim, 4>;                                                                               \
  template class GridSystemData<dim, 5>;                                                                               \
  template class GridSystemData<dim, 6>;                                                                               \
  template class DataSaver<dim, 1>;                                                                                    \
  template class DataSaver<dim, 2>;                                                                                    \
  template class DataSaver<dim, 3>;                                                                                    \
  template class DataSaver<dim, 4>;                                                                                    \
  template class DataSaver<dim, 5>;                                                                                    \
  template class DataSaver<dim, 6>;

Implement(1);
Implement(2);
Implement(3);
#undef Implement
} // namespace vox
