//
//  weno_auxiliary.cpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "weno_auxiliary.h"
#include "../src.common/parallel.h"
#include <execution>
#include <glog/logging.h>

namespace vox {

template <int dim, int order>
WENOAuxiliary<dim, order>::WENOAuxiliary(MeshPtr<dim> g) : grid(g), polyInfo(g), searcher(grid) {
  element_helper.resize(grid->dataSize());
}

template <int dim, int order>
void WENOAuxiliary<dim, order>::buildReconstructPatch(typename PatchSearcher<dim, order>::SearchType type) {
  if constexpr (dim == 1) {
    parallelFor(kZeroSize, grid->dataSize()[0], [&](size_t ele_idx) -> void {
      element_helper[ele_idx].ElementNeighbor.clear();
      searcher.expand({ele_idx}, element_helper[ele_idx].ElementNeighbor, type);

      JET_THROW_INVALID_ARG_WITH_MESSAGE_IF(
          (element_helper[ele_idx].ElementNeighbor.size() < PolyInfo<dim, order>::n_unknown),
          "The Stencil is not big enough");
    });
  } else if constexpr (dim == 2) {
    parallelFor(kZeroSize, grid->dataSize()[0], kZeroSize, grid->dataSize()[1],
                [&](size_t ele_idx, size_t ele_idy) -> void {
                  element_helper({ele_idx, ele_idy}).ElementNeighbor.clear();
                  searcher.expand({ele_idx, ele_idy}, element_helper({ele_idx, ele_idy}).ElementNeighbor, type);

                  JET_THROW_INVALID_ARG_WITH_MESSAGE_IF(
                      (element_helper({ele_idx, ele_idy}).ElementNeighbor.size() < PolyInfo<dim, order>::n_unknown),
                      "The Stencil is not big enough");
                });
  } else {
    parallelFor(
        kZeroSize, grid->dataSize()[0], kZeroSize, grid->dataSize()[1], kZeroSize, grid->dataSize()[2],
        [&](size_t ele_idx, size_t ele_idy, size_t ele_idz) -> void {
          element_helper({ele_idx, ele_idy, ele_idz}).ElementNeighbor.clear();
          searcher.expand({ele_idx, ele_idy, ele_idz}, element_helper({ele_idx, ele_idy, ele_idz}).ElementNeighbor,
                          type);

          JET_THROW_INVALID_ARG_WITH_MESSAGE_IF(
              (element_helper({ele_idx, ele_idy, ele_idz}).ElementNeighbor.size() < PolyInfo<dim, order>::n_unknown),
              "The Stencil is not big enough");
        });
  }
}

template <int dim, int order> void WENOAuxiliary<dim, order>::buildWENOPatch() {
  for (auto &ele : element_helper) {
    ele.idx_choice = 0;
  }

  // find the number of patch
  for (auto &ele : element_helper) {
    auto &rp = ele.ElementNeighbor;
    for (auto &j : rp) {
      element_helper(j).idx_choice++;
    }
  }
  for (auto &ele : element_helper) {
    ele.WENOPatch.resize(ele.idx_choice);
    ele.idx_choice = 0;
  }

  // store every patch for the element
  for (auto &ele : element_helper) {
    auto &rp = ele.ElementNeighbor;
    for (auto &j : rp) {
      auto &wp = element_helper(j).WENOPatch;
      wp[element_helper(j).idx_choice++] = rp;
    }
  }

  constructCache();
  LOG(INFO) << "WENOPatch done...";
}

template <int dim, int order> void WENOAuxiliary<dim, order>::constructCache() {
  // preAlloc
  total_patch = 0;
  size_t sub_total = 0;
  for (const auto &ele : element_helper) {
    total_patch += ele.WENOPatch.size();
    for (const auto &patch : ele.WENOPatch) {
      sub_total += patch.size();
    }
  }
  patch_prefix_sum.reserve(grid->dataSize().length());
  G_inv.resize(total_patch);

  sub_patch_prefix_sum.reserve(total_patch);
  weno_patch.reserve(sub_total);
  patch_polys.resize(sub_total);

  // build memory
  for (const auto &ele : element_helper) {
    patch_prefix_sum.push_back(ele.WENOPatch.size());
    for (const auto &patch : ele.WENOPatch) {
      sub_patch_prefix_sum.push_back(patch.size());
      for (const auto &p : patch) {
        weno_patch.push_back(p);
      }
    }
  }

#if defined(__APPLE__)
  std::inclusive_scan(patch_prefix_sum.begin(), patch_prefix_sum.end(), patch_prefix_sum.begin());
  std::inclusive_scan(sub_patch_prefix_sum.begin(), sub_patch_prefix_sum.end(), sub_patch_prefix_sum.begin());
#else
  std::inclusive_scan(std::execution::par, patch_prefix_sum.begin(), patch_prefix_sum.end(), patch_prefix_sum.begin());
  std::inclusive_scan(std::execution::par, sub_patch_prefix_sum.begin(), sub_patch_prefix_sum.end(),
                      sub_patch_prefix_sum.begin());
#endif
}

template <int dim, int order> void WENOAuxiliary<dim, order>::updateWENOInfo() {
  LOG(INFO) << "updateWENOInfo() begins...";

  if constexpr (dim == 1) {
    parallelFor(kZeroSize, grid->dataSize()[0], [&](size_t ele_idx) -> void {
      // loop for different patches
      for (size_t j = 0; j < n_WENOPatch(ele_idx); ++j) {
        polyInfo.updateLSMatrix({ele_idx}, getPatch(ele_idx, j), getPolyAvgs(ele_idx, j), *getGInv(ele_idx, j));
        getGInv(ele_idx, j)->invert();
      }
    });
  } else if constexpr (dim == 2) {
    parallelFor(
        kZeroSize, grid->dataSize()[0], kZeroSize, grid->dataSize()[1], [&](size_t ele_idx, size_t ele_idy) -> void {
          // loop for different patches
          size_t index = element_helper.index({ele_idx, ele_idy});
          for (size_t j = 0; j < n_WENOPatch(index); ++j) {
            polyInfo.updateLSMatrix({ele_idx, ele_idy}, getPatch(index, j), getPolyAvgs(index, j), *getGInv(index, j));
            getGInv(index, j)->invert();
          }
        });
  } else if constexpr (dim == 3) {
    parallelFor(kZeroSize, grid->dataSize()[0], kZeroSize, grid->dataSize()[1], kZeroSize, grid->dataSize()[2],
                [&](size_t ele_idx, size_t ele_idy, size_t ele_idz) -> void {
                  // loop for different patches
                  size_t index = element_helper.index({ele_idx, ele_idy, ele_idz});
                  for (size_t j = 0; j < n_WENOPatch(index); ++j) {
                    polyInfo.updateLSMatrix({ele_idx, ele_idy, ele_idz}, getPatch(index, j), getPolyAvgs(index, j),
                                            *getGInv(index, j));
                    getGInv(index, j)->invert();
                  }
                });
  }

  LOG(INFO) << "updateWENOInfo() is done...";
}

//----------------------------------------------------------------------------------------------------------------------
template <int dim, int order> size_t WENOAuxiliary<dim, order>::n_WENOPatch(size_t ele_idx) {
  if (ele_idx == 0) {
    return patch_prefix_sum[0];
  } else {
    return patch_prefix_sum[ele_idx] - patch_prefix_sum[ele_idx - 1];
  }
}

template <int dim, int order> const size_t WENOAuxiliary<dim, order>::n_WENOPatch(size_t ele_idx) const {
  if (ele_idx == 0) {
    return patch_prefix_sum[0];
  } else {
    return patch_prefix_sum[ele_idx] - patch_prefix_sum[ele_idx - 1];
  }
}

template <int dim, int order> size_t WENOAuxiliary<dim, order>::n_OnePatch(size_t ele_idx, size_t j) {
  if (ele_idx == 0 && j == 0) {
    return sub_patch_prefix_sum[0];
  } else if (ele_idx == 0 && j != 0) {
    return sub_patch_prefix_sum[j] - sub_patch_prefix_sum[j - 1];
  } else {
    return sub_patch_prefix_sum[patch_prefix_sum[ele_idx - 1] + j] -
           sub_patch_prefix_sum[patch_prefix_sum[ele_idx - 1] + j - 1];
  }
}

template <int dim, int order>
ArrayView1<Vector<size_t, dim>> WENOAuxiliary<dim, order>::getPatch(size_t ele_idx, size_t j) {
  if (ele_idx == 0 && j == 0) {
    return {weno_patch.data(), sub_patch_prefix_sum[0]};
  } else if (ele_idx == 0 && j != 0) {
    return {weno_patch.data() + sub_patch_prefix_sum[j - 1], sub_patch_prefix_sum[j] - sub_patch_prefix_sum[j - 1]};
  } else {
    return {weno_patch.data() + sub_patch_prefix_sum[patch_prefix_sum[ele_idx - 1] + j - 1],
            sub_patch_prefix_sum[patch_prefix_sum[ele_idx - 1] + j] -
                sub_patch_prefix_sum[patch_prefix_sum[ele_idx - 1] + j - 1]};
  }
}

template <int dim, int order>
const Vector<size_t, dim> *WENOAuxiliary<dim, order>::getPatch(size_t ele_idx, size_t j, size_t l) const {
  if (ele_idx == 0 && j == 0) {
    return weno_patch.data() + l;
  } else if (ele_idx == 0 && j != 0) {
    return weno_patch.data() + sub_patch_prefix_sum[j - 1] + l;
  } else {
    return weno_patch.data() + sub_patch_prefix_sum[patch_prefix_sum[ele_idx - 1] + j - 1] + l;
  }
}

template <int dim, int order>
ArrayView1<std::array<double, PolyInfo<dim, order>::n_unknown>> WENOAuxiliary<dim, order>::getPolyAvgs(size_t ele_idx,
                                                                                                       size_t j) {
  if (ele_idx == 0 && j == 0) {
    return {patch_polys.data(), sub_patch_prefix_sum[0]};
  } else if (ele_idx == 0 && j != 0) {
    return {patch_polys.data() + sub_patch_prefix_sum[j - 1], sub_patch_prefix_sum[j] - sub_patch_prefix_sum[j - 1]};
  } else {
    return {patch_polys.data() + sub_patch_prefix_sum[patch_prefix_sum[ele_idx - 1] + j - 1],
            sub_patch_prefix_sum[patch_prefix_sum[ele_idx - 1] + j] -
                sub_patch_prefix_sum[patch_prefix_sum[ele_idx - 1] + j - 1]};
  }
}

template <int dim, int order>
const std::array<double, PolyInfo<dim, order>::n_unknown> *
WENOAuxiliary<dim, order>::getPolyAvgs(size_t ele_idx, size_t j, size_t l) const {
  if (ele_idx == 0 && j == 0) {
    return patch_polys.data() + l;
  } else if (ele_idx == 0 && j != 0) {
    return patch_polys.data() + sub_patch_prefix_sum[j - 1] + l;
  } else {
    return patch_polys.data() + sub_patch_prefix_sum[patch_prefix_sum[ele_idx - 1] + j - 1] + l;
  }
}

template <int dim, int order>
ArrayView1<typename PolyInfo<dim, order>::Mat> WENOAuxiliary<dim, order>::getGInv(size_t ele_idx) {
  if (ele_idx == 0) {
    return {G_inv.data(), patch_prefix_sum[0]};
  } else {
    return {G_inv.data() + patch_prefix_sum[ele_idx - 1], patch_prefix_sum[ele_idx] - patch_prefix_sum[ele_idx - 1]};
  }
}

template <int dim, int order>
typename PolyInfo<dim, order>::Mat *WENOAuxiliary<dim, order>::getGInv(size_t ele_idx, size_t j) {
  if (ele_idx == 0) {
    return G_inv.data() + j;
  } else {
    return G_inv.data() + patch_prefix_sum[ele_idx - 1] + j;
  }
}

template class WENOAuxiliary<1, 1>;
template class WENOAuxiliary<1, 2>;

template class WENOAuxiliary<2, 1>;
template class WENOAuxiliary<2, 2>;

template class WENOAuxiliary<3, 1>;
template class WENOAuxiliary<3, 2>;

} // namespace vox
