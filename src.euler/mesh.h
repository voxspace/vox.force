//
//  mesh.h
//  src.flame
//
//  Created by 杨丰 on 2020/5/13.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef mesh_h
#define mesh_h

#include <cmath>
#include <iostream>
#include <memory>

#include "surface_integrator.h"
#include "volume_integrator.h"

namespace vox {

template <int dim> class Mesh {
public:
  static constexpr int DIM = dim;
  using point_t = Point<dim>;

public:
  explicit Mesh(const Vector<size_t, dim> &resolution,
                const Vector<double, dim> &gridSpacing = Vector<double, dim>::makeConstant(1.0),
                const Vector<double, dim> &origin = Vector<double, dim>(), double initialValue = 0.0);

  //! Returns the actual data point size.
  Vector<size_t, dim> dataSize() const;

  //! Returns the grid origin.
  const Vector<double, dim> &origin() const;

  //! Returns the grid spacing.
  const Vector<double, dim> &gridSpacing() const;

  //! Returns data position for the grid point at (0, 0, ...).
  //! Note that this is different from origin() since origin() returns
  //! the lower corner point of the bounding box.
  Vector<double, dim> dataOrigin() const;

public:
  double SizeOfEle();

  double AreaOfEle();

  double LengthOfBry() {
    if constexpr (dim == 1) {
      return 1.0;
    } else if constexpr (dim == 2) {
      return std::max(_gridSpacing[0], _gridSpacing[1]);
    } else if constexpr (dim == 3) {
      return std::max(_gridSpacing[0] * _gridSpacing[1],
                      std::max(_gridSpacing[0] * _gridSpacing[2], _gridSpacing[1] * _gridSpacing[2]));
    }
  }

  point_t BaryCenter(Vector<size_t, dim> i);

  point_t BoundaryCenter(Vector<size_t, dim> i, Direction d);

public:
  void addPBDescriptor(Direction d);

  point_t periodicLocalize(Vector<size_t, dim> baseIdx, point_t targetPt);

  size_t periodicMap(int idx, Direction d);

private:
  Vector<size_t, dim> _resolution{};
  Vector<double, dim> _gridSpacing = (Vector<double, dim>::makeConstant(1));
  Vector<double, dim> _origin{};

  std::vector<Direction> periodDescriptors;
  void periodicLocalize(Vector<size_t, dim> baseIdx, size_t desc_idx, point_t &targetPt);
};

// MARK:- Typedef 1,2,3-dim Grids
using Mesh1D = Mesh<1>;
using Mesh2D = Mesh<2>;
using Mesh3D = Mesh<3>;

template <int dim> using MeshPtr = std::shared_ptr<Mesh<dim>>;
using MeshPtr1D = std::shared_ptr<Mesh1D>;
using MeshPtr2D = std::shared_ptr<Mesh2D>;
using MeshPtr3D = std::shared_ptr<Mesh3D>;

} // namespace vox

#endif /* mesh_h */
