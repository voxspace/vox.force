//
//  solver_state.hpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef solver_state_hpp
#define solver_state_hpp

#include <memory>

#include "dual_buffer.h"
#include "physics_solver.h"

namespace vox {

template <int dim, int dos> class SolverState {
public:
  static constexpr int DIM = dim;
  static constexpr int DOS = dos;

  virtual ~SolverState() = default;

  virtual void initialize() {}
  virtual void deinitialize() {}

  virtual void createScene() {}
  virtual void destroyScene() {}

  virtual void update(double &dt) {}

  virtual void timeStep(double &dt) {}

  virtual void preProcess() {}
  virtual void postProcess() {}

public:
  void _notifyPhysicsSystem(PhysicsSolver<dim, dos> *system);

protected:
  PhysicsSolver<dim, dos> *mPhysicsSystem;

  DualBuffer<dim, dos> *buffer;
};

} // namespace vox

#endif /* solver_state_hpp */
