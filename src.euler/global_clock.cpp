//
//  globalClock.cpp
//  DigitalFlex
//
//  Created by 杨丰 on 2019/7/10.
//  Copyright © 2019 杨丰. All rights reserved.
//

#include "global_clock.h"

namespace vox {

template <> GlobalClock *Singleton<GlobalClock>::msSingleton = nullptr;
//-----------------------------------------------------------------------
GlobalClock *GlobalClock::getSingletonPtr() { return msSingleton; }
GlobalClock &GlobalClock::getSingleton() {
  assert(msSingleton);
  return (*msSingleton);
}

GlobalClock::GlobalClock() = default;
GlobalClock::~GlobalClock() = default;

void GlobalClock::start(double m) { t = m; }

double GlobalClock::accumulate(double dt) {
  t += dt;
  return t;
}

double GlobalClock::getTime() const { return t; }

} // namespace vox
