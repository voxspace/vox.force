//
//  patch_searcher.hpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/13.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef patch_searcher_hpp
#define patch_searcher_hpp

#include "mesh.h"

namespace vox {
template <int dim, int order> class PatchSearcher final {
public:
  enum SearchType { Node = 1, Bry = 2 };

  explicit PatchSearcher(MeshPtr<dim> grid);

  void setGrid(MeshPtr<dim> grid);

  std::vector<Vector<int, dim>> bryStencil();

  std::vector<Vector<int, dim>> nodeStencil();

public:
  void expand(Vector<size_t, dim> idx, std::vector<Vector<size_t, dim>> &newPatch, SearchType type = Bry);

private:
  MeshPtr<dim> _grid;
};

} // namespace vox

#endif /* patch_searcher_hpp */
