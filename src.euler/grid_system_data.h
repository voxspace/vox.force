//
//  grid_system_data.hpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef grid_system_data_hpp
#define grid_system_data_hpp

#include <string>
#include <vector>

#include "grid_data.h"

namespace vox {

template <int dim> struct BoundaryConditionDescriptor {
  // for data save
  const size_t bry_idx;
  const int identity;

  // for get u
  const size_t ele_idx;
  const typename Mesh<dim>::point_t pt;

  BoundaryConditionDescriptor(size_t idx, int identity, size_t ele_idx, typename Mesh<dim>::point_t pt)
      : bry_idx(idx), identity(identity), ele_idx(ele_idx), pt(pt) {}
};

template <int dim, int dos> class GridSystemData {
public:
  using point_t = typename Mesh<dim>::point_t;

public:
  //! Constructs empty grid system.
  explicit GridSystemData(MeshPtr<dim> grid);
  //! Copy constructor.
  GridSystemData(const GridSystemData &other);
  //! Copy operator
  GridSystemData &operator=(const GridSystemData &other);

  //! Destructor.
  virtual ~GridSystemData();

  //! Returns the copy of the grid instance.
  virtual std::shared_ptr<GridSystemData> deepCopy();

  class Builder;

  static Builder builder();

public:
  // MARK:- Scalar Data Manipulation
  //! Returns the scalar data at given index.
  const GridDataPtr<dim> scalarDataAt(size_t idx) const;

  //! Returns the number of  scalar data.
  size_t numberOfScalarData() const;

  //!
  //! \brief      Adds a scalar data grid by passing its
  //!     builder and initial value.
  //!
  //! This function adds a new scalar data grid. This layer is not advectable,
  //! meaning that during the computation of fluid flow, this layer won't
  //! follow the flow. For the future access of this layer, its index is
  //! returned.
  //!
  //! \param[in]  builder    The grid builder.
  //!
  //!
  void loadScalarData(const GridDataBuilderPtr<dim> &builder);

public:
  // MARK:- whole Grid System Data Manipulation
  void addSystemData(const GridSystemData &data);

  void scaleSystemData(double val);

  void setSystemData(const GridSystemData &data);

  void clearSystemData();

  //! write data into file
  virtual void save_data(const std::string &filename);

  //! write data into file
  virtual void save_cache(const std::string &header);

  //! load data into program
  virtual void load_cache(const std::string &header);

public:
  virtual void calculateBasisFunction();

public:
  // MARK:- Vector Data Manipulation
  //! number of vector data at given index.
  Vector<size_t, dim> numberOfVectorData() const;

  //! Set the scalar data at given index.
  void setVectorData(Vector<size_t, dim> var_idx, const Vector<double, dos> &data);

  //! Add the scalar data at given index.
  void addVectorData(Vector<size_t, dim> var_idx, const Vector<double, dos> &data);

  //! return value of specific index
  Vector<double, dos> value(Vector<size_t, dim> var_idx);

  //! return value of specific point
  virtual Vector<double, dos> value(const point_t &pt, Vector<size_t, dim> var_idx);

  //! return values of many points
  std::vector<Vector<double, dos>> value(const std::vector<point_t> &pt, Vector<size_t, dim> var_idx);

  //! return value of specific point
  virtual Vector<Vector<double, dim>, dos> gradient(const point_t &pt, Vector<size_t, dim> var_idx);

  //! return values of many points
  std::vector<Vector<Vector<double, dim>, dos>> gradient(const std::vector<point_t> &pt, Vector<size_t, dim> var_idx);

public:
  MeshPtr<dim> getGrid() { return _grid; }

protected:
  const MeshPtr<dim> _grid;

  void setScalarBuilder(GridDataBuilderPtr<dim> builder);
  GridDataBuilderPtr<dim> scalarBuilder;
  std::array<GridDataPtr<dim>, dos> _scalarDataList;
};
template <int dim, int dos> using GridSystemDataPtr = std::shared_ptr<GridSystemData<dim, dos>>;

// MARK:- Grid System Data Builder
template <int dim, int dos> class GridSystemData<dim, dos>::Builder {
public:
  GridSystemDataPtr<dim, dos> build(GridDataBuilderPtr<dim> builder, MeshPtr<dim> grid);

  GridSystemDataPtr<dim, dos> build(MeshPtr<dim> grid);
};
template <int dim, int dos>
using GridSystemDataBuilderPtr = std::shared_ptr<typename GridSystemData<dim, dos>::Builder>;

//----------------------------------------------------------------------------------------------------------------------
template <int dim, int dos> struct DataSaver {};

template <int dos> struct DataSaver<1, dos> {
  void save_data(GridSystemData<1, dos> *data, const std::string &filename);
};

template <int dos> struct DataSaver<2, dos> {
  void save_data(GridSystemData<2, dos> *data, const std::string &filename);
};

template <int dos> struct DataSaver<3, dos> {
  void save_data(GridSystemData<3, dos> *data, const std::string &filename);
};
} // namespace vox
#endif /* grid_system_data_hpp */
