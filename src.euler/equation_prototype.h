//
//  equation_ prototype.h
//  DigitalFlex2
//
//  Created by 杨丰 on 2020/5/16.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef equation__prototype_h
#define equation__prototype_h

#include <cassert>
#include <iostream>
#include <memory>
#include <valarray>
#include <vector>

#include "../src.common/matrix.h"

namespace vox {

// conservation law or convection equation
template <int dim, int dos> class ConvEquationType {
public:
  static constexpr int DIM = dim;
  static constexpr int DOS = dos;

  virtual double MaxCharacteristicSpeed(const Vector<double, DOS> &u) = 0;

  // Fa
  virtual void TFlux(const Vector<double, DOS> &u, const std::array<double, dim> &normal,
                     Vector<double, DOS> &flux) = 0;

public:
  virtual void MaxSpeeds(const Vector<double, DOS> &u, const std::array<double, dim> &normal, double &lambda_l,
                         double &lambda_r) {}

  // Return Fa-Fd or Fd
  virtual void TFlux(const Vector<double, DOS> &u, const Vector<Vector<double, dim>, DOS> &gu,
                     const std::array<double, dim> &normal, Vector<double, DOS> &flux, bool isOnlyViscous) {}
};

template <int dim, int dos> using ConvEquationPtr = std::shared_ptr<ConvEquationType<dim, dos>>;

} // namespace vox
#endif /* equation__prototype_h */
