//
//  grid.cpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/13.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "mesh.h"

namespace vox {
template <int dim>
Mesh<dim>::Mesh(const Vector<size_t, dim> &resolution, const Vector<double, dim> &gridSpacing,
                const Vector<double, dim> &origin, double initialValue)
    : _resolution(resolution), _gridSpacing(gridSpacing), _origin(origin) {}

template <int dim> Vector<size_t, dim> Mesh<dim>::dataSize() const { return _resolution; }

template <int dim> const Vector<double, dim> &Mesh<dim>::origin() const { return _origin; }

template <int dim> const Vector<double, dim> &Mesh<dim>::gridSpacing() const { return _gridSpacing; }

template <int dim> Vector<double, dim> Mesh<dim>::dataOrigin() const { return origin() + 0.5 * gridSpacing(); }

//----------------------------------------------------------------------------------------------------------------------
template <int dim> double Mesh<dim>::SizeOfEle() {
  double m = _gridSpacing[0];
  for (int i = 0; i < dim; ++i) {
    m = std::min(m, _gridSpacing[i]);
  }
  return m;
}

template <int dim> double Mesh<dim>::AreaOfEle() {
  double m = 1.0;
  for (int i = 0; i < dim; ++i) {
    m *= _gridSpacing[i];
  }
  return m;
}

template <int dim> typename Mesh<dim>::point_t Mesh<dim>::BaryCenter(Vector<size_t, dim> i) {
  Vector<double, dim> idx = i.template castTo<double>();
  return dataOrigin() + elemMul(_gridSpacing, idx);
}

template <int dim> typename Mesh<dim>::point_t Mesh<dim>::BoundaryCenter(Vector<size_t, dim> i, Direction d) {
  auto p = BaryCenter(i);
  Vector<double, dim> space = _gridSpacing / 2.0;
  switch (d) {
  case left:
    p[0] -= space[0];
    break;

  case right:
    p[0] += space[0];
    break;

  case up:
    p[1] += space[1];
    break;

  case bottom:
    p[1] -= space[1];
    break;

  case front:
    p[2] -= space[2];
    break;

  case back:
    p[2] += space[2];
    break;
  }
  return p;
}

//----------------------------------------------------------------------------------------------------------------------
template <int dim> void Mesh<dim>::addPBDescriptor(Direction d) {
  if (d == left || d == right) {
    periodDescriptors.push_back(left);
    periodDescriptors.push_back(right);
  } else if (d == up || d == bottom) {
    periodDescriptors.push_back(up);
    periodDescriptors.push_back(bottom);
  } else if (d == front || d == back) {
    periodDescriptors.push_back(front);
    periodDescriptors.push_back(back);
  }
}

template <int dim>
typename Mesh<dim>::point_t Mesh<dim>::periodicLocalize(Vector<size_t, dim> baseIdx, point_t targetPt) {
  for (size_t i = 0; i < periodDescriptors.size(); ++i) {
    periodicLocalize(baseIdx, i, targetPt);
  }
  return targetPt;
}

template <int dim> void Mesh<dim>::periodicLocalize(Vector<size_t, dim> baseIdx, size_t desc_idx, point_t &targetPt) {
  point_t tmp = targetPt;
  double L, center, center_d;
  if (periodDescriptors[desc_idx] == left || periodDescriptors[desc_idx] == right) {
    L = _gridSpacing[0] * _resolution[0];
    center = _gridSpacing[0] * _resolution[0] / 2.0 + _origin[0];
    center_d = center - BaryCenter(baseIdx)[0];
    tmp[0] += center_d;
    if (tmp[0] > _origin[0] + L) {
      targetPt[0] -= L;
    } else if (tmp[0] < _origin[0]) {
      targetPt[0] += L;
    }
  } else if (periodDescriptors[desc_idx] == up || periodDescriptors[desc_idx] == bottom) {
    L = _gridSpacing[1] * _resolution[1];
    center = _gridSpacing[1] * _resolution[1] / 2.0 + _origin[1];
    center_d = center - BaryCenter(baseIdx)[1];
    tmp[1] += center_d;
    if (tmp[1] > _origin[1] + L) {
      targetPt[1] -= L;
    } else if (tmp[1] < _origin[1]) {
      targetPt[1] += L;
    }
  } else {
    L = _gridSpacing[2] * _resolution[2];
    center = _gridSpacing[2] * _resolution[2] / 2.0 + _origin[2];
    center_d = center - BaryCenter(baseIdx)[2];
    tmp[2] += center_d;
    if (tmp[2] > _origin[0] + L) {
      targetPt[2] -= L;
    } else if (tmp[2] < _origin[0]) {
      targetPt[2] += L;
    }
  }
}

template <int dim> size_t Mesh<dim>::periodicMap(int idx, Direction d) {
  //inside the domain
  if (d == left || d == right) {
    if (idx > 0 && idx < _resolution[0]) {
      return idx;
    }
  } else if (d == up || d == bottom) {
    if (idx > 0 && idx < _resolution[1]) {
      return idx;
    }
  } else if (d == back || d == front) {
    if (idx > 0 && idx < _resolution[2]) {
      return idx;
    }
  }

  //treat boundary map
  for (const auto &dir : periodDescriptors) {
    if (d == dir) {
      if (d == left || d == right) {
        return (idx + _resolution[0]) % _resolution[0];
      } else if (d == up || d == bottom) {
        return (idx + _resolution[1]) % _resolution[1];
      } else if (d == back || d == front) {
        return (idx + _resolution[2]) % _resolution[2];
      }
    }
  }
  return static_cast<size_t>(-1);
}

template class Mesh<1>;
template class Mesh<2>;
template class Mesh<3>;
} // namespace vox
