//
//  error_approximator.cpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "error_approximator.h"

namespace vox {

template <int dim, int dos>
void ErrorApproximator<dim, dos>::l1Error(
    const GridSystemDataPtr<dim, dos> data,
    std::array<std::function<double(const typename Mesh<dim>::point_t &pt)>, dos> realFunc,
    ConvEquationPtr<dim, dos> eqInfo, Vector<double, dos> &error, double &dx) {

  dx = std::numeric_limits<double>::max();
  for (size_t i = 0; i < dos; i++) {
    GridDataPtr<dim> scalarData = data->scalarDataAt(i);
    if constexpr (dim == 1) {
      for (size_t j = 0; j < grid->dataSize()[0]; j++) {
        error[i] += integrator.template volumeIntegral<double>(
            [&](const typename Mesh<dim>::point_t &pt) -> double {
              return std::abs(realFunc[i](pt) - scalarData->value(pt, {j}));
            },
            {j});
        dx = std::min(dx, grid->SizeOfEle());
      }
    } else if constexpr (dim == 2) {
      for (size_t j = 0; j < grid->dataSize()[0]; j++) {
        for (size_t k = 0; k < grid->dataSize()[1]; k++) {
          error[i] += integrator.template volumeIntegral<double>(
              [&](const typename Mesh<dim>::point_t &pt) -> double {
                return std::abs(realFunc[i](pt) - scalarData->value(pt, {j, k}));
              },
              {j, k});
          dx = std::min(dx, grid->SizeOfEle());
        }
      }
    } else if constexpr (dim == 3) {
      for (size_t j = 0; j < grid->dataSize()[0]; j++) {
        for (size_t k = 0; k < grid->dataSize()[1]; k++) {
          for (size_t s = 0; s < grid->dataSize()[2]; s++) {
            error[i] += integrator.template volumeIntegral<double>(
                [&](const typename Mesh<dim>::point_t &pt) -> double {
                  return std::abs(realFunc[i](pt) - scalarData->value(pt, {j, k, s}));
                },
                {j, k, s});
            dx = std::min(dx, grid->SizeOfEle());
          }
        }
      }
    }
  }
}

template <int dim, int dos>
void ErrorApproximator<dim, dos>::linftyError(
    const GridSystemDataPtr<dim, dos> data,
    std::array<std::function<double(const typename Mesh<dim>::point_t &pt)>, dos> realFunc,
    ConvEquationPtr<dim, dos> eqInfo, Vector<double, dos> &error, double &dx) {

  dx = std::numeric_limits<double>::max();
  for (size_t i = 0; i < dos; i++) {
    GridDataPtr<dim> scalarData = data->scalarDataAt(i);
    if constexpr (dim == 1) {
      for (size_t j = 0; j < grid->dataSize()[0]; j++) {
        error[i] = std::max(error[i], integrator.volumeCompare(
                                          [&](const typename Mesh<dim>::point_t &pt) -> double {
                                            return std::abs(realFunc[i](pt) - scalarData->value(pt, {j}));
                                          },
                                          {j}));
        dx = std::min(dx, grid->SizeOfEle());
      }
    } else if constexpr (dim == 2) {
      for (size_t j = 0; j < grid->dataSize()[0]; j++) {
        for (size_t k = 0; k < grid->dataSize()[1]; k++) {
          error[i] = std::max(error[i], integrator.volumeCompare(
                                            [&](const typename Mesh<dim>::point_t &pt) -> double {
                                              return std::abs(realFunc[i](pt) - scalarData->value(pt, {j, k}));
                                            },
                                            {j, k}));
          dx = std::min(dx, grid->SizeOfEle());
        }
      }
    } else if constexpr (dim == 3) {
      for (size_t j = 0; j < grid->dataSize()[0]; j++) {
        for (size_t k = 0; k < grid->dataSize()[1]; k++) {
          for (size_t s = 0; s < grid->dataSize()[2]; s++) {
            error[i] = std::max(error[i], integrator.volumeCompare(
                                              [&](const typename Mesh<dim>::point_t &pt) -> double {
                                                return std::abs(realFunc[i](pt) - scalarData->value(pt, {j, k, s}));
                                              },
                                              {j, k, s}));
            dx = std::min(dx, grid->SizeOfEle());
          }
        }
      }
    }
  }
}

#define Implement(dim)                                                                                                 \
  template class ErrorApproximator<dim, 1>;                                                                            \
  template class ErrorApproximator<dim, 2>;                                                                            \
  template class ErrorApproximator<dim, 3>;                                                                            \
  template class ErrorApproximator<dim, 4>;                                                                            \
  template class ErrorApproximator<dim, 5>;                                                                            \
  template class ErrorApproximator<dim, 6>;

Implement(1);
Implement(2);
Implement(3);
#undef Implement

} // namespace vox
