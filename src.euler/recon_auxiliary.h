//
//  recon_auxiliary.hpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef recon_auxiliary_hpp
#define recon_auxiliary_hpp

#include "../src.common/array_view.h"
#include "mesh.h"
#include "patch_searcher.h"
#include "polyInfo1d.h"
#include "polyInfo2d.h"
#include "polyInfo3d.h"

namespace vox {

template <int dim, int order> class ReconAuxiliary {
public:
  static_assert(dim >= 1 && dim <= 3, "Not implemented - dim should be either 1, 2 or 3.");

  explicit ReconAuxiliary(MeshPtr<dim> g);

public:
  // MARK:- Builder
  void updateLSMatrix();

  void buildReconstructPatch(
      typename PatchSearcher<dim, order>::SearchType type = PatchSearcher<dim, order>::SearchType::Bry);

  void clearReconstructPatch();

  void constructCache();

public:
  // MARK:- Getter
  PolyInfo<dim, order> *getPolyInfo() { return &polyInfo; }

  size_t n_Patch(size_t ele_idx);

  ArrayView1<Vector<size_t, dim>> getPatch(size_t ele_idx);
  const Vector<size_t, dim> *getPatch(size_t ele_idx, size_t j) const;

  ArrayView1<std::array<double, PolyInfo<dim, order>::n_unknown>> getPolyAvgs(size_t ele_idx);
  const std::array<double, PolyInfo<dim, order>::n_unknown> *getPolyAvgs(size_t ele_idx, size_t j) const;

  const typename PolyInfo<dim, order>::Mat *getGInv(Vector<size_t, dim> ele_idx) const;

protected:
  // MARK:- Internal Data
  MeshPtr<dim> grid;
  PolyInfo<dim, order> polyInfo;
  PatchSearcher<dim, order> searcher;

  std::vector<size_t> patch_prefix_sum;
  std::vector<Vector<size_t, dim>> patch;
  std::vector<std::array<double, PolyInfo<dim, order>::n_unknown>> patch_polys;
  Array<typename PolyInfo<dim, order>::Mat, dim> G_inv;

  struct ElementHelper {
    std::vector<Vector<size_t, dim>> patch;
  };
  Array<ElementHelper, dim> element_helper;
};

template <int dim, int order> using ReconAuxiliaryPtr = std::shared_ptr<ReconAuxiliary<dim, order>>;

} // namespace vox
#endif /* recon_auxiliary_hpp */
