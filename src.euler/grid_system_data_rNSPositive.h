//
//  grid_system_data_rNSPositive.hpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef grid_system_data_rNSPositive_hpp
#define grid_system_data_rNSPositive_hpp

#include "equations/equation_reactingNS.h"
#include "grid_system_data.h"

namespace vox {

template <int dim, int order> class GridSysDataRNSPositive : public GridSystemData<dim, EquationReactingNS<dim>::DOS> {
public:
  static constexpr int dos = EquationReactingNS<dim>::DOS;
  using point_t = typename Mesh<dim>::point_t;

public:
  //! Constructs empty grid system.
  explicit GridSysDataRNSPositive(MeshPtr<dim> grid);
  //! Copy constructor.
  GridSysDataRNSPositive(const GridSysDataRNSPositive &other);
  //! Copy operator
  GridSysDataRNSPositive &operator=(const GridSysDataRNSPositive &other);

  //! Destructor.
  virtual ~GridSysDataRNSPositive();

  //! Returns the copy of the grid instance.
  std::shared_ptr<GridSystemData<dim, EquationReactingNS<dim>::DOS>> deepCopy() override;

  class Builder;

  static Builder builder();

public:
  //! return value of specific point
  Vector<double, EquationReactingNS<dim>::DOS> value(const point_t &pt, Vector<size_t, dim> var_idx) override;

  //! return value of specific point
  Vector<Vector<double, dim>, EquationReactingNS<dim>::DOS> gradient(const point_t &pt, Vector<size_t, dim> var_idx) override;

public:
  void calculateBasisFunction() override;

private:
  void PositivityPreserving();
  void PositivityRhoY(Vector<size_t, dim> var_idx, double eps, double rho, double Y);
  void PositivityPressure(Vector<size_t, dim> var_idx, double eps);

  void updateQuadInfo();

  double Pressure(const point_t &pt, Vector<size_t, dim> var_idx);

private:
  struct PositivePara {
    double theta_rho;
    double theta_Y;
    double theta_P;

    std::vector<point_t> quad_pts;
  };
  Array<PositivePara, dim> PositiveParas;

  void setEquation(EquationReactingNSPtr<dim> eq);

  EquationReactingNSPtr<dim> eqInfo;

  using GridSystemData<dim, dos>::_grid;
};
template <int dim, int order> using GridSysDataRNSPositivePtr = std::shared_ptr<GridSysDataRNSPositive<dim, order>>;

//----------------------------------------------------------------------------------------------------------------------
template <int dim, int order> class GridSysDataRNSPositive<dim, order>::Builder {
public:
  GridSysDataRNSPositivePtr<dim, order> build(GridDataBuilderPtr<dim> builder, MeshPtr<dim> grid,
                                              EquationReactingNSPtr<dim> eq_info);

  GridSysDataRNSPositivePtr<dim, order> build(MeshPtr<dim> grid, EquationReactingNSPtr<dim> eq_info);
};
template <int dim, int order>
using GridSysDataRNSPositiveBuilderPtr = std::shared_ptr<typename GridSysDataRNSPositive<dim, order>::Builder>;
} // namespace vox
#endif /* grid_system_data_rNSPositive_hpp */
