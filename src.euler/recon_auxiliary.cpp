//
//  recon_auxiliary.cpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "recon_auxiliary.h"
#include "../src.common/parallel.h"
#include <execution>

namespace vox {

template <int dim, int order>
ReconAuxiliary<dim, order>::ReconAuxiliary(MeshPtr<dim> g) : grid(g), polyInfo(g), searcher(g) {
  element_helper.resize(grid->dataSize());
}

template <int dim, int order>
void ReconAuxiliary<dim, order>::buildReconstructPatch(typename PatchSearcher<dim, order>::SearchType type) {
  if constexpr (dim == 1) {
    parallelFor(kZeroSize, grid->dataSize()[0], [&](size_t ele_idx) -> void {
      element_helper({ele_idx}).patch.clear();
      searcher.expand({ele_idx}, element_helper[ele_idx].patch, type);

      JET_THROW_INVALID_ARG_WITH_MESSAGE_IF((element_helper[ele_idx].patch.size() < PolyInfo<dim, order>::n_unknown),
                                            "The Stencil is not big enough");
    });
  } else if constexpr (dim == 2) {
    parallelFor(kZeroSize, grid->dataSize()[0], kZeroSize, grid->dataSize()[1],
                [&](size_t ele_idx, size_t ele_idy) -> void {
                  element_helper({ele_idx, ele_idy}).patch.clear();
                  searcher.expand({ele_idx, ele_idy}, element_helper({ele_idx, ele_idy}).patch, type);

                  JET_THROW_INVALID_ARG_WITH_MESSAGE_IF(
                      (element_helper({ele_idx, ele_idy}).patch.size() < PolyInfo<dim, order>::n_unknown),
                      "The Stencil is not big enough");
                });
  } else if constexpr (dim == 3) {
    parallelFor(kZeroSize, grid->dataSize()[0], kZeroSize, grid->dataSize()[1], kZeroSize, grid->dataSize()[2],
                [&](size_t ele_idx, size_t ele_idy, size_t ele_idz) -> void {
                  element_helper({ele_idx, ele_idy, ele_idz}).patch.clear();
                  searcher.expand({ele_idx, ele_idy, ele_idz}, element_helper({ele_idx, ele_idy, ele_idz}).patch, type);

                  JET_THROW_INVALID_ARG_WITH_MESSAGE_IF(
                      (element_helper({ele_idx, ele_idy, ele_idz}).patch.size() < PolyInfo<dim, order>::n_unknown),
                      "The Stencil is not big enough");
                });
  }

  constructCache();
}

template <int dim, int order> void ReconAuxiliary<dim, order>::constructCache() {
  // preAlloc
  size_t total = 0;
  for (const ElementHelper &ele : element_helper) {
    total += ele.patch.size();
  }

  patch_prefix_sum.reserve(grid->dataSize().length());
  G_inv.resize(grid->dataSize());

  patch.reserve(total);
  patch_polys.resize(total);

  // build memory
  for (const ElementHelper &ele : element_helper) {
    patch_prefix_sum.push_back(ele.patch.size());
    for (const Vector<size_t, dim> &patches : ele.patch) {
      patch.push_back(patches);
    }
  }
#if defined(__APPLE__)
  std::inclusive_scan(patch_prefix_sum.begin(), patch_prefix_sum.end(), patch_prefix_sum.begin());
#else
  std::inclusive_scan(std::execution::par, patch_prefix_sum.begin(), patch_prefix_sum.end(), patch_prefix_sum.begin());
#endif
}

template <int dim, int order> void ReconAuxiliary<dim, order>::clearReconstructPatch() {
  for (auto &p : element_helper) {
    p.patch.clear();
  }
}

template <int dim, int order> void ReconAuxiliary<dim, order>::updateLSMatrix() {
  JET_THROW_INVALID_ARG_WITH_MESSAGE_IF(element_helper[0].patch.empty(), "You must call BuildReconstructPatch first!");

  if constexpr (dim == 1) {
    parallelFor(kZeroSize, grid->dataSize()[0], [&](size_t ele_idx) -> void {
      polyInfo.updateLSMatrix({ele_idx}, getPatch(ele_idx), getPolyAvgs(ele_idx), G_inv[ele_idx]);

      G_inv[ele_idx].invert();
    });
  } else if constexpr (dim == 2) {
    parallelFor(kZeroSize, grid->dataSize()[0], kZeroSize, grid->dataSize()[1],
                [&](size_t ele_idx, size_t ele_idy) -> void {
                  polyInfo.updateLSMatrix({ele_idx, ele_idy}, getPatch(G_inv.index({ele_idx, ele_idy})),
                                          getPolyAvgs(G_inv.index({ele_idx, ele_idy})), G_inv({ele_idx, ele_idy}));

                  G_inv({ele_idx, ele_idy}).invert();
                });
  } else if constexpr (dim == 3) {
    parallelFor(kZeroSize, grid->dataSize()[0], kZeroSize, grid->dataSize()[1], kZeroSize, grid->dataSize()[2],
                [&](size_t ele_idx, size_t ele_idy, size_t ele_idz) -> void {
                  polyInfo.updateLSMatrix(
                      {ele_idx, ele_idy, ele_idz}, getPatch(G_inv.index({ele_idx, ele_idy, ele_idz})),
                      getPolyAvgs(G_inv.index({ele_idx, ele_idy, ele_idz})), G_inv({ele_idx, ele_idy, ele_idz}));

                  G_inv({ele_idx, ele_idy, ele_idz}).invert();
                });
  }
}

//----------------------------------------------------------------------------------------------------------------------
template <int dim, int order> size_t ReconAuxiliary<dim, order>::n_Patch(size_t ele_idx) {
  if (ele_idx == 0) {
    return patch_prefix_sum[0];
  } else {
    return patch_prefix_sum[ele_idx] - patch_prefix_sum[ele_idx - 1];
  }
}

template <int dim, int order> ArrayView1<Vector<size_t, dim>> ReconAuxiliary<dim, order>::getPatch(size_t ele_idx) {
  if (ele_idx == 0) {
    return {patch.data(), patch_prefix_sum[0]};
  } else {
    return {patch.data() + patch_prefix_sum[ele_idx - 1], patch_prefix_sum[ele_idx] - patch_prefix_sum[ele_idx - 1]};
  }
}

template <int dim, int order>
const Vector<size_t, dim> *ReconAuxiliary<dim, order>::getPatch(size_t ele_idx, size_t j) const {
  if (ele_idx == 0) {
    return patch.data() + j;
  } else {
    return patch.data() + patch_prefix_sum[ele_idx - 1] + j;
  }
}

template <int dim, int order>
ArrayView1<std::array<double, PolyInfo<dim, order>::n_unknown>>
ReconAuxiliary<dim, order>::getPolyAvgs(size_t ele_idx) {
  if (ele_idx == 0) {
    return {patch_polys.data(), patch_prefix_sum[0]};
  } else {
    return {patch_polys.data() + patch_prefix_sum[ele_idx - 1],
            patch_prefix_sum[ele_idx] - patch_prefix_sum[ele_idx - 1]};
  }
}

template <int dim, int order>
const std::array<double, PolyInfo<dim, order>::n_unknown> *ReconAuxiliary<dim, order>::getPolyAvgs(size_t ele_idx,
                                                                                                   size_t j) const {
  if (ele_idx == 0) {
    return patch_polys.data() + j;
  } else {
    return patch_polys.data() + patch_prefix_sum[ele_idx - 1] + j;
  }
}

template <int dim, int order>
const typename PolyInfo<dim, order>::Mat *ReconAuxiliary<dim, order>::getGInv(Vector<size_t, dim> ele_idx) const {
  return &G_inv(ele_idx);
}

template class ReconAuxiliary<1, 1>;
template class ReconAuxiliary<1, 2>;
template class ReconAuxiliary<1, 3>;

template class ReconAuxiliary<2, 1>;
template class ReconAuxiliary<2, 2>;
template class ReconAuxiliary<2, 3>;

template class ReconAuxiliary<3, 1>;
template class ReconAuxiliary<3, 2>;
template class ReconAuxiliary<3, 3>;

} // namespace vox
