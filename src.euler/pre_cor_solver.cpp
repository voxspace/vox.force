//
//  pre_cor_solver.cpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "pre_cor_solver.h"

namespace vox {

template <int dim, int dos>
void Prediction_CorrectionSolver<dim, dos>::precor(
    GridSystemDataPtr<dim, dos> init, double dt,
    std::function<void(const Vector<double, dos> &in, Vector<double, dos> &out)> sourceFunc,
    std::function<double(const Vector<double, dos> &data)> timeStep,
    std::function<double(const Vector<double, dos> &left, const Vector<double, dos> &right)> ConvergenceNorm,
    GridSystemDataPtr<dim, dos> output) {
  Vector<double, dos> sub_init;
  Vector<double, dos> sub_out;
  if constexpr (dim == 1) {
    parallelFor(kZeroSize, init->numberOfVectorData()[0], [&](size_t ele_idx) -> void {
      sub_init = init->value({ele_idx});
      precorForElement(sub_init, dt, sourceFunc, timeStep, ConvergenceNorm, sub_out);
      output->setVectorData({ele_idx}, sub_out);
    });
  } else if constexpr (dim == 2) {
    parallelFor(kZeroSize, init->numberOfVectorData()[0], kZeroSize, init->numberOfVectorData()[1],
                [&](size_t ele_idx, size_t ele_idy) -> void {
                  sub_init = init->value({ele_idx, ele_idy});
                  precorForElement(sub_init, dt, sourceFunc, timeStep, ConvergenceNorm, sub_out);
                  output->setVectorData({ele_idx, ele_idy}, sub_out);
                });
  } else if constexpr (dim == 3) {
    parallelFor(kZeroSize, init->numberOfVectorData()[0], kZeroSize, init->numberOfVectorData()[1], kZeroSize,
                init->numberOfVectorData()[2], [&](size_t ele_idx, size_t ele_idy, size_t ele_idz) -> void {
                  sub_init = init->value({ele_idx, ele_idy, ele_idz});
                  precorForElement(sub_init, dt, sourceFunc, timeStep, ConvergenceNorm, sub_out);
                  output->setVectorData({ele_idx, ele_idy, ele_idz}, sub_out);
                });
  }
}

template <int dim, int dos>
void Prediction_CorrectionSolver<dim, dos>::precorForElement(
    Vector<double, dos> init, double dt,
    std::function<void(const Vector<double, dos> &in, Vector<double, dos> &out)> sourceFunc,
    std::function<double(const Vector<double, dos> &data)> timeStep,
    std::function<double(const Vector<double, dos> &left, const Vector<double, dos> &right)> ConvergenceNorm,
    Vector<double, dos> &output) {
  /// implicit Trapezoidal rule. Using explicit Euler for prediction, and use Trapezoidal Rule to correction.
  double t = 0.;
  double ddt = 0.;

  Vector<double, dos> step_init;
  Vector<double, dos> source_new = init;
  Vector<double, dos> source_old = init;
  do {
    step_init = source_new;
    sourceFunc(step_init, source_old);
    ddt = timeStep(step_init);

    if (t + ddt > dt) {
      ddt = dt - t;
    }

    Vector<double, dos> correctionInit = source_old;
    correctionInit *= ddt;
    correctionInit += step_init;

    // int total_correction_steps = 5;
    // int the_correction_step = 0;
    do {
      sourceFunc(correctionInit, source_new);

      source_new += source_old;
      source_new *= 0.5 * ddt;
      source_new += step_init;

      correctionInit = source_new;
    } while (ConvergenceNorm(correctionInit, source_new) > 1.0e-08);
    // } while (the_correction_step ++ < total_correction_steps);
    t += ddt;
  } while (fabs(t - dt) > 1.0e-12);

  output = source_new;
}

#define Implement(dim)                                                                                                 \
  template class Prediction_CorrectionSolver<dim, 1>;                                                                  \
  template class Prediction_CorrectionSolver<dim, 2>;                                                                  \
  template class Prediction_CorrectionSolver<dim, 3>;                                                                  \
  template class Prediction_CorrectionSolver<dim, 4>;                                                                  \
  template class Prediction_CorrectionSolver<dim, 5>;                                                                  \
  template class Prediction_CorrectionSolver<dim, 6>;

Implement(1);
Implement(2);
Implement(3);
#undef Implement
} // namespace vox
