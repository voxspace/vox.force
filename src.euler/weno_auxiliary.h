//
//  weno_auxiliary.hpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef weno_auxiliary_hpp
#define weno_auxiliary_hpp

#include "mesh.h"
#include "patch_searcher.h"
#include "polyInfo1d.h"
#include "polyInfo2d.h"
#include "polyInfo3d.h"

namespace vox {

template <int dim, int order> class WENOAuxiliary {
public:
  explicit WENOAuxiliary(MeshPtr<dim> g);

public:
  // MARK:- Builder
  void buildReconstructPatch(
      typename PatchSearcher<dim, order>::SearchType type = PatchSearcher<dim, order>::SearchType::Bry);

  void buildWENOPatch();

  void updateWENOInfo();

  void constructCache();

public:
  // MARK:- Getter
  PolyInfo<dim, order> *getPolyInfo() { return &polyInfo; }

  size_t n_WENOPatch(size_t ele_idx);

  const size_t n_WENOPatch(size_t ele_idx) const;

  size_t n_OnePatch(size_t ele_idx, size_t j);

  ArrayView1<Vector<size_t, dim>> getPatch(size_t ele_idx, size_t j);

  const Vector<size_t, dim> *getPatch(size_t ele_idx, size_t j, size_t l) const;

  ArrayView1<std::array<double, PolyInfo<dim, order>::n_unknown>> getPolyAvgs(size_t ele_idx, size_t j);

  const std::array<double, PolyInfo<dim, order>::n_unknown> *getPolyAvgs(size_t ele_idx, size_t j, size_t l) const;

  ArrayView1<typename PolyInfo<dim, order>::Mat> getGInv(size_t ele_idx);

  typename PolyInfo<dim, order>::Mat *getGInv(size_t ele_idx, size_t j);

private:
  // MARK:- Internal Data
  MeshPtr<dim> grid;
  PolyInfo<dim, order> polyInfo;
  PatchSearcher<dim, order> searcher;
  size_t total_patch{};

  std::vector<size_t> patch_prefix_sum;
  std::vector<typename PolyInfo<dim, order>::Mat> G_inv;

  std::vector<size_t> sub_patch_prefix_sum;
  std::vector<Vector<size_t, dim>> weno_patch;
  std::vector<std::array<double, PolyInfo<dim, order>::n_unknown>> patch_polys;

  struct ElementHelper {
    int idx_choice = 0;
    std::vector<Vector<size_t, dim>> ElementNeighbor;
    std::vector<std::vector<Vector<size_t, dim>>> WENOPatch;
  };
  Array<ElementHelper, dim> element_helper;
};

template <int dim, int order> using WENOAuxiliaryPtr = std::shared_ptr<WENOAuxiliary<dim, order>>;

} // namespace vox

#endif /* weno_auxiliary_hpp */
