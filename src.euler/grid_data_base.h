//
//  grid_data_base.hpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef grid_data_base_hpp
#define grid_data_base_hpp

#include <unordered_map>

#include "../src.common/array.h"

namespace vox {
template <int dim>
class GridDataBase {
public:
  //! vector data chunk.
  using ScalarData = Array<double, dim>;

public:
  //! Default constructor.
  explicit GridDataBase(size_t idx);
  //! Copy constructor.
  GridDataBase(const GridDataBase &other);
  //! Copies from other grid data.
  GridDataBase &operator=(const GridDataBase &other);
  //! Destructor.
  virtual ~GridDataBase();

  //! Swaps the data storage and predefined samplers with given grid.
  void swapGridData(GridDataBase *other);

  //! Sets the data storage and predefined samplers with given grid.
  void setGridData(const GridDataBase &other);

public:
  //! Resize the grid using given parameters.
  void resize(Vector<size_t, dim> numberOfCell, double initialValue = 0.0);

  //! returns the number of grid data
  Vector<size_t, dim> numberOfData() const;

  //! Returns custom vector data layer at given index (immutable).
  ArrayView<const double, dim> scalarDataAt() const;

  //! Returns custom vector data layer at given index (mutable).
  ArrayView<double, dim> scalarDataAt();

  //! Returns the const reference to i-th element.
  const double &operator[](size_t i) const;

  //! Returns the reference to i-th element.
  double &operator[](size_t i);

  const double& operator()(const Vector<size_t, dim> &idx) const;

  double& operator()(const Vector<size_t, dim> &idx);

  //! return value of specific index
  double value(Vector<size_t, dim> i);

  //! Fetches the data into a continuous linear array.
  void getData(std::vector<double> *data) const;

  //! write data into file
  virtual void save_cache(const std::string &header) const;

  //! load data into program
  virtual void load_cache(const std::string &header);

public:
  //!
  //! \brief Invokes the given function \p func for each data point.
  //!
  //! This function invokes the given function object \p func for each data
  //! point in serial manner. The input parameters are i indices of a
  //! data point.
  void forEachDataPointIndex(const std::function<void(size_t)> &func) const;

  //!
  //! \brief Invokes the given function \p func for each data point
  //! parallel.
  //!
  //! This function invokes the given function object \p func for each data
  //! point in parallel manner. This input parameters are i indices of a
  //! data point. The order of execution can be arbitrary since it's
  //! multi-threaded.
  //!
  void parallelForEachDataPointIndex(const std::function<void(size_t)> &func) const;

  //! Sets the data from a continuous linear array.
  void setData(const std::vector<double> &data);

  //! Sets the data from a constants.
  void setData(double value);

protected:
  const size_t gridData_idx;

  ScalarData _gridData;
};

} // namespace vox
#endif /* grid_data_base_hpp */
