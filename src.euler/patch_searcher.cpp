//
//  patch_searcher.cpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/13.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "patch_searcher.h"

namespace vox {
// MARK:- Preparation
template <int dim, int order> PatchSearcher<dim, order>::PatchSearcher(MeshPtr<dim> grid) : _grid(grid) {}

template <int dim, int order> void PatchSearcher<dim, order>::setGrid(MeshPtr<dim> grid) { _grid = grid; }

template <> std::vector<Vector<int, 1>> PatchSearcher<1, 1>::bryStencil() { return {{-1}, {0}, {1}}; }

template <> std::vector<Vector<int, 1>> PatchSearcher<1, 2>::bryStencil() { return {{-2}, {-1}, {0}, {1}, {2}}; }

template <> std::vector<Vector<int, 1>> PatchSearcher<1, 3>::bryStencil() { return {{-2}, {-1}, {0}, {1}, {2}}; }

template <> std::vector<Vector<int, 2>> PatchSearcher<2, 1>::bryStencil() {
  return {{0, 0}, {0, 1}, {0, -1}, {-1, 0}, {1, 0}};
}

template <> std::vector<Vector<int, 2>> PatchSearcher<2, 2>::bryStencil() {
  return {{0, 0},  {0, 1},  {0, 2},   {0, -1}, {0, -2},          //
          {-1, 0}, {-1, 1}, {-1, -1}, {1, 0},  {1, 1},  {1, -1}, //
          {-2, 0}, {0, 2}};
}

template <> std::vector<Vector<int, 2>> PatchSearcher<2, 3>::bryStencil() {
  return {{0, 0},  {0, 1},  {0, 2},   {0, -1}, {0, -2},          //
          {-1, 0}, {-1, 1}, {-1, -1}, {1, 0},  {1, 1},  {1, -1}, //
          {-2, 0}, {0, 2}};
}

template <> std::vector<Vector<int, 3>> PatchSearcher<3, 1>::bryStencil() {
  return {{0, 0, 0}, {0, 1, 0}, {0, -1, 0}, {-1, 0, 0}, {1, 0, 0}, {0, 0, 1}, {0, 0, -1}};
}

template <> std::vector<Vector<int, 3>> PatchSearcher<3, 2>::bryStencil() {
  return {{0, 0, 0},  {0, 1, 0},   {0, 2, 0},   {0, -1, 0}, {0, -2, 0},  //
          {0, 0, -1}, {0, -1, -1}, {0, 1, -1},  {0, 0, -2},              //
          {0, 0, 1},  {0, -1, 1},  {0, 1, 1},   {0, 0, 2},               //
          {-1, 0, 0}, {-1, 1, 0},  {-1, -1, 0}, {-1, 0, 1}, {-1, 0, -1}, //
          {1, 0, 0},  {1, 1, 0},   {1, -1, 0},  {1, 0, 1},  {1, 0, -1},  //
          {-2, 0, 0}, {0, 2, 0}};
}

template <> std::vector<Vector<int, 3>> PatchSearcher<3, 3>::bryStencil() {
  return {{0, 0, 0},  {0, 1, 0},   {0, 2, 0},   {0, -1, 0}, {0, -2, 0},  //
          {0, 0, -1}, {0, -1, -1}, {0, 1, -1},  {0, 0, -2},              //
          {0, 0, 1},  {0, -1, 1},  {0, 1, 1},   {0, 0, 2},               //
          {-1, 0, 0}, {-1, 1, 0},  {-1, -1, 0}, {-1, 0, 1}, {-1, 0, -1}, //
          {1, 0, 0},  {1, 1, 0},   {1, -1, 0},  {1, 0, 1},  {1, 0, -1},  //
          {-2, 0, 0}, {0, 2, 0}};
}

//----------------------------------------------------------------------------------------------------------------------
template <> std::vector<Vector<int, 1>> PatchSearcher<1, 1>::nodeStencil() { return bryStencil(); }

template <> std::vector<Vector<int, 1>> PatchSearcher<1, 2>::nodeStencil() { return bryStencil(); }

template <> std::vector<Vector<int, 1>> PatchSearcher<1, 3>::nodeStencil() { return bryStencil(); }

template <> std::vector<Vector<int, 2>> PatchSearcher<2, 1>::nodeStencil() {
  return {{1, -1},  {1, 0},  {1, 1}, //
          {0, -1},  {0, 0},  {0, 1}, //
          {-1, -1}, {-1, 0}, {-1, 1}};
}

template <> std::vector<Vector<int, 2>> PatchSearcher<2, 2>::nodeStencil() {
  return {{1, -1},  {1, 0},  {1, 1}, //
          {0, -1},  {0, 0},  {0, 1}, //
          {-1, -1}, {-1, 0}, {-1, 1}};
}

template <> std::vector<Vector<int, 2>> PatchSearcher<2, 3>::nodeStencil() {
  return {{2, -2},  {2, -1},  {2, 0},  {2, 1},  {2, 2},  //
          {1, -2},  {1, -1},  {1, 0},  {1, 1},  {1, 2},  //
          {0, -2},  {0, -1},  {0, 0},  {0, 1},  {0, 2},  //
          {-1, -2}, {-1, -1}, {-1, 0}, {-1, 1}, {-1, 2}, //
          {-2, -2}, {-2, -1}, {-2, 0}, {-2, 1}, {-2, 2}};
}

template <> std::vector<Vector<int, 3>> PatchSearcher<3, 1>::nodeStencil() {
  return {{1, -1, 0},   {1, 0, 0},   {1, 1, 0},  //--
          {0, -1, 0},   {0, 0, 0},   {0, 1, 0},  //
          {-1, -1, 0},  {-1, 0, 0},  {-1, 1, 0}, //
          {1, -1, 1},   {1, 0, 1},   {1, 1, 1},  //--
          {0, -1, 1},   {0, 0, 1},   {0, 1, 1},  //
          {-1, -1, 1},  {-1, 0, 1},  {-1, 1, 1}, //
          {1, -1, -1},  {1, 0, -1},  {1, 1, -1}, //--
          {0, -1, -1},  {0, 0, -1},  {0, 1, -1}, //
          {-1, -1, -1}, {-1, 0, -1}, {-1, 1, -1}};
}

template <> std::vector<Vector<int, 3>> PatchSearcher<3, 2>::nodeStencil() {
  return {{1, -1, 0},   {1, 0, 0},   {1, 1, 0},  //--
          {0, -1, 0},   {0, 0, 0},   {0, 1, 0},  //
          {-1, -1, 0},  {-1, 0, 0},  {-1, 1, 0}, //
          {1, -1, 1},   {1, 0, 1},   {1, 1, 1},  //--
          {0, -1, 1},   {0, 0, 1},   {0, 1, 1},  //
          {-1, -1, 1},  {-1, 0, 1},  {-1, 1, 1}, //
          {1, -1, -1},  {1, 0, -1},  {1, 1, -1}, //--
          {0, -1, -1},  {0, 0, -1},  {0, 1, -1}, //
          {-1, -1, -1}, {-1, 0, -1}, {-1, 1, -1}};
}

template <> std::vector<Vector<int, 3>> PatchSearcher<3, 3>::nodeStencil() {
  return {{1, -1, 0},   {1, 0, 0},   {1, 1, 0},  //--
          {0, -1, 0},   {0, 0, 0},   {0, 1, 0},  //
          {-1, -1, 0},  {-1, 0, 0},  {-1, 1, 0}, //
          {1, -1, 1},   {1, 0, 1},   {1, 1, 1},  //--
          {0, -1, 1},   {0, 0, 1},   {0, 1, 1},  //
          {-1, -1, 1},  {-1, 0, 1},  {-1, 1, 1}, //
          {1, -1, -1},  {1, 0, -1},  {1, 1, -1}, //--
          {0, -1, -1},  {0, 0, -1},  {0, 1, -1}, //
          {-1, -1, -1}, {-1, 0, -1}, {-1, 1, -1}};
}

//----------------------------------------------------------------------------------------------------------------------
// MARK:- Expand Public Functions
template <int dim, int order>
void PatchSearcher<dim, order>::expand(Vector<size_t, dim> idx, std::vector<Vector<size_t, dim>> &newPatch,
                                       SearchType type) {
  newPatch.clear();
  std::vector<Vector<int, dim>> s;
  if (type == Bry) {
    s = bryStencil();
  } else {
    s = nodeStencil();
  }

  for (const auto &neigh : s) {
    Vector<size_t, dim> result;
    Vector<int, dim> ele_idx;
    if (dim == 1) {
      ele_idx[0] = idx[0] + neigh[0];
      size_t period_neigh = 0;
      if (ele_idx[0] < 0) {
        period_neigh = _grid->periodicMap(ele_idx[0], left);
      } else if (ele_idx[0] >= _grid->dataSize()[0]) {
        period_neigh = _grid->periodicMap(ele_idx[0], right);
      } else {
        period_neigh = ele_idx[0];
      }
      if (period_neigh != static_cast<size_t>(-1)) {
        result[0] = period_neigh;
      } else {
        continue;
      }
      newPatch.push_back(result);
    } else if (dim == 2) {
      //----------------------------------------------------------------------------------------------------------------
      ele_idx[0] = idx[0] + neigh[0];
      size_t period_neigh = 0;
      if (ele_idx[0] < 0) {
        period_neigh = _grid->periodicMap(ele_idx[0], left);
      } else if (ele_idx[0] >= _grid->dataSize()[0]) {
        period_neigh = _grid->periodicMap(ele_idx[0], right);
      } else {
        period_neigh = ele_idx[0];
      }
      if (period_neigh != static_cast<size_t>(-1)) {
        result[0] = period_neigh;
      } else {
        continue;
      }

      ele_idx[1] = idx[1] + neigh[1];
      period_neigh = 0;
      if (ele_idx[1] < 0) {
        period_neigh = _grid->periodicMap(ele_idx[1], bottom);
      } else if (ele_idx[1] >= _grid->dataSize()[1]) {
        period_neigh = _grid->periodicMap(ele_idx[1], up);
      } else {
        period_neigh = ele_idx[1];
      }
      if (period_neigh != static_cast<size_t>(-1)) {
        result[1] = period_neigh;
      } else {
        continue;
      }
      newPatch.push_back(result);
    } else if (dim == 3) {
      //----------------------------------------------------------------------------------------------------------------
      ele_idx[0] = idx[0] + neigh[0];
      size_t period_neigh = 0;
      if (ele_idx[0] < 0) {
        period_neigh = _grid->periodicMap(ele_idx[0], left);
      } else if (ele_idx[0] >= _grid->dataSize()[0]) {
        period_neigh = _grid->periodicMap(ele_idx[0], right);
      } else {
        period_neigh = ele_idx[0];
      }
      if (period_neigh != static_cast<size_t>(-1)) {
        result[0] = period_neigh;
      } else {
        continue;
      }

      ele_idx[1] = idx[1] + neigh[1];
      period_neigh = 0;
      if (ele_idx[1] < 0) {
        period_neigh = _grid->periodicMap(ele_idx[1], bottom);
      } else if (ele_idx[1] >= _grid->dataSize()[1]) {
        period_neigh = _grid->periodicMap(ele_idx[1], up);
      } else {
        period_neigh = ele_idx[1];
      }
      if (period_neigh != static_cast<size_t>(-1)) {
        result[1] = period_neigh;
      } else {
        continue;
      }

      ele_idx[2] = idx[2] + neigh[2];
      period_neigh = 0;
      if (ele_idx[2] < 0) {
        period_neigh = _grid->periodicMap(ele_idx[2], front);
      } else if (ele_idx[2] >= _grid->dataSize()[2]) {
        period_neigh = _grid->periodicMap(ele_idx[2], back);
      } else {
        period_neigh = ele_idx[2];
      }
      if (period_neigh != static_cast<size_t>(-1)) {
        result[2] = period_neigh;
      } else {
        continue;
      }
      newPatch.push_back(result);
    }
  }
}

template class PatchSearcher<1, 1>;
template class PatchSearcher<1, 2>;
template class PatchSearcher<1, 3>;
template class PatchSearcher<2, 1>;
template class PatchSearcher<2, 2>;
template class PatchSearcher<2, 3>;
template class PatchSearcher<3, 1>;
template class PatchSearcher<3, 2>;
template class PatchSearcher<3, 3>;
} // namespace vox
