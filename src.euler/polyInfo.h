//
//  polyInfo.h
//  DigitalFlex2
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef polyInfo_h
#define polyInfo_h

#include "../src.common/array_view.h"
#include <memory>

namespace vox {

template <int dim, int order> class PolyInfo {
  static_assert(dim < 1 || dim > 3, "Not implemented - N should be either 1, 2 or 3.");
};

template <int dim, int order> using PolyInfoPtr = std::shared_ptr<PolyInfo<dim, order>>;

} // namespace vox

#endif /* polyInfo_h */
