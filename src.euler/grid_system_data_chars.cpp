//
//  grid_system_data_chars.cpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "grid_system_data_chars.h"
#include "../src.common/private_helpers.h"

namespace vox {
template <int dim>
GridSysDataNSCharacter<dim>::GridSysDataNSCharacter(MeshPtr<dim> grid) : GridSystemData<dim, dos>(grid) {}

template <int dim>
GridSysDataNSCharacter<dim>::GridSysDataNSCharacter(const GridSysDataNSCharacter &other)
    : GridSystemData<dim, dos>(other), eqInfo(other.eqInfo) {}

template <int dim>
GridSysDataNSCharacter<dim> &GridSysDataNSCharacter<dim>::operator=(const GridSysDataNSCharacter<dim> &other) {
  GridSystemData<dim, dos>::operator=(other);

  eqInfo = other.eqInfo;

  return *this;
}

template <int dim> GridSysDataNSCharacter<dim>::~GridSysDataNSCharacter() = default;

template <int dim> std::shared_ptr<GridSystemData<dim, EquationNS<dim>::DOS>> GridSysDataNSCharacter<dim>::deepCopy() {
  return CLONE_W_CUSTOM_DELETER(GridSysDataNSCharacter<dim>);
}

// MARK:- Getter
template <int dim>
Vector<double, EquationNS<dim>::DOS> GridSysDataNSCharacter<dim>::value(const point_t &pt,
                                                                        Vector<size_t, dim> var_idx) {
  Vector<double, dos> tmp;
  for (size_t i = 0; i < dos; i++) {
    tmp[i] = _PrimitiveData[i]->value(pt, var_idx);
  }

  return char2cons(tmp, _TransMats(var_idx));
}

template <int dim>
Vector<Vector<double, dim>, EquationNS<dim>::DOS> GridSysDataNSCharacter<dim>::gradient(const point_t &pt,
                                                                                        Vector<size_t, dim> var_idx) {
  Vector<Vector<double, dim>, EquationNS<dim>::DOS> tmp;
  for (size_t j = 0; j < dos; j++) {
    std::array<double, dim> g = _PrimitiveData[j]->gradient(pt, var_idx);
    for (size_t k = 0; k < dim; ++k) {
      tmp[j][k] = g[k];
    }
  }

  for (int i = 0; i < dim; ++i) {
    Vector<double, dos> partial_g;
    for (size_t j = 0; j < dos; ++j) {
      partial_g[j] = tmp[j][i];
    }
    partial_g = char2cons(partial_g, _TransMats(var_idx));
    for (size_t j = 0; j < dos; ++j) {
      tmp[j][i] = partial_g[j];
    }
  }
  return tmp;
}

// MARK:- Character Decomposition Reconstruction
template <int dim> void GridSysDataNSCharacter<dim>::calculateBasisFunction() {
  _TransMats.clear();
  _TransMats.resize(GridSystemData<dim, dos>::numberOfVectorData());
  for (int i = 0; i < dos; i++) {
    _PrimitiveData[i] = _scalarDataList[i]->deepCopy();
  }

  // Trans form the data into the primitive form
  Matrix<double, dos, dos> Trans;
  if constexpr (dim == 1) {
    Vector<size_t, 1> size = GridSystemData<dim, dos>::numberOfVectorData();
    for (size_t i = 0; i < size[0]; ++i) {
      Vector<double, dos> cons = GridSystemData<dim, dos>::value({i});

      RightEigenVec(cons, cons, Trans);
      _TransMats[i] = Trans;

      Vector<double, dos> prim = cons2char(cons, Trans);
      for (size_t t = 0; t < dos; ++t) {
        _PrimitiveData[t]->operator()({i}) = prim[t];
      }
    }
  } else if constexpr (dim == 2) {
    Vector<size_t, 2> size = GridSystemData<dim, dos>::numberOfVectorData();
    for (size_t i = 0; i < size[0]; ++i) {
      for (size_t j = 0; j < size[1]; ++j) {
        Vector<double, dos> cons = GridSystemData<dim, dos>::value({i, j});

        RightEigenVec(cons, cons, Trans);
        _TransMats[i] = Trans;

        Vector<double, dos> prim = cons2char(cons, Trans);
        for (size_t t = 0; t < dos; ++t) {
          _PrimitiveData[t]->operator()({i, j}) = prim[t];
        }
      }
    }
  } else if constexpr (dim == 3) {
    Vector3UZ size = GridSystemData<dim, dos>::numberOfVectorData();
    for (size_t i = 0; i < size[0]; ++i) {
      for (size_t j = 0; j < size[1]; ++j) {
        for (size_t k = 0; k < size[2]; ++k) {
          Vector<double, dos> cons = GridSystemData<dim, dos>::value({i, j, k});

          RightEigenVec(cons, cons, Trans);
          _TransMats[i] = Trans;

          Vector<double, dos> prim = cons2char(cons, Trans);
          for (size_t t = 0; t < dos; ++t) {
            _PrimitiveData[t]->operator()({i, j, k}) = prim[t];
          }
        }
      }
    }
  }

  // Reconstruction
  for (size_t i = 0; i < dos; ++i) {
    _PrimitiveData[i]->calculateBasisFunction();
  }
}

template <> Vector3D GridSysDataNSCharacter<1>::cons2char(const Vector3D &cons, const Matrix3x3D &Trans) {
  Matrix3x3D Kernel = Trans.inverse();

  Vector3D qq;
  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 3; ++j) {
      qq[i] += Kernel(i, j) * cons[j];
    }
  }
  return qq;
}

template <> Vector3D GridSysDataNSCharacter<1>::char2cons(const Vector3D &prim, const Matrix3x3D &Trans) {
  Vector3D uu;

  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 3; ++j) {
      uu[i] += Trans(i, j) * prim[j];
    }
  }
  return uu;
}

template <>
void GridSysDataNSCharacter<1>::RightEigenVec(const Vector3D &ul, const Vector3D &ur, Matrix3x3D &eigenVec) {
  // //Average
  Vector3D _u_ = ul;
  _u_ += ur;
  _u_ /= 2.0;
  Vector3D _q_ = _u_;

  double uu = _q_[1] / _q_[0];
  double p = (eqInfo->Gamma - 1) * (_q_[2] - 0.5 * uu * uu * _q_[0]);
  double HH = (p + _q_[2]) / _q_[0];
  double aa = std::sqrt(eqInfo->Gamma * p / _q_[0]);
  eigenVec(0, 0) = 1.0;
  eigenVec(0, 1) = 1.0;
  eigenVec(0, 2) = 1.0;

  eigenVec(1, 0) = uu - aa;
  eigenVec(1, 1) = uu;
  eigenVec(1, 2) = uu + aa;

  eigenVec(2, 0) = HH - uu * aa;
  eigenVec(2, 1) = 0.5 * uu * uu;
  eigenVec(2, 2) = HH + uu * aa;
}

template <int dim> void GridSysDataNSCharacter<dim>::setEquation(EquationNSPtr<dim> eq) { eqInfo = eq; }

// MARK:- Builder
template <int dim>
GridSysDataNSCharacterPtr<dim> GridSysDataNSCharacter<dim>::Builder::build(MeshPtr<dim> grid,
                                                                           EquationNSPtr<dim> eq_info) {
  GridSysDataNSCharacterPtr<dim> dataSys = std::make_shared<GridSysDataNSCharacter<dim>>(grid);

  dataSys->setScalarBuilder(std::make_shared<typename GridData<dim>::Builder>());
  for (size_t i = 0; i < dos; i++) {
    dataSys->loadScalarData(std::make_shared<typename GridData<dim>::Builder>());
  }

  dataSys->setEquation(eq_info);

  return dataSys;
}

template <int dim>
GridSysDataNSCharacterPtr<dim> GridSysDataNSCharacter<dim>::Builder::build(GridDataBuilderPtr<dim> builder,
                                                                           MeshPtr<dim> grid,
                                                                           EquationNSPtr<dim> eq_info) {
  GridSysDataNSCharacterPtr<dim> dataSys = std::make_shared<GridSysDataNSCharacter<dim>>(grid);

  dataSys->setScalarBuilder(builder);
  for (size_t i = 0; i < dos; i++) {
    dataSys->loadScalarData(builder);
  }

  dataSys->setEquation(eq_info);

  return dataSys;
}

template <int dim> typename GridSysDataNSCharacter<dim>::Builder GridSysDataNSCharacter<dim>::builder() {
  return Builder();
}

template class GridSysDataNSCharacter<1>;
} // namespace vox
