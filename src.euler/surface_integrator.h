//
//  surface_integrator.hpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/13.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef surface_integrator_hpp
#define surface_integrator_hpp

#include "quadrature_infos.h"
#include "type_utils.h"
#include <vector>

namespace vox {

template <int dim, int order> class SurfaceIntegrator {
public:
  SurfaceIntegrator(const Vector<double, dim> &origin, const Vector<double, dim> &gridSpacing)
      : origin(origin), gridSpacing(gridSpacing) {}

public:
  std::vector<Point<dim>> transform(const std::vector<Point<dim - 1>> &pts, const Vector<size_t, dim>& cell_idx, const Direction& d) {
    Vector<double, dim> idx = cell_idx.template castTo<double>();
    Point<dim> center =  origin + elemMul(gridSpacing, idx);
    std::vector<Point<dim>> result;
    result.reserve(pts.size());
    for (const auto &pt : pts) {
      switch (d) {
      case right: {
        Point<dim> p;
        p[0] = gridSpacing[0];
        p[1] = (pt[0] + 1.0) * gridSpacing[1] / 2.0;
        if (dim == 3) {
          p[2] = (pt[1] + 1.0) * gridSpacing[2] / 2.0;
        }
        result.push_back(p + center);
        break;
      }
      case left: {
        Point<dim> p;
        p[0] = 0.0;
        p[1] = (pt[0] + 1.0) * gridSpacing[1] / 2.0;
        if (dim == 3) {
          p[2] = (pt[1] + 1.0) * gridSpacing[2] / 2.0;
        }
        result.push_back(p + center);
        break;
      }
      case up: {
        Point<dim> p;
        p[0] = (pt[0] + 1.0) * gridSpacing[0] / 2.0;
        p[1] = gridSpacing[1];
        if (dim == 3) {
          p[2] = (pt[1] + 1.0) * gridSpacing[2] / 2.0;
        }
        result.push_back(p + center);
        break;
      }
      case bottom: {
        Point<dim> p;
        p[0] = (pt[0] + 1.0) * gridSpacing[0] / 2.0;
        p[1] = 0;
        if (dim == 3) {
          p[2] = (pt[1] + 1.0) * gridSpacing[2] / 2.0;
        }
        result.push_back(p + center);
        break;
      }
      case back: {
        Point<dim> p;
        p[0] = (pt[0] + 1.0) * gridSpacing[0] / 2.0;
        p[1] = (pt[1] + 1.0) * gridSpacing[1] / 2.0;
        p[2] = gridSpacing[2];
        result.push_back(p + center);
        break;
      }
      case front: {
        Point<dim> p;
        p[0] = (pt[0] + 1.0) * gridSpacing[0] / 2.0;
        p[1] = (pt[1] + 1.0) * gridSpacing[1] / 2.0;
        p[2] = 0.0;
        result.push_back(p + center);
        break;
      }
      }
    }
    return result;
  }

  template <typename RETURN_TYPE>
  RETURN_TYPE surfaceIntegral(std::function<RETURN_TYPE(Point<dim> &pt)> func, Vector<size_t, dim> cell_idx,
                              Direction d) {
    int total = std::pow(QuadratureInfo<order>::n_quad, dim-1);
    std::vector<Point<dim - 1>> quad_pts;
    quad_pts.reserve(total);
    std::vector<double> quad_weights;
    quad_weights.reserve(total);
    if constexpr (dim == 2) {
      for (int i = 0; i < QuadratureInfo<order>::n_quad; ++i) {
        quad_pts.push_back(Point<dim - 1>(infos.pnt(i)));
        if (d == up || d == bottom) {
          quad_weights.push_back(infos.weight(i) * gridSpacing[0]);
        } else if (d == left || d == right) {
          quad_weights.push_back(infos.weight(i) * gridSpacing[1]);
        }
      }
    } else if (dim == 3) {
      for (int i = 0; i < QuadratureInfo<order>::n_quad; ++i) {
        for (int j = 0; j < QuadratureInfo<order>::n_quad; ++j) {
          quad_pts.push_back(Point<dim - 1>(infos.pnt(i), infos.pnt(j)));
          if (d == up || d == bottom) {
            quad_weights.push_back(infos.weight(i) * infos.weight(j) * gridSpacing[0] * gridSpacing[2]);
          } else if (d == left || d == right) {
            quad_weights.push_back(infos.weight(i) * infos.weight(j) * gridSpacing[1] * gridSpacing[2]);
          } else if (d == front || d == back) {
            quad_weights.push_back(infos.weight(i) * infos.weight(j) * gridSpacing[0] * gridSpacing[1]);
          }
        }
      }
    }
    auto pts = transform(quad_pts, cell_idx, d);

    RETURN_TYPE v{};
    for (size_t l = 0; l < pts.size(); ++l) {
      v += quad_weights[l] * func(pts[l]);
    }
    return v;
  }

  double surfaceCompare(std::function<double(Point<dim> &pt)> func, Vector<size_t, dim> cell_idx, Direction d) {
    int total = std::pow(QuadratureInfo<order>::n_quad, dim);
    std::vector<Point<dim - 1>> quad_pts;
    quad_pts.reserve(total);
    if constexpr (dim == 2) {
      for (int i = 0; i < QuadratureInfo<order>::n_quad; ++i) {
        quad_pts.push_back(Point<dim - 1>(infos.pnt(i)));
      }
    } else if (dim == 3) {
      for (int i = 0; i < QuadratureInfo<order>::n_quad; ++i) {
        for (int j = 0; j < QuadratureInfo<order>::n_quad; ++j) {
          quad_pts.push_back(Point<dim - 1>(infos.pnt(i), infos.pnt(j)));
        }
      }
    }
    quad_pts = transform(quad_pts);

    double v = 0.;
    for (size_t l = 0; l < quad_pts.size(); ++l) {
      v = std::max(v, func(quad_pts[l]));
    }
    return v;
  }

protected:
  QuadratureInfo<order> infos;

private:
  Vector<double, dim> origin;
  Vector<double, dim> gridSpacing;
};

template <int order> class SurfaceIntegrator<1, order> {
public:
  SurfaceIntegrator(const Vector1D &origin, const Vector1D &gridSpacing) : origin(origin), gridSpacing(gridSpacing) {}

  template <typename RETURN_TYPE>
  RETURN_TYPE
  surfaceIntegral(const std::function<RETURN_TYPE(const Point<1> &pt)> &func,
                  Vector<size_t, 1> cell_idx, Direction d) {
    Point<1> p_0 = origin + gridSpacing * static_cast<double>(cell_idx[0]);
    if (d == right) {
      p_0 += gridSpacing;
    }

    return func(p_0);
  }

  void loadCache() {}

private:
  Vector1D origin;
  Vector1D gridSpacing;
};

} // namespace vox

#endif /* surface_integrator_hpp */
