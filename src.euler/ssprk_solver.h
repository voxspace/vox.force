//
//  ssprk_solver.hpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef ssprk_solver_hpp
#define ssprk_solver_hpp

#include "grid_system_data.h"

namespace vox {

template <int dim, int dos> class SSPRKSolver {
public:
  void ssprk1(GridSystemDataPtr<dim, dos> init, double dt,
              std::function<void(GridSystemDataPtr<dim, dos> in, GridSystemDataPtr<dim, dos> out)> sourceFunc,
              GridSystemDataPtr<dim, dos> output);

  void ssprk2(GridSystemDataPtr<dim, dos> init, double dt,
              std::function<void(GridSystemDataPtr<dim, dos> in, GridSystemDataPtr<dim, dos> out)> sourceFunc,
              GridSystemDataPtr<dim, dos> output);

  void ssprk3(GridSystemDataPtr<dim, dos> init, double dt,
              std::function<void(GridSystemDataPtr<dim, dos> in, GridSystemDataPtr<dim, dos> out)> sourceFunc,
              GridSystemDataPtr<dim, dos> output);
};

template <int dim, int dos> using SSPRKSolverPtr = std::shared_ptr<SSPRKSolver<dim, dos>>;

} // namespace vox

#endif /* ssprk_solver_hpp */
