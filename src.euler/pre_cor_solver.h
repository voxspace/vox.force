//
//  pre_cor_solver.hpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef pre_cor_solver_hpp
#define pre_cor_solver_hpp

#include "grid_system_data.h"

namespace vox {

template <int dim, int dos> class Prediction_CorrectionSolver {
public:
  void precor(GridSystemDataPtr<dim, dos> init, double dt,
              std::function<void(const Vector<double, dos> &in, Vector<double, dos> &out)> sourceFunc,
              std::function<double(const Vector<double, dos> &data)> timeStep,
              std::function<double(const Vector<double, dos> &left, const Vector<double, dos> &right)> ConvergenceNorm,
              GridSystemDataPtr<dim, dos> output);

private:
  void precorForElement(
      Vector<double, dos> init, double dt,
      std::function<void(const Vector<double, dos> &in, Vector<double, dos> &out)> sourceFunc,
      std::function<double(const Vector<double, dos> &data)> timeStep,
      std::function<double(const Vector<double, dos> &left, const Vector<double, dos> &right)> ConvergenceNorm,
      Vector<double, dos> &output);
};

template <int dim, int dos> using PreCorSolverPtr = std::shared_ptr<Prediction_CorrectionSolver<dim, dos>>;

} // namespace vox
#endif /* pre_cor_solver_hpp */
