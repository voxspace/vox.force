//
//  grid_system_data_rNSPositive.cpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "grid_system_data_rNSPositive.h"
#include "quads_auxiliary.h"

namespace vox {

template <int dim, int order>
GridSysDataRNSPositive<dim, order>::GridSysDataRNSPositive(MeshPtr<dim> grid) : GridSystemData<dim, dos>(grid) {
  updateQuadInfo();
}

template <int dim, int order>
GridSysDataRNSPositive<dim, order>::GridSysDataRNSPositive(const GridSysDataRNSPositive<dim, order> &other)
    : GridSystemData<dim, dos>(other), eqInfo(other.eqInfo) {
  PositiveParas = other.PositiveParas;
}

template <int dim, int order>
GridSysDataRNSPositive<dim, order> &
GridSysDataRNSPositive<dim, order>::operator=(const GridSysDataRNSPositive<dim, order> &other) {
  GridSystemData<dim, dos>::operator=(other);

  PositiveParas = other.PositiveParas;

  eqInfo = other.eqInfo;

  return *this;
}

template <int dim, int order> GridSysDataRNSPositive<dim, order>::~GridSysDataRNSPositive() = default;

template <int dim, int order>
std::shared_ptr<GridSystemData<dim, EquationReactingNS<dim>::DOS>> GridSysDataRNSPositive<dim, order>::deepCopy() {
  return std::shared_ptr<GridSysDataRNSPositive<dim, order>>(
      new GridSysDataRNSPositive<dim, order>(*this), [](GridSysDataRNSPositive<dim, order> *obj) { delete obj; });
}

// MARK:- Getter
template <int dim, int order>
Vector<double, EquationReactingNS<dim>::DOS> GridSysDataRNSPositive<dim, order>::value(const point_t &pt,
                                                                                       Vector<size_t, dim> var_idx) {
  // make positivity
  Vector<double, dos> tmp = GridSystemData<dim, dos>::value(pt, var_idx);
  Vector<double, dos> tmp_avg = GridSystemData<dim, dos>::value(var_idx);

  tmp[0] = PositiveParas(var_idx).theta_rho * (tmp[0] - tmp_avg[0]) + tmp_avg[0];
  tmp[4] = PositiveParas(var_idx).theta_Y * (tmp[4] - tmp_avg[4]) + tmp_avg[4];

  tmp = PositiveParas(var_idx).theta_P * (tmp - tmp_avg) + tmp_avg;

  return tmp;
}

template <int dim, int order>
Vector<Vector<double, dim>, EquationReactingNS<dim>::DOS>
GridSysDataRNSPositive<dim, order>::gradient(const point_t &pt, Vector<size_t, dim> var_idx) {
  Vector<Vector<double, dim>, dos> tmp = GridSystemData<dim, dos>::gradient(pt, var_idx);

  tmp[0] *= PositiveParas(var_idx).theta_rho;
  tmp[4] *= PositiveParas(var_idx).theta_Y;

  for (size_t i = 0; i < dos; ++i) {
    tmp[i] *= PositiveParas(var_idx).theta_P;
  }
  return tmp;
}

// MARK:- Positivity modifier
template <int dim, int order> void GridSysDataRNSPositive<dim, order>::calculateBasisFunction() {
  GridSystemData<dim, dos>::calculateBasisFunction();
  PositivityPreserving();
}

template <int dim, int order> void GridSysDataRNSPositive<dim, order>::PositivityPreserving() {
  const double eps = 1.0e-13;
  if constexpr (dim == 1) {
    parallelFor(kZeroSize, _grid->dataSize()[0], [&](size_t ele_idx) -> void {
      Vector<double, dos> avg = GridSystemData<dim, dos>::value({ele_idx});
      PositivityRhoY({ele_idx}, eps, avg[0], avg[4]);
      PositivityPressure({ele_idx}, eps);
    });
  } else if constexpr (dim == 2) {
    parallelFor(kZeroSize, _grid->dataSize()[0], kZeroSize, _grid->dataSize()[1],
                [&](size_t ele_idx, size_t ele_idy) -> void {
                  Vector<double, dos> avg = GridSystemData<dim, dos>::value({ele_idx, ele_idy});
                  PositivityRhoY({ele_idx, ele_idy}, eps, avg[0], avg[5]);
                  PositivityPressure({ele_idx, ele_idy}, eps);
                });
  } else if constexpr (dim == 3) {
    parallelFor(kZeroSize, _grid->dataSize()[0], kZeroSize, _grid->dataSize()[1], kZeroSize, _grid->dataSize()[2],
                [&](size_t ele_idx, size_t ele_idy, size_t ele_idz) -> void {
                  Vector<double, dos> avg = GridSystemData<dim, dos>::value({ele_idx, ele_idy, ele_idz});
                  PositivityRhoY({ele_idx, ele_idy, ele_idz}, eps, avg[0], avg[6]);
                  PositivityPressure({ele_idx, ele_idy, ele_idz}, eps);
                });
  }
}

template <int dim, int order>
void GridSysDataRNSPositive<dim, order>::PositivityRhoY(Vector<size_t, dim> var_idx, double eps, double rho, double Y) {
  Vector<double, dos> vals = GridSystemData<dim, dos>::value(PositiveParas(var_idx).quad_pts[0], var_idx);
  double rho_min = vals[0];
  double Y_min = vals[4];
  for (size_t i = 0; i < PositiveParas(var_idx).quad_pts.size(); ++i) {
    vals = GridSystemData<dim, dos>::value(PositiveParas(var_idx).quad_pts[i], var_idx);
    rho_min = std::min(rho_min, vals[0]);
    Y_min = std::min(Y_min, vals[4]);
  }
  // must be this for right may be NAN
  PositiveParas(var_idx).theta_rho = std::min(1.0, std::abs((rho - eps) / (rho - rho_min)));
  PositiveParas(var_idx).theta_Y = std::min(1.0, std::abs(Y / (Y - Y_min)));
}

template <int dim, int order>
void GridSysDataRNSPositive<dim, order>::PositivityPressure(Vector<size_t, dim> var_idx, double eps) {
  double p_avgs = eqInfo->pressure(GridSystemData<dim, dos>::value(var_idx));

  PositiveParas(var_idx).theta_P = 1.0;
  for (size_t i = 0; i < PositiveParas(var_idx).quad_pts.size(); ++i) {
    double p = Pressure(PositiveParas(var_idx).quad_pts[i], var_idx);
    if (p < eps) {
      double t0 = std::abs((p_avgs - eps) / (p_avgs - p));
      PositiveParas(var_idx).theta_P = std::min(PositiveParas(var_idx).theta_P, t0);
    }
  }
}

template <int dim, int order>
double GridSysDataRNSPositive<dim, order>::Pressure(const point_t &pt, Vector<size_t, dim> var_idx) {
  Vector<double, dos> vals = GridSystemData<dim, dos>::value(pt, var_idx);
  Vector<double, dos> val_avgs = GridSystemData<dim, dos>::value(var_idx);
  vals[0] = PositiveParas(var_idx).theta_rho * (vals[0] - val_avgs[0]) + val_avgs[0];
  vals[dim + 2] = PositiveParas(var_idx).theta_Y * (vals[dim + 2] - val_avgs[dim + 2]) + val_avgs[dim + 2];

  // can't use pressure in EquationReactingNS which will do correction
  double vel = 0.0;
  for (int i = 1; i <= dim; i++) {
    vel += vals[i] * vals[i];
  }

  return (eqInfo->Gamma - 1) * (vals[dim + 1] - 0.5 * vel / vals[0] - vals[dim + 2] * eqInfo->q0);
}

template <int dim, int order> void GridSysDataRNSPositive<dim, order>::updateQuadInfo() {
  PositiveParas.resize(_grid->dataSize());
  if constexpr (dim == 1) {
    for (size_t i = 0; i < PositiveParas.size()[0]; ++i) {
      QuadsAuxiliary<dim, order>::getEleQuadPoint(_grid, {i}, PositiveParas({i}).quad_pts);
    }
  } else if constexpr (dim == 2) {
    for (size_t i = 0; i < PositiveParas.size()[0]; ++i) {
      for (size_t j = 0; j < PositiveParas.size()[1]; ++j) {
        QuadsAuxiliary<dim, order>::getEleQuadPoint(_grid, {i, j}, PositiveParas({i, j}).quad_pts);
      }
    }
  } else if constexpr (dim == 3) {
    for (size_t i = 0; i < PositiveParas.size()[0]; ++i) {
      for (size_t j = 0; j < PositiveParas.size()[1]; ++j) {
        for (size_t k = 0; k < PositiveParas.size()[2]; ++k) {
          QuadsAuxiliary<dim, order>::getEleQuadPoint(_grid, {i, j, k}, PositiveParas({i, j, k}).quad_pts);
        }
      }
    }
  }
}

template <int dim, int order> void GridSysDataRNSPositive<dim, order>::setEquation(EquationReactingNSPtr<dim> eq) {
  eqInfo = eq;
}

// MARK:- Builder
template <int dim, int order>
GridSysDataRNSPositivePtr<dim, order>
GridSysDataRNSPositive<dim, order>::Builder::build(MeshPtr<dim> grid, EquationReactingNSPtr<dim> eq_info) {
  GridSysDataRNSPositivePtr<dim, order> dataSys = std::make_shared<GridSysDataRNSPositive<dim, order>>(grid);

  dataSys->setScalarBuilder(std::make_shared<typename GridData<dim>::Builder>());
  for (size_t i = 0; i < dos; i++) {
    dataSys->loadScalarData(std::make_shared<typename GridData<dim>::Builder>());
  }

  dataSys->setEquation(eq_info);

  return dataSys;
}

template <int dim, int order>
GridSysDataRNSPositivePtr<dim, order>
GridSysDataRNSPositive<dim, order>::Builder::build(GridDataBuilderPtr<dim> builder, MeshPtr<dim> grid,
                                                   EquationReactingNSPtr<dim> eq_info) {
  GridSysDataRNSPositivePtr<dim, order> dataSys = std::make_shared<GridSysDataRNSPositive<dim, order>>(grid);

  dataSys->setScalarBuilder(builder);
  for (size_t i = 0; i < dos; i++) {
    dataSys->loadScalarData(builder);
  }

  dataSys->setEquation(eq_info);

  return dataSys;
}

template <int dim, int order>
typename GridSysDataRNSPositive<dim, order>::Builder GridSysDataRNSPositive<dim, order>::builder() {
  return Builder();
}

template class GridSysDataRNSPositive<1, 1>;
template class GridSysDataRNSPositive<1, 2>;

template class GridSysDataRNSPositive<2, 1>;
template class GridSysDataRNSPositive<2, 2>;

template class GridSysDataRNSPositive<3, 1>;
template class GridSysDataRNSPositive<3, 2>;
} // namespace vox
