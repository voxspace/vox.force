//
//  grid_data_unlimited.hpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef grid_data_unlimited_hpp
#define grid_data_unlimited_hpp

#include "grid_data.h"
#include "recon_auxiliary.h"

namespace vox {

template <int dim, int order> class GridDataUnlimited : public GridData<dim> {
public:
  class Builder;
  using point_t = typename Mesh<dim>::point_t;

  //! Constructs grid data with given grid.
  GridDataUnlimited(size_t idx, MeshPtr<dim> grid, ReconAuxiliaryPtr<dim, order> aux);
  //! Copy constructor.
  GridDataUnlimited(const GridDataUnlimited &other);
  //! Copies from other grid data.
  GridDataUnlimited &operator=(const GridDataUnlimited &other);
  //! Destructor.
  virtual ~GridDataUnlimited();
  //! Returns the copy of the grid instance.
  std::shared_ptr<GridData<dim>> deepCopy() override;

public:
  void calculateBasisFunction() override;

protected:
  void calculateBasisFunction(Vector<size_t, dim> i);

  virtual void solve(Vector<size_t, dim> i, const double &UU, const std::vector<double> &bary_info,
                     typename PolyInfo<dim, order>::Vec &result);

public:
  //! return value of specific point
  double value(const point_t &pt, Vector<size_t, dim> i) override;

  //! return value of specific point
  std::array<double, dim> gradient(const point_t &pt, Vector<size_t, dim> i) override;

protected:
  const ReconAuxiliaryPtr<dim, order> reconAux;

  struct ReconCache {
    typename PolyInfo<dim, order>::Vec Slope;
  };
  Array<ReconCache, dim> recon_cache;

  using GridData<dim>::_grid;
  using GridDataBase<dim>::_gridData;

  friend class GridData1DUnlimitedTest;
  friend class GridData2DUnlimitedTest;
  friend class GridData2DAdaptUnlimitedTest;
  friend class GridData3DUnlimitedTest;
};
template <int dim, int order> using GridDataUnlimitedPtr = std::shared_ptr<GridDataUnlimited<dim, order>>;

//----------------------------------------------------------------------------------------------------------------------
// MARK:- Builder class
template <int dim, int order> class GridDataUnlimited<dim, order>::Builder : public GridData<dim>::Builder {
public:
  void setReconAuxiliary(ReconAuxiliaryPtr<dim, order> aux);

  GridDataPtr<dim> build(size_t idx, MeshPtr<dim> grid) override;

private:
  ReconAuxiliaryPtr<dim, order> reconAux;
};
template <int dim, int order>
using GridDataUnlimitedBuilderPtr = std::shared_ptr<typename GridDataUnlimited<dim, order>::Builder>;

} // namespace vox
#endif /* grid_data_unlimited_hpp */
