//
//  volume_integrator.hpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/13.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef volume_integrator_hpp
#define volume_integrator_hpp

#include "quadrature_infos.h"
#include "type_utils.h"
#include <iostream>
#include <vector>

namespace vox {

template <int dim, int order> class VolumeIntegrator {
public:
  VolumeIntegrator(const Vector<double, dim> &origin, const Vector<double, dim> &gridSpacing)
      : origin(origin), gridSpacing(gridSpacing) {}

public:
  std::vector<Point<dim>> transform(const std::vector<Point<dim>> &pts, Vector<size_t, dim> cell_idx) {
    Vector<double, dim> idx = cell_idx.template castTo<double>();
    Point<dim> center = origin + elemMul(gridSpacing, idx);

    std::vector<Point<dim>> result;
    result.reserve(pts.size());
    for (const auto &pt : pts) {
      result.push_back(center + (pt + 1.0) * gridSpacing / 2.0);
    }
    return result;
  }

  template <typename RETURN_TYPE>
  RETURN_TYPE volumeIntegral(std::function<RETURN_TYPE(Point<dim> &pt)> func, Vector<size_t, dim> cell_idx) {
    int total = std::pow(QuadratureInfo<order>::n_quad, dim);
    std::vector<Point<dim>> quad_pts;
    quad_pts.reserve(total);
    std::vector<double> quad_weights;
    quad_weights.reserve(total);
    if constexpr (dim == 1) {
      for (int i = 0; i < QuadratureInfo<order>::n_quad; ++i) {
        quad_pts.push_back(Point<dim>(infos.pnt(i)));
        quad_weights.push_back(infos.weight(i) * gridSpacing[0]);
      }
    } else if constexpr (dim == 2) {
      for (int i = 0; i < QuadratureInfo<order>::n_quad; ++i) {
        for (int j = 0; j < QuadratureInfo<order>::n_quad; ++j) {
          quad_pts.push_back(Point<dim>(infos.pnt(i), infos.pnt(j)));
          quad_weights.push_back(infos.weight(i) * infos.weight(j) * gridSpacing[0] * gridSpacing[1]);
        }
      }
    } else if constexpr (dim == 3) {
      for (int i = 0; i < QuadratureInfo<order>::n_quad; ++i) {
        for (int j = 0; j < QuadratureInfo<order>::n_quad; ++j) {
          for (int k = 0; k < QuadratureInfo<order>::n_quad; ++k) {
            quad_pts.push_back(Point<dim>(infos.pnt(i), infos.pnt(j), infos.pnt(k)));
            quad_weights.push_back(infos.weight(i) * infos.weight(j) * infos.weight(k) * gridSpacing[0] *
                                   gridSpacing[1] * gridSpacing[2]);
          }
        }
      }
    }
    quad_pts = transform(quad_pts, cell_idx);

    RETURN_TYPE v{};
    for (size_t l = 0; l < quad_pts.size(); ++l) {
      v += quad_weights[l] * func(quad_pts[l]);
    }
    return v;
  }

  double volumeCompare(std::function<double(Point<dim> &pt)> func, Vector<size_t, dim> cell_idx) {
    int total = std::pow(QuadratureInfo<order>::n_quad, dim);
    std::vector<Point<dim>> quad_pts;
    quad_pts.reserve(total);
    if constexpr (dim == 1) {
      for (int i = 0; i < QuadratureInfo<order>::n_quad; ++i) {
        quad_pts.push_back(Point<dim>(infos.pnt(i)));
      }
    } else if constexpr (dim == 2) {
      for (int i = 0; i < QuadratureInfo<order>::n_quad; ++i) {
        for (int j = 0; j < QuadratureInfo<order>::n_quad; ++j) {
          quad_pts.push_back(Point<dim>(infos.pnt(i), infos.pnt(j)));
        }
      }
    } else if constexpr (dim == 3) {
      for (int i = 0; i < QuadratureInfo<order>::n_quad; ++i) {
        for (int j = 0; j < QuadratureInfo<order>::n_quad; ++j) {
          for (int k = 0; k < QuadratureInfo<order>::n_quad; ++k) {
            quad_pts.push_back(Point<dim>(infos.pnt(i), infos.pnt(j), infos.pnt(k)));
          }
        }
      }
    }
    quad_pts = transform(quad_pts, cell_idx);

    double v = 0.;
    for (size_t l = 0; l < quad_pts.size(); ++l) {
      v = std::max(v, func(quad_pts[l]));
    }
    return v;
  }

protected:
  QuadratureInfo<order> infos;

private:
  Vector<double, dim> origin;
  Vector<double, dim> gridSpacing;
};

} // namespace vox

#endif /* volume_integrator_hpp */
