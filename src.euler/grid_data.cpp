//
//  grid_data.cpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "grid_data.h"
#include "../src.common/private_helpers.h"

namespace vox {
// MARK:- Constructor
template <int dim> GridData<dim>::GridData(size_t idx, MeshPtr<dim> grid) : GridDataBase<dim>(idx), _grid(grid) {
  _gridData.resize(_grid->dataSize());
}

template <int dim> GridData<dim>::GridData(const GridData &other) : GridDataBase<dim>(other), _grid(other._grid) {
  _gridData = other._gridData;
}

template <int dim> GridData<dim> &GridData<dim>::operator=(const GridData &other) {
  GridDataBase<dim>::operator=(other);

  return *this;
}

template <int dim> GridData<dim>::~GridData() = default;

template <int dim> std::shared_ptr<GridData<dim>> GridData<dim>::deepCopy() {
  return CLONE_W_CUSTOM_DELETER(GridData<dim>);
}

template <int dim> MeshPtr<dim> GridData<dim>::getGrid() { return _grid; }

// MARK:- Cell Reconstructed Function API
template <int dim> double GridData<dim>::value(const point_t &pt, Vector<size_t, dim> idx) {
  UNUSED_VARIABLE(pt);

  return _gridData(idx);
}

template <int dim> std::vector<double> GridData<dim>::value(const std::vector<point_t> &pt, Vector<size_t, dim> idx) {
  std::vector<double> tmp(pt.size(), 0.0);
  for (size_t i = 0; i < pt.size(); ++i) {
    tmp[i] = value(pt[i], idx);
  }
  return tmp;
}

template <int dim> std::array<double, dim> GridData<dim>::gradient(const point_t &pt, Vector<size_t, dim> idx) {
  UNUSED_VARIABLE(pt);
  UNUSED_VARIABLE(idx);

  return std::array<double, dim>();
}

template <int dim>
std::vector<std::array<double, dim>> GridData<dim>::gradient(const std::vector<point_t> &pt, Vector<size_t, dim> idx) {
  std::vector<std::array<double, dim>> tmp(pt.size(), std::array<double, dim>());
  for (size_t i = 0; i < pt.size(); ++i) {
    tmp[i] = gradient(pt[i], idx);
  }
  return tmp;
}

//------------------------------------------------------------------------------
template <int dim> GridDataPtr<dim> GridData<dim>::Builder::build(size_t idx, MeshPtr<dim> grid) {
  return std::make_shared<GridData<dim>>(idx, grid);
}

// MARK:- Helper Functions
template <int dim> void add(GridDataPtr<dim> data1, const GridDataPtr<dim> data2) {
  JET_ASSERT(data1->getGrid() == data2->getGrid());

  data1->parallelForEachDataPointIndex([&](size_t i) -> void { data1->operator[](i) += data2->operator[](i); });
}

template <int dim> void scale(GridDataPtr<dim> data, double scale) {
  data->parallelForEachDataPointIndex([&](size_t i) -> void { data->operator[](i) *= scale; });
}
//-------------------------------------------------------------------------
template class GridData<1>;
template class GridData<2>;
template class GridData<3>;

template void add(GridDataPtr<1> data1, const GridDataPtr<1> data2);
template void add(GridDataPtr<2> data1, const GridDataPtr<2> data2);
template void add(GridDataPtr<3> data1, const GridDataPtr<3> data2);

template void scale(GridDataPtr<1> data, double scale);
template void scale(GridDataPtr<2> data, double scale);
template void scale(GridDataPtr<3> data, double scale);
} // namespace vox
