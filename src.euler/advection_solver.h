//
//  advection_solver.hpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef advection_solver_hpp
#define advection_solver_hpp

#include "flux_base.h"
#include "grid_system_data.h"
#include "problemDef.h"

namespace vox {

template <int dim, int dos> class AdvectionSolver {
public:
  enum AdvectionType { inviscid_only, Viscous_only, Whole_flux };

  AdvectionSolver(MeshPtr<dim> grid, FluxPtr<dim, dos> flux);
  ~AdvectionSolver();

public:
  void setFlux(FluxPtr<dim, dos> flux);
  FluxPtr<dim, dos> getFlux();

  // using dual buffer to accelerate
  void advect(GridSystemDataPtr<dim, dos> input, double dt, ProblemDefPtr<dim, dos> def, AdvectionType type,
              GridSystemDataPtr<dim, dos> output);

private:
  void advectForElement(Vector<size_t, dim> ele_idx, GridSystemDataPtr<dim, dos> input, double dt,
                        ProblemDefPtr<dim, dos> def, AdvectionType type, GridSystemDataPtr<dim, dos> output);

  void fluxOneSide(const GridSystemDataPtr<dim, dos>& input, const ProblemDefPtr<dim, dos>& def, const std::array<double, dim>& out_normal,
                   const Vector<size_t, dim>& ele_idx, const Vector<size_t, dim>& nei_ele_idx, Direction d, double dt, AdvectionType type,
                   GridSystemDataPtr<dim, dos> output);

  MeshPtr<dim> grid;
  FluxPtr<dim, dos> fluxPtr;
  SurfaceIntegrator<dim, 2> integrator;
};

template <int dim, int dos> using AdvectionSolverPtr = std::shared_ptr<AdvectionSolver<dim, dos>>;

} // namespace vox
#endif /* advection_solver_hpp */
