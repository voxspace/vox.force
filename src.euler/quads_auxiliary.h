//
//  quads_auxiliary.hpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef quads_auxiliary_hpp
#define quads_auxiliary_hpp

#include "mesh.h"
#include "polyInfo1d.h"
#include "polyInfo2d.h"
#include "polyInfo3d.h"

namespace vox {
template <int dim, int order> class QuadsAuxiliary {
public:
  static void getEleQuadPoint(MeshPtr<dim> g, Vector<size_t, dim> ele_idx,
                              std::vector<typename Mesh<dim>::point_t> &quad_pts);
  static void getEleInnerQuadPoint(const MeshPtr<dim> &g, const Vector<size_t, dim> &ele_idx,
                                   std::vector<typename Mesh<dim>::point_t> &quad_pts);
  static void getBryQuadPoint(MeshPtr<dim> g, Vector<size_t, dim> ele_idx,
                              std::vector<typename Mesh<dim>::point_t> &quad_pts);
};
template <int dim, int order> using QuadsAuxiliaryPtr = std::shared_ptr<QuadsAuxiliary<dim, order>>;

} // namespace vox
#endif /* quads_auxiliary_hpp */
