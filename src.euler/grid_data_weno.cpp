//
//  grid_data_weno.cpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "grid_data_weno.h"

namespace vox {

template <int dim, int order>
GridDataWENO<dim, order>::GridDataWENO(size_t idx, MeshPtr<dim> grid, WENOAuxiliaryPtr<dim, order> aux)
    : GridData<dim>(idx, grid), wenoAux(aux) {
  recon_cache.resize(_grid->dataSize());
}

template <int dim, int order>
GridDataWENO<dim, order>::GridDataWENO(const GridDataWENO &other) : GridData<dim>(other), wenoAux(other.wenoAux) {
  recon_cache = other.recon_cache;
}

template <int dim, int order> GridDataWENO<dim, order> &GridDataWENO<dim, order>::operator=(const GridDataWENO &other) {
  GridData<dim>::operator=(other);

  recon_cache = other.recon_cache;

  return *this;
}

template <int dim, int order> GridDataWENO<dim, order>::~GridDataWENO() = default;

template <int dim, int order> std::shared_ptr<GridData<dim>> GridDataWENO<dim, order>::deepCopy() {
  return std::shared_ptr<GridDataWENO<dim, order>>(new GridDataWENO<dim, order>(*this),
                                                   [](GridDataWENO<dim, order> *obj) { delete obj; });
}

// MARK:- Cell Reconstructed Function API
template <int dim, int order> double GridDataWENO<dim, order>::value(const point_t &pt, Vector<size_t, dim> idx) {
  return wenoAux->getPolyInfo()->funcValue(idx, pt, _gridData(idx), recon_cache(idx).Slope);
}

template <int dim, int order>
std::array<double, dim> GridDataWENO<dim, order>::gradient(const point_t &pt, Vector<size_t, dim> idx) {
  return wenoAux->getPolyInfo()->funcGradient(idx, pt, recon_cache(idx).Slope);
}

// MARK:- Reconstruction Solvers

template <int dim, int order> void GridDataWENO<dim, order>::calculateBasisFunction() {
  const double epsilon = 1.0e-04;

  if constexpr (dim == 1) {
    parallelFor(kZeroSize, _grid->dataSize()[0],
                [&](size_t ele_idx) -> void { calculateBasisFunction({ele_idx}, epsilon); });
  } else if constexpr (dim == 2) {
    parallelFor(kZeroSize, _grid->dataSize()[0], kZeroSize, _grid->dataSize()[1],
                [&](size_t ele_idx, size_t ele_idy) -> void {
                  calculateBasisFunction({ele_idx, ele_idy}, epsilon);
                });
  } else if constexpr (dim == 3) {
    parallelFor(kZeroSize, _grid->dataSize()[0], kZeroSize, _grid->dataSize()[1], kZeroSize, _grid->dataSize()[2],
                [&](size_t ele_idx, size_t ele_idy, size_t ele_idz) -> void {
                  calculateBasisFunction({ele_idx, ele_idy, ele_idz}, epsilon);
                });
  }
}

template <int dim, int order>
void GridDataWENO<dim, order>::calculateBasisFunction(Vector<size_t, dim> idx, double epsilon) {
  // initialization
  size_t index = recon_cache.index(idx);
  recon_cache(idx).grad_choice.resize(wenoAux->n_WENOPatch(index));

  for (size_t j = 0; j < wenoAux->n_WENOPatch(index); ++j) {
    calculateOnePatch(idx, j);
  }

  // WENO procedure
  recon_cache(idx).Slope.fill(0.0);
  const size_t n_choice = recon_cache(idx).grad_choice.size();

  std::vector<double> weights(n_choice, 0.0);
  double sum = 0;
  for (size_t j = 0; j < n_choice; ++j) {
    weights[j] = weight(recon_cache(idx).grad_choice[j], idx);
    weights[j] = 1. / pow(epsilon + weights[j], 2.); /// 2 for all previous tests
    sum += weights[j];
  }
  for (size_t j = 0; j < n_choice; ++j) {
    weights[j] /= sum;
  }

  for (size_t j = 0; j < n_choice; ++j) {
    recon_cache(idx).Slope += weights[j] * recon_cache(idx).grad_choice[j];
  }
}

template <int dim, int order> void GridDataWENO<dim, order>::calculateOnePatch(Vector<size_t, dim> idx, size_t j) {
  // prepare for stencil neighbor
  size_t index = recon_cache.index(idx);
  const ArrayView1<Vector<size_t, dim>> &ind = wenoAux->getPatch(index, j);
  // prepare for stencil bary
  std::vector<double> stencil_bary(ind.length());
  for (size_t k = 0; k < ind.length(); ++k) {
    stencil_bary[k] = _gridData(ind[k]);
  }

  // solve
  solve(idx, j, _gridData(idx), stencil_bary, recon_cache(idx).grad_choice[j]);
}

template <int dim, int order>
void GridDataWENO<dim, order>::solve(Vector<size_t, dim> idx, const size_t j, const double &UU,
                                     const std::vector<double> &bary_info, typename PolyInfo<dim, order>::Vec &result) {
  size_t index = recon_cache.index(idx);
  // prepare c
  typename PolyInfo<dim, order>::Vec cc;
  cc.fill(0.0);
  ArrayView1<std::array<double, PolyInfo<dim, order>::n_unknown>> s = wenoAux->getPolyAvgs(index, j);
  for (size_t l = 0; l < wenoAux->n_OnePatch(index, j); ++l) {
    for (int w = 0; w < PolyInfo<dim, order>::n_unknown; ++w) {
      cc[w] += -(bary_info[l] - UU) * s[l][w];
    }
  }

  // solve
  result = -*wenoAux->getGInv(index, j) * cc;
}

// MARK:- Weights function
template <> double GridDataWENO<1, 1>::weight(typename PolyInfo<1, 1>::Vec &slope, const Vector<size_t, 1> &idx) {
  return pow(slope[0], 2.) * _grid->AreaOfEle();
}

template <> double GridDataWENO<1, 2>::weight(typename PolyInfo<1, 2>::Vec &slope, const Vector<size_t, 1> &idx) {
  double H = slope[1] / _grid->SizeOfEle() * 2.0;

  VolumeIntegrator<1, 2> integrator(_grid->origin(), _grid->gridSpacing());

  return integrator.volumeIntegral<double>(
      [&](const Mesh<1>::point_t &pt) -> double {
        std::array<double, 1> g = gradient(pt, idx);

        return std::pow(g[0], 2) + H * H * _grid->AreaOfEle();
      },
      idx);
}

template <> double GridDataWENO<2, 1>::weight(typename PolyInfo<2, 1>::Vec &slope, const Vector<size_t, 2> &idx) {
  double weight = 0.0;
  for (int t = 0; t < PolyInfo<2, 1>::n_unknown; ++t) {
    weight += pow(slope[t], 2.) * _grid->AreaOfEle();
  }

  return weight;
}

template <> double GridDataWENO<2, 2>::weight(typename PolyInfo<2, 2>::Vec &slope, const Vector<size_t, 2> &idx) {
  // yy xy xx
  double Hyy = slope[2] / _grid->SizeOfEle() * 2.0;
  double Hxy = slope[3] / _grid->SizeOfEle();
  double Hxx = slope[4] / _grid->SizeOfEle() * 2.0;

  VolumeIntegrator<2, 2> integrator(_grid->origin(), _grid->gridSpacing());

  return integrator.volumeIntegral<double>(
      [&](const Mesh<2>::point_t &pt) -> double {
        std::array<double, 2> g = gradient(pt, idx);

        return std::pow(g[0], 2) + std::pow(g[1], 2) + (Hxx * Hxx + Hyy * Hyy + Hxy * Hxy) * _grid->AreaOfEle();
      },
      idx);
}

template <> double GridDataWENO<3, 1>::weight(typename PolyInfo<3, 1>::Vec &slope, const Vector3UZ &idx) {
  double weight = 0.0;
  for (int t = 0; t < PolyInfo<3, 1>::n_unknown; ++t) {
    weight += pow(slope[t], 2.) * _grid->AreaOfEle();
  }

  return weight;
}

template <> double GridDataWENO<3, 2>::weight(typename PolyInfo<3, 2>::Vec &slope, const Vector3UZ &idx) {
  // z^2, yz, y^2, xz, xy, x^2
  double Hzz = slope[3] / std::pow(_grid->SizeOfEle(), 2) * 2.0;
  double Hyz = slope[4] / std::pow(_grid->SizeOfEle(), 2);
  double Hyy = slope[5] / std::pow(_grid->SizeOfEle(), 2) * 2.0;
  double Hxz = slope[6] / std::pow(_grid->SizeOfEle(), 2);
  double Hxy = slope[7] / std::pow(_grid->SizeOfEle(), 2);
  double Hxx = slope[8] / std::pow(_grid->SizeOfEle(), 2) * 2.0;

  VolumeIntegrator<3, 2> integrator(_grid->origin(), _grid->gridSpacing());

  return integrator.volumeIntegral<double>(
      [&](const Mesh<3>::point_t &pt) -> double {
        std::array<double, 3> g = gradient(pt, idx);

        return std::pow(g[0], 2) + std::pow(g[1], 2) + std::pow(g[2], 2) +
               (Hzz * Hzz + Hyz * Hyz + Hyy * Hyy + Hxz * Hxz + Hxy * Hxy + Hxx * Hxx) / _grid->AreaOfEle();
      },
      idx);
}

//----------------------------------------------------------------------------------------------------------------------
// MARK:- Grid Data Builder
template <int dim, int order>
void GridDataWENO<dim, order>::Builder::setWENOAuxiliary(WENOAuxiliaryPtr<dim, order> aux) {
  wenoAux = aux;
}

template <int dim, int order> GridDataPtr<dim> GridDataWENO<dim, order>::Builder::build(size_t idx, MeshPtr<dim> grid) {
  return std::make_shared<GridDataWENO<dim, order>>(idx, grid, wenoAux);
}

template class GridDataWENO<1, 1>;
template class GridDataWENO<1, 2>;

template class GridDataWENO<2, 1>;
template class GridDataWENO<2, 2>;

template class GridDataWENO<3, 1>;
template class GridDataWENO<3, 2>;
} // namespace vox
