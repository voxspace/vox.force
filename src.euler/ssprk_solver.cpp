//
//  ssprk_solver.cpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "ssprk_solver.h"

namespace vox {

template <int dim, int dos>
void SSPRKSolver<dim, dos>::ssprk1(
    GridSystemDataPtr<dim, dos> init, double dt,
    std::function<void(GridSystemDataPtr<dim, dos> in, GridSystemDataPtr<dim, dos> out)> sourceFunc,
    GridSystemDataPtr<dim, dos> output) {
  sourceFunc(init, output);
  output->scaleSystemData(dt);
  output->addSystemData(*init);
}

template <int dim, int dos>
void SSPRKSolver<dim, dos>::ssprk2(
    GridSystemDataPtr<dim, dos> init, double dt,
    std::function<void(GridSystemDataPtr<dim, dos> in, GridSystemDataPtr<dim, dos> out)> sourceFunc,
    GridSystemDataPtr<dim, dos> output) {
  // step 1
  GridSystemDataPtr<dim, dos> midResult = init->deepCopy();
  sourceFunc(init, midResult);
  midResult->scaleSystemData(dt);
  midResult->addSystemData(*init);

  // step 2
  sourceFunc(midResult, output);
  output->scaleSystemData(dt);
  output->addSystemData(*midResult);

  output->addSystemData(*init);
  output->scaleSystemData(0.5);
}

template <int dim, int dos>
void SSPRKSolver<dim, dos>::ssprk3(
    GridSystemDataPtr<dim, dos> init, double dt,
    std::function<void(GridSystemDataPtr<dim, dos> in, GridSystemDataPtr<dim, dos> out)> sourceFunc,
    GridSystemDataPtr<dim, dos> output) {
  // step 1
  GridSystemDataPtr<dim, dos> midResult1 = init->deepCopy();
  sourceFunc(init, midResult1);
  midResult1->scaleSystemData(dt);
  midResult1->addSystemData(*init);

  // step 2
  GridSystemDataPtr<dim, dos> midResult2 = init->deepCopy();
  sourceFunc(midResult1, midResult2);
  midResult2->scaleSystemData(dt);
  midResult2->addSystemData(*midResult1);
  midResult2->scaleSystemData(0.25);
  midResult1 = init->deepCopy();
  midResult1->scaleSystemData(0.75);
  midResult2->addSystemData(*midResult1);

  // step 3
  sourceFunc(midResult2, output);
  output->scaleSystemData(dt);
  output->addSystemData(*midResult2);
  output->scaleSystemData(2.0);
  output->addSystemData(*init);
  output->scaleSystemData(1.0 / 3.0);
}

#define Implement(dim)                                                                                                 \
  template class SSPRKSolver<dim, 1>;                                                                                  \
  template class SSPRKSolver<dim, 2>;                                                                                  \
  template class SSPRKSolver<dim, 3>;                                                                                  \
  template class SSPRKSolver<dim, 4>;                                                                                  \
  template class SSPRKSolver<dim, 5>;                                                                                  \
  template class SSPRKSolver<dim, 6>;

Implement(1);
Implement(2);
Implement(3);
#undef Implement
} // namespace vox
