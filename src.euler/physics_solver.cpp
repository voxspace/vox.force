//
//  physics_solver.cpp
//  src.flame
//
//  Created by 杨丰 on 2020/5/14.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "physics_solver.h"
#include "solver_state.h"
#include "../src.common/timer.h"
#include <glog/logging.h>
#include <iomanip>
#include <iostream>

namespace vox {
template <int dim, int dos>
PhysicsSolver<dim, dos>::PhysicsSolver(SolverState<dim, dos> *state) : dt(0), mSolverState(state) {
  mGlobalClock = new GlobalClock;
  GlobalClock::getSingleton().start();
}

template <int dim, int dos> PhysicsSolver<dim, dos>::~PhysicsSolver() { delete mGlobalClock; }

template <int dim, int dos> void PhysicsSolver<dim, dos>::Run(double t_end) {
  bool finished = false;
  Timer timer;
  double total_time = 0;
  double step_time = 0;
  while (!finished) {
    mSolverState->timeStep(dt);
    if (GlobalClock::getSingleton().getTime() + dt > t_end) {
      dt = t_end - GlobalClock::getSingleton().getTime();
      finished = true;
    }

    timer.reset();
    mSolverState->update(dt);
    GlobalClock::getSingleton().accumulate(dt);
    step_time = timer.durationInSeconds();
    total_time += step_time;
    LOG(INFO) << "t =\t" << std::setprecision(7) << std::fixed << GlobalClock::getSingleton().getTime() << "\tdt =\t"
              << dt << "\t run time for this step is \t " << step_time << "s"
              << "\t total run time is \t " << total_time << "s" << std::endl;
  }
}

template <int dim, int dos> void PhysicsSolver<dim, dos>::StepRun(int step) {
  Timer timer;
  double total_time = 0;
  double step_time = 0;
  for (int i = 0; i < step; ++i) {
    mSolverState->timeStep(dt);

    timer.reset();
    mSolverState->update(dt);
    GlobalClock::getSingleton().accumulate(dt);
    step_time = timer.durationInSeconds();
    total_time += step_time;
    LOG(INFO) << "t =\t" << std::setprecision(7) << std::fixed << GlobalClock::getSingleton().getTime() << "\tdt =\t"
              << dt << "\t run time for this step is \t " << step_time << "s"
              << "\t total run time is \t " << total_time << "s" << std::endl;
  }
}

template <int dim, int dos> void PhysicsSolver<dim, dos>::initialize() { mSolverState->initialize(); }

template <int dim, int dos> void PhysicsSolver<dim, dos>::createScene() { mSolverState->createScene(); }

template <int dim, int dos> void PhysicsSolver<dim, dos>::preProcess() { mSolverState->preProcess(); }

template <int dim, int dos> void PhysicsSolver<dim, dos>::postProcess() { mSolverState->postProcess(); }

template <int dim, int dos> DualBuffer<dim, dos> *PhysicsSolver<dim, dos>::getDualBuffer() { return &buffer; }

#define Implement(dim)                                                                                                 \
  template class PhysicsSolver<dim, 1>;                                                                                \
  template class PhysicsSolver<dim, 2>;                                                                                \
  template class PhysicsSolver<dim, 3>;                                                                                \
  template class PhysicsSolver<dim, 4>;                                                                                \
  template class PhysicsSolver<dim, 5>;                                                                                \
  template class PhysicsSolver<dim, 6>;

Implement(1);
Implement(2);
Implement(3);
#undef Implement
} // namespace vox
