//  Copyright © 2020 Feng Yang. All rights reserved.
//
// I am making my contributions/submissions to this project solely in my
// personal capacity and am not conveying any rights to any intellectual
// property of any third parties.

#ifndef INCLUDE_VDB_GRID3_H_
#define INCLUDE_VDB_GRID3_H_

#include "../src.common/bounding_box.h"
#include "../src.common/matrix.h"
#include "../src.common/parallel.h"

#include <functional>
#include <memory>
#include <openvdb/openvdb.h>
#include <string>
#include <utility> // just make cpplint happy..
#include <vector>

namespace vdb {

//!
//! \brief Abstract base class for 3-D cartesian grid structure.
//!
//! This class represents 3-D cartesian grid structure. This class is an
//! abstract base class and does not store any data. The class only stores the
//! shape of the grid. The grid structure is axis-aligned and can have different
//! grid spacing per axis.
//!
class Grid3 {
public:
  //! Function type for mapping data index to actual position.
  using DataPositionFunc = std::function<vox::Vector3D(openvdb::Coord)>;

  //! Constructs an empty grid.
  Grid3() = default;

  //! Default destructor.
  virtual ~Grid3() = default;

  //! Returns the type name of derived grid.
  [[nodiscard]] virtual std::string typeName() const = 0;

  //! Returns the grid resolution.
  [[nodiscard]] const vox::Vector3UZ &resolution() const;

  //! Returns the grid origin.
  [[nodiscard]] const vox::Vector3D &origin() const;

  //! Returns the grid spacing.
  [[nodiscard]] const vox::Vector3D &gridSpacing() const;

  //! Returns the bounding box of the grid.
  [[nodiscard]] const vox::BoundingBox3D &boundingBox() const;

  //! Returns the function that maps grid index to the cell-center position.
  [[nodiscard]] DataPositionFunc cellCenterPosition() const;

  //!
  //! \brief Invokes the given function \p func for each grid cell.
  //!
  //! This function invokes the given function object \p func for each grid
  //! cell in serial manner. The input parameters are i, j, and k indices of a
  //! grid cell. The order of execution is i-first, j-next, k-last.
  //!
  void forEachCellIndex(const std::function<void(uint, uint, uint)> &func) const;

  //!
  //! \brief Invokes the given function \p func for each grid cell parallel.
  //!
  //! This function invokes the given function object \p func for each grid
  //! cell in parallel manner. The input parameters are i, j, and k indices of
  //! a grid cell. The order of execution can be arbitrary since it's
  //! multi-threaded.
  //!
  void parallelForEachCellIndex(const std::function<void(uint, uint, uint)> &func) const;

  //! Returns true if resolution, grid-spacing and origin are same.
  [[nodiscard]] bool hasSameShape(const Grid3 &other) const;

  //! Swaps the data with other grid.
  virtual void swap(Grid3 *other) = 0;

protected:
  //! Sets the size parameters including the resolution, grid spacing, and
  //! origin.
  void setSizeParameters(const vox::Vector3UZ &resolution, const vox::Vector3D &gridSpacing,
                         const vox::Vector3D &origin);

  //! Swaps the size parameters with given grid \p other.
  void swapGrid(Grid3 *other);

  //! Sets the size parameters with given grid \p other.
  void setGrid(const Grid3 &other);

private:
  vox::Vector3UZ _resolution;
  vox::Vector3D _gridSpacing = vox::Vector3D(1, 1, 1);
  vox::Vector3D _origin;
  vox::BoundingBox3D _boundingBox = vox::BoundingBox3D(vox::Vector3D(), vox::Vector3D());
};

using Grid3Ptr = std::shared_ptr<Grid3>;

#define JET_GRID3_TYPE_NAME(DerivedClassName)                                                                          \
  std::string typeName() const override { return #DerivedClassName; }

} // namespace vdb

#endif // INCLUDE_VDB_GRID3_H_
