//  Copyright © 2020 Feng Yang. All rights reserved.
//
// I am making my contributions/submissions to this project solely in my
// personal capacity and am not conveying any rights to any intellectual
// property of any third parties.

#ifndef INCLUDE_VDB_FDM_UTILS_H_
#define INCLUDE_VDB_FDM_UTILS_H_

#include "../src.common/matrix.h"
#include <iostream>
#include <openvdb/openvdb.h>

namespace vdb {
//! \brief Returns 3-D gradient vector from given 3-D scalar grid-like array
//!        \p data, \p gridSpacing, and array index (\p i, \p j, \p k).
vox::Vector3D gradient3(const openvdb::DoubleGrid::Ptr &data, const vox::Vector3UZ &size,
                        const vox::Vector3D &gridSpacing, int i, int j, int k);

//! \brief Returns 3-D gradient vectors from given 3-D vector grid-like array
//!        \p data, \p gridSpacing, and array index (\p i, \p j, \p k).
std::array<vox::Vector3D, 3> gradient3(const openvdb::Vec3dGrid::Ptr &data, const vox::Vector3UZ &size,
                                       const vox::Vector3D &gridSpacing, int i, int j, int k);

//! \brief Returns Laplacian value from given 3-D scalar grid-like array
//!        \p data, \p gridSpacing, and array index (\p i, \p j, \p k).
double laplacian3(const openvdb::DoubleGrid::Ptr &data, const vox::Vector3UZ &size, const vox::Vector3D &gridSpacing,
                  int i, int j, int k);

//! \brief Returns 3-D Laplacian vectors from given 3-D vector grid-like array
//!        \p data, \p gridSpacing, and array index (\p i, \p j, \p k).
vox::Vector3D laplacian3(const openvdb::Vec3dGrid::Ptr &data, const vox::Vector3UZ &size,
                         const vox::Vector3D &gridSpacing, int i, int j, int k);

} // namespace vdb

#endif // INCLUDE_VDB_FDM_UTILS_H_
