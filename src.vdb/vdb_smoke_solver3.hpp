//
//  vdb_smoke_solver3.hpp
//  Solvers
//
//  Created by Feng Yang on 2020/2/21.
//  Copyright © 2020 Feng Yang. All rights reserved.
//

#ifndef INCLUDE_VDB_GRID_SMOKE_SOLVER3_H_
#define INCLUDE_VDB_GRID_SMOKE_SOLVER3_H_

#include "vdb_flip_solver3.hpp"

namespace vdb {

//!
//! \brief      3-D grid-based smoke solver.
//!
//! This class extends GridFluidSolver3 to implement smoke simulation solver.
//! It adds smoke density and temperature fields to define the smoke and uses
//! buoyancy force to simulate hot rising smoke.
//!
//! \see Fedkiw, Ronald, Jos Stam, and Henrik Wann Jensen.
//!     "Visual simulation of smoke." Proceedings of the 28th annual conference
//!     on Computer graphics and interactive techniques. ACM, 2001.
//!
class GridSmokeSolver3 : public FlipSolver3 {
public:
  class Builder;

  //! Default constructor.
  GridSmokeSolver3();

  //! Constructs solver with initial grid size.
  GridSmokeSolver3(const vox::Vector3UZ &resolution, const vox::Vector3D &gridSpacing, const vox::Vector3D &gridOrigin);

  //! Deleted copy constructor.
  GridSmokeSolver3(const GridSmokeSolver3 &) = delete;

  //! Deleted move constructor.
  GridSmokeSolver3(GridSmokeSolver3 &&) noexcept = delete;

  //! Default virtual destructor.
  ~GridSmokeSolver3() override = default;

  //! Deleted copy assignment operator.
  GridSmokeSolver3 &operator=(const GridSmokeSolver3 &) = delete;

  //! Deleted move assignment operator.
  GridSmokeSolver3 &operator=(GridSmokeSolver3 &&) noexcept = delete;

  //! Returns smoke diffusion coefficient.
  [[nodiscard]] double smokeDiffusionCoefficient() const;

  //! Sets smoke diffusion coefficient.
  void setSmokeDiffusionCoefficient(double newValue);

  //! Returns temperature diffusion coefficient.
  [[nodiscard]] double temperatureDiffusionCoefficient() const;

  //! Sets temperature diffusion coefficient.
  void setTemperatureDiffusionCoefficient(double newValue);

  //!
  //! \brief      Returns the buoyancy factor which will be multiplied to the
  //!     smoke density.
  //!
  //! This class computes buoyancy by looking up the value of smoke density
  //! and temperature, compare them to the average values, and apply
  //! multiplier factor to the diff between the value and the average. That
  //! multiplier is defined for each smoke density and temperature separately.
  //! For example, negative smoke density buoyancy factor means a heavier
  //! smoke should sink.
  //!
  //! \return     The buoyancy factor for the smoke density.
  //!
  [[nodiscard]] double buoyancySmokeDensityFactor() const;

  //!
  //! \brief          Sets the buoyancy factor which will be multiplied to the
  //!     smoke density.
  //!
  //! This class computes buoyancy by looking up the value of smoke density
  //! and temperature, compare them to the average values, and apply
  //! multiplier factor to the diff between the value and the average. That
  //! multiplier is defined for each smoke density and temperature separately.
  //! For example, negative smoke density buoyancy factor means a heavier
  //! smoke should sink.
  //!
  //! \param newValue The new buoyancy factor for smoke density.
  //!
  void setBuoyancySmokeDensityFactor(double newValue);

  //!
  //! \brief      Returns the buoyancy factor which will be multiplied to the
  //!     temperature.
  //!
  //! This class computes buoyancy by looking up the value of smoke density
  //! and temperature, compare them to the average values, and apply
  //! multiplier factor to the diff between the value and the average. That
  //! multiplier is defined for each smoke density and temperature separately.
  //! For example, negative smoke density buoyancy factor means a heavier
  //! smoke should sink.
  //!
  //! \return     The buoyancy factor for the temperature.
  //!
  [[nodiscard]] double buoyancyTemperatureFactor() const;

  //!
  //! \brief          Sets the buoyancy factor which will be multiplied to the
  //!     temperature.
  //!
  //! This class computes buoyancy by looking up the value of smoke density
  //! and temperature, compare them to the average values, and apply
  //! multiplier factor to the diff between the value and the average. That
  //! multiplier is defined for each smoke density and temperature separately.
  //! For example, negative smoke density buoyancy factor means a heavier
  //! smoke should sink.
  //!
  //! \param newValue The new buoyancy factor for temperature.
  //!
  void setBuoyancyTemperatureFactor(double newValue);

  //!
  //! \brief      Returns smoke decay factor.
  //!
  //! In addition to the diffusion, the smoke also can fade-out over time by
  //! setting the decay factor between 0 and 1.
  //!
  //! \return     The decay factor for smoke density.
  //!
  [[nodiscard]] double smokeDecayFactor() const;

  //!
  //! \brief      Sets the smoke decay factor.
  //!
  //! In addition to the diffusion, the smoke also can fade-out over time by
  //! setting the decay factor between 0 and 1.
  //!
  //! \param[in]  newValue The new decay factor.
  //!
  void setSmokeDecayFactor(double newValue);

  //!
  //! \brief      Returns temperature decay factor.
  //!
  //! In addition to the diffusion, the smoke also can fade-out over time by
  //! setting the decay factor between 0 and 1.
  //!
  //! \return     The decay factor for smoke temperature.
  //!
  [[nodiscard]] double smokeTemperatureDecayFactor() const;

  //!
  //! \brief      Sets the temperature decay factor.
  //!
  //! In addition to the diffusion, the temperature also can fade-out over
  //! time by setting the decay factor between 0 and 1.
  //!
  //! \param[in]  newValue The new decay factor.
  //!
  void setTemperatureDecayFactor(double newValue);

  //! Returns smoke density field.
  [[nodiscard]] ScalarGrid3Ptr smokeDensity() const;

  //! Returns temperature field.
  [[nodiscard]] ScalarGrid3Ptr temperature() const;

  //! Returns builder fox GridSmokeSolver3.
  static Builder builder();

protected:
  void onEndAdvanceTimeStep(double timeIntervalInSeconds) override;

  void computeExternalForces(double timeIntervalInSeconds) override;

  //! Computes the advection term of the fluid solver.
  void computeAdvection(double timeIntervalInSeconds) override;

private:
  size_t _smokeDensityDataId{};
  size_t _temperatureDataId{};
  double _smokeDiffusionCoefficient = 0.0;
  double _temperatureDiffusionCoefficient = 0.0;
  double _buoyancySmokeDensityFactor = -0.000625;
  double _buoyancyTemperatureFactor = 5.0;
  double _smokeDecayFactor = 0.001;
  double _temperatureDecayFactor = 0.001;

  void computeDiffusion(double timeIntervalInSeconds);

  void computeBuoyancyForce(double timeIntervalInSeconds);
};

//! Shared pointer type for the GridSmokeSolver3.
using GridSmokeSolver3Ptr = std::shared_ptr<GridSmokeSolver3>;

//----------------------------------------------------------------------------------------------------------------------
//!
//! \brief Front-end to create GridSmokeSolver3 objects step by step.
//!
class GridSmokeSolver3::Builder final : public GridFluidSolverBuilderBase3<GridSmokeSolver3::Builder> {
public:
  //! Builds GridSmokeSolver3.
  [[nodiscard]] GridSmokeSolver3 build() const;

  //! Builds shared pointer of GridSmokeSolver3 instance.
  [[nodiscard]] GridSmokeSolver3Ptr makeShared() const;
};

} // namespace vdb

#endif // INCLUDE_VDB_GRID_SMOKE_SOLVER3_H_
