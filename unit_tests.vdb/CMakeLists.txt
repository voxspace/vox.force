CMAKE_MINIMUM_REQUIRED(VERSION 3.14)

# Target name
set(target unit_tests.vdb)

# Sources
file(GLOB sources
        ${CMAKE_CURRENT_SOURCE_DIR}/*.cpp
        "${CMAKE_CURRENT_SOURCE_DIR}/*.cu")
link_directories("/usr/local/lib")
add_definitions(-DJET_USE_CUDA)

# Build executable
add_executable(${target}
        ${sources})

# Project options
set_target_properties(
        ${target}
        PROPERTIES
        CUDA_SEPARABLE_COMPILATION ON)

# Link libraries
target_link_libraries(${target}
        PUBLIC
        ${DEFAULT_LINKER_OPTIONS}
        ${DEFAULT_LIBRARIES}
        gtest gtest_main gmock gmock_main Flex_VDB Flex_Common TBB::tbb)
