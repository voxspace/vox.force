//
//  quads_aux1d_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2019/6/30.
//  Copyright © 2019 杨丰. All rights reserved.
//

#include "../src.euler/mesh.h"
#include "../src.euler/quads_auxiliary.h"
#include <gtest/gtest.h>

using namespace vox;

class QuadsAux1DTest : public testing::Test {
public:
  void SetUp() override {
    grid = std::make_shared<Mesh<1>>(Vector<size_t, 1>({1000}), Vector<double, 1>({1.0 / 1000.0}));
    grid->addPBDescriptor(Direction::left);
  }

  const int order = 2;
  MeshPtr<1> grid;
};

TEST_F(QuadsAux1DTest, getEleInnerQuadPoint) {
  std::vector<Mesh1D::point_t> qi;
  QuadsAuxiliary<1, 2>::getEleInnerQuadPoint(grid, {0}, qi);

  const double h = 0.001;
  EXPECT_EQ(qi.size(), (size_t)1);
  EXPECT_EQ(qi[0][0], h / 2);
}

TEST_F(QuadsAux1DTest, getBryQuadPoint) {
  std::vector<Mesh1D::point_t> n;
  QuadsAuxiliary<1, 2>::getBryQuadPoint(grid, {0}, n);

  const double h = 0.001;
  EXPECT_EQ(n.size(), (size_t)2);
  EXPECT_EQ(n[0][0], 0.0);
  EXPECT_EQ(n[1][0], h);
}

TEST_F(QuadsAux1DTest, getEleQuadPoint) {
  std::vector<Mesh1D::point_t> n;
  QuadsAuxiliary<1, 2>::getEleQuadPoint(grid, {0}, n);

  const double h = 0.001;
  EXPECT_EQ(n.size(), (size_t)3);
  EXPECT_EQ(n[0][0], h / 2);
  EXPECT_EQ(n[1][0], 0.0);
  EXPECT_EQ(n[2][0], h);
}
