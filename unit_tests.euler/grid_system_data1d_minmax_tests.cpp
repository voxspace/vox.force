//
//  GridSysData1DMaxPrinciple_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2019/9/1.
//  Copyright © 2019 杨丰. All rights reserved.
//

#include "../src.euler/equations/equation_scalar.h"
#include "../src.euler/grid_system_data_minmax.h"
#include <gtest/gtest.h>

namespace vox {

class GridSysData1DMaxPrincipleTest : public testing::Test {
public:
  void SetUp() override {
    grid = std::make_shared<Mesh<1>>(Vector<size_t, 1>({1000}), Vector<double, 1>({1.0 / 1000.0}));
    grid->addPBDescriptor(Direction::left);

    std::array<EquationScalar<1>::fluxFunc, 1> flux = {ScalarEquationDataBase::Advection::flux(1.0)};
    std::array<EquationScalar<1>::fluxFunc, 1> fluxDer = {ScalarEquationDataBase::Advection::fluxDer(1.0)};
    std::array<EquationScalar<1>::viscousFunc, 1> vfuncs{};
    eqInfo = std::make_shared<EquationScalar<1>>(flux, fluxDer, vfuncs);

    data = GridSysDataMaxPrinciple<1, 2>::builder().build(grid, -1, 1);

    Vector1D tmp;
    for (size_t i = 0; i < grid->dataSize().length(); ++i) {
      init1d(grid->BaryCenter({i}), tmp);
      data->setVectorData({i}, tmp);
    }
  }

  MeshPtr<1> grid;
  EquationScalarPtr<1> eqInfo;
  GridSysDataMaxPrinciplePtr<1, 2> data;

  static void init1d(const Mesh1D::point_t &pt, Vector<double, 1> &value) { value[0] = sin(2 * M_PI * pt[0]); }

  static void bd1d(const Vector<double, 1> &val, Vector<double, 1> &result, const std::array<double, 1> &outNormal,
                   const Mesh1D::point_t &pt, int flag) {
    result = val;
  }

public:
  double theta(const Vector<size_t, 1> &idx) { return data->thetas(idx); }

  std::vector<Mesh1D::point_t> inner_qi(const Vector<size_t, 1> &idx) { return data->quad_pts(idx); }

  double minimum() { return data->minimum; }

  double maximum() { return data->maximum; }

  double deepCopyMin() { return std::dynamic_pointer_cast<GridSysDataMaxPrinciple<1, 2>>(data->deepCopy())->minimum; }

  double deepCopyMax() { return std::dynamic_pointer_cast<GridSysDataMaxPrinciple<1, 2>>(data->deepCopy())->maximum; }
};

TEST_F(GridSysData1DMaxPrincipleTest, minimum) { EXPECT_EQ(minimum(), -1); }

TEST_F(GridSysData1DMaxPrincipleTest, deepCopyMin) { EXPECT_EQ(deepCopyMin(), -1); }

TEST_F(GridSysData1DMaxPrincipleTest, maximum) { EXPECT_EQ(maximum(), 1); }

TEST_F(GridSysData1DMaxPrincipleTest, deepCopyMax) { EXPECT_EQ(deepCopyMax(), 1); }

TEST_F(GridSysData1DMaxPrincipleTest, innerqi) {
  std::vector<Mesh1D::point_t> qi = inner_qi({0});
  EXPECT_EQ(qi.size(), (size_t)3);

  const double h = 0.001;
  EXPECT_EQ(qi[0][0], h / 2.0);
  EXPECT_EQ(qi[1][0], 0);
  EXPECT_EQ(qi[2][0], h);
}

TEST_F(GridSysData1DMaxPrincipleTest, theta) {
  data->calculateBasisFunction();
  for (size_t i = 0; i < data->numberOfVectorData()[0]; ++i) {
    EXPECT_EQ(theta({i}), 1.0);
  }
}

} // namespace vox
