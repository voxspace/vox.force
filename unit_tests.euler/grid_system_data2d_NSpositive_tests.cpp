//
//  grid_system_data2d_NSpositive_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2020/5/16.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "../src.euler/equations/equation_NS.h"
#include "../src.euler/grid_system_data_NSPositive.h"
#include "../src.euler/mesh.h"
#include <gtest/gtest.h>

namespace vox {

class GridSysData2DNSPositiveTest : public testing::Test {
public:
  void SetUp() override {
    grid = std::make_shared<Mesh<2>>(Vector<size_t, 2>({10, 10}), Vector<double, 2>({0.1, 0.1}));

    eqInfo = std::make_shared<EquationNS<2>>(1.4);

    data = GridSysDataNSPositive<2, 2>::builder().build(grid, eqInfo);

    Vector4D tmp;
    for (size_t i = 0; i < grid->dataSize()[0]; ++i) {
      for (size_t j = 0; j < grid->dataSize()[1]; ++j) {
        Init(grid->BaryCenter({i, j}), tmp);
        data->setVectorData({i, j}, tmp);
      }
    }
  }

  MeshPtr<2> grid;
  EquationNSPtr<2> eqInfo;
  GridSysDataNSPositivePtr<2, 2> data;

  static void Init(const Mesh2D::point_t &x, Vector4D &u) {
    if (x[0] < .5) {
      u[0] = 1.0;
      u[1] = 0;
      u[2] = 0;
      u[3] = 2.5;
      /*
       u[0] = 0.445;
       u[1] = u[0]*0.698;
       u[2] = 0;
       u[3] = 3.528/0.4+0.5*0.445*0.698*0.698;
       */
    } else {
      u[0] = 0.125;
      u[1] = 0;
      u[2] = 0;
      u[3] = 0.25;
      /*
       u[0] = 0.5;
       u[1] = 0;
       u[2] = 0;
       u[3] = 0.571/0.4;
       */
    }
  }

  static void BoundaryCondition(const Vector4D &val, Vector4D &result, const std::array<double, 2> &outNormal,
                                const Mesh2D::point_t &pt, int flag) {
    result = val;
  }

public:
  double theta0(const Vector<size_t, 2> &idx) { return data->PositiveParas(idx).theta0; }

  double theta1(const Vector<size_t, 2> &idx) { return data->PositiveParas(idx).theta1; }

  std::vector<Mesh2D::point_t> inner_qi(const Vector<size_t, 2> &idx) { return data->PositiveParas(idx).quad_pts; }
};

TEST_F(GridSysData2DNSPositiveTest, Gamma) { EXPECT_EQ(eqInfo->Gamma, 1.4); }

TEST_F(GridSysData2DNSPositiveTest, innerqi) {
  std::vector<Mesh2D::point_t> n = inner_qi({0, 0});
  EXPECT_EQ(n.size(), (size_t)9);

  Mesh2D::point_t bc = grid->BaryCenter({0, 0});
  EXPECT_DOUBLE_EQ(n[0][0], bc[0]);
  EXPECT_DOUBLE_EQ(n[0][1], bc[1]);

  const double h = 0.1;
  EXPECT_DOUBLE_EQ(n[1][1], (1.0 - 1.0 / std::sqrt(3)) * h / 2.0);
  EXPECT_DOUBLE_EQ(n[1][0], 0);
  EXPECT_DOUBLE_EQ(n[2][1], (1.0 + 1.0 / std::sqrt(3)) * h / 2.0);
  EXPECT_DOUBLE_EQ(n[2][0], 0);

  EXPECT_DOUBLE_EQ(n[3][1], (1.0 - 1.0 / std::sqrt(3)) * h / 2.0);
  EXPECT_NEAR(n[3][0], h, 1.0e-12);
  EXPECT_DOUBLE_EQ(n[4][1], (1.0 + 1.0 / std::sqrt(3)) * h / 2.0);
  EXPECT_NEAR(n[4][0], h, 1.0e-12);

  EXPECT_DOUBLE_EQ(n[5][1], 0);
  EXPECT_NEAR(n[5][0], (1.0 - 1.0 / std::sqrt(3)) * h / 2.0, 1.0e-13);
  EXPECT_DOUBLE_EQ(n[6][1], 0);
  EXPECT_NEAR(n[6][0], (1.0 + 1.0 / std::sqrt(3)) * h / 2.0, 1.0e-12);

  EXPECT_DOUBLE_EQ(n[7][1], h);
  EXPECT_NEAR(n[7][0], (1.0 - 1.0 / std::sqrt(3)) * h / 2.0, 1.0e-13);
  EXPECT_DOUBLE_EQ(n[8][1], h);
  EXPECT_NEAR(n[8][0], (1.0 + 1.0 / std::sqrt(3)) * h / 2.0, 1.0e-12);
}

TEST_F(GridSysData2DNSPositiveTest, theta0) {
  data->calculateBasisFunction();
  EXPECT_EQ(theta0({0, 0}), 1.0);
}

TEST_F(GridSysData2DNSPositiveTest, theta1) {
  data->calculateBasisFunction();
  EXPECT_EQ(theta1({0, 0}), 1.0);
}

} // namespace vox
