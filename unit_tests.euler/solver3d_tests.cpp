//
//  Solver3d_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2019/8/25.
//  Copyright © 2019 杨丰. All rights reserved.
//

#include "../src.euler/advection_solver.h"
#include "../src.euler/dual_buffer.h"
#include "../src.euler/equations/equation_scalar.h"
#include "../src.euler/fluxes/flux_LF.h"
#include "../src.euler/grid_data_unlimited.h"
#include "../src.euler/mesh.h"
#include "../src.euler/recon_auxiliary.h"
#include "../src.euler/ssprk_solver.h"
#include <gtest/gtest.h>
#include <utility>

using namespace vox;

class Solver3DTest : public testing::Test {
public:
  void SetUp() override {
    grid = std::make_shared<Mesh<3>>(Vector3UZ({10, 10, 10}), Vector<double, 3>({0.1, 0.1, 0.1}));

    grid->addPBDescriptor(Direction::left);
    grid->addPBDescriptor(Direction::bottom);
    grid->addPBDescriptor(Direction::front);

    std::array<EquationScalar<3>::fluxFunc, 3> flux;
    flux[0] = ScalarEquationDataBase::Advection::flux(1.0);
    flux[1] = ScalarEquationDataBase::Advection::flux(2.0);
    flux[2] = ScalarEquationDataBase::Advection::flux(3.0);
    std::array<EquationScalar<3>::fluxFunc, 3> fluxDer;
    fluxDer[0] = ScalarEquationDataBase::Advection::fluxDer(1.0);
    fluxDer[1] = ScalarEquationDataBase::Advection::fluxDer(2.0);
    fluxDer[2] = ScalarEquationDataBase::Advection::fluxDer(3.0);
    std::array<EquationScalar<3>::viscousFunc, 3> vfuncs = {ScalarEquationDataBase::ViscousTerm::constVal(1.0),
                                                            ScalarEquationDataBase::ViscousTerm::constVal(1.0),
                                                            ScalarEquationDataBase::ViscousTerm::constVal(1.0)};

    eqInfo = std::make_shared<EquationScalar<3>>(flux, fluxDer, vfuncs);

    problemDef = std::make_shared<ProblemDef<3, 1>>(eqInfo, grid, bd3d, VisBoundaryCondition, init3d, 1, "3Dadv");

    ReconAuxiliaryPtr<3, 1> aux = std::make_shared<ReconAuxiliary<3, 1>>(grid);
    aux->buildReconstructPatch(PatchSearcher<3, 1>::Node);
    aux->updateLSMatrix();
    GridDataUnlimitedBuilderPtr<3, 1> UnlimitedDataBuilder = std::make_shared<GridDataUnlimited<3, 1>::Builder>();
    UnlimitedDataBuilder->setReconAuxiliary(aux);

    // set data structure
    buffer = new DualBuffer<3, 1>;
    buffer->setNewBuffer(GridSystemData<3, 1>::builder().build(UnlimitedDataBuilder, grid));
    buffer->setOldBuffer(buffer->newBuffer()->deepCopy());
  }

  MeshPtr<3> grid;
  EquationScalarPtr<3> eqInfo;
  ProblemDefPtr<3, 1> problemDef;
  DualBuffer<3, 1> *buffer;

  static void init3d(const Mesh3D::point_t &pt, Vector1D &value) {
    value[0] = std::sin(2 * M_PI * pt[0]) * std::sin(2 * M_PI * pt[1]) * std::sin(2 * M_PI * pt[2]);
  }

  static void bd3d(const Vector1D &val, Vector1D &result, const std::array<double, 3> &out_normal,
                   const Mesh3D::point_t &pt, int flag) {
    result = val;
  }

  static void VisBoundaryCondition(const Vector<Vector<double, 3>, 1> &val, Vector<Vector<double, 3>, 1> &result,
                                   const std::array<double, 3> &out_normal, const Mesh3D::point_t &pt, int flag) {
    result = val;
  }

public:
  void initialize() {
    Vector1D tmp;
    for (size_t i = 0; i < grid->dataSize()[0]; ++i) {
      for (size_t j = 0; j < grid->dataSize()[1]; ++j) {
        for (size_t k = 0; k < grid->dataSize()[2]; ++k) {
          problemDef->initialCondition(grid->BaryCenter({i, j, k}), tmp);
          buffer->newBuffer()->setVectorData({i, j, k}, tmp);
        }
      }
    }
  }

  double timeStep() {
    const double _CFL = 0.1;
    double dt = std::numeric_limits<double>::max();
    for (size_t i = 0; i < grid->dataSize()[0]; ++i) {
      for (size_t j = 0; j < grid->dataSize()[1]; ++j) {
        for (size_t k = 0; k < grid->dataSize()[2]; ++k) {
          dt = std::min(dt, grid->SizeOfEle() * _CFL /
                                eqInfo->MaxCharacteristicSpeed(buffer->newBuffer()->value({i, j, k})));
        }
      }
    }
    return dt;
  }

  void updateViscid(double dt) {
    AdvectionSolverPtr<3, 1> _advectionSolver =
        std::make_shared<AdvectionSolver<3, 1>>(grid, std::make_shared<FluxLF<3, 1>>());
    SSPRKSolverPtr<3, 1> _ssprkSolver = std::make_shared<SSPRKSolver<3, 1>>();

    // use dual buffer strategy to swap the pointer and avoid copy of all data
    _ssprkSolver->ssprk1(
        buffer->newBuffer(), dt,
        [&](const GridSystemDataPtr<3, 1> &in, GridSystemDataPtr<3, 1> out) -> void {
          in->calculateBasisFunction();
          _advectionSolver->advect(in, dt, problemDef, AdvectionSolver<3, 1>::Viscous_only, std::move(out));
        },
        buffer->oldBuffer());

    buffer->swapBuffer();
  }

  void updateViscous(double dt) {
    AdvectionSolverPtr<3, 1> _advectionSolver =
        std::make_shared<AdvectionSolver<3, 1>>(grid, std::make_shared<FluxLF<3, 1>>());

    buffer->newBuffer()->calculateBasisFunction();
    _advectionSolver->advect(buffer->newBuffer(), dt, problemDef, AdvectionSolver<3, 1>::Viscous_only,
                             buffer->oldBuffer());
    buffer->swapBuffer();
  }
};

TEST_F(Solver3DTest, Alloc) {
  GridSystemDataPtr<3, 1> newBuffer = buffer->newBuffer();
  EXPECT_EQ(newBuffer->numberOfVectorData()[0], (size_t)10);
  EXPECT_EQ(newBuffer->numberOfVectorData()[1], (size_t)10);
  EXPECT_EQ(newBuffer->numberOfVectorData()[2], (size_t)10);
}

TEST_F(Solver3DTest, swapBuffer) {
  initialize();
  buffer->swapBuffer();
  GridSystemDataPtr<3, 1> oldBuffer = buffer->oldBuffer();

  Mesh3D::point_t pt = grid->BaryCenter({0, 0, 3});
  EXPECT_EQ(oldBuffer->value({0, 0, 3})[0],
            std::sin(2 * M_PI * pt[0]) * std::sin(2 * M_PI * pt[1]) * std::sin(2 * M_PI * pt[2]));
}

TEST_F(Solver3DTest, initialize) {
  initialize();
  GridSystemDataPtr<3, 1> newBuffer = buffer->newBuffer();

  Mesh3D::point_t pt = grid->BaryCenter({0, 0, 3});
  EXPECT_EQ(newBuffer->value({0, 0, 3})[0],
            std::sin(2 * M_PI * pt[0]) * std::sin(2 * M_PI * pt[1]) * std::sin(2 * M_PI * pt[2]));
}

TEST_F(Solver3DTest, timeStep) {
  initialize();
  EXPECT_NEAR(0.01 / std::sqrt(1 + 4 + 9), timeStep(), 1.0e-13);
}

TEST_F(Solver3DTest, updateViscous) {
  initialize();
  updateViscous(timeStep());
  double vl = (buffer->newBuffer()->value({0, 0, 0}))[0];

  Vector<Vector<double, 3>, 1> cg1 = buffer->oldBuffer()->gradient(grid->BoundaryCenter({0, 0, 0}, front), {0, 0, 0});
  Vector<Vector<double, 3>, 1> cg2 = buffer->oldBuffer()->gradient(grid->BoundaryCenter({0, 0, 0}, back), {0, 0, 0});
  Vector<Vector<double, 3>, 1> cg3 = buffer->oldBuffer()->gradient(grid->BoundaryCenter({0, 0, 0}, bottom), {0, 0, 0});
  Vector<Vector<double, 3>, 1> cg4 = buffer->oldBuffer()->gradient(grid->BoundaryCenter({0, 0, 0}, up), {0, 0, 0});
  Vector<Vector<double, 3>, 1> cg5 = buffer->oldBuffer()->gradient(grid->BoundaryCenter({0, 0, 0}, left), {0, 0, 0});
  Vector<Vector<double, 3>, 1> cg6 = buffer->oldBuffer()->gradient(grid->BoundaryCenter({0, 0, 0}, right), {0, 0, 0});

  Vector<Vector<double, 3>, 1> leftg =
      buffer->oldBuffer()->gradient(grid->BoundaryCenter({0, 0, 9}, back), {0, 0, 9}); // up
  Vector<Vector<double, 3>, 1> rightg =
      buffer->oldBuffer()->gradient(grid->BoundaryCenter({0, 0, 1}, front), {0, 0, 1}); // bottom
  Vector<Vector<double, 3>, 1> bottomg =
      buffer->oldBuffer()->gradient(grid->BoundaryCenter({0, 9, 0}, up), {0, 9, 0}); // right
  Vector<Vector<double, 3>, 1> upg =
      buffer->oldBuffer()->gradient(grid->BoundaryCenter({0, 1, 0}, bottom), {0, 1, 0}); // left
  Vector<Vector<double, 3>, 1> frontg =
      buffer->oldBuffer()->gradient(grid->BoundaryCenter({9, 0, 0}, right), {9, 0, 0}); // front
  Vector<Vector<double, 3>, 1> backg =
      buffer->oldBuffer()->gradient(grid->BoundaryCenter({1, 0, 0}, left), {1, 0, 0}); // back

  std::array<double, 3> leftOut = {0.0, 0.0, -1.0};
  std::array<double, 3> rightOut = {0.0, 0.0, 1.0};
  std::array<double, 3> bottomOut = {0.0, -1.0, 0.0};
  std::array<double, 3> upOut = {0.0, 1.0, 0.0};
  std::array<double, 3> frontOut = {-1.0, 0.0, 0.0};
  std::array<double, 3> backOut = {1.0, 0.0, 0.0};

  double avg = -(leftg[0][0] + cg1[0][0]) / 2 * leftOut[0] * grid->LengthOfBry() -
               (leftg[0][1] + cg1[0][1]) / 2 * leftOut[1] * grid->LengthOfBry() -
               (leftg[0][2] + cg1[0][2]) / 2 * leftOut[2] * grid->LengthOfBry() -
               (rightg[0][0] + cg2[0][0]) / 2 * rightOut[0] * grid->LengthOfBry() -
               (rightg[0][1] + cg2[0][1]) / 2 * rightOut[1] * grid->LengthOfBry() -
               (rightg[0][2] + cg2[0][2]) / 2 * rightOut[2] * grid->LengthOfBry() -
               (bottomg[0][0] + cg3[0][0]) / 2 * bottomOut[0] * grid->LengthOfBry() -
               (bottomg[0][1] + cg3[0][1]) / 2 * bottomOut[1] * grid->LengthOfBry() -
               (bottomg[0][2] + cg3[0][2]) / 2 * bottomOut[2] * grid->LengthOfBry() -
               (upg[0][0] + cg4[0][0]) / 2 * upOut[0] * grid->LengthOfBry() -
               (upg[0][1] + cg4[0][1]) / 2 * upOut[1] * grid->LengthOfBry() -
               (upg[0][2] + cg4[0][2]) / 2 * upOut[2] * grid->LengthOfBry() -
               (frontg[0][0] + cg5[0][0]) / 2 * frontOut[0] * grid->LengthOfBry() -
               (frontg[0][1] + cg5[0][1]) / 2 * frontOut[1] * grid->LengthOfBry() -
               (frontg[0][2] + cg5[0][2]) / 2 * frontOut[2] * grid->LengthOfBry() -
               (backg[0][0] + cg6[0][0]) / 2 * backOut[0] * grid->LengthOfBry() -
               (backg[0][1] + cg6[0][1]) / 2 * backOut[1] * grid->LengthOfBry() -
               (backg[0][2] + cg6[0][2]) / 2 * backOut[2] * grid->LengthOfBry();
  avg /= grid->AreaOfEle();

  EXPECT_DOUBLE_EQ(vl, avg);
}
