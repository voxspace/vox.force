//
//  weno_aux2d_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2020/5/16.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "../src.euler/mesh.h"
#include "../src.euler/weno_auxiliary.h"
#include <gtest/gtest.h>

using namespace vox;

class WENOAux2DTest : public testing::Test {
public:
  void SetUp() override {
    grid = std::make_shared<Mesh<2>>(Vector<size_t, 2>({10, 10}), Vector<double, 2>({0.1, 0.1}));

    grid->addPBDescriptor(Direction::left);
    grid->addPBDescriptor(Direction::bottom);

    Aux = std::make_shared<WENOAuxiliary<2, 1>>(grid);
    Aux->buildReconstructPatch();
    Aux->buildWENOPatch();
    Aux->updateWENOInfo();
  }

  MeshPtr<2> grid;
  WENOAuxiliaryPtr<2, 1> Aux;
};

// MARK:- n_WENOPatch
TEST_F(WENOAux2DTest, n_WENOPatch) {
  EXPECT_EQ(Aux->n_WENOPatch(10), (size_t)5);
  EXPECT_EQ(Aux->n_WENOPatch(0), (size_t)5);
}

// MARK:- getPatch
TEST_F(WENOAux2DTest, getPatch) {
  ArrayView1<Vector<size_t, 2>> patch = Aux->getPatch(0, 0);
  EXPECT_EQ(patch.length(), (size_t)5);
  EXPECT_EQ(patch[0][0], 0);
  EXPECT_EQ(patch[0][1], 0);
  // up
  EXPECT_EQ(patch[1][0], 0);
  EXPECT_EQ(patch[1][1], 1);
  // bottom
  EXPECT_EQ(patch[2][0], 0);
  EXPECT_EQ(patch[2][1], 9);
  // left
  EXPECT_EQ(patch[3][0], 9);
  EXPECT_EQ(patch[3][1], 0);
  // right
  EXPECT_EQ(patch[4][0], 1);
  EXPECT_EQ(patch[4][1], 0);

  // right-------------------------------------
  patch = Aux->getPatch(0, 1);
  EXPECT_EQ(patch.length(), (size_t)5);
  EXPECT_EQ(patch[0][0], 1);
  EXPECT_EQ(patch[0][1], 0);
  // up
  EXPECT_EQ(patch[1][0], 1);
  EXPECT_EQ(patch[1][1], 1);
  // bottom
  EXPECT_EQ(patch[2][0], 1);
  EXPECT_EQ(patch[2][1], 9);
  // left
  EXPECT_EQ(patch[3][0], 0);
  EXPECT_EQ(patch[3][1], 0);
  // right
  EXPECT_EQ(patch[4][0], 2);
  EXPECT_EQ(patch[4][1], 0);

  // left--------------------------------------
  patch = Aux->getPatch(0, 2);
  EXPECT_EQ(patch.length(), (size_t)5);
  EXPECT_EQ(patch[0][0], 9);
  EXPECT_EQ(patch[0][1], 0);
  // up
  EXPECT_EQ(patch[1][0], 9);
  EXPECT_EQ(patch[1][1], 1);
  // bottom
  EXPECT_EQ(patch[2][0], 9);
  EXPECT_EQ(patch[2][1], 9);
  // left
  EXPECT_EQ(patch[3][0], 8);
  EXPECT_EQ(patch[3][1], 0);
  // right
  EXPECT_EQ(patch[4][0], 0);
  EXPECT_EQ(patch[4][1], 0);

  // up-----------------------------------------
  patch = Aux->getPatch(0, 3);
  EXPECT_EQ(patch.length(), (size_t)5);
  EXPECT_EQ(patch[0][0], 0);
  EXPECT_EQ(patch[0][1], 1);
  // up
  EXPECT_EQ(patch[1][0], 0);
  EXPECT_EQ(patch[1][1], 2);
  // bottom
  EXPECT_EQ(patch[2][0], 0);
  EXPECT_EQ(patch[2][1], 0);
  // left
  EXPECT_EQ(patch[3][0], 9);
  EXPECT_EQ(patch[3][1], 1);
  // right
  EXPECT_EQ(patch[4][0], 1);
  EXPECT_EQ(patch[4][1], 1);

  // bottom----------------------------------------
  patch = Aux->getPatch(0, 4);
  EXPECT_EQ(patch.length(), (size_t)5);
  EXPECT_EQ(patch[0][0], 0);
  EXPECT_EQ(patch[0][1], 9);
  // up
  EXPECT_EQ(patch[1][0], 0);
  EXPECT_EQ(patch[1][1], 0);
  // bottom
  EXPECT_EQ(patch[2][0], 0);
  EXPECT_EQ(patch[2][1], 8);
  // left
  EXPECT_EQ(patch[3][0], 9);
  EXPECT_EQ(patch[3][1], 9);
  // right
  EXPECT_EQ(patch[4][0], 1);
  EXPECT_EQ(patch[4][1], 9);
}

// MARK:- getGInv
TEST_F(WENOAux2DTest, getGInv) {
  auto InvA = Aux->getGInv(0);
  EXPECT_EQ(InvA.length(), (size_t)5);

  Matrix2x2D benchmark(0.5, 0, 0, 0.5);
  benchmark *= 100.0;
  for (int i = 0; i < 2; i++) {
    for (int j = 0; j < 2; j++) {
      EXPECT_NEAR(benchmark(i, j), InvA[0](i, j), 1.0e-10);
    }
  }

  // right
  benchmark = Matrix2x2D(0.5, 0, 0, 1.0 / 7); // y x
  benchmark *= 100.0;
  for (int i = 0; i < 2; i++) {
    for (int j = 0; j < 2; j++) {
      EXPECT_NEAR(benchmark(i, j), InvA[1](i, j), 1.0e-10);
    }
  }

  // left
  benchmark = Matrix2x2D(0.5, 0, 0, 1.0 / 7); // y x
  benchmark *= 100.0;
  for (int i = 0; i < 2; i++) {
    for (int j = 0; j < 2; j++) {
      EXPECT_NEAR(benchmark(i, j), InvA[2](i, j), 1.0e-10);
    }
  }

  // up
  benchmark = Matrix2x2D(1.0 / 7, 0, 0, 0.5); // y x
  benchmark *= 100.0;
  for (int i = 0; i < 2; i++) {
    for (int j = 0; j < 2; j++) {
      EXPECT_NEAR(benchmark(i, j), InvA[3](i, j), 1.0e-10);
    }
  }

  // bottom
  benchmark = Matrix2x2D(1.0 / 7, 0, 0, 0.5); // y x
  benchmark *= 100.0;
  for (int i = 0; i < 2; i++) {
    for (int j = 0; j < 2; j++) {
      EXPECT_NEAR(benchmark(i, j), InvA[4](i, j), 1.0e-10);
    }
  }
}
