//
//  gridData_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2019/8/21.
//  Copyright © 2019 杨丰. All rights reserved.
//

#include "../src.euler/grid_data.h"

#include <gtest/gtest.h>

using namespace vox;

class GridData1DTest : public testing::Test {
public:
  void SetUp() override {
    grid = std::make_shared<Mesh<1>>(Vector<size_t, 1>({1000}), Vector<double, 1>({1.0 / 1000.0}));
    grid->addPBDescriptor(Direction::left);

    data = std::make_shared<GridData<1>>(0, grid);
  }

  MeshPtr<1> grid;
  GridDataPtr<1> data;
};

TEST_F(GridData1DTest, numberOfData) { EXPECT_EQ(data->numberOfData().length(), (size_t)1000); }

TEST_F(GridData1DTest, operators) {
  data->operator[](10) = 23.32;
  EXPECT_EQ(data->operator[](10), 23.32);
}

TEST_F(GridData1DTest, IndexValue) {
  data->operator[](10) = 23.32;
  EXPECT_EQ(data->GridDataBase::value({10}), 23.32);
}

TEST_F(GridData1DTest, setConstantData) {
  data->setData(34.25);
  std::vector<double> tmp;
  data->getData(&tmp);
  EXPECT_EQ(tmp, std::vector<double>(1000, 34.25));
}

TEST_F(GridData1DTest, setVectorData) {
  data->setData(std::vector<double>(1000, 26.94));
  std::vector<double> tmp;
  data->getData(&tmp);
  EXPECT_EQ(tmp, std::vector<double>(1000, 26.94));
}

// MARK:- ArrayAccessorSize
TEST_F(GridData1DTest, ArrayAccessorSize) { EXPECT_EQ(data->scalarDataAt().length(), (size_t)1000); }

// MARK:- ArrayAccessorSet
TEST_F(GridData1DTest, ArrayAccessorSet) {
  data->scalarDataAt()(10) = 34.28;
  EXPECT_EQ(data->scalarDataAt().operator[](10), 34.28);
}

// MARK:- deepCopy
TEST_F(GridData1DTest, deepCopy) {
  data->setData(std::vector<double>(1000, 26.94));
  GridDataPtr<1> data2 = data->deepCopy();
  data2->setData(std::vector<double>(1000, 36.28));
  EXPECT_EQ(data2->operator[](10), 36.28);
  EXPECT_EQ(data->operator[](10), 26.94);
}

TEST_F(GridData1DTest, Pointvalue) {
  data->operator[](10) = 100.23;
  Mesh1D::point_t pt = {1.0};
  EXPECT_EQ(data->value(pt, {10}), 100.23);
}

TEST_F(GridData1DTest, Gradient) {
  data->operator[](10) = 100.23;
  Mesh1D::point_t pt = {1.0};
  std::array<double, 1> result{};
  EXPECT_EQ(data->gradient(pt, {10}), result);
}
