//
//  PatchSearcher_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2019/7/11.
//  Copyright © 2019 杨丰. All rights reserved.
//

#include "../src.euler/mesh.h"
#include "../src.euler/patch_searcher.h"
#include <gtest/gtest.h>
using namespace vox;

class PatchSearcherTest : public testing::Test {
public:
  void SetUp() override {
    eq1d = std::make_shared<Mesh<1>>(Vector<size_t, 1>({1000}));
    eq2d = std::make_shared<Mesh<2>>(Vector<size_t, 2>({100, 100}));
    eq3d = std::make_shared<Mesh<3>>(Vector3UZ({100, 100, 100}));
  }

  MeshPtr<1> eq1d;
  MeshPtr<2> eq2d;
  MeshPtr<3> eq3d;
};

// MARK:- 1D Period Neighbor
TEST_F(PatchSearcherTest, 1DNode) {
  eq1d->addPBDescriptor(Direction::left);

  PatchSearcher<1, 1> searcher(eq1d);
  std::vector<Vector<size_t, 1>> Patch;
  searcher.expand({0}, Patch);
  EXPECT_EQ(Patch.size(), (size_t)3);
  EXPECT_EQ(Patch[0][0], 999);
  EXPECT_EQ(Patch[1][0], 0);
  EXPECT_EQ(Patch[2][0], 1);
}

TEST_F(PatchSearcherTest, 1DNodeNode) {
  eq1d->addPBDescriptor(left);

  PatchSearcher<1, 3> searcher(eq1d);
  std::vector<Vector<size_t, 1>> Patch;
  searcher.expand({0}, Patch);
  EXPECT_EQ(Patch.size(), (size_t)5);
  EXPECT_EQ(Patch[0][0], 998);
  EXPECT_EQ(Patch[1][0], 999);
  EXPECT_EQ(Patch[2][0], 0);
  EXPECT_EQ(Patch[3][0], 1);
  EXPECT_EQ(Patch[4][0], 2);
}

// MARK:- 1D Non-Period Neighbor
TEST_F(PatchSearcherTest, 1D_Non_Period) {
  PatchSearcher<1, 1> searcher(eq1d);
  std::vector<Vector<size_t, 1>> Patch;
  searcher.expand({0}, Patch);

  EXPECT_EQ(Patch.size(), (size_t)2);
  EXPECT_EQ(Patch[0][0], 0);
  EXPECT_EQ(Patch[1][0], 1);
}
//----------------------------------------------------------------------------------------------------------------------
// MARK:- 2D Period Bry Neighbor
TEST_F(PatchSearcherTest, 2DBry) {
  eq2d->addPBDescriptor(Direction::left);
  eq2d->addPBDescriptor(Direction::bottom);

  PatchSearcher<2, 1> searcher(eq2d);
  std::vector<Vector<size_t, 2>> Patch;
  searcher.expand({0, 0}, Patch);

  EXPECT_EQ(Patch.size(), (size_t)5);
  EXPECT_EQ(Patch[0][0], 0);
  EXPECT_EQ(Patch[0][1], 0);

  EXPECT_EQ(Patch[1][0], 0);
  EXPECT_EQ(Patch[1][1], 1);

  EXPECT_EQ(Patch[2][0], 0);
  EXPECT_EQ(Patch[2][1], 99);

  EXPECT_EQ(Patch[3][0], 99);
  EXPECT_EQ(Patch[3][1], 0);

  EXPECT_EQ(Patch[4][0], 1);
  EXPECT_EQ(Patch[4][1], 0);
}

// MARK:- 2D Non-Period Bry Neighbor
TEST_F(PatchSearcherTest, 2D_Non_Period) {
  PatchSearcher<2, 1> searcher(eq2d);
  std::vector<Vector<size_t, 2>> Patch;
  searcher.expand({0, 0}, Patch);

  EXPECT_EQ(Patch.size(), (size_t)3);
  EXPECT_EQ(Patch[0][0], 0);
  EXPECT_EQ(Patch[0][1], 0);

  EXPECT_EQ(Patch[1][0], 0);
  EXPECT_EQ(Patch[1][1], 1);

  EXPECT_EQ(Patch[2][0], 1);
  EXPECT_EQ(Patch[2][1], 0);
}
//----------------------------------------------------------------------------------------------------------------------
// MARK:- 3D Period Bry Neighbor
TEST_F(PatchSearcherTest, 3DBry) {
  eq3d->addPBDescriptor(Direction::left);
  eq3d->addPBDescriptor(Direction::bottom);
  eq3d->addPBDescriptor(Direction::front);
  PatchSearcher<3, 1> search(eq3d);
  std::vector<Vector3UZ> Patch;
  search.expand({0, 0, 0}, Patch);

  EXPECT_EQ(Patch.size(), (size_t)7);
  EXPECT_EQ(Patch[0][0], 0);
  EXPECT_EQ(Patch[0][1], 0);
  EXPECT_EQ(Patch[0][2], 0);

  EXPECT_EQ(Patch[1][0], 0);
  EXPECT_EQ(Patch[1][1], 1);
  EXPECT_EQ(Patch[1][2], 0);

  EXPECT_EQ(Patch[2][0], 0);
  EXPECT_EQ(Patch[2][1], 99);
  EXPECT_EQ(Patch[2][2], 0);

  EXPECT_EQ(Patch[3][0], 99);
  EXPECT_EQ(Patch[3][1], 0);
  EXPECT_EQ(Patch[3][2], 0);

  EXPECT_EQ(Patch[4][0], 1);
  EXPECT_EQ(Patch[4][1], 0);
  EXPECT_EQ(Patch[4][2], 0);

  EXPECT_EQ(Patch[5][0], 0);
  EXPECT_EQ(Patch[5][1], 0);
  EXPECT_EQ(Patch[5][2], 1);

  EXPECT_EQ(Patch[6][0], 0);
  EXPECT_EQ(Patch[6][1], 0);
  EXPECT_EQ(Patch[6][2], 99);
}

// MARK:- 3D Non-Period Bry Neighbor
TEST_F(PatchSearcherTest, 3DBryBry) {
  PatchSearcher<3, 1> search(eq3d);
  std::vector<Vector3UZ> Patch;
  search.expand({0, 0, 0}, Patch);

  EXPECT_EQ(Patch.size(), (size_t)4);
  EXPECT_EQ(Patch[0][0], 0);
  EXPECT_EQ(Patch[0][1], 0);
  EXPECT_EQ(Patch[0][2], 0);

  EXPECT_EQ(Patch[1][0], 0);
  EXPECT_EQ(Patch[1][1], 1);
  EXPECT_EQ(Patch[1][2], 0);

  EXPECT_EQ(Patch[2][0], 1);
  EXPECT_EQ(Patch[2][1], 0);
  EXPECT_EQ(Patch[2][2], 0);

  EXPECT_EQ(Patch[3][0], 0);
  EXPECT_EQ(Patch[3][1], 0);
  EXPECT_EQ(Patch[3][2], 1);
}
