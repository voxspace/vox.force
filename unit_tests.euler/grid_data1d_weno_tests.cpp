//
//  gridData1dWENO_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2019/8/22.
//  Copyright © 2019 杨丰. All rights reserved.
//

#include "../src.euler/grid_data_weno.h"
#include "../src.euler/weno_auxiliary.h"

#include <gtest/gtest.h>

namespace vox {

class GridData1DWENOTest : public testing::Test {
public:
  void SetUp() override {
    grid = std::make_shared<Mesh<1>>(Vector<size_t, 1>({1000}), Vector<double, 1>({1.0 / 1000.0}));
    grid->addPBDescriptor(Direction::left);

    wenoAux = std::make_shared<WENOAuxiliary<1, order>>(grid);
    wenoAux->buildReconstructPatch();
    wenoAux->buildWENOPatch();
    wenoAux->updateWENOInfo();
    GridDataWENOBuilderPtr<1, order> dataBuilder = std::make_shared<GridDataWENO<1, order>::Builder>();
    dataBuilder->setWENOAuxiliary(wenoAux);

    data = std::dynamic_pointer_cast<GridDataWENO<1, order>>(dataBuilder->build(0, grid));
    for (size_t i = 0; i < grid->dataSize().length(); ++i) {
      data->operator[](i) = std::sin(2 * M_PI * grid->BaryCenter({i})[0]);
    }
    data->calculateBasisFunction();
  }

  static constexpr int order = 1;
  MeshPtr<1> grid;
  GridDataWENOPtr<1, order> data;
  WENOAuxiliaryPtr<1, order> wenoAux;

public:
  std::vector<typename PolyInfo<1, order>::Vec> grad_choice(int idx) { return data->recon_cache[idx].grad_choice; }

  double gradient(int idx) { return data->recon_cache[idx].Slope[0]; }
};

TEST_F(GridData1DWENOTest, grad_choice) {
  const double h = 0.001;
  auto grads = grad_choice(0);
  EXPECT_EQ(grads.size(), (size_t)3);

  double v_b = (grid->BaryCenter({1}) - grid->BaryCenter({0})).eval()[0] *
                   (data->GridDataBase::value({1}) - data->GridDataBase::value({0})) +
               ((grid->BaryCenter({999}) - grid->BaryCenter({0})).eval()[0] - 1.0) *
                   (data->GridDataBase::value({999}) - data->GridDataBase::value({0}));
  EXPECT_NEAR(grads[0][0], v_b / ((1 + 1) * h * h), 1.0e-12);

  v_b = (grid->BaryCenter({1}) - grid->BaryCenter({0})).eval()[0] *
            (data->GridDataBase::value({1}) - data->GridDataBase::value({0})) +
        ((grid->BaryCenter({2}) - grid->BaryCenter({0})).eval()[0]) *
            (data->GridDataBase::value({2}) - data->GridDataBase::value({0}));
  EXPECT_DOUBLE_EQ(grads[1][0], v_b / ((1 + 4) * h * h));

  v_b = ((grid->BaryCenter({998}) - grid->BaryCenter({0})).eval()[0] - 1.0) *
            (data->GridDataBase::value({998}) - data->GridDataBase::value({0})) +
        ((grid->BaryCenter({999}) - grid->BaryCenter({0})).eval()[0] - 1.0) *
            (data->GridDataBase::value({999}) - data->GridDataBase::value({0}));
  EXPECT_NEAR(grads[2][0], v_b / ((1 + 4) * h * h), 1.0e-12);
}

TEST_F(GridData1DWENOTest, gradient) {
  const double epsilon = 1.0e-04;
  auto grads = grad_choice(0);
  std::vector<double> weights(grads.size(), 0.0);

  double sum = 0;
  for (size_t j = 0; j < grads.size(); ++j) {
    weights[j] = pow(grads[j][0], 2.) * grid->AreaOfEle();
    weights[j] = 1. / pow(epsilon + weights[j], 2.);
    sum += weights[j];
  }
  for (size_t j = 0; j < grads.size(); ++j) {
    weights[j] /= sum;
  }

  double result = 0.0;
  for (size_t j = 0; j < grads.size(); ++j) {
    result += weights[j] * grads[j][0];
  }

  EXPECT_DOUBLE_EQ(gradient(0), result);
}

TEST_F(GridData1DWENOTest, value) {
  auto grad = gradient(0);
  EXPECT_DOUBLE_EQ(data->value(grid->BoundaryCenter({0}, right), {0}),
                   data->GridDataBase::value({0}) + grad * 0.001 / 2);
}

TEST_F(GridData1DWENOTest, gradientValue) {
  EXPECT_DOUBLE_EQ(data->gradient(grid->BoundaryCenter({0}, right), {0})[0], gradient(0));
}

TEST_F(GridData1DWENOTest, deepCopy) {
  GridDataWENOPtr<1, order> data2 = std::dynamic_pointer_cast<GridDataWENO<1, order>>(data->deepCopy());

  auto grad = gradient(0);
  EXPECT_DOUBLE_EQ(data2->value(grid->BoundaryCenter({0}, right), {0}),
                   data->GridDataBase::value({0}) + grad * 0.001 / 2);
}

} // namespace vox
