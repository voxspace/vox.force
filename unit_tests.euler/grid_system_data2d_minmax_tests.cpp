//
//  grid_system_data2d_minmax_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2020/5/16.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "../src.euler/equations/equation_scalar.h"
#include "../src.euler/grid_system_data_minmax.h"
#include "../src.euler/mesh.h"
#include <gtest/gtest.h>

namespace vox {

class GridSysData2DMaxPrincipleTest : public testing::Test {
public:
  void SetUp() override {
    grid = std::make_shared<Mesh<2>>(Vector<size_t, 2>({10, 10}), Vector<double, 2>({0.1, 0.1}));

    grid->addPBDescriptor(Direction::left);
    grid->addPBDescriptor(Direction::bottom);

    std::array<EquationScalar<2>::fluxFunc, 2> flux;
    flux[0] = ScalarEquationDataBase::Advection::flux(1.0);
    flux[1] = ScalarEquationDataBase::Advection::flux(2.0);
    std::array<EquationScalar<2>::fluxFunc, 2> fluxDer;
    fluxDer[0] = ScalarEquationDataBase::Advection::fluxDer(1.0);
    fluxDer[1] = ScalarEquationDataBase::Advection::fluxDer(2.0);
    std::array<EquationScalar<2>::viscousFunc, 2> vfuncs{};

    eqInfo = std::make_shared<EquationScalar<2>>(flux, fluxDer, vfuncs);

    data = GridSysDataMaxPrinciple<2, 2>::builder().build(grid, -1, 1);

    Vector1D tmp;
    for (size_t i = 0; i < grid->dataSize()[0]; ++i) {
      for (size_t j = 0; j < grid->dataSize()[1]; ++j) {
        init2d(grid->BaryCenter({i, j}), tmp);
        data->setVectorData({i, j}, tmp);
      }
    }
  }

  MeshPtr<2> grid;
  EquationScalarPtr<2> eqInfo;
  GridSysDataMaxPrinciplePtr<2, 2> data;

  static void init2d(const Mesh2D::point_t &pt, Vector1D &value) {
    value[0] = std::sin(2 * M_PI * pt[0]) * std::sin(2 * M_PI * pt[1]);
  }

  static void bd2d(const Vector1D &val, Vector1D &result, const std::array<double, 2> &out_normal,
                   const Mesh2D::point_t &pt, int flag) {
    result = val;
  }

public:
  double theta(const Vector<size_t, 2> &idx) { return data->thetas(idx); }

  std::vector<Mesh2D::point_t> inner_qi(const Vector<size_t, 2> &idx) { return data->quad_pts(idx); }

  double minimum() { return data->minimum; }

  double maximum() { return data->maximum; }

  double deepCopyMin() { return std::dynamic_pointer_cast<GridSysDataMaxPrinciple<2, 2>>(data->deepCopy())->minimum; }

  double deepCopyMax() { return std::dynamic_pointer_cast<GridSysDataMaxPrinciple<2, 2>>(data->deepCopy())->maximum; }
};

TEST_F(GridSysData2DMaxPrincipleTest, minimum) { EXPECT_EQ(minimum(), -1); }

TEST_F(GridSysData2DMaxPrincipleTest, deepCopyMin) { EXPECT_EQ(deepCopyMin(), -1); }

TEST_F(GridSysData2DMaxPrincipleTest, maximum) { EXPECT_EQ(maximum(), 1); }

TEST_F(GridSysData2DMaxPrincipleTest, deepCopyMax) { EXPECT_EQ(deepCopyMax(), 1); }

TEST_F(GridSysData2DMaxPrincipleTest, innerqi) {
  std::vector<Mesh2D::point_t> n = inner_qi({0, 0});
  EXPECT_EQ(n.size(), (size_t)9);

  Mesh2D::point_t bc = grid->BaryCenter({0, 0});
  EXPECT_DOUBLE_EQ(n[0][0], bc[0]);
  EXPECT_DOUBLE_EQ(n[0][1], bc[1]);

  const double h = 0.1;
  EXPECT_DOUBLE_EQ(n[1][1], (1.0 - 1.0 / std::sqrt(3)) * h / 2.0);
  EXPECT_DOUBLE_EQ(n[1][0], 0);
  EXPECT_DOUBLE_EQ(n[2][1], (1.0 + 1.0 / std::sqrt(3)) * h / 2.0);
  EXPECT_DOUBLE_EQ(n[2][0], 0);

  EXPECT_DOUBLE_EQ(n[3][1], (1.0 - 1.0 / std::sqrt(3)) * h / 2.0);
  EXPECT_NEAR(n[3][0], h, 1.0e-12);
  EXPECT_DOUBLE_EQ(n[4][1], (1.0 + 1.0 / std::sqrt(3)) * h / 2.0);
  EXPECT_NEAR(n[4][0], h, 1.0e-12);

  EXPECT_DOUBLE_EQ(n[5][1], 0);
  EXPECT_NEAR(n[5][0], (1.0 - 1.0 / std::sqrt(3)) * h / 2.0, 1.0e-13);
  EXPECT_DOUBLE_EQ(n[6][1], 0);
  EXPECT_NEAR(n[6][0], (1.0 + 1.0 / std::sqrt(3)) * h / 2.0, 1.0e-12);

  EXPECT_DOUBLE_EQ(n[7][1], h);
  EXPECT_NEAR(n[7][0], (1.0 - 1.0 / std::sqrt(3)) * h / 2.0, 1.0e-13);
  EXPECT_DOUBLE_EQ(n[8][1], h);
  EXPECT_NEAR(n[8][0], (1.0 + 1.0 / std::sqrt(3)) * h / 2.0, 1.0e-12);
}

TEST_F(GridSysData2DMaxPrincipleTest, theta) {
  data->calculateBasisFunction();
  for (size_t i = 0; i < grid->dataSize()[0]; ++i) {
    for (size_t j = 0; j < grid->dataSize()[1]; ++j) {
      EXPECT_EQ(theta({i, j}), 1.0);
    }
  }
}

} // namespace vox
