//
//  quads_aux3d_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2019/8/25.
//  Copyright © 2019 杨丰. All rights reserved.
//

#include "../src.euler/mesh.h"
#include "../src.euler/quads_auxiliary.h"
#include <gtest/gtest.h>

using namespace vox;

class QuadsAux3DTest : public testing::Test {
public:
  void SetUp() override {
    grid = std::make_shared<Mesh<3>>(Vector3UZ({10, 10, 10}), Vector<double, 3>({0.1, 0.1, 0.1}));

    grid->addPBDescriptor(Direction::left);
    grid->addPBDescriptor(Direction::bottom);
    grid->addPBDescriptor(Direction::front);
  }

  const int order = 2;
  MeshPtr<3> grid;
  QuadsAuxiliaryPtr<3, 2> quadsAux;
};

TEST_F(QuadsAux3DTest, getEleInnerQuadPoint) {
  std::vector<Mesh3D::point_t> qi;
  QuadsAuxiliary<3, 2>::getEleInnerQuadPoint(grid, {0, 0, 0}, qi);
  EXPECT_EQ(qi.size(), (size_t)1);

  Mesh3D::point_t bc = grid->BaryCenter({0, 0, 0});
  EXPECT_DOUBLE_EQ(qi[0][0], bc[0]);
  EXPECT_DOUBLE_EQ(qi[0][1], bc[1]);
  EXPECT_DOUBLE_EQ(qi[0][2], bc[2]);
}

TEST_F(QuadsAux3DTest, getBryQuadPoint) {
  std::vector<Mesh3D::point_t> n;
  quadsAux->getBryQuadPoint(grid, {0, 0, 0}, n);
  EXPECT_EQ(n.size(), (size_t)4 * 6);

  const double h = 0.1;
  {
    EXPECT_DOUBLE_EQ(n[0][0], 0.0);
    EXPECT_DOUBLE_EQ(n[0][1], -1.0 / std::sqrt(3) * h / 2.0 + h / 2);
    EXPECT_NEAR(n[0][2], -1.0 / std::sqrt(3) * h / 2.0 + h / 2, 1.0e-12);

    EXPECT_DOUBLE_EQ(n[1][0], 0.0);
    EXPECT_DOUBLE_EQ(n[1][1], -1.0 / std::sqrt(3) * h / 2.0 + h / 2);
    EXPECT_NEAR(n[1][2], 1.0 / std::sqrt(3) * h / 2.0 + h / 2, 1.0e-12);

    EXPECT_DOUBLE_EQ(n[2][0], 0.0);
    EXPECT_DOUBLE_EQ(n[2][1], 1.0 / std::sqrt(3) * h / 2.0 + h / 2);
    EXPECT_NEAR(n[2][2], -1.0 / std::sqrt(3) * h / 2.0 + h / 2, 1.0e-12);

    EXPECT_DOUBLE_EQ(n[3][0], 0.0);
    EXPECT_DOUBLE_EQ(n[3][1], 1.0 / std::sqrt(3) * h / 2.0 + h / 2);
    EXPECT_NEAR(n[3][2], 1.0 / std::sqrt(3) * h / 2.0 + h / 2, 1.0e-12);
  }

  {
    EXPECT_DOUBLE_EQ(n[4][0], h);
    EXPECT_DOUBLE_EQ(n[4][1], -1.0 / std::sqrt(3) * h / 2.0 + h / 2);
    EXPECT_NEAR(n[4][2], -1.0 / std::sqrt(3) * h / 2.0 + h / 2, 1.0e-12);

    EXPECT_DOUBLE_EQ(n[5][0], h);
    EXPECT_DOUBLE_EQ(n[5][1], -1.0 / std::sqrt(3) * h / 2.0 + h / 2);
    EXPECT_NEAR(n[5][2], 1.0 / std::sqrt(3) * h / 2.0 + h / 2, 1.0e-12);

    EXPECT_DOUBLE_EQ(n[6][0], h);
    EXPECT_DOUBLE_EQ(n[6][1], 1.0 / std::sqrt(3) * h / 2.0 + h / 2);
    EXPECT_NEAR(n[6][2], -1.0 / std::sqrt(3) * h / 2.0 + h / 2, 1.0e-12);

    EXPECT_DOUBLE_EQ(n[7][0], h);
    EXPECT_DOUBLE_EQ(n[7][1], 1.0 / std::sqrt(3) * h / 2.0 + h / 2);
    EXPECT_NEAR(n[7][2], 1.0 / std::sqrt(3) * h / 2.0 + h / 2, 1.0e-12);
  }

  {
    EXPECT_DOUBLE_EQ(n[8][0], -1.0 / std::sqrt(3) * h / 2.0 + h / 2);
    EXPECT_DOUBLE_EQ(n[8][1], 0.0);
    EXPECT_NEAR(n[8][2], -1.0 / std::sqrt(3) * h / 2.0 + h / 2, 1.0e-12);

    EXPECT_DOUBLE_EQ(n[9][0], -1.0 / std::sqrt(3) * h / 2.0 + h / 2);
    EXPECT_DOUBLE_EQ(n[9][1], 0.0);
    EXPECT_NEAR(n[9][2], 1.0 / std::sqrt(3) * h / 2.0 + h / 2, 1.0e-12);

    EXPECT_DOUBLE_EQ(n[10][0], 1.0 / std::sqrt(3) * h / 2.0 + h / 2);
    EXPECT_DOUBLE_EQ(n[10][1], 0.0);
    EXPECT_NEAR(n[10][2], -1.0 / std::sqrt(3) * h / 2.0 + h / 2, 1.0e-12);

    EXPECT_DOUBLE_EQ(n[11][0], 1.0 / std::sqrt(3) * h / 2.0 + h / 2);
    EXPECT_DOUBLE_EQ(n[11][1], 0.0);
    EXPECT_NEAR(n[11][2], 1.0 / std::sqrt(3) * h / 2.0 + h / 2, 1.0e-12);
  }

  {
    EXPECT_DOUBLE_EQ(n[12][0], -1.0 / std::sqrt(3) * h / 2.0 + h / 2);
    EXPECT_DOUBLE_EQ(n[12][1], h);
    EXPECT_NEAR(n[12][2], -1.0 / std::sqrt(3) * h / 2.0 + h / 2, 1.0e-12);

    EXPECT_DOUBLE_EQ(n[13][0], -1.0 / std::sqrt(3) * h / 2.0 + h / 2);
    EXPECT_DOUBLE_EQ(n[13][1], h);
    EXPECT_NEAR(n[13][2], 1.0 / std::sqrt(3) * h / 2.0 + h / 2, 1.0e-12);

    EXPECT_DOUBLE_EQ(n[14][0], 1.0 / std::sqrt(3) * h / 2.0 + h / 2);
    EXPECT_DOUBLE_EQ(n[14][1], h);
    EXPECT_NEAR(n[14][2], -1.0 / std::sqrt(3) * h / 2.0 + h / 2, 1.0e-12);

    EXPECT_DOUBLE_EQ(n[15][0], 1.0 / std::sqrt(3) * h / 2.0 + h / 2);
    EXPECT_DOUBLE_EQ(n[15][1], h);
    EXPECT_NEAR(n[15][2], 1.0 / std::sqrt(3) * h / 2.0 + h / 2, 1.0e-12);
  }

  {
    EXPECT_DOUBLE_EQ(n[16][0], -1.0 / std::sqrt(3) * h / 2.0 + h / 2);
    EXPECT_NEAR(n[16][1], -1.0 / std::sqrt(3) * h / 2.0 + h / 2, 1.0e-12);
    EXPECT_DOUBLE_EQ(n[16][2], 0.0);

    EXPECT_DOUBLE_EQ(n[17][0], -1.0 / std::sqrt(3) * h / 2.0 + h / 2);
    EXPECT_NEAR(n[17][1], 1.0 / std::sqrt(3) * h / 2.0 + h / 2, 1.0e-12);
    EXPECT_DOUBLE_EQ(n[17][2], 0.0);

    EXPECT_DOUBLE_EQ(n[18][0], 1.0 / std::sqrt(3) * h / 2.0 + h / 2);
    EXPECT_NEAR(n[18][1], -1.0 / std::sqrt(3) * h / 2.0 + h / 2, 1.0e-12);
    EXPECT_DOUBLE_EQ(n[18][2], 0.0);

    EXPECT_DOUBLE_EQ(n[19][0], 1.0 / std::sqrt(3) * h / 2.0 + h / 2);
    EXPECT_NEAR(n[19][1], 1.0 / std::sqrt(3) * h / 2.0 + h / 2, 1.0e-12);
    EXPECT_DOUBLE_EQ(n[19][2], 0.0);
  }

  {
    EXPECT_DOUBLE_EQ(n[20][0], -1.0 / std::sqrt(3) * h / 2.0 + h / 2);
    EXPECT_NEAR(n[20][1], -1.0 / std::sqrt(3) * h / 2.0 + h / 2, 1.0e-12);
    EXPECT_DOUBLE_EQ(n[20][2], h);

    EXPECT_DOUBLE_EQ(n[21][0], -1.0 / std::sqrt(3) * h / 2.0 + h / 2);
    EXPECT_NEAR(n[21][1], 1.0 / std::sqrt(3) * h / 2.0 + h / 2, 1.0e-12);
    EXPECT_DOUBLE_EQ(n[21][2], h);

    EXPECT_DOUBLE_EQ(n[22][0], 1.0 / std::sqrt(3) * h / 2.0 + h / 2);
    EXPECT_NEAR(n[22][1], -1.0 / std::sqrt(3) * h / 2.0 + h / 2, 1.0e-12);
    EXPECT_DOUBLE_EQ(n[22][2], h);

    EXPECT_DOUBLE_EQ(n[23][0], 1.0 / std::sqrt(3) * h / 2.0 + h / 2);
    EXPECT_NEAR(n[23][1], 1.0 / std::sqrt(3) * h / 2.0 + h / 2, 1.0e-12);
    EXPECT_DOUBLE_EQ(n[23][2], h);
  }
}

TEST_F(QuadsAux3DTest, getEleQuadPoint) {
  std::vector<Mesh3D::point_t> n;
  QuadsAuxiliary<3, 2>::getEleQuadPoint(grid, {0, 0, 0}, n);
  EXPECT_EQ(n.size(), (size_t)25);
}