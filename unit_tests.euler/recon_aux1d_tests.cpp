//
//  reconAux1d_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2019/11/27.
//  Copyright © 2019 杨丰. All rights reserved.
//

#include "../src.euler/mesh.h"
#include "../src.euler/recon_auxiliary.h"
#include <gtest/gtest.h>

using namespace vox;

class ReconAux1DTest : public testing::Test {
public:
  void SetUp() override {
    grid = std::make_shared<Mesh<1>>(Vector<size_t, 1>({1000}), Vector<double, 1>({1.0 / 1000.0}));
    grid->addPBDescriptor(Direction::left);

    reconAux = std::make_shared<ReconAuxiliary<1, 1>>(grid);
    reconAux->buildReconstructPatch();
    reconAux->updateLSMatrix();
  }

  static constexpr int order = 1;
  MeshPtr<1> grid;
  ReconAuxiliaryPtr<1, order> reconAux;
};

TEST_F(ReconAux1DTest, n_Patch) { EXPECT_EQ(reconAux->n_Patch(0), (size_t)3); }

TEST_F(ReconAux1DTest, getPatch) {
  const ArrayView1<Vector<size_t, 1>> &patch = reconAux->getPatch(0);
  EXPECT_EQ(patch.length(), (size_t)3);

  EXPECT_EQ(patch[0][0], 999);
  EXPECT_EQ(patch[1][0], 0);
  EXPECT_EQ(patch[2][0], 1);
}

TEST_F(ReconAux1DTest, getPolyAvgs) {
  const ArrayView1<std::array<double, PolyInfo<1, order>::n_unknown>> &m = reconAux->getPolyAvgs(0);
  EXPECT_EQ(m.length(), (size_t)3);

  const double h = 0.001;
  EXPECT_NEAR(m[0][0], -h, 1.0e-16);

  EXPECT_DOUBLE_EQ(m[1][0], 0.0);

  EXPECT_DOUBLE_EQ(m[2][0], h);
}

TEST_F(ReconAux1DTest, getG) {
  PolyInfo<1, order>::Mat GG = reconAux->getGInv({0})->inverse();
  const double h = 0.001;
  EXPECT_NEAR(2 * h * h, GG(0, 0), 1.0e-18);
}

TEST_F(ReconAux1DTest, getGInv) {
  PolyInfo<1, order>::Mat GG_inv = *reconAux->getGInv({0});
  const double h = 0.001;
  EXPECT_NEAR(1.0 / (2 * h * h), GG_inv(0, 0), 1.0e-7);
}
