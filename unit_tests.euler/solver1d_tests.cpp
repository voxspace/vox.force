//
//  Solver1d_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2019/6/12.
//  Copyright © 2019 杨丰. All rights reserved.
//

#include "../src.euler/advection_solver.h"
#include "../src.euler/dual_buffer.h"
#include "../src.euler/equations/equation_scalar.h"
#include "../src.euler/fluxes/flux_LF.h"
#include "../src.euler/grid_data_unlimited.h"
#include "../src.euler/recon_auxiliary.h"
#include "../src.euler/ssprk_solver.h"
#include <gtest/gtest.h>
#include <utility>

using namespace vox;

class Solver1DTest : public testing::Test {
public:
  void SetUp() override {
    grid = std::make_shared<Mesh<1>>(Vector<size_t, 1>({1000}), Vector<double, 1>({1.0 / 1000.0}));
    grid->addPBDescriptor(Direction::left);

    std::array<EquationScalar<1>::fluxFunc, 1> flux = {ScalarEquationDataBase::Advection::flux(1.0)};
    std::array<EquationScalar<1>::fluxFunc, 1> fluxDer = {ScalarEquationDataBase::Advection::fluxDer(1.0)};
    std::array<EquationScalar<1>::viscousFunc, 1> vfuncs = {ScalarEquationDataBase::ViscousTerm::constVal(1.0)};
    eqInfo = std::make_shared<EquationScalar<1>>(flux, fluxDer, vfuncs);

    problemDef = std::make_shared<ProblemDef<1, 1>>(eqInfo, grid, bd1d, VisBoundaryCondition, init1d, 1, "1Dadv");

    ReconAuxiliaryPtr<1, 1> aux = std::make_shared<ReconAuxiliary<1, 1>>(grid);
    aux->buildReconstructPatch();
    aux->updateLSMatrix();
    GridDataUnlimitedBuilderPtr<1, 1> UnlimitedDataBuilder = std::make_shared<GridDataUnlimited<1, 1>::Builder>();
    UnlimitedDataBuilder->setReconAuxiliary(aux);

    // set data structure
    buffer = new DualBuffer<1, 1>;
    buffer->setNewBuffer(GridSystemData<1, 1>::builder().build(UnlimitedDataBuilder, grid));
    buffer->setOldBuffer(buffer->newBuffer()->deepCopy());
  }

  MeshPtr<1> grid;
  EquationScalarPtr<1> eqInfo;
  ProblemDefPtr<1, 1> problemDef;
  DualBuffer<1, 1> *buffer{};

  static void init1d(const Mesh1D::point_t &pt, Vector1D &value) { value[0] = sin(2 * M_PI * pt[0]); }

  static void bd1d(const Vector1D &val, Vector1D &result, const std::array<double, 1> &out_normal,
                   const Mesh1D::point_t &pt, int flag) {
    result = val;
  }

  static void VisBoundaryCondition(const Vector<Vector<double, 1>, 1> &val, Vector<Vector<double, 1>, 1> &result,
                                   const std::array<double, 1> &out_normal, const Mesh1D::point_t &pt, int flag) {
    result = val;
  }

public:
  void initialize() {
    Vector1D tmp;
    for (size_t i = 0; i < buffer->newBuffer()->numberOfVectorData()[0]; i++) {
      problemDef->initialCondition(grid->BaryCenter({i}), tmp);

      buffer->newBuffer()->setVectorData({i}, tmp);
    }
  }

  double timeStep() {
    const double _CFL = 0.1;
    double dt = std::numeric_limits<double>::max();
    for (size_t i = 0; i < buffer->newBuffer()->numberOfVectorData()[0]; ++i) {
      dt = std::min(dt, grid->SizeOfEle() * _CFL / eqInfo->MaxCharacteristicSpeed(buffer->newBuffer()->value({i})));
    }

    return dt;
  }

  void updateViscid(double dt) {
    AdvectionSolverPtr<1, 1> _advectionSolver =
        std::make_shared<AdvectionSolver<1, 1>>(grid, std::make_shared<FluxLF<1, 1>>());
    SSPRKSolverPtr<1, 1> _ssprkSolver = std::make_shared<SSPRKSolver<1, 1>>();

    // use dual buffer strategy to swap the pointer and avoid copy of all data
    _ssprkSolver->ssprk1(
        buffer->newBuffer(), dt,
        [&](const GridSystemDataPtr<1, 1> &in, GridSystemDataPtr<1, 1> out) -> void {
          in->calculateBasisFunction();
          _advectionSolver->advect(in, dt, problemDef, AdvectionSolver<1, 1>::inviscid_only, std::move(out));
        },
        buffer->oldBuffer());

    buffer->swapBuffer();
  }

  void updateViscous(double dt) {
    AdvectionSolverPtr<1, 1> _advectionSolver =
        std::make_shared<AdvectionSolver<1, 1>>(grid, std::make_shared<FluxLF<1, 1>>());

    buffer->newBuffer()->calculateBasisFunction();
    _advectionSolver->advect(buffer->newBuffer(), dt, problemDef, AdvectionSolver<1, 1>::Viscous_only,
                             buffer->oldBuffer());
    buffer->swapBuffer();
  }
};

TEST_F(Solver1DTest, Alloc) {
  GridSystemDataPtr<1, 1> newBuffer = buffer->newBuffer();
  EXPECT_EQ(newBuffer->numberOfVectorData()[0], (size_t)1000);
}

TEST_F(Solver1DTest, swapBuffer) {
  initialize();
  buffer->swapBuffer();
  GridSystemDataPtr<1, 1> oldBuffer = buffer->oldBuffer();
  EXPECT_EQ(oldBuffer->value({1})[0], std::sin(2 * M_PI * 0.0015));
}

TEST_F(Solver1DTest, initialize) {
  initialize();

  GridSystemDataPtr<1, 1> newBuffer = buffer->newBuffer();
  EXPECT_EQ(newBuffer->value({1})[0], std::sin(2 * M_PI * 0.0015));
}

TEST_F(Solver1DTest, timeStep) {
  initialize();
  EXPECT_NEAR(0.0001, timeStep(), 1.0e-15);
}

TEST_F(Solver1DTest, updateViscid) {
  initialize();
  updateViscid(timeStep());
  double vl = (buffer->newBuffer()->value({0}))[0];
  EXPECT_NEAR(vl, 0.9 * std::sin(2 * M_PI * 0.0005) + 0.1 * std::sin(2 * M_PI * 0.9995), 1.0e-15);

  updateViscid(timeStep());
  vl = (buffer->newBuffer()->value({0}))[0];
  EXPECT_NEAR(vl, 0.8 * std::sin(2 * M_PI * 0.0005) + 0.2 * std::sin(2 * M_PI * 0.9995), 1.0e-8);
}

TEST_F(Solver1DTest, updateViscous) {
  initialize();
  updateViscous(timeStep());
  double vl = (buffer->newBuffer()->value({0}))[0];

  Vector<Vector<double, 1>, 1> lg = buffer->oldBuffer()->gradient(grid->BoundaryCenter({999}, right), {999});
  Vector<Vector<double, 1>, 1> cg1 = buffer->oldBuffer()->gradient(grid->BoundaryCenter({0}, left), {0});
  Vector<Vector<double, 1>, 1> cg2 = buffer->oldBuffer()->gradient(grid->BoundaryCenter({0}, right), {0});
  Vector<Vector<double, 1>, 1> rg = buffer->oldBuffer()->gradient(grid->BoundaryCenter({1}, left), {1});

  EXPECT_DOUBLE_EQ(vl, (lg[0][0] + cg1[0][0]) / 2 / 0.001 - (rg[0][0] + cg2[0][0]) / 2 / 0.001);
}
