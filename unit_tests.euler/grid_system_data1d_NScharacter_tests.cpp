//
//  GridSysData1DEulerCharacter_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2019/8/24.
//  Copyright © 2019 杨丰. All rights reserved.
//

#include "../src.euler/equations/equation_NS.h"
#include "../src.euler/grid_system_data_chars.h"
#include <gtest/gtest.h>

namespace vox {

class GridSysData1DNSCharacterTest : public testing::Test {
public:
  void SetUp() override {
    grid = std::make_shared<Mesh<1>>(Vector<size_t, 1>({1000}), Vector<double, 1>({1.0 / 1000.0}));

    Gamma = 1.4;
    eqInfo = std::make_shared<EquationNS<1>>(Gamma);

    data = GridSysDataNSCharacter<1>::builder().build(grid, eqInfo);

    Vector3D tmp;
    for (size_t i = 0; i < grid->dataSize().length(); ++i) {
      init1d(grid->BaryCenter({i}), tmp);
      data->setVectorData({i}, tmp);
    }
  }

  double Gamma{};
  MeshPtr<1> grid;
  EquationNSPtr<1> eqInfo;
  GridSysDataNSCharacterPtr<1> data;

  static void init1d(const Mesh1D::point_t &pt, Vector<double, 3> &u) {
    if (pt[0] > 0.5) {
      u[0] = 1.0;
      u[1] = 0.0;
      u[2] = 0.01 / 0.4;
    } else {
      u[0] = 1.0;
      u[1] = 0.0;
      u[2] = 1000. / 0.4;
    }
  }

  static void bd1d(const std::valarray<double> &val, std::valarray<double> &result,
                   const std::vector<double> &outNormal, const Mesh1D::point_t &pt, int flag) {
    result = val;
  }

public:
  Matrix3x3D RightEigenVec(const Vector3D &cons) {
    Matrix3x3D Trans;
    data->RightEigenVec(cons, cons, Trans);
    return Trans;
  }

  Vector3D cons2char(const Vector3D &cons, const Matrix3x3D &Trans) { return data->cons2char(cons, Trans); }

  Vector3D char2cons(const Vector3D &prim, const Matrix3x3D &Trans) { return data->char2cons(prim, Trans); }
};

TEST_F(GridSysData1DNSCharacterTest, RightEigenVec) {
  Vector3D val;
  val[0] = 1.0;
  val[1] = 2.0;
  val[2] = 3.0;
  Matrix3x3D Trans = RightEigenVec(val);

  double P = (Gamma - 1) * (val[2] - 0.5 * val[1] * val[1] / val[0]);
  double a = std::sqrt(Gamma * P / val[0]);
  double u = val[1] / val[0];
  Matrix3x3D Jacob(0, 1, 0, 0.5 * (Gamma - 3) * u * u, (3 - Gamma) * u, Gamma - 1,
                   0.5 * (Gamma - 2) * std::pow(u, 3) - a * a * u / (Gamma - 1),
                   0.5 * (3 - 2 * Gamma) * u * u + a * a / (Gamma - 1), Gamma * u);

  Matrix3x3D lambda(u - a, 0, 0, 0, u, 0, 0, 0, u + a);

  Matrix3x3D res = Jacob * Trans - Trans * lambda;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      EXPECT_NEAR(res(i, j), 0, 1.0e-14);
    }
  }
}

TEST_F(GridSysData1DNSCharacterTest, Trans) {
  Vector3D val = {1.0, 2.0, 3.0};
  Matrix3x3D Trans = RightEigenVec(val);

  val -= char2cons(cons2char(val, Trans), Trans);
  EXPECT_NEAR(val[0], 0.0, 1.0e-14);
  EXPECT_NEAR(val[1], 0.0, 1.0e-14);
  EXPECT_NEAR(val[2], 0.0, 1.0e-14);
}

TEST_F(GridSysData1DNSCharacterTest, Trans2) {
  data->calculateBasisFunction();
  Vector3D val = data->value(grid->BoundaryCenter({0}, right), {0});
  Vector3D avg = data->GridSystemData<1, 3>::value({0});
  EXPECT_DOUBLE_EQ(val[0], avg[0]);
  EXPECT_DOUBLE_EQ(val[1], avg[1]);
  EXPECT_DOUBLE_EQ(val[2], avg[2]);
}

} // namespace vox
