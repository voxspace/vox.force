//
//  grid_data2d_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2020/5/16.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "../src.euler/grid_data.h"
#include "../src.euler/mesh.h"

#include <gtest/gtest.h>

using namespace vox;

class GridData2DTest : public testing::Test {
public:
  void SetUp() override {
    grid = std::make_shared<Mesh<2>>(Vector<size_t, 2>({10, 10}), Vector<double, 2>({0.1, 0.1}));

    grid->addPBDescriptor(Direction::left);
    grid->addPBDescriptor(Direction::bottom);

    data = std::make_shared<GridData<2>>(0, grid);
  }

  MeshPtr<2> grid;
  GridDataPtr<2> data;
};

// MARK:- numberOfData
TEST_F(GridData2DTest, numberOfData) {
  auto total = product<size_t, 2>(data->numberOfData(), 1);
  EXPECT_EQ(total, (size_t)100);
}

// MARK:- deepCopy
TEST_F(GridData2DTest, deepCopy) {
  data->setData(std::vector<double>(100, 26.94));
  GridDataPtr<2> data2 = data->deepCopy();
  data2->setData(std::vector<double>(100, 36.28));
  EXPECT_EQ(data2->operator[](10), 36.28);
  EXPECT_EQ(data->operator[](10), 26.94);
}

// MARK:- PointValue
TEST_F(GridData2DTest, Pointvalue) {
  data->operator[](0) = 100.23;
  Mesh2D::point_t pt(1.0, 3.2);
  EXPECT_EQ(data->value(pt, {0, 0}), 100.23);
}

// MARK:- Gradient
TEST_F(GridData2DTest, Gradient) {
  data->operator[](0) = 100.23;
  Mesh2D::point_t pt(1.0, 3.2);
  std::array<double, 2> result{};
  EXPECT_EQ(data->gradient(pt, {0, 0}), result);
}
