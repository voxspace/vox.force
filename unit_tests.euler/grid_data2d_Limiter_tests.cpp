//
//  grid_data2d_Limiter_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2020/5/16.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "../src.euler/grid_data_limiter.h"
#include "../src.euler/mesh.h"
#include "../src.euler/recon_auxiliary.h"

#include <gtest/gtest.h>

namespace vox {

class GridData2DLimiterTest : public testing::Test {
public:
  void SetUp() override {
    grid = std::make_shared<Mesh<2>>(Vector<size_t, 2>({10, 10}), Vector<double, 2>({0.1, 0.1}));

    grid->addPBDescriptor(Direction::left);
    grid->addPBDescriptor(Direction::bottom);

    ReconAuxiliaryPtr<2, 1> aux = std::make_shared<ReconAuxiliary<2, 1>>(grid);
    aux->buildReconstructPatch();
    aux->updateLSMatrix();
    GridDataLimiterBuilderPtr<2> dataBuilder = std::make_shared<GridDataLimiter<2>::Builder>();
    dataBuilder->setLimiterAuxiliary(aux);
    dataBuilder->setLimiterType(GridDataLimiter<2>::BARTH);

    data = std::dynamic_pointer_cast<GridDataLimiter<2>>(dataBuilder->build(0, grid));
    for (size_t i = 0; i < grid->dataSize()[0]; ++i) {
      for (size_t j = 0; j < grid->dataSize()[1]; ++j) {
        data->operator()({i, j}) =
            std::sin(2 * M_PI * grid->BaryCenter({i, j})[0]) * std::sin(2 * M_PI * grid->BaryCenter({i, j})[1]);
      }
    }
    data->calculateBasisFunction();
  }

  MeshPtr<2> grid;
  GridDataLimiterPtr<2> data;

public:
  typename PolyInfo<2, 1>::Vec Slope(int idx) { return data->recon_cache[idx].Slope; }

  Vector2D LS() {
    // left right up down four neighbors
    Matrix2x2D G_inv(1.0 / 0.02, 0, 0, 1.0 / 0.02);

    Vector2D vb = Vector2D::makeZero();
    Vector2D tmp(0, 0.1);
    tmp *= data->GridDataBase::value({0, 1}) - data->GridDataBase::value({0, 0});
    vb -= tmp;

    tmp = Vector2D(0, -0.1);
    tmp *= data->GridDataBase::value({0, 9}) - data->GridDataBase::value({0, 0});
    vb -= tmp;

    tmp = Vector2D(-0.1, 0);
    tmp *= data->GridDataBase::value({9, 0}) - data->GridDataBase::value({0, 0});
    vb -= tmp;

    tmp = Vector2D(0.1, 0);
    tmp *= data->GridDataBase::value({1, 0}) - data->GridDataBase::value({0, 0});
    vb -= tmp;

    vb = -G_inv * vb;

    return vb;
  }
};

TEST_F(GridData2DLimiterTest, Slope) {
  auto slope = Slope(0);
  auto Grads = LS();

  const double h = 0.1;
  // right
  Mesh2D::point_t p1 = {0, h / 2};
  double right_val = data->GridDataBase::value({0, 1});
  // left
  Mesh2D::point_t p2 = {0, -h / 2};
  double left_val = data->GridDataBase::value({0, 9});
  // up
  Mesh2D::point_t p3 = {h / 2, 0};
  double up_val = data->GridDataBase::value({1, 0});
  // bottom
  Mesh2D::point_t p4 = {-h / 2, 0.0}; // use 0 will have problem in initial
  double bottom_val = data->GridDataBase::value({9, 0});

  Matrix<double, 4, 2> benchmark(p1[1], p1[0], p2[1], p2[0], p3[1], p3[0], p4[1], p4[0]);
  // y, x
  Vector4D bnd_vals = benchmark * Grads;

  double right_phi = 1.0;
  if (bnd_vals[0] > std::max(right_val - data->GridDataBase::value({0, 0}), 0.0)) {
    right_phi = std::max(right_val - data->GridDataBase::value({0, 0}), 0.0) / bnd_vals[0];
  } else if (bnd_vals[0] < std::min(right_val - data->GridDataBase::value({0, 0}), 0.0)) {
    right_phi = std::min(right_val - data->GridDataBase::value({0, 0}), 0.0) / bnd_vals[0];
  }

  double left_phi = 1.0;
  if (bnd_vals[1] > std::max(left_val - data->GridDataBase::value({0, 0}), 0.0)) {
    left_phi = std::max(left_val - data->GridDataBase::value({0, 0}), 0.0) / bnd_vals[1];
  } else if (bnd_vals[1] < std::min(left_val - data->GridDataBase::value({0, 0}), 0.0)) {
    left_phi = std::min(left_val - data->GridDataBase::value({0, 0}), 0.0) / bnd_vals[1];
  }

  double up_phi = 1.0;
  if (bnd_vals[2] > std::max(up_val - data->GridDataBase::value({0, 0}), 0.0)) {
    up_phi = std::max(up_val - data->GridDataBase::value({0, 0}), 0.0) / bnd_vals[2];
  } else if (bnd_vals[2] < std::min(up_val - data->GridDataBase::value({0, 0}), 0.0)) {
    up_phi = std::min(up_val - data->GridDataBase::value({0, 0}), 0.0) / bnd_vals[2];
  }

  double bottom_phi = 1.0;
  if (bnd_vals[3] > std::max(bottom_val - data->GridDataBase::value({0, 0}), 0.0)) {
    bottom_phi = std::max(bottom_val - data->GridDataBase::value({0, 0}), 0.0) / bnd_vals[3];
  } else if (bnd_vals[3] < std::min(bottom_val - data->GridDataBase::value({0, 0}), 0.0)) {
    bottom_phi = std::min(bottom_val - data->GridDataBase::value({0, 0}), 0.0) / bnd_vals[3];
  }

  Grads *= std::min(std::min(up_phi, bottom_phi), std::min(left_phi, right_phi));
  EXPECT_NEAR(slope[0], Grads[0], 1.0e-11);
  EXPECT_NEAR(slope[1], Grads[1], 1.0e-11);
}

TEST_F(GridData2DLimiterTest, value) {
  auto slope = Slope(0);
  Mesh2D::point_t pt = grid->BoundaryCenter({0, 0}, left);
  Mesh2D::point_t bc = grid->BaryCenter({0, 0});
  EXPECT_DOUBLE_EQ(data->value(grid->BoundaryCenter({0, 0}, left), {0, 0}),
                   data->GridDataBase::value({0, 0}) + slope[0] * (pt[1] - bc[1]) + slope[1] * (pt[0] - bc[0]));
}

TEST_F(GridData2DLimiterTest, gradientValue) {
  std::array<double, 2> slope = {Slope(0)[1], Slope(0)[0]}; // y, x
  EXPECT_EQ(data->gradient(grid->BoundaryCenter({0, 0}, left), {0, 0}), slope);
}

TEST_F(GridData2DLimiterTest, deepCopy) {
  GridDataLimiterPtr<2> data2 = std::dynamic_pointer_cast<GridDataLimiter<2>>(data->deepCopy());

  auto slope = Slope(0);
  Mesh2D::point_t pt = grid->BoundaryCenter({0, 0}, left);
  Mesh2D::point_t bc = grid->BaryCenter({0, 0});
  EXPECT_NEAR(data2->value(grid->BoundaryCenter({0, 0}, left), {0, 0}),
              data->GridDataBase::value({0, 0}) + slope[0] * (pt[1] - bc[1]) + slope[1] * (pt[0] - bc[0]), 1.0e-14);
}

} // namespace vox
