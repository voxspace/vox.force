//
//  polyInfo1D_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2019/11/27.
//  Copyright © 2019 杨丰. All rights reserved.
//

#include "../src.euler/mesh.h"
#include "../src.euler/polyInfo1d.h"
#include <gtest/gtest.h>

using namespace vox;

class PolyInfo1DTest : public testing::Test {
public:
  PolyInfo1DTest() : polyInfo(nullptr) {}

  void SetUp() override {
    grid = std::make_shared<Mesh<1>>(Vector<size_t, 1>({1000}), Vector<double, 1>({1.0 / 1000.0}));
    grid->addPBDescriptor(Direction::left);

    polyInfo.resetGrid(grid);
    polyInfo.loadBasisFunc();
  }

  static constexpr int order = 2;
  MeshPtr<1> grid;
  PolyInfo<1, order> polyInfo;
};

TEST_F(PolyInfo1DTest, n_unknown) { EXPECT_EQ((PolyInfo<1, order>::n_unknown), 2); }

TEST_F(PolyInfo1DTest, basisfuncValue) {
  double h = 0.001;

  std::array<double, PolyInfo<1, order>::n_unknown> result{};
  polyInfo.basisFuncValue({0}, grid->BoundaryCenter({0}, Direction::right), result);
  EXPECT_DOUBLE_EQ(result[0], h / 2.0);
  EXPECT_DOUBLE_EQ(result[1], (1.0 / 4.0 - 1.0 / 12.0) * h);
}

TEST_F(PolyInfo1DTest, funcValue) {
  typename PolyInfo<1, order>::Vec slope;
  EXPECT_EQ(slope.rows(), 2);

  slope[0] = 100;
  slope[1] = 200;
  double h = 0.001;
  double val = 20 + slope[0] / 2.0 * h + slope[1] * (1.0 / 4.0 - 1.0 / 12.0) * h;
  EXPECT_DOUBLE_EQ(polyInfo.funcValue({0}, grid->BoundaryCenter({0}, Direction::right), 20, slope), val);
}

TEST_F(PolyInfo1DTest, funcGradient) {
  typename PolyInfo<1, order>::Vec slope;
  slope[0] = 100;
  slope[1] = 200;
  EXPECT_DOUBLE_EQ(polyInfo.funcGradient({0}, grid->BoundaryCenter({0}, Direction::right), slope)[0], 100 + 200);
}

TEST_F(PolyInfo1DTest, averageBasisFunc) {
  const double h = 0.001;

  std::array<double, PolyInfo<1, order>::n_unknown> result{};
  polyInfo.averageBasisFunc({0}, {1}, result);
  EXPECT_DOUBLE_EQ(result[0], h);
  EXPECT_DOUBLE_EQ(result[1], h);

  polyInfo.averageBasisFunc({0}, {0}, result);
  EXPECT_DOUBLE_EQ(result[0], 0);
  EXPECT_DOUBLE_EQ(result[1], 0);

  polyInfo.averageBasisFunc({0}, {999}, result);
  EXPECT_NEAR(result[0], -h, 1.0e-16);
  EXPECT_NEAR(result[1], h, 1.0e-16);
}
