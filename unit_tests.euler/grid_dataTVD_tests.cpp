//
//  gridData1dTVD_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2019/8/29.
//  Copyright © 2019 杨丰. All rights reserved.
//

#include "../src.euler/grid_data_TVD.h"

#include <gtest/gtest.h>

namespace vox {

class GridDataTVDTest : public testing::Test {
public:
  void SetUp() override {
    grid = std::make_shared<Mesh<1>>(Vector<size_t, 1>({1000}), Vector<double, 1>({1.0 / 1000.0}));
    grid->addPBDescriptor(Direction::left);

    GridDataTVDBuilderPtr dataBuilder = std::make_shared<GridDataTVD::Builder>();
    dataBuilder->setLimiterType(GridDataTVD::MIN_MODE);

    data = std::dynamic_pointer_cast<GridDataTVD>(dataBuilder->build(0, grid));
    for (size_t i = 0; i < grid->dataSize().length(); ++i) {
      data->operator[](i) = std::sin(2 * M_PI * grid->BaryCenter({i})[0]);
    }
    data->calculateBasisFunction();
  }

  MeshPtr<1> grid;
  GridDataTVDPtr data;

public:
  double gradient(int idx) { return data->recon_cache[idx].gradient; }

  static double min_mod(double a, double b) {
    if (a > 0) {
      if (b > 0) {
        return std::min(a, b);
      }
      return 0;
    } else {
      if (b < 0) {
        return std::max(a, b);
      }
      return 0;
    }
  }
};

TEST_F(GridDataTVDTest, gradient) {
  double leftg = (data->GridDataBase::value({0}) - data->GridDataBase::value({999})) / (grid->SizeOfEle());
  double rightg = (data->GridDataBase::value({1}) - data->GridDataBase::value({0})) / (grid->SizeOfEle());

  EXPECT_DOUBLE_EQ(gradient(0), min_mod(leftg, rightg));
}

TEST_F(GridDataTVDTest, value) {
  auto grad = gradient(0);
  EXPECT_DOUBLE_EQ(data->value(grid->BoundaryCenter({0}, right), {0}),
                   data->GridDataBase::value({0}) + grad * 0.001 / 2);
}

TEST_F(GridDataTVDTest, gradientValue) {
  EXPECT_DOUBLE_EQ(data->gradient(grid->BoundaryCenter({0}, right), {0})[0], gradient(0));
}

TEST_F(GridDataTVDTest, deepCopy) {
  GridDataTVDPtr data2 = std::dynamic_pointer_cast<GridDataTVD>(data->deepCopy());

  auto grad = gradient(0);
  EXPECT_DOUBLE_EQ(data2->value(grid->BoundaryCenter({0}, right), {0}),
                   data->GridDataBase::value({0}) + grad * 0.001 / 2);
}

} // namespace vox
