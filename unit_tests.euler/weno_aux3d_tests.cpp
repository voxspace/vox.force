//
//  WENOAux3d_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2019/8/25.
//  Copyright © 2019 杨丰. All rights reserved.
//

#include "../src.euler/mesh.h"
#include "../src.euler/weno_auxiliary.h"
#include <gtest/gtest.h>

using namespace vox;

class WENOAux3DTest : public testing::Test {
public:
  void SetUp() override {
    grid = std::make_shared<Mesh<3>>(Vector3UZ({10, 10, 10}), Vector<double, 3>({0.1, 0.1, 0.1}));

    grid->addPBDescriptor(Direction::left);
    grid->addPBDescriptor(Direction::bottom);
    grid->addPBDescriptor(Direction::front);

    Aux = std::make_shared<WENOAuxiliary<3, 1>>(grid);
    Aux->buildReconstructPatch();
    Aux->buildWENOPatch();
    Aux->updateWENOInfo();
  }

  MeshPtr<3> grid;
  WENOAuxiliaryPtr<3, 1> Aux;
};

TEST_F(WENOAux3DTest, n_WENOPatch) {
  EXPECT_EQ(Aux->n_WENOPatch(10), (size_t)7);
  EXPECT_EQ(Aux->n_WENOPatch(0), (size_t)7);
}

TEST_F(WENOAux3DTest, getPatch) {
  Array3<double> index(grid->dataSize());

  ArrayView1<Vector3UZ> patch = Aux->getPatch(0, 0);
  EXPECT_EQ(patch.length(), (size_t)7);
  EXPECT_EQ(index.index(patch[0]), 0);
  EXPECT_EQ(index.index(patch[4]), 1);
  EXPECT_EQ(index.index(patch[3]), 9);
  EXPECT_EQ(index.index(patch[1]), 10);
  EXPECT_EQ(index.index(patch[2]), 90);
  EXPECT_EQ(index.index(patch[5]), 100);
  EXPECT_EQ(index.index(patch[6]), 900);

  // right-x
  patch = Aux->getPatch(0, 1);
  EXPECT_EQ(patch.length(), (size_t)7);
  EXPECT_EQ(index.index(patch[3]), 0);
  EXPECT_EQ(index.index(patch[0]), 1);
  EXPECT_EQ(index.index(patch[4]), 2);
  EXPECT_EQ(index.index(patch[1]), 11);
  EXPECT_EQ(index.index(patch[2]), 91);
  EXPECT_EQ(index.index(patch[5]), 101);
  EXPECT_EQ(index.index(patch[6]), 901);

  // left-x
  patch = Aux->getPatch(0, 2);
  EXPECT_EQ(patch.length(), (size_t)7);
  EXPECT_EQ(index.index(patch[4]), 0);
  EXPECT_EQ(index.index(patch[3]), 8);
  EXPECT_EQ(index.index(patch[0]), 9);
  EXPECT_EQ(index.index(patch[1]), 19);
  EXPECT_EQ(index.index(patch[2]), 99);
  EXPECT_EQ(index.index(patch[5]), 109);
  EXPECT_EQ(index.index(patch[6]), 909);

  // up-y
  patch = Aux->getPatch(0, 3);
  EXPECT_EQ(patch.length(), (size_t)7);
  EXPECT_EQ(index.index(patch[2]), 0);
  EXPECT_EQ(index.index(patch[0]), 10);
  EXPECT_EQ(index.index(patch[4]), 11);
  EXPECT_EQ(index.index(patch[3]), 19);
  EXPECT_EQ(index.index(patch[1]), 20);
  EXPECT_EQ(index.index(patch[5]), 110);
  EXPECT_EQ(index.index(patch[6]), 910);

  // bottom-y
  patch = Aux->getPatch(0, 4);
  EXPECT_EQ(patch.length(), (size_t)7);
  EXPECT_EQ(index.index(patch[1]), 0);
  EXPECT_EQ(index.index(patch[2]), 80);
  EXPECT_EQ(index.index(patch[0]), 90);
  EXPECT_EQ(index.index(patch[3]), 99);
  EXPECT_EQ(index.index(patch[4]), 91);
  EXPECT_EQ(index.index(patch[5]), 190);
  EXPECT_EQ(index.index(patch[6]), 990);

  // front-z
  patch = Aux->getPatch(0, 5);
  EXPECT_EQ(patch.length(), (size_t)7);
  EXPECT_EQ(index.index(patch[6]), 0);
  EXPECT_EQ(index.index(patch[4]), 101);
  EXPECT_EQ(index.index(patch[3]), 109);
  EXPECT_EQ(index.index(patch[0]), 100);
  EXPECT_EQ(index.index(patch[1]), 110);
  EXPECT_EQ(index.index(patch[2]), 190);
  EXPECT_EQ(index.index(patch[5]), 200);

  // back-z
  patch = Aux->getPatch(0, 6);
  EXPECT_EQ(patch.length(), (size_t)7);
  EXPECT_EQ(index.index(patch[5]), 0);
  EXPECT_EQ(index.index(patch[6]), 800);
  EXPECT_EQ(index.index(patch[0]), 900);
  EXPECT_EQ(index.index(patch[4]), 901);
  EXPECT_EQ(index.index(patch[3]), 909);
  EXPECT_EQ(index.index(patch[1]), 910);
  EXPECT_EQ(index.index(patch[2]), 990);
}

TEST_F(WENOAux3DTest, getGInv) {
  auto InvA = Aux->getGInv(0);
  EXPECT_EQ(InvA.length(), (size_t)7);

  Matrix3x3D benchmark(0.5, 0, 0, 0, 0.5, 0, 0, 0, 0.5);
  benchmark *= 100.0;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      EXPECT_NEAR(benchmark(i, j), InvA[0](i, j), 1.0e-10);
    }
  }

  // z, y, x
  benchmark = Matrix3x3D(0.5, 0, 0, 0, 0.5, 0, 0, 0, 1.0 / 9);
  benchmark *= 100.0;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      EXPECT_NEAR(benchmark(i, j), InvA[1](i, j), 1.0e-10);
    }
  }

  // z, y, x
  benchmark = Matrix3x3D(0.5, 0, 0, 0, 0.5, 0, 0, 0, 1.0 / 9);
  benchmark *= 100.0;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      EXPECT_NEAR(benchmark(i, j), InvA[2](i, j), 1.0e-10);
    }
  }

  // z, y, x
  benchmark = Matrix3x3D(0.5, 0, 0, 0, 1.0 / 9, 0, 0, 0, 0.5);
  benchmark *= 100.0;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      EXPECT_NEAR(benchmark(i, j), InvA[3](i, j), 1.0e-10);
    }
  }

  // z, y, x
  benchmark = Matrix3x3D(0.5, 0, 0, 0, 1.0 / 9, 0, 0, 0, 0.5);
  benchmark *= 100.0;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      EXPECT_NEAR(benchmark(i, j), InvA[4](i, j), 1.0e-10);
    }
  }

  // z, y, x
  benchmark = Matrix3x3D(1.0 / 9, 0, 0, 0, 0.5, 0, 0, 0, 0.5);
  benchmark *= 100.0;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      EXPECT_NEAR(benchmark(i, j), InvA[5](i, j), 1.0e-10);
    }
  }

  // z, y, x
  benchmark = Matrix3x3D(1.0 / 9, 0, 0, 0, 0.5, 0, 0, 0, 0.5);
  benchmark *= 100.0;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      EXPECT_NEAR(benchmark(i, j), InvA[6](i, j), 1.0e-10);
    }
  }
}
