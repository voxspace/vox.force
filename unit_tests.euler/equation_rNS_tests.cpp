//
//  EqREuler_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2019/8/21.
//  Copyright © 2019 杨丰. All rights reserved.
//

#include "../src.euler/equations/equation_reactingNS.h"
#include <gtest/gtest.h>

using namespace vox;

class EqRNSTest : public testing::Test {
public:
  void SetUp() override {
    Mu = 0.1;
    Kappa = 0.2;
    D = 0.3;
    eq = std::make_shared<EquationReactingNS<1>>(1.2, 50, Mu, Kappa, D);
  }

  double Mu;
  double Kappa;
  double D;
  EquationReactingNSPtr<1> eq;
};

TEST_F(EqRNSTest, getDos) { EXPECT_EQ(EquationReactingNS<1>::DOS, (size_t)1 + 3); }

TEST_F(EqRNSTest, getGamma) { EXPECT_EQ(eq->Gamma, 1.2); }

TEST_F(EqRNSTest, getQ) { EXPECT_EQ(eq->q0, 50); }

TEST_F(EqRNSTest, MaxCharacteristicSpeed) {
  // test for negative Pressure mechanics
  EXPECT_EQ(eq->MaxCharacteristicSpeed({1.0, 1.0, 1.0, 1.0}), 1);

  EXPECT_EQ(eq->MaxCharacteristicSpeed({1.0, 1.0, 100.0, 1.0}), 1 + std::sqrt(1.2 * 9.9));
}

TEST_F(EqRNSTest, MaxSpeeds) {
  double l, r;
  std::array<double, 1> norm = {1.0};
  eq->MaxSpeeds({1.0, 1.0, 100.0, 1.0}, norm, l, r);
  EXPECT_DOUBLE_EQ(l, 1 - std::sqrt(1.2 * 9.9));
  EXPECT_DOUBLE_EQ(r, 1 + std::sqrt(1.2 * 9.9));
}

TEST_F(EqRNSTest, TFlux1D) {
  Vector<double, EquationReactingNS<1>::DOS> flux;
  eq->TFlux({1.0, 1.0, 100.0, 1.0}, {1.0}, flux);
  EXPECT_EQ(flux.rows(), (size_t)4);
  EXPECT_DOUBLE_EQ(flux[0], 1);
  EXPECT_DOUBLE_EQ(flux[1], 1 + 9.9);
  EXPECT_DOUBLE_EQ(flux[2], 100 + 9.9);
  EXPECT_DOUBLE_EQ(flux[3], 1);
}

TEST_F(EqRNSTest, TFlux2D) {
  EquationReactingNSPtr<2> eq = std::make_shared<EquationReactingNS<2>>(1.2, 50);

  Vector<double, EquationReactingNS<2>::DOS> flux;
  eq->TFlux({1.0, 1.0, 1.0, 100.0, 1.0}, {1.0, 1.0}, flux);
  EXPECT_EQ(flux.rows(), (size_t)5);
  EXPECT_DOUBLE_EQ(flux[0], 2);
  EXPECT_DOUBLE_EQ(flux[1], 2 + 9.8);
  EXPECT_DOUBLE_EQ(flux[2], 2 + 9.8);
  EXPECT_DOUBLE_EQ(flux[3], 2 * (100 + 9.8));
  EXPECT_DOUBLE_EQ(flux[4], 2);
}

TEST_F(EqRNSTest, TFluxViscous) {
  Vector<double, EquationReactingNS<1>::DOS> flux;
  eq->TFlux({1.0, 1.0, 1.0, 1.0}, {{1.0}, {2.0}, {3.0}, {2.0}}, {1.0}, flux, true);
  EXPECT_EQ(flux.rows(), (size_t)4);

  double P = 1.0 / 3;
  double strain_rate = 1.0;
  double stress = 2 * Mu * (strain_rate - P);
  double J = -1.0 * D * 1.0;
  double q = -Kappa * 1.0;
  EXPECT_EQ(flux[0], 0.0);
  EXPECT_EQ(flux[1], stress);
  EXPECT_EQ(flux[2], stress - q);
  EXPECT_EQ(flux[3], -J);
}
