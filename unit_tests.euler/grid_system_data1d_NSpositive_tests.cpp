//
//  GridSysData1DEulerPositive_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2019/8/24.
//  Copyright © 2019 杨丰. All rights reserved.
//

#include "../src.euler/equations/equation_NS.h"
#include "../src.euler/grid_system_data_NSPositive.h"
#include <gtest/gtest.h>

namespace vox {

class GridSysData1DNSPositiveTest : public testing::Test {
public:
  void SetUp() override {
    grid = std::make_shared<Mesh<1>>(Vector<size_t, 1>({1000}), Vector<double, 1>({1.0 / 1000.0}));

    eqInfo = std::make_shared<EquationNS<1>>(1.4);

    data = GridSysDataNSPositive<1, 2>::builder().build(grid, eqInfo);

    Vector3D tmp;
    for (size_t i = 0; i < grid->dataSize().length(); ++i) {
      init1d(grid->BaryCenter({i}), tmp);
      data->setVectorData({i}, tmp);
    }
  }

  MeshPtr<1> grid;
  EquationNSPtr<1> eqInfo;
  GridSysDataNSPositivePtr<1, 2> data;

  static void init1d(const Mesh1D::point_t &pt, Vector3D &u) {
    if (pt[0] > 0.5) {
      u[0] = 1.0;
      u[1] = 0.0;
      u[2] = 0.01 / 0.4;
    } else {
      u[0] = 1.0;
      u[1] = 0.0;
      u[2] = 1000. / 0.4;
    }
  }

  static void bd1d(const Vector3D &val, Vector3D &result, const std::array<double, 3> &outNormal,
                   const Mesh1D::point_t &pt, int flag) {
    result = val;
  }

public:
  double theta0(const Vector<size_t, 1> &idx) { return data->PositiveParas(idx).theta0; }

  double theta1(const Vector<size_t, 1> &idx) { return data->PositiveParas(idx).theta1; }

  std::vector<Mesh1D::point_t> inner_qi(const Vector<size_t, 1> &idx) { return data->PositiveParas(idx).quad_pts; }
};

TEST_F(GridSysData1DNSPositiveTest, Gamma) { EXPECT_EQ(eqInfo->Gamma, 1.4); }

TEST_F(GridSysData1DNSPositiveTest, innerqi) {
  std::vector<Mesh1D::point_t> qi = inner_qi({0});
  EXPECT_EQ(qi.size(), (size_t)3);

  const double h = 0.001;
  EXPECT_EQ(qi[0][0], h / 2.0);
  EXPECT_EQ(qi[1][0], 0);
  EXPECT_EQ(qi[2][0], h);
}

TEST_F(GridSysData1DNSPositiveTest, theta0) {
  data->calculateBasisFunction();
  EXPECT_EQ(theta0({0}), 1.0);
}

TEST_F(GridSysData1DNSPositiveTest, theta1) {
  data->calculateBasisFunction();
  EXPECT_EQ(theta1({0}), 1.0);
}

} // namespace vox
