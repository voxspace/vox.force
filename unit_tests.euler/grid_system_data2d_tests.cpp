//
//  grid_system_data2d_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2020/5/16.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "../src.euler/equations/equation_scalar.h"
#include "../src.euler/error_approximator.h"
#include "../src.euler/global_clock.h"
#include "../src.euler/grid_system_data.h"
#include "../src.euler/mesh.h"
#include <gtest/gtest.h>

using namespace vox;

class GridSystemData2DTest : public testing::Test {
public:
  void SetUp() override {
    grid = std::make_shared<Mesh<2>>(Vector<size_t, 2>({10, 10}), Vector<double, 2>({0.1, 0.1}));

    grid->addPBDescriptor(Direction::left);
    grid->addPBDescriptor(Direction::bottom);

    std::array<EquationScalar<2>::fluxFunc, 2> flux;
    flux[0] = ScalarEquationDataBase::Advection::flux(1.0);
    flux[1] = ScalarEquationDataBase::Advection::flux(2.0);
    std::array<EquationScalar<2>::fluxFunc, 2> fluxDer;
    fluxDer[0] = ScalarEquationDataBase::Advection::fluxDer(1.0);
    fluxDer[1] = ScalarEquationDataBase::Advection::fluxDer(2.0);
    std::array<EquationScalar<2>::viscousFunc, 2> vfuncs{};

    eqInfo = std::make_shared<EquationScalar<2>>(flux, fluxDer, vfuncs);

    data = GridSystemData<2, 1>::builder().build(grid);
    Vector1D tmp;
    for (size_t i = 0; i < grid->dataSize()[0]; ++i) {
      for (size_t j = 0; j < grid->dataSize()[1]; ++j) {
        init2d(grid->BaryCenter({i, j}), tmp);
        data->setVectorData({i, j}, tmp);
      }
    }

    mGlobalClock = new GlobalClock;
  }

  void TearDown() override { delete mGlobalClock; }

  GlobalClock *mGlobalClock{};

  MeshPtr<2> grid;
  EquationScalarPtr<2> eqInfo;
  GridSystemDataPtr<2, 1> data;

  static void init2d(const Mesh2D::point_t &pt, Vector1D &value) {
    value[0] = std::sin(2 * M_PI * pt[0]) * std::sin(2 * M_PI * pt[1]);
  }

  static void bd2d(const Vector1D &val, Vector1D &result, const std::array<double, 2> &outNormal,
                   const Mesh2D::point_t &pt, int flag) {
    result = val;
  }
};

TEST_F(GridSystemData2DTest, scalarDataAt) {
  GridDataPtr<2> scalarData = data->scalarDataAt(0);
  EXPECT_DOUBLE_EQ(scalarData->GridDataBase::value({2, 3}),
                   std::sin(2 * M_PI * grid->BaryCenter({2, 3})[0]) * std::sin(2 * M_PI * grid->BaryCenter({2, 3})[1]));
}

TEST_F(GridSystemData2DTest, deepCopy) {
  GridDataPtr<2> scalarData = data->deepCopy()->scalarDataAt(0);
  EXPECT_DOUBLE_EQ(scalarData->GridDataBase::value({2, 3}),
                   std::sin(2 * M_PI * grid->BaryCenter({2, 3})[0]) * std::sin(2 * M_PI * grid->BaryCenter({2, 3})[1]));
}

TEST_F(GridSystemData2DTest, numberOfScalarData) { EXPECT_EQ(data->numberOfScalarData(), (size_t)1); }

TEST_F(GridSystemData2DTest, addSystemData) {
  GridSystemDataPtr<2, 1> data2 = GridSystemData<2, 1>::builder().build(grid);
  Vector1D tmp;
  for (size_t i = 0; i < grid->dataSize()[0]; ++i) {
    for (size_t j = 0; j < grid->dataSize()[1]; ++j) {
      tmp[0] = 1.0;
      data2->setVectorData({i, j}, tmp);
    }
  }

  data->addSystemData(*data2);
  EXPECT_DOUBLE_EQ(data->scalarDataAt(0)->GridDataBase::value({2, 3}),
                   std::sin(2 * M_PI * grid->BaryCenter({2, 3})[0]) * std::sin(2 * M_PI * grid->BaryCenter({2, 3})[1]) +
                       1);
}

TEST_F(GridSystemData2DTest, scaleSystemData) {
  data->scaleSystemData(0.1);
  EXPECT_DOUBLE_EQ(data->scalarDataAt(0)->GridDataBase::value({2, 3}),
                   std::sin(2 * M_PI * grid->BaryCenter({2, 3})[0]) * std::sin(2 * M_PI * grid->BaryCenter({2, 3})[1]) *
                       0.1);
}

TEST_F(GridSystemData2DTest, setSystemData) {
  GridSystemDataPtr<2, 1> data2 = GridSystemData<2, 1>::builder().build(grid);
  Vector1D tmp;
  for (size_t i = 0; i < grid->dataSize()[0]; ++i) {
    for (size_t j = 0; j < grid->dataSize()[1]; ++j) {
      tmp[0] = 1.0;
      data2->setVectorData({i, j}, tmp);
    }
  }

  data->setSystemData(*data2);
  EXPECT_DOUBLE_EQ(data->scalarDataAt(0)->GridDataBase::value({2, 3}), 1);
}

TEST_F(GridSystemData2DTest, clearSystemData) {
  data->clearSystemData();
  EXPECT_DOUBLE_EQ(data->scalarDataAt(0)->GridDataBase::value({2, 3}), 0.0);
}

TEST_F(GridSystemData2DTest, caculateBasisFunction) {
  data->calculateBasisFunction();
  EXPECT_DOUBLE_EQ(data->scalarDataAt(0)->value(grid->BoundaryCenter({0, 0}, left), {0, 0}),
                   std::sin(2 * M_PI * grid->BaryCenter({0, 0})[0]) * std::sin(2 * M_PI * grid->BaryCenter({0, 0})[1]));
}

TEST_F(GridSystemData2DTest, numberOfVectorData) {
  EXPECT_DOUBLE_EQ(data->numberOfVectorData()[0], grid->dataSize()[0]);
  EXPECT_DOUBLE_EQ(data->numberOfVectorData()[1], grid->dataSize()[1]);
}

TEST_F(GridSystemData2DTest, addVectorData) {
  Vector1D tmp = {1.0};
  data->addVectorData({2, 3}, tmp);
  EXPECT_DOUBLE_EQ(data->scalarDataAt(0)->GridDataBase::value({2, 3}),
                   std::sin(2 * M_PI * grid->BaryCenter({2, 3})[0]) * std::sin(2 * M_PI * grid->BaryCenter({2, 3})[1]) +
                       1.0);
  EXPECT_DOUBLE_EQ(data->scalarDataAt(0)->GridDataBase::value({2, 0}),
                   std::sin(2 * M_PI * grid->BaryCenter({2, 0})[0]) * std::sin(2 * M_PI * grid->BaryCenter({2, 0})[1]));
}

TEST_F(GridSystemData2DTest, value) {
  Vector1D tmp = data->value({0, 0});
  EXPECT_DOUBLE_EQ(tmp[0],
                   std::sin(2 * M_PI * grid->BaryCenter({0, 0})[0]) * std::sin(2 * M_PI * grid->BaryCenter({0, 0})[1]));
}

TEST_F(GridSystemData2DTest, ptValue) {
  Vector1D tmp = data->value(grid->BoundaryCenter({0, 0}, left), {0, 0});
  EXPECT_DOUBLE_EQ(tmp[0],
                   std::sin(2 * M_PI * grid->BaryCenter({0, 0})[0]) * std::sin(2 * M_PI * grid->BaryCenter({0, 0})[1]));
}

TEST_F(GridSystemData2DTest, gradient) {
  Vector<Vector<double, 2>, 1> tmp = data->gradient(grid->BoundaryCenter({0, 0}, left), {0, 0});
  EXPECT_EQ(tmp.rows(), (size_t)1);    // number of equation
  EXPECT_EQ(tmp[0].rows(), (size_t)2); // dim of space
  EXPECT_EQ(tmp[0][0], 0.0);
  EXPECT_EQ(tmp[0][1], 0.0);
}

TEST_F(GridSystemData2DTest, loadCache) {
  GridSystemDataPtr<2, 1> new_data = GridSystemData<2, 1>::builder().build(grid);
  mGlobalClock->start(0.125);

  data->save_cache("middleInfo");
  mGlobalClock->accumulate(0.555); // useless

  new_data->load_cache("middleInfo");
  EXPECT_DOUBLE_EQ(new_data->value({3, 5})[0], data->value({3, 5})[0]);
  EXPECT_DOUBLE_EQ(mGlobalClock->getTime(), 0.125);
}

TEST_F(GridSystemData2DTest, L1Error) {
  Vector1D error;
  double dx;
  std::array<std::function<double(const Mesh2D::point_t &pt)>, 1> realFunc;
  realFunc[0] = [&](const Mesh2D::point_t &pt) -> double { return 0; };

  ErrorApproximator<2, 1> L1ErrorApprox(grid);
  L1ErrorApprox.l1Error(data, realFunc, eqInfo, error, dx);

  double err = 0.0;
  for (size_t i = 0; i < grid->dataSize()[0]; ++i) {
    for (size_t j = 0; j < grid->dataSize()[1]; ++j) {
      err += grid->AreaOfEle() * std::abs(std::sin(2 * M_PI * grid->BaryCenter({i, j})[0]) *
                                          std::sin(2 * M_PI * grid->BaryCenter({i, j})[1]));
    }
  }

  EXPECT_DOUBLE_EQ(error[0], err);
}

TEST_F(GridSystemData2DTest, LinftyError) {
  Vector1D error;
  double dx;
  std::array<std::function<double(const Mesh2D::point_t &pt)>, 1> realFunc;
  realFunc[0] = [&](const Mesh2D::point_t &pt) -> double { return 0; };

  ErrorApproximator<2, 1> LinftyErrorApprox(grid);
  LinftyErrorApprox.linftyError(data, realFunc, eqInfo, error, dx);

  double err = 0.0;
  for (size_t i = 0; i < grid->dataSize()[0]; ++i) {
    for (size_t j = 0; j < grid->dataSize()[1]; ++j) {
      err = std::max(err, std::abs(std::sin(2 * M_PI * grid->BaryCenter({i, j})[0]) *
                                   std::sin(2 * M_PI * grid->BaryCenter({i, j})[1])));
    }
  }

  EXPECT_DOUBLE_EQ(error[0], err);
}
