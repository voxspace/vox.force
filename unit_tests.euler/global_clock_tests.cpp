//
//  Utility_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2019/8/25.
//  Copyright © 2019 杨丰. All rights reserved.
//

#include "../src.euler/global_clock.h"

#include <gtest/gtest.h>

using namespace vox;

TEST(GlobalClockTest, start) {
  auto *clock = new GlobalClock;
  clock->start();
  EXPECT_DOUBLE_EQ(GlobalClock::getSingleton().getTime(), 0.0);

  delete clock;
}

TEST(GlobalClockTest, accumulate) {
  auto *clock = new GlobalClock;
  clock->start();
  GlobalClock::getSingleton().accumulate(0.134);
  EXPECT_DOUBLE_EQ(GlobalClock::getSingleton().getTime(), 0.134);

  delete clock;
}
