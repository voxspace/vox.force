//
//  GridSystemData3D_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2019/8/25.
//  Copyright © 2019 杨丰. All rights reserved.
//

#include "../src.euler/equations/equation_scalar.h"
#include "../src.euler/error_approximator.h"
#include "../src.euler/global_clock.h"
#include "../src.euler/grid_system_data.h"
#include "../src.euler/mesh.h"
#include <gtest/gtest.h>

using namespace vox;

class GridSystemData3DTest : public testing::Test {
public:
  void SetUp() override {
    grid = std::make_shared<Mesh<3>>(Vector3UZ({10, 10, 10}), Vector<double, 3>({0.1, 0.1, 0.1}));

    grid->addPBDescriptor(Direction::left);
    grid->addPBDescriptor(Direction::bottom);
    grid->addPBDescriptor(Direction::front);

    std::array<EquationScalar<3>::fluxFunc, 3> flux;
    flux[0] = ScalarEquationDataBase::Advection::flux(1.0);
    flux[1] = ScalarEquationDataBase::Advection::flux(2.0);
    flux[2] = ScalarEquationDataBase::Advection::flux(3.0);
    std::array<EquationScalar<3>::fluxFunc, 3> fluxDer;
    fluxDer[0] = ScalarEquationDataBase::Advection::fluxDer(1.0);
    fluxDer[1] = ScalarEquationDataBase::Advection::fluxDer(2.0);
    fluxDer[2] = ScalarEquationDataBase::Advection::fluxDer(3.0);
    std::array<EquationScalar<3>::viscousFunc, 3> vfuncs{};

    eqInfo = std::make_shared<EquationScalar<3>>(flux, fluxDer, vfuncs);

    data = GridSystemData<3, 1>::builder().build(grid);
    Vector1D tmp;
    for (size_t i = 0; i < grid->dataSize()[0]; ++i) {
      for (size_t j = 0; j < grid->dataSize()[1]; ++j) {
        for (size_t k = 0; k < grid->dataSize()[2]; ++k) {
          init3d(grid->BaryCenter({i, j, k}), tmp);
          data->setVectorData({i, j, k}, tmp);
        }
      }
    }

    mGlobalClock = new GlobalClock;
  }

  void TearDown() override { delete mGlobalClock; }

  GlobalClock *mGlobalClock{};

  MeshPtr<3> grid;
  EquationScalarPtr<3> eqInfo;
  GridSystemDataPtr<3, 1> data;

  static void init3d(const Mesh3D::point_t &pt, Vector1D &value) {
    value[0] = std::sin(2 * M_PI * pt[0]) * std::sin(2 * M_PI * pt[1]) * std::sin(2 * M_PI * pt[2]);
  }

  static void bd3d(const Vector1D &val, Vector1D &result, const std::array<double, 3> &out_normal,
                   const Mesh3D::point_t &pt, int flag) {
    result = val;
  }
};

TEST_F(GridSystemData3DTest, scalarDataAt) {
  GridDataPtr<3> scalarData = data->scalarDataAt(0);
  EXPECT_DOUBLE_EQ(scalarData->GridDataBase::value({0, 2, 3}), std::sin(2 * M_PI * grid->BaryCenter({0, 2, 3})[0]) *
                                                                   std::sin(2 * M_PI * grid->BaryCenter({0, 2, 3})[1]) *
                                                                   std::sin(2 * M_PI * grid->BaryCenter({0, 2, 3})[2]));
}

TEST_F(GridSystemData3DTest, deepCopy) {
  GridDataPtr<3> scalarData = data->deepCopy()->scalarDataAt(0);
  EXPECT_DOUBLE_EQ(scalarData->GridDataBase::value({0, 2, 3}), std::sin(2 * M_PI * grid->BaryCenter({0, 2, 3})[0]) *
                                                                   std::sin(2 * M_PI * grid->BaryCenter({0, 2, 3})[1]) *
                                                                   std::sin(2 * M_PI * grid->BaryCenter({0, 2, 3})[2]));
}

TEST_F(GridSystemData3DTest, numberOfScalarData) { EXPECT_EQ(data->numberOfScalarData(), (size_t)1); }

TEST_F(GridSystemData3DTest, addSystemData) {
  GridSystemDataPtr<3, 1> data2 = GridSystemData<3, 1>::builder().build(grid);
  Vector1D tmp;
  for (size_t i = 0; i < grid->dataSize()[0]; ++i) {
    for (size_t j = 0; j < grid->dataSize()[1]; ++j) {
      for (size_t k = 0; k < grid->dataSize()[2]; ++k) {
        tmp[0] = 1.0;
        data2->setVectorData({i, j, k}, tmp);
      }
    }
  }

  data->addSystemData(*data2);
  EXPECT_DOUBLE_EQ(data->scalarDataAt(0)->GridDataBase::value({0, 2, 3}),
                   std::sin(2 * M_PI * grid->BaryCenter({0, 2, 3})[0]) *
                           std::sin(2 * M_PI * grid->BaryCenter({0, 2, 3})[1]) *
                           std::sin(2 * M_PI * grid->BaryCenter({0, 2, 3})[2]) +
                       1);
}

TEST_F(GridSystemData3DTest, scaleSystemData) {
  data->scaleSystemData(0.1);
  EXPECT_DOUBLE_EQ(data->scalarDataAt(0)->GridDataBase::value({0, 2, 3}),
                   std::sin(2 * M_PI * grid->BaryCenter({0, 2, 3})[0]) *
                       std::sin(2 * M_PI * grid->BaryCenter({0, 2, 3})[1]) *
                       std::sin(2 * M_PI * grid->BaryCenter({0, 2, 3})[2]) * 0.1);
}

TEST_F(GridSystemData3DTest, setSystemData) {
  GridSystemDataPtr<3, 1> data2 = GridSystemData<3, 1>::builder().build(grid);
  Vector1D tmp;
  for (size_t i = 0; i < grid->dataSize()[0]; ++i) {
    for (size_t j = 0; j < grid->dataSize()[1]; ++j) {
      for (size_t k = 0; k < grid->dataSize()[2]; ++k) {
        tmp[0] = 1.0;
        data2->setVectorData({i, j, k}, tmp);
      }
    }
  }

  data->setSystemData(*data2);
  EXPECT_DOUBLE_EQ(data->scalarDataAt(0)->GridDataBase::value({0, 2, 3}), 1);
}

TEST_F(GridSystemData3DTest, clearSystemData) {
  data->clearSystemData();
  EXPECT_DOUBLE_EQ(data->scalarDataAt(0)->GridDataBase::value({0, 2, 3}), 0.0);
}

TEST_F(GridSystemData3DTest, caculateBasisFunction) {
  data->calculateBasisFunction();
  EXPECT_DOUBLE_EQ(data->scalarDataAt(0)->value(grid->BoundaryCenter({0, 0, 0}, left), {0, 0, 0}),
                   std::sin(2 * M_PI * grid->BaryCenter({0, 0, 0})[0]) *
                       std::sin(2 * M_PI * grid->BaryCenter({0, 0, 0})[1]) *
                       std::sin(2 * M_PI * grid->BaryCenter({0, 0, 0})[2]));
}

TEST_F(GridSystemData3DTest, numberOfVectorData) {
  EXPECT_DOUBLE_EQ(data->numberOfVectorData()[0], grid->dataSize()[0]);
  EXPECT_DOUBLE_EQ(data->numberOfVectorData()[1], grid->dataSize()[1]);
  EXPECT_DOUBLE_EQ(data->numberOfVectorData()[2], grid->dataSize()[2]);
}

TEST_F(GridSystemData3DTest, addVectorData) {
  Vector1D tmp = {1.0};
  data->addVectorData({0, 2, 3}, tmp);
  EXPECT_DOUBLE_EQ(data->scalarDataAt(0)->GridDataBase::value({0, 2, 3}),
                   std::sin(2 * M_PI * grid->BaryCenter({0, 2, 3})[0]) *
                           std::sin(2 * M_PI * grid->BaryCenter({0, 2, 3})[1]) *
                           std::sin(2 * M_PI * grid->BaryCenter({0, 2, 3})[2]) +
                       1.0);
  EXPECT_DOUBLE_EQ(data->scalarDataAt(0)->GridDataBase::value({0, 2, 0}),
                   std::sin(2 * M_PI * grid->BaryCenter({0, 2, 0})[0]) *
                       std::sin(2 * M_PI * grid->BaryCenter({0, 2, 0})[1]) *
                       std::sin(2 * M_PI * grid->BaryCenter({0, 2, 0})[2]));
}

TEST_F(GridSystemData3DTest, value) {
  Vector1D tmp = data->value({0, 0, 0});
  EXPECT_DOUBLE_EQ(tmp[0], std::sin(2 * M_PI * grid->BaryCenter({0, 0, 0})[0]) *
                               std::sin(2 * M_PI * grid->BaryCenter({0, 0, 0})[1]) *
                               std::sin(2 * M_PI * grid->BaryCenter({0, 0, 0})[2]));
}

TEST_F(GridSystemData3DTest, ptValue) {
  Vector1D tmp = data->value(grid->BoundaryCenter({0, 0, 0}, left), {0, 0, 0});
  EXPECT_DOUBLE_EQ(tmp[0], std::sin(2 * M_PI * grid->BaryCenter({0, 0, 0})[0]) *
                               std::sin(2 * M_PI * grid->BaryCenter({0, 0, 0})[1]) *
                               std::sin(2 * M_PI * grid->BaryCenter({0, 0, 0})[2]));
}

TEST_F(GridSystemData3DTest, gradient) {
  Vector<Vector<double, 3>, 1> tmp = data->gradient(grid->BoundaryCenter({0, 0, 0}, left), {0, 0, 0});
  EXPECT_EQ(tmp.rows(), (size_t)1);    // number of equation
  EXPECT_EQ(tmp[0].rows(), (size_t)3); // dim of space
  EXPECT_EQ(tmp[0][0], 0.0);
  EXPECT_EQ(tmp[0][1], 0.0);
  EXPECT_EQ(tmp[0][2], 0.0);
}

TEST_F(GridSystemData3DTest, loadCache) {
  GridSystemDataPtr<3, 1> new_data = GridSystemData<3, 1>::builder().build(grid);
  mGlobalClock->start(0.125);

  data->save_cache("middleInfo");
  mGlobalClock->accumulate(0.555); // useless

  new_data->load_cache("middleInfo");
  EXPECT_DOUBLE_EQ(new_data->value({0, 3, 5})[0], data->value({0, 3, 5})[0]);
  EXPECT_DOUBLE_EQ(mGlobalClock->getTime(), 0.125);
}

TEST_F(GridSystemData3DTest, L1Error) {
  Vector1D error;
  double dx;
  std::array<std::function<double(const Mesh3D::point_t &pt)>, 1> realFunc;
  realFunc[0] = [&](const Mesh3D::point_t &pt) -> double { return 0; };

  ErrorApproximator<3, 1> L1ErrorApprox(grid);
  L1ErrorApprox.l1Error(data, realFunc, eqInfo, error, dx);

  double err = 0.0;
  for (size_t i = 0; i < grid->dataSize()[0]; ++i) {
    for (size_t j = 0; j < grid->dataSize()[1]; ++j) {
      for (size_t k = 0; k < grid->dataSize()[2]; ++k) {
        err += grid->AreaOfEle() * std::abs(std::sin(2 * M_PI * grid->BaryCenter({i, j, k})[0]) *
                                            std::sin(2 * M_PI * grid->BaryCenter({i, j, k})[1]) *
                                            std::sin(2 * M_PI * grid->BaryCenter({i, j, k})[2]));
      }
    }
  }

  EXPECT_DOUBLE_EQ(error[0], err);
}

TEST_F(GridSystemData3DTest, LinftyError) {
  Vector1D error;
  double dx;
  std::array<std::function<double(const Mesh3D::point_t &pt)>, 1> realFunc;
  realFunc[0] = [&](const Mesh3D::point_t &pt) -> double { return 0; };

  ErrorApproximator<3, 1> LinftyErrorApprox(grid);
  LinftyErrorApprox.linftyError(data, realFunc, eqInfo, error, dx);

  double err = 0.0;
  for (size_t i = 0; i < grid->dataSize()[0]; ++i) {
    for (size_t j = 0; j < grid->dataSize()[1]; ++j) {
      for (size_t k = 0; k < grid->dataSize()[2]; ++k) {
        err = std::max(err, std::abs(std::sin(2 * M_PI * grid->BaryCenter({i, j, k})[0]) *
                                     std::sin(2 * M_PI * grid->BaryCenter({i, j, k})[1]) *
                                     std::sin(2 * M_PI * grid->BaryCenter({i, j, k})[2])));
      }
    }
  }

  EXPECT_DOUBLE_EQ(error[0], err);
}
