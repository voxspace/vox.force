//
//  gridData1dUnlimit_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2019/8/22.
//  Copyright © 2019 杨丰. All rights reserved.
//

#include "../src.euler/grid_data_unlimited.h"
#include "../src.euler/recon_auxiliary.h"
#include <gtest/gtest.h>

namespace vox {

class GridData1DUnlimitedTest : public testing::Test {
public:
  void SetUp() override {
    grid = std::make_shared<Mesh<1>>(Vector<size_t, 1>({1000}), Vector<double, 1>({1.0 / 1000.0}));
    grid->addPBDescriptor(Direction::left);

    ReconAuxiliaryPtr<1, 1> aux = std::make_shared<ReconAuxiliary<1, 1>>(grid);
    aux->buildReconstructPatch();
    aux->updateLSMatrix();
    GridDataUnlimitedBuilderPtr<1, 1> UnlimitedDataBuilder = std::make_shared<GridDataUnlimited<1, 1>::Builder>();
    UnlimitedDataBuilder->setReconAuxiliary(aux);

    data = std::dynamic_pointer_cast<GridDataUnlimited<1, 1>>(UnlimitedDataBuilder->build(0, grid));
    for (size_t i = 0; i < grid->dataSize().length(); ++i) {
      data->operator[](i) = std::sin(2 * M_PI * grid->BaryCenter({i})[0]);
    }
    data->calculateBasisFunction();
  }

  MeshPtr<1> grid;
  GridDataUnlimitedPtr<1, 1> data;

public:
  typename PolyInfo<1, 1>::Vec Slope(int idx) { return data->recon_cache[idx].Slope; }
};

TEST_F(GridData1DUnlimitedTest, Slope) {
  auto slope = Slope(0);
  const double h = 0.001;
  double G_inv = 1.0 / (2 * h * h); // get from IQR G's first row and col
  double vb = -(data->GridDataBase::value({1}) - data->GridDataBase::value({0})) * h;
  vb += -(data->GridDataBase::value({999}) - data->GridDataBase::value({0})) * (-h);
  EXPECT_NEAR(slope[0], -G_inv * vb, 1.0e-12);
}

TEST_F(GridData1DUnlimitedTest, value) {
  auto slope = Slope(0);
  EXPECT_DOUBLE_EQ(data->value(grid->BoundaryCenter({0}, right), {0}),
                   data->GridDataBase::value({0}) + slope[0] * 0.001 / 2);
}

TEST_F(GridData1DUnlimitedTest, gradientValue) {
  EXPECT_EQ(data->gradient(grid->BoundaryCenter({0}, right), {0})[0], Slope(0)[0]);
}

TEST_F(GridData1DUnlimitedTest, deepCopy) {
  GridDataUnlimitedPtr<1, 1> data2 = std::dynamic_pointer_cast<GridDataUnlimited<1, 1>>(data->deepCopy());

  auto slope = Slope(0);
  EXPECT_DOUBLE_EQ(data2->value(grid->BoundaryCenter({0}, right), {0}),
                   data->GridDataBase::value({0}) + slope[0] * 0.001 / 2);
}

} // namespace vox
