//
//  polyInfo3D_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2019/11/27.
//  Copyright © 2019 杨丰. All rights reserved.
//

#include "../src.euler/mesh.h"
#include "../src.euler/polyInfo3d.h"
#include <gtest/gtest.h>

using namespace vox;

class PolyInfo3DTest : public testing::Test {
public:
  PolyInfo3DTest() : polyInfo(nullptr) {}

  void SetUp() override {
    grid = std::make_shared<Mesh<3>>(Vector3UZ({10, 10, 10}), Vector<double, 3>({0.1, 0.1, 0.1}));

    grid->addPBDescriptor(Direction::left);
    grid->addPBDescriptor(Direction::bottom);
    grid->addPBDescriptor(Direction::front);

    polyInfo.resetGrid(grid);
    polyInfo.loadBasisFunc();
  }

  static constexpr int order = 2;
  MeshPtr<3> grid;
  PolyInfo<3, order> polyInfo;
};

template <int order> const int PolyInfo<3, order>::n_unknown;

TEST_F(PolyInfo3DTest, n_unknown) { EXPECT_EQ((PolyInfo<3, order>::n_unknown), 9); }

TEST_F(PolyInfo3DTest, basisfuncValue) {
  const double h = 0.1;
  Mesh3D::point_t pt = grid->BoundaryCenter({0, 0, 0}, up);
  pt -= grid->BaryCenter({0, 0, 0});

  std::array<double, PolyInfo<3, order>::n_unknown> result{};
  polyInfo.basisFuncValue({0, 0, 0}, grid->BoundaryCenter({0, 0, 0}, up), result);

  EXPECT_DOUBLE_EQ(result[0], pt[2]);
  EXPECT_NEAR(result[1], pt[1], 1.0e-17);
  EXPECT_NEAR(result[2], pt[0], 1.0e-17);
  EXPECT_NEAR(result[3], pt[2] * pt[2] / h - 1.0 / 12 * h, 1.0e-13);
  EXPECT_DOUBLE_EQ(result[4], pt[2] * pt[1] / h);
  EXPECT_NEAR(result[5], pt[1] * pt[1] / h - 1.0 / 12 * h, 1.0e-14);
  EXPECT_DOUBLE_EQ(result[6], pt[0] * pt[2] / h);
  EXPECT_NEAR(result[7], pt[0] * pt[1] / h, 1.0e-17);
  EXPECT_NEAR(result[8], pt[0] * pt[0] / h - 1.0 / 12 * h, 1.0e-13);
  // z, y, x, zz, zy, yy, xz, xy, xx
}

TEST_F(PolyInfo3DTest, funcValue) {
  typename PolyInfo<3, order>::Vec slope;
  EXPECT_EQ(slope.rows(), 9);

  slope[0] = 100;
  slope[1] = 200;
  slope[2] = 300;
  slope[3] = 400;
  slope[4] = 500;
  slope[5] = 600;
  slope[6] = 700;
  slope[7] = 800;
  slope[8] = 900;
  const double h = 0.1;
  Mesh3D::point_t pt = grid->BoundaryCenter({0, 0, 0}, up);
  pt -= grid->BaryCenter({0, 0, 0});
  double val = 20 + 100 * pt[2] + 200 * pt[1] + 300 * pt[0] + 400 * (pt[2] * pt[2] - h * h / 12) / h +
               500 * (pt[2] * pt[1] - 0) / h + 600 * (pt[1] * pt[1] - h * h / 12) / h + 700 * (pt[0] * pt[2] - 0) / h +
               800 * (pt[0] * pt[1] - 0) / h + 900 * (pt[0] * pt[0] - h * h / 12) / h;

  EXPECT_NEAR(polyInfo.funcValue({0, 0, 0}, grid->BoundaryCenter({0, 0, 0}, up), 20, slope), val, 1.0e-10);
}

TEST_F(PolyInfo3DTest, funcGradient) {
  typename PolyInfo<3, order>::Vec slope;
  slope[0] = 100;
  slope[1] = 200;
  slope[2] = 300;
  slope[3] = 400;
  slope[4] = 500;
  slope[5] = 600;
  slope[6] = 700;
  slope[7] = 800;
  slope[8] = 900;

  std::array<double, 3> grad = polyInfo.funcGradient({0, 0, 0}, grid->BoundaryCenter({0, 0, 0}, up), slope);
  EXPECT_EQ(grad.size(), (size_t)3);

  const double h = 0.1;
  Mesh3D::point_t pt = grid->BoundaryCenter({0, 0, 0}, up);
  pt -= grid->BaryCenter({0, 0, 0});
  EXPECT_NEAR(grad[2], slope[0] + slope[3] * (pt[2] * 2 / h) + slope[4] * pt[1] / h + slope[6] * (pt[0] / h), 1.0e-9);
  EXPECT_NEAR(grad[1], slope[1] + slope[5] * (pt[1] * 2 / h) + slope[4] * pt[2] / h + slope[7] * (pt[0] / h), 1.0e-9);
  EXPECT_NEAR(grad[0], slope[2] + slope[8] * (pt[0] * 2 / h) + slope[6] * pt[2] / h + slope[7] * (pt[1] / h), 1.0e-9);
}

TEST_F(PolyInfo3DTest, averageBasisFunc) {
  std::array<double, PolyInfo<3, order>::n_unknown> result{};
  polyInfo.averageBasisFunc({0, 0, 0}, {0, 0, 0}, result);
  EXPECT_DOUBLE_EQ(result[0], 0);
  EXPECT_DOUBLE_EQ(result[1], 0);
  EXPECT_DOUBLE_EQ(result[2], 0);
  EXPECT_DOUBLE_EQ(result[3], 0);
  EXPECT_DOUBLE_EQ(result[4], 0);
  EXPECT_DOUBLE_EQ(result[5], 0);
  EXPECT_DOUBLE_EQ(result[6], 0);
  EXPECT_DOUBLE_EQ(result[7], 0);
  EXPECT_DOUBLE_EQ(result[8], 0);
}
