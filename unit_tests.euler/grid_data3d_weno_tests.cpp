//
//  gridData3dWENO_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2019/8/25.
//  Copyright © 2019 杨丰. All rights reserved.
//

#include "../src.euler/grid_data_weno.h"
#include "../src.euler/mesh.h"
#include "../src.euler/weno_auxiliary.h"

#include <gtest/gtest.h>

namespace vox {

class GridData3DWENOTest : public testing::Test {
public:
  void SetUp() override {
    grid = std::make_shared<Mesh<3>>(Vector3UZ({10, 10, 10}), Vector<double, 3>({0.1, 0.1, 0.1}));

    grid->addPBDescriptor(Direction::left);
    grid->addPBDescriptor(Direction::bottom);
    grid->addPBDescriptor(Direction::front);

    wenoAux = std::make_shared<WENOAuxiliary<3, order>>(grid);
    wenoAux->buildReconstructPatch();
    wenoAux->buildWENOPatch();
    wenoAux->updateWENOInfo();
    GridDataWENOBuilderPtr<3, order> dataBuilder = std::make_shared<GridDataWENO<3, order>::Builder>();
    dataBuilder->setWENOAuxiliary(wenoAux);

    data = std::dynamic_pointer_cast<GridDataWENO<3, order>>(dataBuilder->build(0, grid));
    for (size_t i = 0; i < grid->dataSize()[0]; ++i) {
      for (size_t j = 0; j < grid->dataSize()[1]; ++j) {
        for (size_t k = 0; k < grid->dataSize()[2]; ++k) {
          data->operator()({i, j, k}) = std::sin(2 * M_PI * grid->BaryCenter({i, j, k})[0]) *
                                        std::sin(2 * M_PI * grid->BaryCenter({i, j, k})[1]) *
                                        std::sin(2 * M_PI * grid->BaryCenter({i, j, k})[2]);
        }
      }
    }
    data->calculateBasisFunction();
  }

  static constexpr int order = 1;
  MeshPtr<3> grid;
  GridDataWENOPtr<3, order> data;
  WENOAuxiliaryPtr<3, order> wenoAux;

public:
  std::vector<typename PolyInfo<3, order>::Vec> grad_choice(int idx) { return data->recon_cache[idx].grad_choice; }

  typename PolyInfo<3, order>::Vec gradient(int idx) { return data->recon_cache[idx].Slope; }
};

TEST_F(GridData3DWENOTest, grad_choice) {
  auto grads = grad_choice(0);
  EXPECT_EQ(grads.size(), (size_t)7);

  // center {0, 1, 9, 10, 90, 100, 900}
  Vector3D v_b;
  v_b[0] = (grid->BaryCenter({0, 0, 1}) - grid->BaryCenter({0, 0, 0})).eval()[0] *
               (data->GridDataBase::value({0, 0, 1}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 0, 9}) - grid->BaryCenter({0, 0, 0})).eval()[0]) *
               (data->GridDataBase::value({0, 0, 9}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 1, 0}) - grid->BaryCenter({0, 0, 0})).eval()[0]) *
               (data->GridDataBase::value({0, 1, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 9, 0}) - grid->BaryCenter({0, 0, 0})).eval()[0]) *
               (data->GridDataBase::value({0, 9, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({1, 0, 0}) - grid->BaryCenter({0, 0, 0})).eval()[0]) *
               (data->GridDataBase::value({1, 0, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({9, 0, 0}) - grid->BaryCenter({0, 0, 0})).eval()[0] - 1.0) *
               (data->GridDataBase::value({9, 0, 0}) - data->GridDataBase::value({0, 0, 0}));

  v_b[1] = (grid->BaryCenter({0, 0, 1}) - grid->BaryCenter({0, 0, 0})).eval()[1] *
               (data->GridDataBase::value({0, 0, 1}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 0, 9}) - grid->BaryCenter({0, 0, 0})).eval()[1]) *
               (data->GridDataBase::value({0, 0, 9}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 1, 0}) - grid->BaryCenter({0, 0, 0})).eval()[1]) *
               (data->GridDataBase::value({0, 1, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 9, 0}) - grid->BaryCenter({0, 0, 0})).eval()[1] - 1.0) *
               (data->GridDataBase::value({0, 9, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({1, 0, 0}) - grid->BaryCenter({0, 0, 0})).eval()[1]) *
               (data->GridDataBase::value({1, 0, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({9, 0, 0}) - grid->BaryCenter({0, 0, 0})).eval()[1]) *
               (data->GridDataBase::value({9, 0, 0}) - data->GridDataBase::value({0, 0, 0}));

  v_b[2] = (grid->BaryCenter({0, 0, 1}) - grid->BaryCenter({0, 0, 0})).eval()[2] *
               (data->GridDataBase::value({0, 0, 1}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 0, 9}) - grid->BaryCenter({0, 0, 0})).eval()[2] - 1.0) *
               (data->GridDataBase::value({0, 0, 9}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 1, 0}) - grid->BaryCenter({0, 0, 0})).eval()[2]) *
               (data->GridDataBase::value({0, 1, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 9, 0}) - grid->BaryCenter({0, 0, 0})).eval()[2]) *
               (data->GridDataBase::value({0, 9, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({1, 0, 0}) - grid->BaryCenter({0, 0, 0})).eval()[2]) *
               (data->GridDataBase::value({1, 0, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({9, 0, 0}) - grid->BaryCenter({0, 0, 0})).eval()[2]) *
               (data->GridDataBase::value({9, 0, 0}) - data->GridDataBase::value({0, 0, 0}));
  double vb0 = v_b[0] * 50;
  double vb1 = v_b[1] * 50;
  double vb2 = v_b[2] * 50;
  EXPECT_NEAR(grads[0][2], vb2, 1.0e-10); // z
  EXPECT_NEAR(grads[0][1], vb1, 1.0e-10); // y
  EXPECT_NEAR(grads[0][0], vb1, 1.0e-10); // x

  // up {0, 1, 2, 11, 91, 101, 901}
  v_b[0] = (grid->BaryCenter({0, 0, 1}) - grid->BaryCenter({0, 0, 0})).eval()[0] *
               (data->GridDataBase::value({0, 0, 1}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 0, 2}) - grid->BaryCenter({0, 0, 0})).eval()[0]) *
               (data->GridDataBase::value({0, 0, 2}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 1, 1}) - grid->BaryCenter({0, 0, 0})).eval()[0]) *
               (data->GridDataBase::value({0, 1, 1}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 9, 1}) - grid->BaryCenter({0, 0, 0})).eval()[0]) *
               (data->GridDataBase::value({0, 9, 1}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({1, 0, 1}) - grid->BaryCenter({0, 0, 0})).eval()[0]) *
               (data->GridDataBase::value({1, 0, 1}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({9, 0, 1}) - grid->BaryCenter({0, 0, 0})).eval()[0] - 1.0) *
               (data->GridDataBase::value({9, 0, 1}) - data->GridDataBase::value({0, 0, 0}));

  v_b[1] = (grid->BaryCenter({0, 0, 1}) - grid->BaryCenter({0, 0, 0})).eval()[1] *
               (data->GridDataBase::value({0, 0, 1}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 0, 2}) - grid->BaryCenter({0, 0, 0})).eval()[1]) *
               (data->GridDataBase::value({0, 0, 2}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 1, 1}) - grid->BaryCenter({0, 0, 0})).eval()[1]) *
               (data->GridDataBase::value({0, 1, 1}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 9, 1}) - grid->BaryCenter({0, 0, 0})).eval()[1] - 1.0) *
               (data->GridDataBase::value({0, 9, 1}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({1, 0, 1}) - grid->BaryCenter({0, 0, 0})).eval()[1]) *
               (data->GridDataBase::value({1, 0, 1}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({9, 0, 1}) - grid->BaryCenter({0, 0, 0})).eval()[1]) *
               (data->GridDataBase::value({9, 0, 1}) - data->GridDataBase::value({0, 0, 0}));

  v_b[2] = (grid->BaryCenter({0, 0, 1}) - grid->BaryCenter({0, 0, 0})).eval()[2] *
               (data->GridDataBase::value({0, 0, 1}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 0, 2}) - grid->BaryCenter({0, 0, 0})).eval()[2]) *
               (data->GridDataBase::value({0, 0, 2}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 1, 1}) - grid->BaryCenter({0, 0, 0})).eval()[2]) *
               (data->GridDataBase::value({0, 1, 1}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 9, 1}) - grid->BaryCenter({0, 0, 0})).eval()[2]) *
               (data->GridDataBase::value({0, 9, 1}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({1, 0, 1}) - grid->BaryCenter({0, 0, 0})).eval()[2]) *
               (data->GridDataBase::value({1, 0, 1}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({9, 0, 1}) - grid->BaryCenter({0, 0, 0})).eval()[2]) *
               (data->GridDataBase::value({9, 0, 1}) - data->GridDataBase::value({0, 0, 0}));
  vb0 = v_b[0] * 50;
  vb1 = v_b[1] * 50;
  vb2 = v_b[2] * 100 / 9.0;
  EXPECT_NEAR(grads[1][2], vb2, 1.0e-10); // z
  EXPECT_NEAR(grads[1][1], vb1, 1.0e-10); // y
  EXPECT_NEAR(grads[1][0], vb0, 1.0e-10); // x

  // bottom {0, 8, 9, 19, 99, 109, 909}
  v_b[0] = (grid->BaryCenter({0, 0, 8}) - grid->BaryCenter({0, 0, 0})).eval()[0] *
               (data->GridDataBase::value({0, 0, 8}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 0, 9}) - grid->BaryCenter({0, 0, 0})).eval()[0]) *
               (data->GridDataBase::value({0, 0, 9}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 1, 9}) - grid->BaryCenter({0, 0, 0})).eval()[0]) *
               (data->GridDataBase::value({0, 1, 9}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 9, 9}) - grid->BaryCenter({0, 0, 0})).eval()[0]) *
               (data->GridDataBase::value({0, 9, 9}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({1, 0, 9}) - grid->BaryCenter({0, 0, 0})).eval()[0]) *
               (data->GridDataBase::value({1, 0, 9}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({9, 0, 9}) - grid->BaryCenter({0, 0, 0})).eval()[0] - 1.0) *
               (data->GridDataBase::value({9, 0, 9}) - data->GridDataBase::value({0, 0, 0}));

  v_b[1] = (grid->BaryCenter({0, 0, 8}) - grid->BaryCenter({0, 0, 0})).eval()[1] *
               (data->GridDataBase::value({0, 0, 8}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 0, 9}) - grid->BaryCenter({0, 0, 0})).eval()[1]) *
               (data->GridDataBase::value({0, 0, 9}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 1, 9}) - grid->BaryCenter({0, 0, 0})).eval()[1]) *
               (data->GridDataBase::value({0, 1, 9}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 9, 9}) - grid->BaryCenter({0, 0, 0})).eval()[1] - 1.0) *
               (data->GridDataBase::value({0, 9, 9}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({1, 0, 9}) - grid->BaryCenter({0, 0, 0})).eval()[1]) *
               (data->GridDataBase::value({1, 0, 9}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({9, 0, 9}) - grid->BaryCenter({0, 0, 0})).eval()[1]) *
               (data->GridDataBase::value({9, 0, 9}) - data->GridDataBase::value({0, 0, 0}));

  v_b[2] = ((grid->BaryCenter({0, 0, 8}) - grid->BaryCenter({0, 0, 0})).eval()[2] - 1.0) *
               (data->GridDataBase::value({0, 0, 8}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 0, 9}) - grid->BaryCenter({0, 0, 0})).eval()[2] - 1.0) *
               (data->GridDataBase::value({0, 0, 9}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 1, 9}) - grid->BaryCenter({0, 0, 0})).eval()[2] - 1.0) *
               (data->GridDataBase::value({0, 1, 9}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 9, 9}) - grid->BaryCenter({0, 0, 0})).eval()[2] - 1.0) *
               (data->GridDataBase::value({0, 9, 9}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({1, 0, 9}) - grid->BaryCenter({0, 0, 0})).eval()[2] - 1.0) *
               (data->GridDataBase::value({1, 0, 9}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({9, 0, 9}) - grid->BaryCenter({0, 0, 0})).eval()[2] - 1.0) *
               (data->GridDataBase::value({9, 0, 9}) - data->GridDataBase::value({0, 0, 0}));
  vb0 = v_b[0] * 50;
  vb1 = v_b[1] * 50;
  vb2 = v_b[2] * 100 / 9.0;
  EXPECT_NEAR(grads[2][2], vb2, 1.0e-10); // z
  EXPECT_NEAR(grads[2][1], vb1, 1.0e-10); // y
  EXPECT_NEAR(grads[2][0], vb0, 1.0e-10); // x

  // right {0, 10, 11, 19, 20, 110, 910}
  v_b[0] = (grid->BaryCenter({0, 1, 0}) - grid->BaryCenter({0, 0, 0})).eval()[0] *
               (data->GridDataBase::value({0, 1, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 1, 1}) - grid->BaryCenter({0, 0, 0})).eval()[0]) *
               (data->GridDataBase::value({0, 1, 1}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 1, 9}) - grid->BaryCenter({0, 0, 0})).eval()[0]) *
               (data->GridDataBase::value({0, 1, 9}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 2, 0}) - grid->BaryCenter({0, 0, 0})).eval()[0]) *
               (data->GridDataBase::value({0, 2, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({1, 1, 0}) - grid->BaryCenter({0, 0, 0})).eval()[0]) *
               (data->GridDataBase::value({1, 1, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({9, 1, 0}) - grid->BaryCenter({0, 0, 0})).eval()[0] - 1.0) *
               (data->GridDataBase::value({9, 1, 0}) - data->GridDataBase::value({0, 0, 0}));

  v_b[1] = (grid->BaryCenter({0, 1, 0}) - grid->BaryCenter({0, 0, 0})).eval()[1] *
               (data->GridDataBase::value({0, 1, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 1, 1}) - grid->BaryCenter({0, 0, 0})).eval()[1]) *
               (data->GridDataBase::value({0, 1, 1}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 1, 9}) - grid->BaryCenter({0, 0, 0})).eval()[1]) *
               (data->GridDataBase::value({0, 1, 9}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 2, 0}) - grid->BaryCenter({0, 0, 0})).eval()[1]) *
               (data->GridDataBase::value({0, 2, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({1, 1, 0}) - grid->BaryCenter({0, 0, 0})).eval()[1]) *
               (data->GridDataBase::value({1, 1, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({9, 1, 0}) - grid->BaryCenter({0, 0, 0})).eval()[1]) *
               (data->GridDataBase::value({9, 1, 0}) - data->GridDataBase::value({0, 0, 0}));

  v_b[2] = (grid->BaryCenter({0, 1, 0}) - grid->BaryCenter({0, 0, 0})).eval()[2] *
               (data->GridDataBase::value({0, 1, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 1, 1}) - grid->BaryCenter({0, 0, 0})).eval()[2]) *
               (data->GridDataBase::value({0, 1, 1}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 1, 9}) - grid->BaryCenter({0, 0, 0})).eval()[2] - 1.0) *
               (data->GridDataBase::value({0, 1, 9}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 2, 0}) - grid->BaryCenter({0, 0, 0})).eval()[2]) *
               (data->GridDataBase::value({0, 2, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({1, 1, 0}) - grid->BaryCenter({0, 0, 0})).eval()[2]) *
               (data->GridDataBase::value({1, 1, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({9, 1, 0}) - grid->BaryCenter({0, 0, 0})).eval()[2]) *
               (data->GridDataBase::value({9, 1, 0}) - data->GridDataBase::value({0, 0, 0}));
  vb0 = v_b[0] * 50;
  vb1 = v_b[1] * 100 / 9.0;
  vb2 = v_b[2] * 50;
  EXPECT_NEAR(grads[3][2], vb2, 1.0e-10); // z
  EXPECT_NEAR(grads[3][1], vb1, 1.0e-10); // y
  EXPECT_NEAR(grads[3][0], vb0, 1.0e-10); // x

  // left {0, 80, 90, 91, 99, 190, 990}
  v_b[0] = (grid->BaryCenter({0, 8, 0}) - grid->BaryCenter({0, 0, 0})).eval()[0] *
               (data->GridDataBase::value({0, 8, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 9, 0}) - grid->BaryCenter({0, 0, 0})).eval()[0]) *
               (data->GridDataBase::value({0, 9, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 9, 1}) - grid->BaryCenter({0, 0, 0})).eval()[0]) *
               (data->GridDataBase::value({0, 9, 1}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 9, 9}) - grid->BaryCenter({0, 0, 0})).eval()[0]) *
               (data->GridDataBase::value({0, 9, 9}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({1, 9, 0}) - grid->BaryCenter({0, 0, 0})).eval()[0]) *
               (data->GridDataBase::value({1, 9, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({9, 9, 0}) - grid->BaryCenter({0, 0, 0})).eval()[0] - 1.0) *
               (data->GridDataBase::value({9, 9, 0}) - data->GridDataBase::value({0, 0, 0}));

  v_b[1] = ((grid->BaryCenter({0, 8, 0}) - grid->BaryCenter({0, 0, 0})).eval()[1] - 1.0) *
               (data->GridDataBase::value({0, 8, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 9, 0}) - grid->BaryCenter({0, 0, 0})).eval()[1] - 1.0) *
               (data->GridDataBase::value({0, 9, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 9, 1}) - grid->BaryCenter({0, 0, 0})).eval()[1] - 1.0) *
               (data->GridDataBase::value({0, 9, 1}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 9, 9}) - grid->BaryCenter({0, 0, 0})).eval()[1] - 1.0) *
               (data->GridDataBase::value({0, 9, 9}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({1, 9, 0}) - grid->BaryCenter({0, 0, 0})).eval()[1] - 1.0) *
               (data->GridDataBase::value({1, 9, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({9, 9, 0}) - grid->BaryCenter({0, 0, 0})).eval()[1] - 1.0) *
               (data->GridDataBase::value({9, 9, 0}) - data->GridDataBase::value({0, 0, 0}));

  v_b[2] = (grid->BaryCenter({0, 8, 0}) - grid->BaryCenter({0, 0, 0})).eval()[2] *
               (data->GridDataBase::value({0, 8, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 9, 0}) - grid->BaryCenter({0, 0, 0})).eval()[2]) *
               (data->GridDataBase::value({0, 9, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 9, 1}) - grid->BaryCenter({0, 0, 0})).eval()[2]) *
               (data->GridDataBase::value({0, 9, 1}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({0, 9, 9}) - grid->BaryCenter({0, 0, 0})).eval()[2] - 1.0) *
               (data->GridDataBase::value({0, 9, 9}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({1, 9, 0}) - grid->BaryCenter({0, 0, 0})).eval()[2]) *
               (data->GridDataBase::value({1, 9, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({9, 9, 0}) - grid->BaryCenter({0, 0, 0})).eval()[2]) *
               (data->GridDataBase::value({9, 9, 0}) - data->GridDataBase::value({0, 0, 0}));
  vb0 = v_b[0] * 50;
  vb1 = v_b[1] * 100 / 9.0;
  vb2 = v_b[2] * 50;
  EXPECT_NEAR(grads[4][2], vb2, 1.0e-10); // z
  EXPECT_NEAR(grads[4][1], vb1, 1.0e-10); // y
  EXPECT_NEAR(grads[4][0], vb0, 1.0e-10); // x

  // front {0, 100, 101, 109, 110, 190, 200}
  v_b[0] = (grid->BaryCenter({1, 0, 0}) - grid->BaryCenter({0, 0, 0})).eval()[0] *
               (data->GridDataBase::value({1, 0, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({1, 0, 1}) - grid->BaryCenter({0, 0, 0})).eval()[0]) *
               (data->GridDataBase::value({1, 0, 1}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({1, 0, 9}) - grid->BaryCenter({0, 0, 0})).eval()[0]) *
               (data->GridDataBase::value({1, 0, 9}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({1, 1, 0}) - grid->BaryCenter({0, 0, 0})).eval()[0]) *
               (data->GridDataBase::value({1, 1, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({1, 9, 0}) - grid->BaryCenter({0, 0, 0})).eval()[0]) *
               (data->GridDataBase::value({1, 9, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({2, 0, 0}) - grid->BaryCenter({0, 0, 0})).eval()[0]) *
               (data->GridDataBase::value({2, 0, 0}) - data->GridDataBase::value({0, 0, 0}));

  v_b[1] = (grid->BaryCenter({1, 0, 0}) - grid->BaryCenter({0, 0, 0})).eval()[1] *
               (data->GridDataBase::value({1, 0, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({1, 0, 1}) - grid->BaryCenter({0, 0, 0})).eval()[1]) *
               (data->GridDataBase::value({1, 0, 1}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({1, 0, 9}) - grid->BaryCenter({0, 0, 0})).eval()[1]) *
               (data->GridDataBase::value({1, 0, 9}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({1, 1, 0}) - grid->BaryCenter({0, 0, 0})).eval()[1]) *
               (data->GridDataBase::value({1, 1, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({1, 9, 0}) - grid->BaryCenter({0, 0, 0})).eval()[1] - 1.0) *
               (data->GridDataBase::value({1, 9, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({2, 0, 0}) - grid->BaryCenter({0, 0, 0})).eval()[1]) *
               (data->GridDataBase::value({2, 0, 0}) - data->GridDataBase::value({0, 0, 0}));

  v_b[2] = (grid->BaryCenter({1, 0, 0}) - grid->BaryCenter({0, 0, 0})).eval()[2] *
               (data->GridDataBase::value({1, 0, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({1, 0, 1}) - grid->BaryCenter({0, 0, 0})).eval()[2]) *
               (data->GridDataBase::value({1, 0, 1}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({1, 0, 9}) - grid->BaryCenter({0, 0, 0})).eval()[2] - 1.0) *
               (data->GridDataBase::value({1, 0, 9}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({1, 1, 0}) - grid->BaryCenter({0, 0, 0})).eval()[2]) *
               (data->GridDataBase::value({1, 1, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({1, 9, 0}) - grid->BaryCenter({0, 0, 0})).eval()[2]) *
               (data->GridDataBase::value({1, 9, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({2, 0, 0}) - grid->BaryCenter({0, 0, 0})).eval()[2]) *
               (data->GridDataBase::value({2, 0, 0}) - data->GridDataBase::value({0, 0, 0}));
  vb0 = v_b[0] * 100 / 9.0;
  vb1 = v_b[1] * 50;
  vb2 = v_b[2] * 50;
  EXPECT_NEAR(grads[5][2], vb2, 1.0e-10); // z
  EXPECT_NEAR(grads[5][1], vb1, 1.0e-10); // y
  EXPECT_NEAR(grads[5][0], vb0, 1.0e-10); // x

  // back {0, 800, 900, 901, 909, 910, 990}
  v_b[0] = ((grid->BaryCenter({8, 0, 0}) - grid->BaryCenter({0, 0, 0})).eval()[0] - 1.0) *
               (data->GridDataBase::value({8, 0, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({9, 0, 0}) - grid->BaryCenter({0, 0, 0})).eval()[0] - 1.0) *
               (data->GridDataBase::value({9, 0, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({9, 0, 1}) - grid->BaryCenter({0, 0, 0})).eval()[0] - 1.0) *
               (data->GridDataBase::value({9, 0, 1}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({9, 0, 9}) - grid->BaryCenter({0, 0, 0})).eval()[0] - 1.0) *
               (data->GridDataBase::value({9, 0, 9}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({9, 1, 0}) - grid->BaryCenter({0, 0, 0})).eval()[0] - 1.0) *
               (data->GridDataBase::value({9, 1, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({9, 9, 0}) - grid->BaryCenter({0, 0, 0})).eval()[0] - 1.0) *
               (data->GridDataBase::value({9, 9, 0}) - data->GridDataBase::value({0, 0, 0}));

  v_b[1] = (grid->BaryCenter({8, 0, 0}) - grid->BaryCenter({0, 0, 0})).eval()[1] *
               (data->GridDataBase::value({8, 0, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({9, 0, 0}) - grid->BaryCenter({0, 0, 0})).eval()[1]) *
               (data->GridDataBase::value({9, 0, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({9, 0, 1}) - grid->BaryCenter({0, 0, 0})).eval()[1]) *
               (data->GridDataBase::value({9, 0, 1}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({9, 0, 9}) - grid->BaryCenter({0, 0, 0})).eval()[1]) *
               (data->GridDataBase::value({9, 0, 9}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({9, 1, 0}) - grid->BaryCenter({0, 0, 0})).eval()[1]) *
               (data->GridDataBase::value({9, 1, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({9, 9, 0}) - grid->BaryCenter({0, 0, 0})).eval()[1] - 1.0) *
               (data->GridDataBase::value({9, 9, 0}) - data->GridDataBase::value({0, 0, 0}));

  v_b[2] = (grid->BaryCenter({8, 0, 0}) - grid->BaryCenter({0, 0, 0})).eval()[2] *
               (data->GridDataBase::value({8, 0, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({9, 0, 0}) - grid->BaryCenter({0, 0, 0})).eval()[2]) *
               (data->GridDataBase::value({9, 0, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({9, 0, 1}) - grid->BaryCenter({0, 0, 0})).eval()[2]) *
               (data->GridDataBase::value({9, 0, 1}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({9, 0, 9}) - grid->BaryCenter({0, 0, 0})).eval()[2] - 1.0) *
               (data->GridDataBase::value({9, 0, 9}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({9, 1, 0}) - grid->BaryCenter({0, 0, 0})).eval()[2]) *
               (data->GridDataBase::value({9, 1, 0}) - data->GridDataBase::value({0, 0, 0})) +
           ((grid->BaryCenter({9, 9, 0}) - grid->BaryCenter({0, 0, 0})).eval()[2]) *
               (data->GridDataBase::value({9, 9, 0}) - data->GridDataBase::value({0, 0, 0}));
  vb0 = v_b[0] * 100 / 9.0;
  vb1 = v_b[1] * 50;
  vb2 = v_b[2] * 50;
  EXPECT_NEAR(grads[6][2], vb2, 1.0e-10); // z
  EXPECT_NEAR(grads[6][1], vb1, 1.0e-10); // y
  EXPECT_NEAR(grads[6][0], vb0, 1.0e-10); // x
}

TEST_F(GridData3DWENOTest, gradient) {
  const double epsilon = 1.0e-04;
  auto grads = grad_choice(0);
  std::vector<double> weights(grads.size(), 0.0);

  double sum = 0;
  for (size_t j = 0; j < grads.size(); ++j) {
    weights[j] = (pow(grads[j][0], 2.) + pow(grads[j][1], 2.) + pow(grads[j][2], 2)) * grid->AreaOfEle();
    weights[j] = 1. / pow(epsilon + weights[j], 2.); // 2 for all previous tests
    sum += weights[j];
  }
  for (size_t j = 0; j < grads.size(); ++j) {
    weights[j] /= sum;
  }

  Vector3D result = Vector3D::makeZero();
  for (size_t j = 0; j < grads.size(); ++j) {
    result += weights[j] * grads[j];
  }

  EXPECT_NEAR(gradient(0)[0], result[0], 1.0e-16);
  EXPECT_NEAR(gradient(0)[1], result[1], 1.0e-16);
  EXPECT_DOUBLE_EQ(gradient(0)[2], result[2]);
}

TEST_F(GridData3DWENOTest, value) {
  auto grad = gradient(0);
  Mesh3D::point_t pt = grid->BoundaryCenter({0, 0, 0}, left);
  Mesh3D::point_t bc = grid->BaryCenter({0, 0, 0});
  EXPECT_DOUBLE_EQ(data->value(grid->BoundaryCenter({0, 0, 0}, left), {0, 0, 0}),
                   data->GridDataBase::value({0, 0, 0}) + grad[2] * (pt[0] - bc[0]) + grad[1] * (pt[1] - bc[1]) +
                       grad[0] * (pt[2] - bc[2]));
  // z, y, x
}

TEST_F(GridData3DWENOTest, gradientValue) {
  EXPECT_DOUBLE_EQ(data->gradient(grid->BoundaryCenter({0, 0, 0}, left), {0, 0, 0})[0], gradient(0)[2]); // z
  EXPECT_DOUBLE_EQ(data->gradient(grid->BoundaryCenter({0, 0, 0}, left), {0, 0, 0})[1], gradient(0)[1]); // y
  EXPECT_DOUBLE_EQ(data->gradient(grid->BoundaryCenter({0, 0, 0}, left), {0, 0, 0})[2], gradient(0)[0]); // x
}

TEST_F(GridData3DWENOTest, deepCopy) {
  GridDataPtr<3> data2 = data->deepCopy();

  auto grad = gradient(0);
  Mesh3D::point_t pt = grid->BoundaryCenter({0, 0, 0}, left);
  Mesh3D::point_t bc = grid->BaryCenter({0, 0, 0});
  EXPECT_DOUBLE_EQ(data2->value(grid->BoundaryCenter({0, 0, 0}, left), {0, 0, 0}),
                   data->GridDataBase::value({0, 0, 0}) + grad[2] * (pt[0] - bc[0]) + grad[1] * (pt[1] - bc[1]) +
                       grad[0] * (pt[2] - bc[2]));
  // z, y, x
}

} // namespace vox
