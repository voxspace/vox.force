//
//  EqEuler_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2019/8/21.
//  Copyright © 2019 杨丰. All rights reserved.
//

#include "../src.euler/equations/equation_NS.h"
#include <gtest/gtest.h>

using namespace vox;

class EqNSTest : public testing::Test {
public:
  void SetUp() override {
    Mu = 0.1;
    Kappa = 0.2;
    eq = std::make_shared<EquationNS<1>>(1.4, Mu, Kappa);
  }

  double Mu;
  double Kappa;
  EquationNSPtr<1> eq;
};

TEST_F(EqNSTest, getDos) { EXPECT_EQ(EquationNS<1>::DOS, (size_t)1 + 2); }

TEST_F(EqNSTest, getGamma) { EXPECT_EQ(eq->Gamma, 1.4); }

TEST_F(EqNSTest, MaxCharacteristicSpeed) {
  EXPECT_EQ(eq->MaxCharacteristicSpeed({1.0, 1.0, 1.0}), 1 + std::sqrt(0.28));

  // test for negative Pressure mechanics
  EXPECT_EQ(eq->MaxCharacteristicSpeed({1.0, 1.0, 0.0}), 1);
}

TEST_F(EqNSTest, TFlux1D) {
  Vector<double, EquationNS<1>::DOS> flux;
  eq->TFlux({1, 1, 1}, {1}, flux);
  EXPECT_EQ(flux.rows(), (size_t)3);
  EXPECT_DOUBLE_EQ(flux[0], 1);
  EXPECT_DOUBLE_EQ(flux[1], 1 + 0.2);
  EXPECT_DOUBLE_EQ(flux[2], 1 + 0.2);
}

TEST_F(EqNSTest, TFlux2D) {
  EquationNSPtr<2> eq = std::make_shared<EquationNS<2>>(1.4);

  Vector<double, EquationNS<2>::DOS> flux;
  eq->TFlux({1, 1, 1, 1}, {1, 1}, flux);
  EXPECT_EQ(flux.rows(), (size_t)4);
  EXPECT_DOUBLE_EQ(flux[0], 2);
  EXPECT_DOUBLE_EQ(flux[1], 2);
  EXPECT_DOUBLE_EQ(flux[2], 2);
  EXPECT_DOUBLE_EQ(flux[3], 2);
}

TEST_F(EqNSTest, TFluxViscous) {
  Vector<double, EquationNS<1>::DOS> flux;
  eq->TFlux({1.0, 1.0, 1.0}, {{1.0}, {2.0}, {3.0}}, {1.0}, flux, true);
  EXPECT_EQ(flux.rows(), (size_t)3);

  double P = 1.0 / 3;
  double strain_rate = 1.0;
  double stress = 2 * Mu * (strain_rate - P);
  double q = -Kappa * 1.0;
  EXPECT_EQ(flux[0], 0.0);
  EXPECT_EQ(flux[1], stress);
  EXPECT_EQ(flux[2], stress - q);
}

TEST_F(EqNSTest, MaxSpeeds) {
  double l, r;
  std::array<double, 1> norm = {1.0};
  eq->MaxSpeeds({1, 1, 1}, norm, l, r);
  EXPECT_DOUBLE_EQ(l, 1 - std::sqrt(0.28));
  EXPECT_DOUBLE_EQ(r, 1 + std::sqrt(0.28));
}
