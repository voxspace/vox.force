//
//  grid_data2d_unlimited_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2020/5/16.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "../src.euler/grid_data_unlimited.h"
#include "../src.euler/mesh.h"
#include "../src.euler/recon_auxiliary.h"
#include <gtest/gtest.h>

namespace vox {

class GridData2DUnlimitedTest : public testing::Test {
public:
  void SetUp() override {
    grid = std::make_shared<Mesh<2>>(Vector<size_t, 2>({10, 10}), Vector<double, 2>({0.1, 0.1}));

    grid->addPBDescriptor(Direction::left);
    grid->addPBDescriptor(Direction::bottom);

    ReconAuxiliaryPtr<2, 1> aux = std::make_shared<ReconAuxiliary<2, 1>>(grid);
    aux->buildReconstructPatch();
    aux->updateLSMatrix();
    GridDataUnlimitedBuilderPtr<2, 1> UnlimitedDataBuilder = std::make_shared<GridDataUnlimited<2, 1>::Builder>();
    UnlimitedDataBuilder->setReconAuxiliary(aux);

    data = std::dynamic_pointer_cast<GridDataUnlimited<2, 1>>(UnlimitedDataBuilder->build(0, grid));
    for (size_t i = 0; i < grid->dataSize()[0]; ++i) {
      for (size_t j = 0; j < grid->dataSize()[1]; ++j) {
        data->operator()({i, j}) =
            std::sin(2 * M_PI * grid->BaryCenter({i, j})[0]) * std::sin(2 * M_PI * grid->BaryCenter({i, j})[1]);
      }
    }
    data->calculateBasisFunction();
  }

  MeshPtr<2> grid;
  GridDataUnlimitedPtr<2, 1> data;

public:
  typename PolyInfo<2, 1>::Vec Slope(int idx) { return data->recon_cache[idx].Slope; }
};

TEST_F(GridData2DUnlimitedTest, Slope) {
  auto slope = Slope(0);

  // left right up down four neighbors
  Matrix2x2D G_inv(1.0 / 0.02, 0, 0, 1.0 / 0.02);

  Vector2D vb = Vector2D::makeZero();
  Vector2D tmp(0, 0.1);
  tmp *= data->GridDataBase::value({1, 0}) - data->GridDataBase::value({0, 0});
  vb -= tmp;

  tmp = Vector2D(0, -0.1);
  tmp *= data->GridDataBase::value({9, 0}) - data->GridDataBase::value({0, 0});
  vb -= tmp;

  tmp = Vector2D(-0.1, 0);
  tmp *= data->GridDataBase::value({0, 9}) - data->GridDataBase::value({0, 0});
  vb -= tmp;

  tmp = Vector2D(0.1, 0);
  tmp *= data->GridDataBase::value({0, 1}) - data->GridDataBase::value({0, 0});
  vb -= tmp;

  vb = -G_inv * vb;
  EXPECT_NEAR(slope[0], vb[0], 1.0e-11);
  EXPECT_NEAR(slope[1], vb[1], 1.0e-11);
}

TEST_F(GridData2DUnlimitedTest, value) {
  auto slope = Slope(0);
  Mesh2D::point_t pt = grid->BoundaryCenter({0, 0}, left);
  Mesh2D::point_t bc = grid->BaryCenter({0, 0});
  EXPECT_DOUBLE_EQ(data->value(grid->BoundaryCenter({0, 0}, left), {0, 0}),
                   data->GridDataBase::value({0, 0}) + slope[0] * (pt[1] - bc[1]) + slope[1] * (pt[0] - bc[0]));
}

TEST_F(GridData2DUnlimitedTest, gradientValue) {
  std::array<double, 2> slope = {Slope(0)[1], Slope(0)[0]}; // y, x
  EXPECT_EQ(data->gradient(grid->BoundaryCenter({0, 0}, left), {0, 0}), slope);
}

TEST_F(GridData2DUnlimitedTest, deepCopy) {
  GridDataUnlimitedPtr<2, 1> data2 = std::dynamic_pointer_cast<GridDataUnlimited<2, 1>>(data->deepCopy());

  auto slope = Slope(0);
  Mesh2D::point_t pt = grid->BoundaryCenter({0, 0}, left);
  Mesh2D::point_t bc = grid->BaryCenter({0, 0});
  EXPECT_NEAR(data2->value(grid->BoundaryCenter({0, 0}, left), {0, 0}),
              data->GridDataBase::value({0, 0}) + slope[0] * (pt[1] - bc[1]) + slope[1] * (pt[0] - bc[0]), 1.0e-14);
}

} // namespace vox
