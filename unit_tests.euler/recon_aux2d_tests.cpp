//
//  recon_aux2d_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2020/5/16.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "../src.euler/mesh.h"
#include "../src.euler/recon_auxiliary.h"
#include <gtest/gtest.h>

using namespace vox;

class ReconAux2DTest : public testing::Test {
public:
  void SetUp() override {
    grid = std::make_shared<Mesh<2>>(Vector<size_t, 2>({10, 10}), Vector<double, 2>({0.1, 0.1}));

    grid->addPBDescriptor(Direction::left);
    grid->addPBDescriptor(Direction::bottom);

    reconAux = std::make_shared<ReconAuxiliary<2, 2>>(grid);
    reconAux->buildReconstructPatch(PatchSearcher<2, 2>::Node);
    reconAux->updateLSMatrix();
  }

  static constexpr int order = 2;
  MeshPtr<2> grid;
  ReconAuxiliaryPtr<2, order> reconAux;

public:
  static Vector<double, 5> subJ(Mesh2D::point_t pt) {
    const double h = 0.1;
    const double half_h = h / 2;
    const double diameter = h;

    Vector<double, 5> subJ = {
        (std::pow(pt[1] + half_h, 2) - std::pow(pt[1] - half_h, 2)) / (2.0 * h),
        (std::pow(pt[0] + half_h, 2) - std::pow(pt[0] - half_h, 2)) / (2.0 * h),
        ((std::pow(pt[1] + half_h, 3) - std::pow(pt[1] - half_h, 3)) / (3.0 * h) - 1. / 12 * h * h) / diameter,
        ((std::pow(pt[1] + half_h, 2) - std::pow(pt[1] - half_h, 2)) *
         (std::pow(pt[0] + half_h, 2) - std::pow(pt[0] - half_h, 2)) / (4.0 * h * h)) /
            diameter,
        ((std::pow(pt[0] + half_h, 3) - std::pow(pt[0] - half_h, 3)) / (3.0 * h) - 1. / 12 * h * h) /
            diameter}; // y, x, y^2, xy, x^2
    return subJ;
  }
};

TEST_F(ReconAux2DTest, n_Patch) { EXPECT_EQ(reconAux->n_Patch(0), (size_t)9); }

// MARK:- getPatch
TEST_F(ReconAux2DTest, getPatch) {
  ArrayView1<Vector<size_t, 2>> Stencil = reconAux->getPatch(0);
  EXPECT_EQ(Stencil.length(), (size_t)9);
  // right bottom
  EXPECT_EQ(Stencil[0][0], 1);
  EXPECT_EQ(Stencil[0][1], 9);

  // right
  EXPECT_EQ(Stencil[1][0], 1);
  EXPECT_EQ(Stencil[1][1], 0);

  // right up
  EXPECT_EQ(Stencil[2][0], 1);
  EXPECT_EQ(Stencil[2][1], 1);

  // mid bottom
  EXPECT_EQ(Stencil[3][0], 0);
  EXPECT_EQ(Stencil[3][1], 9);

  // mid
  EXPECT_EQ(Stencil[4][0], 0);
  EXPECT_EQ(Stencil[4][1], 0);

  // mid up
  EXPECT_EQ(Stencil[5][0], 0);
  EXPECT_EQ(Stencil[5][1], 1);

  // left bottom
  EXPECT_EQ(Stencil[6][0], 9);
  EXPECT_EQ(Stencil[6][1], 9);

  // left
  EXPECT_EQ(Stencil[7][0], 9);
  EXPECT_EQ(Stencil[7][1], 0);

  // left up
  EXPECT_EQ(Stencil[8][0], 9);
  EXPECT_EQ(Stencil[8][1], 1);
}

// MARK:- getPolyAvgs
TEST_F(ReconAux2DTest, getPolyAvgs) {
  ArrayView1<std::array<double, PolyInfo<2, order>::n_unknown>> m = reconAux->getPolyAvgs(0);
  EXPECT_EQ(m.length(), (size_t)9);
  for (int i = 0; i < 13; ++i) {
    EXPECT_EQ(m[i].size(), (size_t)5);
  }

  const double h = 0.1;
  Mesh2D::point_t bc(h / 2.0, h / 2.0);
  // right bottom
  Mesh2D::point_t pt(3 * h / 2.0, -h / 2.0);
  pt -= bc;
  Vector<double, 5> moment = ReconAux2DTest::subJ(pt);
  for (int i = 0; i < 5; ++i) {
    EXPECT_NEAR(m[0][i], moment[i], 1.0e-12) << i;
  }

  // right
  pt = Mesh2D::point_t(3 * h / 2.0, h / 2.0);
  pt -= bc;
  moment = ReconAux2DTest::subJ(pt);
  for (int i = 0; i < 5; ++i) {
    EXPECT_NEAR(m[1][i], moment[i], 1.0e-12) << i;
  }

  // right up
  pt = Mesh2D::point_t(3 * h / 2.0, 3 * h / 2.0);
  pt -= bc;
  moment = ReconAux2DTest::subJ(pt);
  for (int i = 0; i < 5; ++i) {
    EXPECT_NEAR(m[2][i], moment[i], 1.0e-12) << i;
  }

  // mid bottom
  pt = Mesh2D::point_t(h / 2.0, -h / 2.0);
  pt -= bc;
  moment = ReconAux2DTest::subJ(pt);
  for (int i = 0; i < 5; ++i) {
    EXPECT_NEAR(m[3][i], moment[i], 1.0e-12) << i;
  }

  // mid
  pt = Mesh2D::point_t(h / 2.0, h / 2.0);
  pt -= bc;
  moment = ReconAux2DTest::subJ(pt);
  for (int i = 0; i < 5; ++i) {
    EXPECT_NEAR(m[4][i], moment[i], 1.0e-12) << i;
  }

  // mid up
  pt = Mesh2D::point_t(h / 2.0, 3 * h / 2.0);
  pt -= bc;
  moment = ReconAux2DTest::subJ(pt);
  for (int i = 0; i < 5; ++i) {
    EXPECT_NEAR(m[5][i], moment[i], 1.0e-12) << i;
  }

  // left bottom
  pt = Mesh2D::point_t(-h / 2.0, -h / 2.0);
  pt -= bc;
  moment = ReconAux2DTest::subJ(pt);
  for (int i = 0; i < 5; ++i) {
    EXPECT_NEAR(m[6][i], moment[i], 1.0e-12) << i;
  }

  // left
  pt = Mesh2D::point_t(-h / 2.0, h / 2.0);
  pt -= bc;
  moment = ReconAux2DTest::subJ(pt);
  for (int i = 0; i < 5; ++i) {
    EXPECT_NEAR(m[7][i], moment[i], 1.0e-12) << i;
  }

  // left up
  pt = Mesh2D::point_t(-h / 2.0, 3 * h / 2.0);
  pt -= bc;
  moment = ReconAux2DTest::subJ(pt);
  for (int i = 0; i < 5; ++i) {
    EXPECT_NEAR(m[8][i], moment[i], 1.0e-12) << i;
  }
}

// MARK:- getG
TEST_F(ReconAux2DTest, getG) {
  ArrayView1<std::array<double, PolyInfo<2, order>::n_unknown>> m = reconAux->getPolyAvgs(0);
  Matrix<double, 5, 5> benchmark = Matrix<double, 5, 5>::makeZero();

  for (int i = 0; i < 9; ++i) {
    Matrix<double, 5, 5> tmp = Matrix<double, 5, 5>::makeZero();
    for (int j = 0; j < 5; ++j) {
      for (int k = 0; k < 5; ++k) {
        tmp(j, k) += m[i][j] * m[i][k];
      }
    }

    benchmark += tmp;
  }

  PolyInfo<2, order>::Mat GG = reconAux->getGInv({0, 0})->inverse();
  EXPECT_EQ(GG.rows(), 5);
  EXPECT_EQ(GG.cols(), 5);
  for (int i = 0; i < 5; i++) {
    for (int j = 0; j < 5; j++) {
      EXPECT_NEAR(benchmark(i, j), GG(i, j), 1.0e-16);
    }
  }
}
