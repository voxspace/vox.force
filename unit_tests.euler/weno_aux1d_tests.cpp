//
//  WENO1d_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2019/8/22.
//  Copyright © 2019 杨丰. All rights reserved.
//

#include "../src.euler/mesh.h"
#include "../src.euler/weno_auxiliary.h"
#include <gtest/gtest.h>

using namespace vox;

class WENOAux1DTest : public testing::Test {
public:
  void SetUp() override {
    grid = std::make_shared<Mesh<1>>(Vector<size_t, 1>({1000}), Vector<double, 1>({1.0 / 1000.0}));
    grid->addPBDescriptor(Direction::left);

    Aux = std::make_shared<WENOAuxiliary<1, 1>>(grid);
    Aux->buildReconstructPatch();
    Aux->buildWENOPatch();
    Aux->updateWENOInfo();
  }

  MeshPtr<1> grid;
  WENOAuxiliaryPtr<1, 1> Aux;
};

TEST_F(WENOAux1DTest, n_WENOPatch) {
  EXPECT_EQ(Aux->n_WENOPatch(0), (size_t)3);
  EXPECT_EQ(Aux->n_WENOPatch(10), (size_t)3);
}

TEST_F(WENOAux1DTest, getPatch) {
  ArrayView1<Vector<size_t, 1>> patch = Aux->getPatch(0, 0);
  EXPECT_EQ(patch.length(), (size_t)3);
  EXPECT_EQ(patch[0][0], 999);
  EXPECT_EQ(patch[1][0], 0);
  EXPECT_EQ(patch[2][0], 1);

  patch = Aux->getPatch(0, 1);
  EXPECT_EQ(patch.length(), (size_t)3);
  EXPECT_EQ(patch[0][0], 0);
  EXPECT_EQ(patch[1][0], 1);
  EXPECT_EQ(patch[2][0], 2);

  patch = Aux->getPatch(0, 2);
  EXPECT_EQ(patch.length(), (size_t)3);
  EXPECT_EQ(patch[0][0], 998);
  EXPECT_EQ(patch[1][0], 999);
  EXPECT_EQ(patch[2][0], 0);
}

TEST_F(WENOAux1DTest, getGInv) {
  auto InvA = Aux->getGInv(0);
  EXPECT_EQ(InvA.length(), (size_t)3);
  double h = 0.001;
  EXPECT_NEAR(InvA[0](0, 0), 1. / ((1 + 1) * h * h), 1.0e-7);
  EXPECT_DOUBLE_EQ(InvA[1](0, 0), 1. / ((1 + 4) * h * h));
  EXPECT_NEAR(InvA[2](0, 0), 1. / ((1 + 4) * h * h), 1.0e-7);
}
