//
//  EqLaplacian_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2019/8/21.
//  Copyright © 2019 杨丰. All rights reserved.
//

#include "../src.euler/equations/equation_scalar.h"
#include <gtest/gtest.h>

using namespace vox;

class EqScalar1DTest : public testing::Test {
public:
  void SetUp() override {
    std::array<EquationScalar<1>::fluxFunc, 1> flux{};
    std::array<EquationScalar<1>::fluxFunc, 1> fluxDer{};
    std::array<EquationScalar<1>::viscousFunc, 1> vfuncs = {ScalarEquationDataBase::ViscousTerm::constVal(1.0)};

    eq = std::make_shared<EquationScalar<1>>(flux, fluxDer, vfuncs);
  }

  EquationScalarPtr<1> eq;
};

TEST_F(EqScalar1DTest, getDos) { EXPECT_EQ(EquationScalar<1>::DOS, 1); }

TEST_F(EqScalar1DTest, TFluxViscous) {
  Vector<double, 1> flux;
  Vector<double, 1> u(1.0);
  Vector<Vector<double, 1>, 1> gu{{1.0}};
  eq->TFlux(u, gu, {1.0}, flux, true);
  EXPECT_EQ(flux[0], 1.0);
}

class EqScalar2DTest : public testing::Test {
public:
  void SetUp() override {
    std::array<EquationScalar<2>::fluxFunc, 2> flux = {ScalarEquationDataBase::Advection::flux(1.0),
                                                       ScalarEquationDataBase::Advection::flux(2.0)};
    std::array<EquationScalar<2>::fluxFunc, 2> fluxDer = {ScalarEquationDataBase::Advection::fluxDer(1.0),
                                                          ScalarEquationDataBase::Advection::fluxDer(2.0)};
    std::array<EquationScalar<2>::viscousFunc, 2> vfuncs = {ScalarEquationDataBase::ViscousTerm::constVal(1.0),
                                                            ScalarEquationDataBase::ViscousTerm::constVal(1.0)};

    eq = std::make_shared<EquationScalar<2>>(flux, fluxDer, vfuncs);
  }

  EquationScalarPtr<2> eq;
};

TEST_F(EqScalar2DTest, getDim) { EXPECT_EQ(EquationScalar<2>::DIM, 2); }

TEST_F(EqScalar2DTest, MaxCharacteristicSpeed) { EXPECT_EQ(eq->MaxCharacteristicSpeed({0}), std::sqrt(5)); }

TEST_F(EqScalar2DTest, TFluxVisicd) {
  Vector<double, 1> flux;
  Vector<double, 1> u(1.0);
  std::array<double, 2> outNorm = {2, 3};

  eq->TFlux(u, outNorm, flux);
  EXPECT_EQ(flux.rows(), (size_t)1);
  EXPECT_DOUBLE_EQ(flux[0], 8);
}

TEST_F(EqScalar2DTest, TFluxFull) {
  Vector<double, 1> flux;
  Vector<double, 1> u(1.0);
  Vector<Vector<double, 2>, 1> gu = {{1.0, 1.0}};
  std::array<double, 2> outNorm = {2, 3};

  eq->TFlux(u, gu, outNorm, flux, false);
  EXPECT_EQ(flux.rows(), (size_t)1);
  EXPECT_DOUBLE_EQ(flux[0], 3);
}
