//
//  gridData3d_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2019/8/25.
//  Copyright © 2019 杨丰. All rights reserved.
//

#include "../src.euler/grid_data.h"
#include "../src.euler/mesh.h"

#include <gtest/gtest.h>

using namespace vox;

class GridData3DTest : public testing::Test {
public:
  void SetUp() override {
    grid = std::make_shared<Mesh<3>>(Vector3UZ({10, 10, 10}), Vector<double, 3>({0.1, 0.1, 0.1}));

    grid->addPBDescriptor(Direction::left);
    grid->addPBDescriptor(Direction::bottom);
    grid->addPBDescriptor(Direction::front);

    data = std::make_shared<GridData<3>>(0, grid);
  }

  MeshPtr<3> grid;
  GridDataPtr<3> data;
};

TEST_F(GridData3DTest, numberOfData) {
  auto total = product<size_t, 3>(data->numberOfData(), 1);
  EXPECT_EQ(total, (size_t)1000);
}

TEST_F(GridData3DTest, deepCopy) {
  data->setData(std::vector<double>(1000, 26.94));
  GridDataPtr<3> data2 = data->deepCopy();
  data2->setData(std::vector<double>(1000, 36.28));
  EXPECT_EQ(data2->operator[](10), 36.28);
  EXPECT_EQ(data->operator[](10), 26.94);
}

TEST_F(GridData3DTest, Pointvalue1) {
  data->operator[](0) = 100.23;
  Mesh3D::point_t pt = {1.0, 3.2, 5.3};
  EXPECT_EQ(data->value(pt, {0, 0, 0}), 100.23);
}

TEST_F(GridData3DTest, Gradient1) {
  data->operator[](0) = 100.23;
  Mesh3D::point_t pt = {1.0, 3.2, 5.3};
  std::array<double, 3> result{};
  EXPECT_EQ(data->gradient(pt, {0, 0, 0}), result);
}