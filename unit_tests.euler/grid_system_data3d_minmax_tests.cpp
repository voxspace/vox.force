//
//  GridSysData3DMaxPrinciple_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2019/9/1.
//  Copyright © 2019 杨丰. All rights reserved.
//

#include "../src.euler/equations/equation_scalar.h"
#include "../src.euler/grid_system_data_minmax.h"
#include "../src.euler/mesh.h"
#include <gtest/gtest.h>

namespace vox {

class GridSysData3DMaxPrincipleTest : public testing::Test {
public:
  void SetUp() override {
    grid = std::make_shared<Mesh<3>>(Vector3UZ({10, 10, 10}), Vector<double, 3>({0.1, 0.1, 0.1}));

    grid->addPBDescriptor(Direction::left);
    grid->addPBDescriptor(Direction::bottom);
    grid->addPBDescriptor(Direction::front);

    std::array<EquationScalar<3>::fluxFunc, 3> flux;
    flux[0] = ScalarEquationDataBase::Advection::flux(1.0);
    flux[1] = ScalarEquationDataBase::Advection::flux(2.0);
    flux[2] = ScalarEquationDataBase::Advection::flux(3.0);
    std::array<EquationScalar<3>::fluxFunc, 3> fluxDer;
    fluxDer[0] = ScalarEquationDataBase::Advection::fluxDer(1.0);
    fluxDer[1] = ScalarEquationDataBase::Advection::fluxDer(2.0);
    fluxDer[2] = ScalarEquationDataBase::Advection::fluxDer(3.0);
    std::array<EquationScalar<3>::viscousFunc, 3> vfuncs{};

    eqInfo = std::make_shared<EquationScalar<3>>(flux, fluxDer, vfuncs);

    data = GridSysDataMaxPrinciple<3, 2>::builder().build(grid, -1, 1);

    Vector1D tmp;
    for (size_t i = 0; i < grid->dataSize()[0]; ++i) {
      for (size_t j = 0; j < grid->dataSize()[1]; ++j) {
        for (size_t k = 0; k < grid->dataSize()[2]; ++k) {
          init3d(grid->BaryCenter({i, j, k}), tmp);
          data->setVectorData({i, j, k}, tmp);
        }
      }
    }
  }

  MeshPtr<3> grid;
  EquationScalarPtr<3> eqInfo;
  GridSysDataMaxPrinciplePtr<3, 2> data;

  static void init3d(const Mesh3D::point_t &pt, Vector1D &value) {
    value[0] = std::sin(2 * M_PI * pt[0]) * std::sin(2 * M_PI * pt[1]) * std::sin(2 * M_PI * pt[2]);
  }

  static void bd3d(const Vector1D &val, Vector1D &result, const std::array<double, 3> &outNormal,
                   const Mesh3D::point_t &pt, int flag) {
    result = val;
  }

public:
  double theta(const Vector3UZ &idx) { return data->thetas(idx); }

  std::vector<Mesh3D::point_t> inner_qi(const Vector3UZ &idx) { return data->quad_pts(idx); }

  double minimum() { return data->minimum; }

  double maximum() { return data->maximum; }

  double deepCopyMin() { return std::dynamic_pointer_cast<GridSysDataMaxPrinciple<3, 2>>(data->deepCopy())->minimum; }

  double deepCopyMax() { return std::dynamic_pointer_cast<GridSysDataMaxPrinciple<3, 2>>(data->deepCopy())->maximum; }
};

TEST_F(GridSysData3DMaxPrincipleTest, minimum) { EXPECT_EQ(minimum(), -1); }

TEST_F(GridSysData3DMaxPrincipleTest, deepCopyMin) { EXPECT_EQ(deepCopyMin(), -1); }

TEST_F(GridSysData3DMaxPrincipleTest, maximum) { EXPECT_EQ(maximum(), 1); }

TEST_F(GridSysData3DMaxPrincipleTest, deepCopyMax) { EXPECT_EQ(deepCopyMax(), 1); }

TEST_F(GridSysData3DMaxPrincipleTest, theta) {
  data->calculateBasisFunction();
  for (size_t i = 0; i < grid->dataSize()[0]; ++i) {
    for (size_t j = 0; j < grid->dataSize()[1]; ++j) {
      for (size_t k = 0; k < grid->dataSize()[2]; ++k) {
        EXPECT_EQ(theta({i, j, k}), 1.0);
      }
    }
  }
}

TEST_F(GridSysData3DMaxPrincipleTest, innerqi) {
  std::vector<Mesh3D::point_t> qi = inner_qi({0, 0, 0});
  EXPECT_EQ(qi.size(), (size_t)1 + 4 * 6);

  const double h = 0.1;
  Mesh3D::point_t bc = {1.0 / 2.0 * h, 1.0 / 2.0 * h, 1.0 / 2.0 * h};
  std::vector<Mesh3D::point_t> p(4 * 6);
  // front
  p[0] = {0, -1.0 / std::sqrt(3) * h / 2.0 + h / 2, -1.0 / std::sqrt(3) * h / 2.0 + h / 2};
  p[1] = {0, -1.0 / std::sqrt(3) * h / 2.0 + h / 2, 1.0 / std::sqrt(3) * h / 2.0 + h / 2};
  p[2] = {0, 1.0 / std::sqrt(3) * h / 2.0 + h / 2, -1.0 / std::sqrt(3) * h / 2.0 + h / 2};
  p[3] = {0, 1.0 / std::sqrt(3) * h / 2.0 + h / 2, 1.0 / std::sqrt(3) * h / 2.0 + h / 2};

  // back
  p[4] = {h, -1.0 / std::sqrt(3) * h / 2.0 + h / 2, -1.0 / std::sqrt(3) * h / 2.0 + h / 2};
  p[5] = {h, -1.0 / std::sqrt(3) * h / 2.0 + h / 2, 1.0 / std::sqrt(3) * h / 2.0 + h / 2};
  p[6] = {h, 1.0 / std::sqrt(3) * h / 2.0 + h / 2, -1.0 / std::sqrt(3) * h / 2.0 + h / 2};
  p[7] = {h, 1.0 / std::sqrt(3) * h / 2.0 + h / 2, 1.0 / std::sqrt(3) * h / 2.0 + h / 2};

  // bottom
  p[8] = {-1.0 / std::sqrt(3) * h / 2.0 + h / 2, 0.0, -1.0 / std::sqrt(3) * h / 2.0 + h / 2};
  p[9] = {-1.0 / std::sqrt(3) * h / 2.0 + h / 2, 0.0, 1.0 / std::sqrt(3) * h / 2.0 + h / 2};
  p[10] = {1.0 / std::sqrt(3) * h / 2.0 + h / 2, 0.0, -1.0 / std::sqrt(3) * h / 2.0 + h / 2};
  p[11] = {1.0 / std::sqrt(3) * h / 2.0 + h / 2, 0.0, 1.0 / std::sqrt(3) * h / 2.0 + h / 2};

  // up
  p[12] = {-1.0 / std::sqrt(3) * h / 2.0 + h / 2, h, -1.0 / std::sqrt(3) * h / 2.0 + h / 2};
  p[13] = {-1.0 / std::sqrt(3) * h / 2.0 + h / 2, h, 1.0 / std::sqrt(3) * h / 2.0 + h / 2};
  p[14] = {1.0 / std::sqrt(3) * h / 2.0 + h / 2, h, -1.0 / std::sqrt(3) * h / 2.0 + h / 2};
  p[15] = {1.0 / std::sqrt(3) * h / 2.0 + h / 2, h, 1.0 / std::sqrt(3) * h / 2.0 + h / 2};

  // left
  p[16] = {-1.0 / std::sqrt(3) * h / 2.0 + h / 2, -1.0 / std::sqrt(3) * h / 2.0 + h / 2, 0.0};
  p[17] = {-1.0 / std::sqrt(3) * h / 2.0 + h / 2, 1.0 / std::sqrt(3) * h / 2.0 + h / 2, 0.0};
  p[18] = {1.0 / std::sqrt(3) * h / 2.0 + h / 2, -1.0 / std::sqrt(3) * h / 2.0 + h / 2, 0.0};
  p[19] = {1.0 / std::sqrt(3) * h / 2.0 + h / 2, 1.0 / std::sqrt(3) * h / 2.0 + h / 2, 0.0};

  // right
  p[20] = {-1.0 / std::sqrt(3) * h / 2.0 + h / 2, -1.0 / std::sqrt(3) * h / 2.0 + h / 2, h};
  p[21] = {-1.0 / std::sqrt(3) * h / 2.0 + h / 2, 1.0 / std::sqrt(3) * h / 2.0 + h / 2, h};
  p[22] = {1.0 / std::sqrt(3) * h / 2.0 + h / 2, -1.0 / std::sqrt(3) * h / 2.0 + h / 2, h};
  p[23] = {1.0 / std::sqrt(3) * h / 2.0 + h / 2, 1.0 / std::sqrt(3) * h / 2.0 + h / 2, h};

  EXPECT_NEAR(qi[0][0], bc[0], 1.0e-13);
  EXPECT_DOUBLE_EQ(qi[0][1], bc[1]);
  EXPECT_DOUBLE_EQ(qi[0][2], bc[2]);

  for (int i = 0; i < 4 * 6; ++i) {
    EXPECT_NEAR(qi[i + 1][0], p[i][0], 1.0e-12);
    EXPECT_DOUBLE_EQ(qi[i + 1][1], p[i][1]);
    EXPECT_DOUBLE_EQ(qi[i + 1][2], p[i][2]);
  }
}

} // namespace vox
