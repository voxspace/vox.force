//
//  reconAux3d_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2019/11/27.
//  Copyright © 2019 杨丰. All rights reserved.
//

#include "../src.euler/mesh.h"
#include "../src.euler/recon_auxiliary.h"
#include <gtest/gtest.h>

using namespace vox;

class ReconAux3DTest : public testing::Test {
public:
  void SetUp() override {
    grid = std::make_shared<Mesh<3>>(Vector3UZ({10, 10, 10}), Vector<double, 3>({0.1, 0.1, 0.1}));

    grid->addPBDescriptor(Direction::left);
    grid->addPBDescriptor(Direction::bottom);
    grid->addPBDescriptor(Direction::front);

    reconAux = std::make_shared<ReconAuxiliary<3, 2>>(grid);
    reconAux->buildReconstructPatch(PatchSearcher<3, 2>::Node);
    reconAux->updateLSMatrix();
  }

  static constexpr int order = 2;
  MeshPtr<3> grid;
  ReconAuxiliaryPtr<3, order> reconAux;

public:
  std::vector<double> subJ(Mesh3D::point_t pt) {
    const double h = 0.1;
    const double half_h = h / 2;
    std::vector<double> subJ = {
        (std::pow(pt[2] + half_h, 2) - std::pow(pt[2] - half_h, 2)) / (2.0 * h),
        (std::pow(pt[1] + half_h, 2) - std::pow(pt[1] - half_h, 2)) / (2.0 * h),
        (std::pow(pt[0] + half_h, 2) - std::pow(pt[0] - half_h, 2)) / (2.0 * h),
        ((std::pow(pt[2] + half_h, 3) - std::pow(pt[2] - half_h, 3)) / (3.0 * h) - 1. / 12 * h * h) / h,
        ((std::pow(pt[2] + half_h, 2) - std::pow(pt[2] - half_h, 2)) *
         (std::pow(pt[1] + half_h, 2) - std::pow(pt[1] - half_h, 2)) / (4.0 * h * h)) /
            h,
        ((std::pow(pt[1] + half_h, 3) - std::pow(pt[1] - half_h, 3)) / (3.0 * h) - 1. / 12 * h * h) / h,
        ((std::pow(pt[2] + half_h, 2) - std::pow(pt[2] - half_h, 2)) *
         (std::pow(pt[0] + half_h, 2) - std::pow(pt[0] - half_h, 2)) / (4.0 * h * h)) /
            h,
        ((std::pow(pt[1] + half_h, 2) - std::pow(pt[1] - half_h, 2)) *
         (std::pow(pt[0] + half_h, 2) - std::pow(pt[0] - half_h, 2)) / (4.0 * h * h)) /
            h,
        ((std::pow(pt[0] + half_h, 3) - std::pow(pt[0] - half_h, 3)) / (3.0 * h) - 1. / 12 * h * h) /
            h}; // z, y, x, z^2, yz, y^2, xz, xy, x^2
    return subJ;
  }
};

TEST_F(ReconAux3DTest, n_Patch) { EXPECT_EQ(reconAux->n_Patch(0), (size_t)27); }

TEST_F(ReconAux3DTest, getPatch) {
  ArrayView1<Vector3UZ> Stencil = reconAux->getPatch(0);
  EXPECT_EQ(Stencil.length(), (size_t)27);

  Array3<double> index(grid->dataSize());
  EXPECT_EQ(index.index(Stencil[4]), 0);
  EXPECT_EQ(index.index(Stencil[1]), 1);
  EXPECT_EQ(index.index(Stencil[7]), 9);
  EXPECT_EQ(index.index(Stencil[5]), 10);
  EXPECT_EQ(index.index(Stencil[2]), 11);
  EXPECT_EQ(index.index(Stencil[8]), 19);
  EXPECT_EQ(index.index(Stencil[3]), 90);
  EXPECT_EQ(index.index(Stencil[0]), 91);
  EXPECT_EQ(index.index(Stencil[6]), 99);
  EXPECT_EQ(index.index(Stencil[13]), 100);
  EXPECT_EQ(index.index(Stencil[10]), 101);
  EXPECT_EQ(index.index(Stencil[16]), 109);
  EXPECT_EQ(index.index(Stencil[14]), 110);
  EXPECT_EQ(index.index(Stencil[11]), 111);
  EXPECT_EQ(index.index(Stencil[17]), 119);
  EXPECT_EQ(index.index(Stencil[12]), 190);
  EXPECT_EQ(index.index(Stencil[9]), 191);
  EXPECT_EQ(index.index(Stencil[15]), 199);
  EXPECT_EQ(index.index(Stencil[22]), 900);
  EXPECT_EQ(index.index(Stencil[19]), 901);
  EXPECT_EQ(index.index(Stencil[25]), 909);
  EXPECT_EQ(index.index(Stencil[23]), 910);
  EXPECT_EQ(index.index(Stencil[20]), 911);
  EXPECT_EQ(index.index(Stencil[26]), 919);
  EXPECT_EQ(index.index(Stencil[21]), 990);
  EXPECT_EQ(index.index(Stencil[18]), 991);
  EXPECT_EQ(index.index(Stencil[24]), 999);
}

TEST_F(ReconAux3DTest, getPolyAvgs) {
  ArrayView1<std::array<double, PolyInfo<3, order>::n_unknown>> m = reconAux->getPolyAvgs(0);
  EXPECT_EQ(m.length(), (size_t)27);
  for (int i = 0; i < 27; ++i) {
    EXPECT_EQ(m[i].size(), (size_t)9);
  }

  const double h = 0.1;
  Mesh3D::point_t bc = grid->BaryCenter({0, 0, 0});

  // 0
  Mesh3D::point_t pt = {3 * h / 2.0, -h / 2.0, h / 2.0};
  pt -= bc;
  std::vector<double> moment = ReconAux3DTest::subJ(pt);
  for (int i = 0; i < 9; ++i) {
    EXPECT_NEAR(m[0][i], moment[i], 1.0e-13);
  }

  // 1
  pt = {3 * h / 2.0, h / 2.0, h / 2.0};
  pt -= bc;
  moment = ReconAux3DTest::subJ(pt);
  for (int i = 0; i < 9; ++i) {
    EXPECT_NEAR(m[1][i], moment[i], 1.0e-13);
  }

  // 2
  pt = {3 * h / 2.0, 3 * h / 2.0, h / 2.0};
  pt -= bc;
  moment = ReconAux3DTest::subJ(pt);
  for (int i = 0; i < 9; ++i) {
    EXPECT_NEAR(m[2][i], moment[i], 1.0e-13);
  }

  // 3
  pt = {h / 2.0, -h / 2.0, h / 2.0};
  pt -= bc;
  moment = ReconAux3DTest::subJ(pt);
  for (int i = 0; i < 9; ++i) {
    EXPECT_NEAR(m[3][i], moment[i], 1.0e-13);
  }

  // 4
  for (int i = 0; i < 9; ++i) {
    EXPECT_DOUBLE_EQ(m[4][i], 0.0);
  }

  // 5
  pt = {h / 2.0, 3 * h / 2.0, h / 2.0};
  pt -= bc;
  moment = ReconAux3DTest::subJ(pt);
  for (int i = 0; i < 9; ++i) {
    EXPECT_NEAR(m[5][i], moment[i], 1.0e-13);
  }

  // 6
  pt = {-h / 2.0, -h / 2.0, h / 2.0};
  pt -= bc;
  moment = ReconAux3DTest::subJ(pt);
  for (int i = 0; i < 9; ++i) {
    EXPECT_NEAR(m[6][i], moment[i], 1.0e-13);
  }

  // 7
  pt = {-h / 2.0, h / 2.0, h / 2.0};
  pt -= bc;
  moment = ReconAux3DTest::subJ(pt);
  for (int i = 0; i < 9; ++i) {
    EXPECT_NEAR(m[7][i], moment[i], 1.0e-13);
  }

  // 8
  pt = {-h / 2.0, 3 * h / 2.0, h / 2.0};
  pt -= bc;
  moment = ReconAux3DTest::subJ(pt);
  for (int i = 0; i < 9; ++i) {
    EXPECT_NEAR(m[8][i], moment[i], 1.0e-13);
  }

  // 9
  pt = {3 * h / 2.0, -h / 2.0, 3 * h / 2.0};
  pt -= bc;
  moment = ReconAux3DTest::subJ(pt);
  for (int i = 0; i < 9; ++i) {
    EXPECT_NEAR(m[9][i], moment[i], 1.0e-12);
  }

  // 10
  pt = {3 * h / 2.0, h / 2.0, 3 * h / 2.0};
  pt -= bc;
  moment = ReconAux3DTest::subJ(pt);
  for (int i = 0; i < 9; ++i) {
    EXPECT_NEAR(m[10][i], moment[i], 1.0e-12);
  }

  // 11
  pt = {3 * h / 2.0, 3 * h / 2.0, 3 * h / 2.0};
  pt -= bc;
  moment = ReconAux3DTest::subJ(pt);
  for (int i = 0; i < 9; ++i) {
    EXPECT_NEAR(m[11][i], moment[i], 1.0e-12);
  }

  // 12
  pt = {h / 2.0, -h / 2.0, 3 * h / 2.0};
  pt -= bc;
  moment = ReconAux3DTest::subJ(pt);
  for (int i = 0; i < 9; ++i) {
    EXPECT_NEAR(m[12][i], moment[i], 1.0e-12);
  }

  // 13
  pt = {h / 2.0, h / 2.0, 3 * h / 2.0};
  pt -= bc;
  moment = ReconAux3DTest::subJ(pt);
  for (int i = 0; i < 9; ++i) {
    EXPECT_NEAR(m[13][i], moment[i], 1.0e-12);
  }

  // 14
  pt = {h / 2.0, 3 * h / 2.0, 3 * h / 2.0};
  pt -= bc;
  moment = ReconAux3DTest::subJ(pt);
  for (int i = 0; i < 9; ++i) {
    EXPECT_NEAR(m[14][i], moment[i], 1.0e-12);
  }

  // 15
  pt = {-h / 2.0, -h / 2.0, 3 * h / 2.0};
  pt -= bc;
  moment = ReconAux3DTest::subJ(pt);
  for (int i = 0; i < 9; ++i) {
    EXPECT_NEAR(m[15][i], moment[i], 1.0e-12);
  }

  // 16
  pt = {-h / 2.0, h / 2.0, 3 * h / 2.0};
  pt -= bc;
  moment = ReconAux3DTest::subJ(pt);
  for (int i = 0; i < 9; ++i) {
    EXPECT_NEAR(m[16][i], moment[i], 1.0e-12);
  }

  // 17
  pt = {-h / 2.0, 3 * h / 2.0, 3 * h / 2.0};
  pt -= bc;
  moment = ReconAux3DTest::subJ(pt);
  for (int i = 0; i < 9; ++i) {
    EXPECT_NEAR(m[17][i], moment[i], 1.0e-12);
  }

  // 18
  pt = {3 * h / 2.0, -h / 2.0, -h / 2.0};
  pt -= bc;
  moment = ReconAux3DTest::subJ(pt);
  for (int i = 0; i < 9; ++i) {
    EXPECT_NEAR(m[18][i], moment[i], 1.0e-12);
  }

  // 19
  pt = {3 * h / 2.0, h / 2.0, -h / 2.0};
  pt -= bc;
  moment = ReconAux3DTest::subJ(pt);
  for (int i = 0; i < 9; ++i) {
    EXPECT_NEAR(m[19][i], moment[i], 1.0e-12);
  }

  // 20
  pt = {3 * h / 2.0, 3 * h / 2.0, -h / 2.0};
  pt -= bc;
  moment = ReconAux3DTest::subJ(pt);
  for (int i = 0; i < 9; ++i) {
    EXPECT_NEAR(m[20][i], moment[i], 1.0e-12);
  }

  // 21
  pt = {h / 2.0, -h / 2.0, -h / 2.0};
  pt -= bc;
  moment = ReconAux3DTest::subJ(pt);
  for (int i = 0; i < 9; ++i) {
    EXPECT_NEAR(m[21][i], moment[i], 1.0e-12);
  }

  // 22
  pt = {h / 2.0, h / 2.0, -h / 2.0};
  pt -= bc;
  moment = ReconAux3DTest::subJ(pt);
  for (int i = 0; i < 9; ++i) {
    EXPECT_NEAR(m[22][i], moment[i], 1.0e-12);
  }

  // 23
  pt = {h / 2.0, 3 * h / 2.0, -h / 2.0};
  pt -= bc;
  moment = ReconAux3DTest::subJ(pt);
  for (int i = 0; i < 9; ++i) {
    EXPECT_NEAR(m[23][i], moment[i], 1.0e-12);
  }

  // 24
  pt = {-h / 2.0, -h / 2.0, -h / 2.0};
  pt -= bc;
  moment = ReconAux3DTest::subJ(pt);
  for (int i = 0; i < 9; ++i) {
    EXPECT_NEAR(m[24][i], moment[i], 1.0e-12);
  }

  // 25
  pt = {-h / 2.0, h / 2.0, -h / 2.0};
  pt -= bc;
  moment = ReconAux3DTest::subJ(pt);
  for (int i = 0; i < 9; ++i) {
    EXPECT_NEAR(m[25][i], moment[i], 1.0e-12);
  }

  // 26
  pt = {-h / 2.0, 3 * h / 2.0, -h / 2.0};
  pt -= bc;
  moment = ReconAux3DTest::subJ(pt);
  for (int i = 0; i < 9; ++i) {
    EXPECT_NEAR(m[26][i], moment[i], 1.0e-12);
  }
}

TEST_F(ReconAux3DTest, getG) {
  ArrayView1<std::array<double, PolyInfo<3, order>::n_unknown>> m = reconAux->getPolyAvgs(0);
  Matrix<double, 9, 9> benchmark = Matrix<double, 9, 9>::makeZero();

  for (int i = 0; i < 27; ++i) {
    Matrix<double, 9, 9> tmp = Matrix<double, 9, 9>::makeZero();
    for (int j = 0; j < 9; ++j) {
      for (int k = 0; k < 9; ++k) {
        tmp(j, k) += m[i][j] * m[i][k];
      }
    }

    benchmark += tmp;
  }

  PolyInfo<3, order>::Mat GG = reconAux->getGInv({0, 0, 0})->inverse();
  EXPECT_EQ(GG.rows(), 9);
  EXPECT_EQ(GG.cols(), 9);
  for (int i = 0; i < 9; i++) {
    for (int j = 0; j < 9; j++) {
      EXPECT_NEAR(benchmark(i, j), GG(i, j), 1.0e-15);
    }
  }
}
