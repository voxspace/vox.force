//
//  gridData3dUnlimit_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2019/8/25.
//  Copyright © 2019 杨丰. All rights reserved.
//

#include "../src.euler/grid_data_unlimited.h"
#include "../src.euler/mesh.h"
#include "../src.euler/recon_auxiliary.h"
#include <gtest/gtest.h>

namespace vox {

class GridData3DUnlimitedTest : public testing::Test {
public:
  void SetUp() override {
    grid = std::make_shared<Mesh<3>>(Vector3UZ({10, 10, 10}), Vector<double, 3>({0.1, 0.1, 0.1}));

    grid->addPBDescriptor(Direction::left);
    grid->addPBDescriptor(Direction::bottom);
    grid->addPBDescriptor(Direction::front);

    ReconAuxiliaryPtr<3, 1> aux = std::make_shared<ReconAuxiliary<3, 1>>(grid);
    aux->buildReconstructPatch(PatchSearcher<3, 1>::Node);
    aux->updateLSMatrix();
    GridDataUnlimitedBuilderPtr<3, 1> UnlimitedDataBuilder = std::make_shared<GridDataUnlimited<3, 1>::Builder>();
    UnlimitedDataBuilder->setReconAuxiliary(aux);

    data = std::dynamic_pointer_cast<GridDataUnlimited<3, 1>>(UnlimitedDataBuilder->build(0, grid));
    for (size_t i = 0; i < grid->dataSize()[0]; ++i) {
      for (size_t j = 0; j < grid->dataSize()[1]; ++j) {
        for (size_t k = 0; k < grid->dataSize()[2]; ++k) {
          data->operator()({i, j, k}) = std::sin(2 * M_PI * grid->BaryCenter({i, j, k})[0]) *
                                        std::sin(2 * M_PI * grid->BaryCenter({i, j, k})[1]) *
                                        std::sin(2 * M_PI * grid->BaryCenter({i, j, k})[2]);
        }
      }
    }
    data->calculateBasisFunction();
  }

  MeshPtr<3> grid;
  GridDataUnlimitedPtr<3, 1> data;

public:
  typename PolyInfo<3, 1>::Vec Slope(int idx) { return data->recon_cache[idx].Slope; }

  static Vector3D subJ(Mesh3D::point_t pt) {
    const double h = 0.1;
    const double half_h = h / 2;
    Vector3D subJ((std::pow(pt[2] + half_h, 2) - std::pow(pt[2] - half_h, 2)) / (2.0 * h),
                  (std::pow(pt[1] + half_h, 2) - std::pow(pt[1] - half_h, 2)) / (2.0 * h),
                  (std::pow(pt[0] + half_h, 2) - std::pow(pt[0] - half_h, 2)) / (2.0 * h)); // z, y, x
    return subJ;
  }
};

TEST_F(GridData3DUnlimitedTest, Slope) {
  auto slope = Slope(0);

  Matrix3x3D G_inv = Matrix3x3D::makeIdentity();
  G_inv /= 0.18; // get from IQR G's first two row and col

  Vector3D c = Vector3D::makeZero();
  const double h = 0.1;
  Mesh3D::point_t bc = grid->BaryCenter({0, 0, 0});
  // 1
  Mesh3D::point_t pt = {h / 2.0, h / 2.0, 3 * h / 2.0};
  pt -= bc;
  Vector3D moment = subJ(pt);
  moment *= data->GridDataBase::value({0, 0, 1}) - data->GridDataBase::value({0, 0, 0});
  c += moment;

  // 9
  pt = {h / 2.0, h / 2.0, -h / 2.0};
  pt -= bc;
  moment = subJ(pt);
  moment *= data->GridDataBase::value({0, 0, 9}) - data->GridDataBase::value({0, 0, 0});
  c += moment;

  // 10
  pt = {h / 2.0, 3 * h / 2.0, h / 2.0};
  pt -= bc;
  moment = subJ(pt);
  moment *= data->GridDataBase::value({0, 1, 0}) - data->GridDataBase::value({0, 0, 0});
  c += moment;

  // 11
  pt = {h / 2.0, 3 * h / 2.0, 3 * h / 2.0};
  pt -= bc;
  moment = subJ(pt);
  moment *= data->GridDataBase::value({0, 1, 1}) - data->GridDataBase::value({0, 0, 0});
  c += moment;

  // 19
  pt = {h / 2.0, 3 * h / 2.0, -h / 2.0};
  pt -= bc;
  moment = subJ(pt);
  moment *= data->GridDataBase::value({0, 1, 9}) - data->GridDataBase::value({0, 0, 0});
  c += moment;

  // 90
  pt = {h / 2.0, -h / 2.0, h / 2.0};
  pt -= bc;
  moment = subJ(pt);
  moment *= data->GridDataBase::value({0, 9, 0}) - data->GridDataBase::value({0, 0, 0});
  c += moment;

  // 91
  pt = {h / 2.0, -h / 2.0, 3 * h / 2.0};
  pt -= bc;
  moment = subJ(pt);
  moment *= data->GridDataBase::value({0, 9, 1}) - data->GridDataBase::value({0, 0, 0});
  c += moment;

  // 99
  pt = {h / 2.0, -h / 2.0, -h / 2.0};
  pt -= bc;
  moment = subJ(pt);
  moment *= data->GridDataBase::value({0, 9, 9}) - data->GridDataBase::value({0, 0, 0});
  c += moment;

  // 100
  pt = {3 * h / 2.0, h / 2.0, h / 2.0};
  pt -= bc;
  moment = subJ(pt);
  moment *= data->GridDataBase::value({1, 0, 0}) - data->GridDataBase::value({0, 0, 0});
  c += moment;

  // 101
  pt = {3 * h / 2.0, h / 2.0, 3 * h / 2.0};
  pt -= bc;
  moment = subJ(pt);
  moment *= data->GridDataBase::value({1, 0, 1}) - data->GridDataBase::value({0, 0, 0});
  c += moment;

  // 109
  pt = {3 * h / 2.0, h / 2.0, -h / 2.0};
  pt -= bc;
  moment = subJ(pt);
  moment *= data->GridDataBase::value({1, 0, 9}) - data->GridDataBase::value({0, 0, 0});
  c += moment;

  // 110
  pt = {3 * h / 2.0, 3 * h / 2.0, h / 2.0};
  pt -= bc;
  moment = subJ(pt);
  moment *= data->GridDataBase::value({1, 1, 0}) - data->GridDataBase::value({0, 0, 0});
  c += moment;

  // 111
  pt = {3 * h / 2.0, 3 * h / 2.0, 3 * h / 2.0};
  pt -= bc;
  moment = subJ(pt);
  moment *= data->GridDataBase::value({1, 1, 1}) - data->GridDataBase::value({0, 0, 0});
  c += moment;

  // 119
  pt = {3 * h / 2.0, 3 * h / 2.0, -h / 2.0};
  pt -= bc;
  moment = subJ(pt);
  moment *= data->GridDataBase::value({1, 1, 9}) - data->GridDataBase::value({0, 0, 0});
  c += moment;

  // 190
  pt = {3 * h / 2.0, -h / 2.0, h / 2.0};
  pt -= bc;
  moment = subJ(pt);
  moment *= data->GridDataBase::value({1, 9, 0}) - data->GridDataBase::value({0, 0, 0});
  c += moment;

  // 191
  pt = {3 * h / 2.0, -h / 2.0, 3 * h / 2.0};
  pt -= bc;
  moment = subJ(pt);
  moment *= data->GridDataBase::value({1, 9, 1}) - data->GridDataBase::value({0, 0, 0});
  c += moment;

  // 199
  pt = {3 * h / 2.0, -h / 2.0, -h / 2.0};
  pt -= bc;
  moment = subJ(pt);
  moment *= data->GridDataBase::value({1, 9, 9}) - data->GridDataBase::value({0, 0, 0});
  c += moment;

  // 900
  pt = {-h / 2.0, h / 2.0, h / 2.0};
  pt -= bc;
  moment = subJ(pt);
  moment *= data->GridDataBase::value({9, 0, 0}) - data->GridDataBase::value({0, 0, 0});
  c += moment;

  // 901
  pt = {-h / 2.0, h / 2.0, 3 * h / 2.0};
  pt -= bc;
  moment = subJ(pt);
  moment *= data->GridDataBase::value({9, 0, 1}) - data->GridDataBase::value({0, 0, 0});
  c += moment;

  // 909
  pt = {-h / 2.0, h / 2.0, -h / 2.0};
  pt -= bc;
  moment = subJ(pt);
  moment *= data->GridDataBase::value({9, 0, 9}) - data->GridDataBase::value({0, 0, 0});
  c += moment;

  // 910
  pt = {-h / 2.0, 3 * h / 2.0, h / 2.0};
  pt -= bc;
  moment = subJ(pt);
  moment *= data->GridDataBase::value({9, 1, 0}) - data->GridDataBase::value({0, 0, 0});
  c += moment;

  // 911
  pt = {-h / 2.0, 3 * h / 2.0, 3 * h / 2.0};
  pt -= bc;
  moment = subJ(pt);
  moment *= data->GridDataBase::value({9, 1, 1}) - data->GridDataBase::value({0, 0, 0});
  c += moment;

  // 919
  pt = {-h / 2.0, 3 * h / 2.0, -h / 2.0};
  pt -= bc;
  moment = subJ(pt);
  moment *= data->GridDataBase::value({9, 1, 9}) - data->GridDataBase::value({0, 0, 0});
  c += moment;

  // 990
  pt = {-h / 2.0, -h / 2.0, h / 2.0};
  pt -= bc;
  moment = subJ(pt);
  moment *= data->GridDataBase::value({9, 9, 0}) - data->GridDataBase::value({0, 0, 0});
  c += moment;

  // 991
  pt = {-h / 2.0, -h / 2.0, 3 * h / 2.0};
  pt -= bc;
  moment = subJ(pt);
  moment *= data->GridDataBase::value({9, 9, 1}) - data->GridDataBase::value({0, 0, 0});
  c += moment;

  // 999
  pt = {-h / 2.0, -h / 2.0, -h / 2.0};
  pt -= bc;
  moment = subJ(pt);
  moment *= data->GridDataBase::value({9, 9, 9}) - data->GridDataBase::value({0, 0, 0});
  c += moment;

  c = G_inv * c;
  EXPECT_NEAR(c[0], slope[0], 1.0e-13);
  EXPECT_NEAR(c[1], slope[1], 1.0e-15);
  EXPECT_NEAR(c[2], slope[2], 1.0e-12);
}

TEST_F(GridData3DUnlimitedTest, value) {
  auto slope = Slope(0);
  Mesh3D::point_t pt = grid->BoundaryCenter({0, 0, 0}, left);
  Mesh3D::point_t bc = grid->BaryCenter({0, 0, 0});
  EXPECT_DOUBLE_EQ(data->value(grid->BoundaryCenter({0, 0, 0}, left), {0, 0, 0}),
                   data->GridDataBase::value({0, 0, 0}) + slope[0] * (pt[2] - bc[2]) + slope[1] * (pt[1] - bc[1]) +
                       slope[2] * (pt[0] - bc[0])); // z, y, x
}

TEST_F(GridData3DUnlimitedTest, gradientValue) {
  std::array<double, 3> slope = {Slope(0)[2], Slope(0)[1], Slope(0)[0]}; // z, y, x
  EXPECT_EQ(data->gradient(grid->BoundaryCenter({0, 0, 0}, left), {0, 0, 0}), slope);
}

TEST_F(GridData3DUnlimitedTest, deepCopy) {
  GridDataUnlimitedPtr<3, 1> data2 = std::dynamic_pointer_cast<GridDataUnlimited<3, 1>>(data->deepCopy());

  auto slope = Slope(0);
  Mesh3D::point_t pt = grid->BoundaryCenter({0, 0, 0}, left);
  Mesh3D::point_t bc = grid->BaryCenter({0, 0, 0});
  EXPECT_NEAR(data2->value(grid->BoundaryCenter({0, 0, 0}, left), {0, 0, 0}),
              data->GridDataBase::value({0, 0, 0}) + slope[0] * (pt[2] - bc[2]) + slope[1] * (pt[1] - bc[1]) +
                  slope[2] * (pt[0] - bc[0]),
              1.0e-14);
}

} // namespace vox
