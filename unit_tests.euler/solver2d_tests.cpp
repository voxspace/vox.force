//
//  solver2d_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2020/5/16.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "../src.euler/advection_solver.h"
#include "../src.euler/dual_buffer.h"
#include "../src.euler/equations/equation_scalar.h"
#include "../src.euler/fluxes/flux_LF.h"
#include "../src.euler/grid_data_unlimited.h"
#include "../src.euler/mesh.h"
#include "../src.euler/recon_auxiliary.h"
#include "../src.euler/ssprk_solver.h"
#include <gtest/gtest.h>
#include <utility>

using namespace vox;

class Solver2DTest : public testing::Test {
public:
  void SetUp() override {
    grid = std::make_shared<Mesh<2>>(Vector<size_t, 2>({10, 10}), Vector<double, 2>({0.1, 0.1}));

    grid->addPBDescriptor(Direction::left);
    grid->addPBDescriptor(Direction::bottom);

    std::array<EquationScalar<2>::fluxFunc, 2> flux;
    flux[0] = ScalarEquationDataBase::Advection::flux(1.0);
    flux[1] = ScalarEquationDataBase::Advection::flux(2.0);
    std::array<EquationScalar<2>::fluxFunc, 2> fluxDer;
    fluxDer[0] = ScalarEquationDataBase::Advection::fluxDer(1.0);
    fluxDer[1] = ScalarEquationDataBase::Advection::fluxDer(2.0);
    std::array<EquationScalar<2>::viscousFunc, 2> vfuncs = {ScalarEquationDataBase::ViscousTerm::constVal(1.0),
                                                            ScalarEquationDataBase::ViscousTerm::constVal(1.0)};

    eqInfo = std::make_shared<EquationScalar<2>>(flux, fluxDer, vfuncs);

    problemDef = std::make_shared<ProblemDef<2, 1>>(eqInfo, grid, bd2d, VisBoundaryCondition, init2d, 1, "1Dadv");

    ReconAuxiliaryPtr<2, 1> aux = std::make_shared<ReconAuxiliary<2, 1>>(grid);
    aux->buildReconstructPatch(PatchSearcher<2, 1>::Node);
    aux->updateLSMatrix();
    GridDataUnlimitedBuilderPtr<2, 1> UnlimitedDataBuilder = std::make_shared<GridDataUnlimited<2, 1>::Builder>();
    UnlimitedDataBuilder->setReconAuxiliary(aux);

    // set data structure
    buffer = new DualBuffer<2, 1>;
    buffer->setNewBuffer(GridSystemData<2, 1>::builder().build(UnlimitedDataBuilder, grid));
    buffer->setOldBuffer(buffer->newBuffer()->deepCopy());
  }

  MeshPtr<2> grid;
  EquationScalarPtr<2> eqInfo;
  ProblemDefPtr<2, 1> problemDef;
  DualBuffer<2, 1> *buffer{};

  static void init2d(const Mesh2D::point_t &pt, Vector1D &value) {
    value[0] = std::sin(2 * M_PI * pt[0]) * std::sin(2 * M_PI * pt[1]);
  }

  static void bd2d(const Vector1D &val, Vector1D &result, const std::array<double, 2> &out_normal,
                   const Mesh2D::point_t &pt, int flag) {
    result = val;
  }

  static void VisBoundaryCondition(const Vector<Vector<double, 2>, 1> &val, Vector<Vector<double, 2>, 1> &result,
                                   const std::array<double, 2> &out_normal, const Mesh2D::point_t &pt, int flag) {
    result = val;
  }

public:
  void initialize() {
    Vector1D tmp;
    for (size_t i = 0; i < grid->dataSize()[0]; ++i) {
      for (size_t j = 0; j < grid->dataSize()[1]; ++j) {
        problemDef->initialCondition(grid->BaryCenter({i, j}), tmp);
        buffer->newBuffer()->setVectorData({i, j}, tmp);
      }
    }
  }

  double timeStep() {
    const double _CFL = 0.1;
    double dt = std::numeric_limits<double>::max();
    for (size_t i = 0; i < grid->dataSize()[0]; ++i) {
      for (size_t j = 0; j < grid->dataSize()[1]; ++j) {
        dt =
            std::min(dt, grid->SizeOfEle() * _CFL / eqInfo->MaxCharacteristicSpeed(buffer->newBuffer()->value({i, j})));
      }
    }
    return dt;
  }

  void updateViscid(double dt) {
    AdvectionSolverPtr<2, 1> _advectionSolver =
        std::make_shared<AdvectionSolver<2, 1>>(grid, std::make_shared<FluxLF<2, 1>>());
    SSPRKSolverPtr<2, 1> _ssprkSolver = std::make_shared<SSPRKSolver<2, 1>>();

    // use dual buffer strategy to swap the pointer and avoid copy of all data
    _ssprkSolver->ssprk1(
        buffer->newBuffer(), dt,
        [&](const GridSystemDataPtr<2, 1> &in, GridSystemDataPtr<2, 1> out) -> void {
          in->calculateBasisFunction();
          _advectionSolver->advect(in, dt, problemDef, AdvectionSolver<2, 1>::inviscid_only, std::move(out));
        },
        buffer->oldBuffer());

    buffer->swapBuffer();
  }

  void updateViscous(double dt) {
    AdvectionSolverPtr<2, 1> _advectionSolver =
        std::make_shared<AdvectionSolver<2, 1>>(grid, std::make_shared<FluxLF<2, 1>>());

    buffer->newBuffer()->calculateBasisFunction();
    _advectionSolver->advect(buffer->newBuffer(), dt, problemDef, AdvectionSolver<2, 1>::Viscous_only,
                             buffer->oldBuffer());
    buffer->swapBuffer();
  }
};

TEST_F(Solver2DTest, Alloc) {
  GridSystemDataPtr<2, 1> newBuffer = buffer->newBuffer();
  EXPECT_EQ(newBuffer->numberOfVectorData()[0], 10);
  EXPECT_EQ(newBuffer->numberOfVectorData()[1], 10);
}

TEST_F(Solver2DTest, swapBuffer) {
  initialize();
  buffer->swapBuffer();
  GridSystemDataPtr<2, 1> oldBuffer = buffer->oldBuffer();

  Mesh2D::point_t pt = grid->BaryCenter({0, 3});
  EXPECT_EQ(oldBuffer->value({0, 3})[0], std::sin(2 * M_PI * pt[0]) * std::sin(2 * M_PI * pt[1]));
}

TEST_F(Solver2DTest, initialize) {
  initialize();
  GridSystemDataPtr<2, 1> newBuffer = buffer->newBuffer();

  Mesh2D::point_t pt = grid->BaryCenter({0, 3});
  EXPECT_EQ(newBuffer->value({0, 3})[0], std::sin(2 * M_PI * pt[0]) * std::sin(2 * M_PI * pt[1]));
}

TEST_F(Solver2DTest, timeStep) {
  initialize();
  EXPECT_NEAR(0.01 / std::sqrt(5), timeStep(), 1.0e-13);
}

TEST_F(Solver2DTest, updateViscous) {
  const double h = 0.1;

  initialize();
  updateViscous(timeStep());
  double vl = (buffer->newBuffer()->value({0, 0}))[0];

  Vector<Vector<double, 2>, 1> cg1 = buffer->oldBuffer()->gradient(Mesh2D::point_t(h / 2.0, 0), {0, 0}); // bottom
  Vector<Vector<double, 2>, 1> cg2 = buffer->oldBuffer()->gradient(Mesh2D::point_t(h / 2.0, h), {0, 0}); // up
  Vector<Vector<double, 2>, 1> cg3 = buffer->oldBuffer()->gradient(Mesh2D::point_t(0, h / 2.0), {0, 0}); // left
  Vector<Vector<double, 2>, 1> cg4 = buffer->oldBuffer()->gradient(Mesh2D::point_t(h, h / 2.0), {0, 0}); // front

  Vector<Vector<double, 2>, 1> bottomg = buffer->oldBuffer()->gradient(Mesh2D::point_t(h / 2.0, 1), {0, 9}); // bottom
  Vector<Vector<double, 2>, 1> upg = buffer->oldBuffer()->gradient(Mesh2D::point_t(h / 2.0, h), {0, 1});     // up
  Vector<Vector<double, 2>, 1> leftg = buffer->oldBuffer()->gradient(Mesh2D::point_t(1, h / 2.0), {9, 0});   // left
  Vector<Vector<double, 2>, 1> frontg = buffer->oldBuffer()->gradient(Mesh2D::point_t(h, h / 2.0), {1, 0});  // front

  std::array<double, 2> bottomOut = {0.0, -1.0};
  std::array<double, 2> upOut = {0.0, 1.0};
  std::array<double, 2> leftOut = {-1.0, 0.0};
  std::array<double, 2> frontOut = {1.0, 0.0};

  double avg = -(upg[0][0] + cg2[0][0]) / 2 * upOut[0] * h - (upg[0][1] + cg2[0][1]) / 2 * upOut[1] * h -
               (bottomg[0][0] + cg1[0][0]) / 2 * bottomOut[0] * h - (bottomg[0][1] + cg1[0][1]) / 2 * bottomOut[1] * h -
               (leftg[0][0] + cg4[0][0]) / 2 * leftOut[0] * h - (leftg[0][1] + cg4[0][1]) / 2 * leftOut[1] * h -
               (frontg[0][0] + cg3[0][0]) / 2 * frontOut[0] * h - (frontg[0][1] + cg3[0][1]) / 2 * frontOut[1] * h;
  avg /= h * h;

  EXPECT_NEAR(vl, avg, 1.0e-11);
}
