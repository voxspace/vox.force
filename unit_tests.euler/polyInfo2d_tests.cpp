//
//  polyInfo2d_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2020/5/16.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "../src.euler/mesh.h"
#include "../src.euler/polyInfo2d.h"
#include <gtest/gtest.h>

using namespace vox;

class PolyInfo2DTest : public testing::Test {
public:
  PolyInfo2DTest() : polyInfo(nullptr) {}

  void SetUp() override {
    grid = std::make_shared<Mesh<2>>(Vector<size_t, 2>({10, 10}), Vector<double, 2>({0.1, 0.1}));

    grid->addPBDescriptor(Direction::left);
    grid->addPBDescriptor(Direction::right);

    polyInfo.resetGrid(grid);
    polyInfo.loadBasisFunc();
  }

  static constexpr int order = 2;
  MeshPtr<2> grid;
  PolyInfo<2, order> polyInfo;
};

TEST_F(PolyInfo2DTest, n_unknown) { EXPECT_EQ((PolyInfo<2, order>::n_unknown), 5); }

TEST_F(PolyInfo2DTest, basisfuncValue) {
  double h = 0.1;
  Mesh2D::point_t pt = grid->BoundaryCenter({0, 0}, bottom);
  pt -= grid->BaryCenter({0, 0});

  std::array<double, PolyInfo<2, order>::n_unknown> result{};
  polyInfo.basisFuncValue({0, 0}, grid->BoundaryCenter({0, 0}, bottom), result);
  EXPECT_DOUBLE_EQ(result[0], pt[1]);
  EXPECT_DOUBLE_EQ(result[1], pt[0]);
  EXPECT_NEAR(result[2], pt[1] * pt[1] / h - 1.0 / 12 * h, 1.0e-13);
  EXPECT_DOUBLE_EQ(result[3], pt[1] * pt[0] / h);
  EXPECT_NEAR(result[4], pt[0] * pt[0] / h - 1.0 / 12 * h, 1.0e-13);
  // y, x, yy, xy, xx
}

TEST_F(PolyInfo2DTest, funcValue) {
  typename PolyInfo<2, order>::Vec slope;
  EXPECT_EQ(slope.rows(), 5);

  slope[0] = 100;
  slope[1] = 200;
  slope[2] = 300;
  slope[3] = 400;
  slope[4] = 500;
  const double h = 0.1;
  Mesh2D::point_t pt = grid->BoundaryCenter({0, 0}, up);
  pt -= grid->BaryCenter({0, 0});
  double val = 20 + 100 * pt[1] + 200 * pt[0] + 300 * (std::pow(pt[1], 2) / h - 1.0 / 12.0 * h) +
               400 * (pt[0] * pt[1] / h) + 500 * (std::pow(pt[0], 2) / h - 1.0 / 12.0 * h);

  EXPECT_NEAR(polyInfo.funcValue({0, 0}, grid->BoundaryCenter({0, 0}, up), 20, slope), val, 1.0e-10);
}

TEST_F(PolyInfo2DTest, funcGradient) {
  typename PolyInfo<2, order>::Vec slope;
  slope[0] = 100;
  slope[1] = 200;
  slope[2] = 300;
  slope[3] = 400;
  slope[4] = 500;

  std::array<double, 2> grad = polyInfo.funcGradient({0, 0}, grid->BoundaryCenter({0, 0}, left), slope);
  EXPECT_EQ(grad.size(), (size_t)2);

  const double h = 0.1;
  Mesh2D::point_t pt = grid->BoundaryCenter({0, 0}, left);
  pt -= grid->BaryCenter({0, 0});
  EXPECT_NEAR(grad[1], slope[0] + slope[2] * (pt[1] * 2 / h) + slope[3] * (pt[0] / h), 1.0e-9);
  EXPECT_NEAR(grad[0], slope[1] + slope[4] * (pt[0] * 2 / h) + slope[3] * (pt[1] / h), 1.0e-9);
}

TEST_F(PolyInfo2DTest, averageBasisFunc) {
  std::array<double, PolyInfo<2, order>::n_unknown> result{};
  polyInfo.averageBasisFunc({0, 0}, {0, 0}, result);
  EXPECT_DOUBLE_EQ(result[0], 0);
  EXPECT_DOUBLE_EQ(result[1], 0);
  EXPECT_DOUBLE_EQ(result[2], 0);
  EXPECT_DOUBLE_EQ(result[3], 0);
  EXPECT_DOUBLE_EQ(result[4], 0);
}
