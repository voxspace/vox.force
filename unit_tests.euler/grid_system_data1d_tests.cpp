//
//  GridSystemData1D_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2019/8/23.
//  Copyright © 2019 杨丰. All rights reserved.
//

#include "../src.euler/equations/equation_scalar.h"
#include "../src.euler/error_approximator.h"
#include "../src.euler/global_clock.h"
#include "../src.euler/grid_system_data.h"
#include <gtest/gtest.h>

using namespace vox;

class GridSystemData1DTest : public testing::Test {
public:
  void SetUp() override {
    grid = std::make_shared<Mesh<1>>(Vector<size_t, 1>({1000}), Vector<double, 1>({1.0 / 1000.0}));
    grid->addPBDescriptor(Direction::left);

    std::array<EquationScalar<1>::fluxFunc, 1> flux = {ScalarEquationDataBase::Advection::flux(1.0)};
    std::array<EquationScalar<1>::fluxFunc, 1> fluxDer = {ScalarEquationDataBase::Advection::fluxDer(1.0)};
    std::array<EquationScalar<1>::viscousFunc, 1> vfuncs{};
    eqInfo = std::make_shared<EquationScalar<1>>(flux, fluxDer, vfuncs);

    data = GridSystemData<1, 1>::builder().build(grid);
    Vector1D tmp;
    for (size_t i = 0; i < grid->dataSize().length(); ++i) {
      init1d(grid->BaryCenter({i}), tmp);
      data->setVectorData({i}, tmp);
    }

    mGlobalClock = new GlobalClock;
  }

  void TearDown() override { delete mGlobalClock; }

  GlobalClock *mGlobalClock{};

  MeshPtr<1> grid;
  EquationScalarPtr<1> eqInfo;
  GridSystemDataPtr<1, 1> data;

  static void init1d(const Mesh1D::point_t &pt, Vector1D &value) { value[0] = sin(2 * M_PI * pt[0]); }

  static void bd1d(const Vector1D &val, Vector1D &result, const std::array<double, 1> &outNormal,
                   const Mesh1D::point_t &pt, int flag) {
    result = val;
  }
};

TEST_F(GridSystemData1DTest, scalarDataAt) {
  GridDataPtr<1> scalarData = data->scalarDataAt(0);
  EXPECT_DOUBLE_EQ(scalarData->GridDataBase::value({234}), std::sin(2 * M_PI * grid->BaryCenter({234})[0]));
}

TEST_F(GridSystemData1DTest, deepCopy) {
  GridDataPtr<1> scalarData = data->deepCopy()->scalarDataAt(0);
  EXPECT_DOUBLE_EQ(scalarData->GridDataBase::value({234}), std::sin(2 * M_PI * grid->BaryCenter({234})[0]));
}

TEST_F(GridSystemData1DTest, numberOfScalarData) { EXPECT_EQ(data->numberOfScalarData(), (size_t)1); }

TEST_F(GridSystemData1DTest, addSystemData) {
  GridSystemDataPtr<1, 1> data2 = GridSystemData<1, 1>::builder().build(grid);
  Vector1D tmp;
  for (size_t i = 0; i < grid->dataSize()[0]; ++i) {
    tmp[0] = 1.0;
    data2->setVectorData({i}, tmp);
  }

  data->addSystemData(*data2);
  EXPECT_DOUBLE_EQ(data->scalarDataAt(0)->GridDataBase::value({234}),
                   std::sin(2 * M_PI * grid->BaryCenter({234})[0]) + 1);
}

TEST_F(GridSystemData1DTest, scaleSystemData) {
  data->scaleSystemData(0.1);
  EXPECT_DOUBLE_EQ(data->scalarDataAt(0)->GridDataBase::value({234}),
                   std::sin(2 * M_PI * grid->BaryCenter({234})[0]) * 0.1);
}

TEST_F(GridSystemData1DTest, setSystemData) {
  GridSystemDataPtr<1, 1> data2 = GridSystemData<1, 1>::builder().build(grid);
  Vector1D tmp;
  for (size_t i = 0; i < grid->dataSize()[0]; ++i) {
    tmp[0] = 1.0;
    data2->setVectorData({i}, tmp);
  }

  data->setSystemData(*data2);
  EXPECT_DOUBLE_EQ(data->scalarDataAt(0)->GridDataBase::value({234}), 1);
}

TEST_F(GridSystemData1DTest, clearSystemData) {
  data->clearSystemData();
  EXPECT_DOUBLE_EQ(data->scalarDataAt(0)->GridDataBase::value({234}), 0.0);
}

TEST_F(GridSystemData1DTest, caculateBasisFunction) {
  data->calculateBasisFunction();
  EXPECT_DOUBLE_EQ(data->scalarDataAt(0)->value(grid->BoundaryCenter({0}, right), {0}),
                   std::sin(2 * M_PI * grid->BaryCenter({0})[0]));
}

TEST_F(GridSystemData1DTest, numberOfVectorData) {
  EXPECT_DOUBLE_EQ(data->numberOfVectorData()[0], grid->dataSize()[0]);
}

TEST_F(GridSystemData1DTest, addVectorData) {
  Vector1D tmp = {1.0};
  data->addVectorData({234}, tmp);
  EXPECT_DOUBLE_EQ(data->scalarDataAt(0)->GridDataBase::value({234}),
                   std::sin(2 * M_PI * grid->BaryCenter({234})[0]) + 1.0);
  EXPECT_DOUBLE_EQ(data->scalarDataAt(0)->GridDataBase::value({230}), std::sin(2 * M_PI * grid->BaryCenter({230})[0]));
}

TEST_F(GridSystemData1DTest, value) {
  Vector1D tmp = data->value({0});
  EXPECT_DOUBLE_EQ(tmp[0], std::sin(2 * M_PI * grid->BaryCenter({0})[0]));
}

TEST_F(GridSystemData1DTest, ptValue) {
  Vector1D tmp = data->value(grid->BoundaryCenter({0}, right), {0});
  EXPECT_DOUBLE_EQ(tmp[0], std::sin(2 * M_PI * grid->BaryCenter({0})[0]));
}

TEST_F(GridSystemData1DTest, gradient) {
  Vector<Vector<double, 1>, 1> tmp = data->gradient(grid->BoundaryCenter({0}, right), {0});
  EXPECT_EQ(tmp.rows(), (size_t)1);    // number of equation
  EXPECT_EQ(tmp[0].rows(), (size_t)1); // dim of space
  EXPECT_EQ(tmp[0][0], 0.0);
}

TEST_F(GridSystemData1DTest, loadCache) {
  GridSystemDataPtr<1, 1> new_data = GridSystemData<1, 1>::builder().build(grid);
  mGlobalClock->start(0.125);

  data->save_cache("middleInfo");
  mGlobalClock->accumulate(0.555); // useless

  new_data->load_cache("middleInfo");
  EXPECT_DOUBLE_EQ(new_data->value({35})[0], data->value({35})[0]);
  EXPECT_DOUBLE_EQ(mGlobalClock->getTime(), 0.125);
}

TEST_F(GridSystemData1DTest, L1Error) {
  Vector<double, 1> error;
  double dx;
  std::array<std::function<double(const Mesh1D::point_t &pt)>, 1> realFunc;
  realFunc[0] = [&](const Mesh1D::point_t &pt) -> double { return 0; };

  ErrorApproximator<1, 1> L1ErrorApprox(grid);
  L1ErrorApprox.l1Error(data, realFunc, eqInfo, error, dx);

  double err = 0.0;
  for (size_t i = 0; i < data->numberOfVectorData()[0]; ++i) {
    err += grid->AreaOfEle() * std::abs(std::sin(2 * M_PI * grid->BaryCenter({i})[0]));
  }

  EXPECT_DOUBLE_EQ(error[0], err);
}

TEST_F(GridSystemData1DTest, LinftyError) {
  Vector1D error;
  double dx;
  std::array<std::function<double(const Mesh1D::point_t &pt)>, 1> realFunc;
  realFunc[0] = [&](const Mesh1D::point_t &pt) -> double { return 0; };

  ErrorApproximator<1, 1> LinftyErrorApprox(grid);
  LinftyErrorApprox.linftyError(data, realFunc, eqInfo, error, dx);

  double err = 0.0;
  for (size_t i = 0; i < data->numberOfVectorData()[0]; ++i) {
    err = std::max(err, std::abs(std::sin(2 * M_PI * grid->BaryCenter({i})[0])));
  }

  EXPECT_DOUBLE_EQ(error[0], err);
}
