//
//  grid_data2d_weno_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2020/5/16.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "../src.euler/grid_data_weno.h"
#include "../src.euler/mesh.h"
#include "../src.euler/weno_auxiliary.h"

#include <gtest/gtest.h>

namespace vox {

class GridData2DWENOTest : public testing::Test {
public:
  void SetUp() override {
    grid = std::make_shared<Mesh<2>>(Vector<size_t, 2>({10, 10}), Vector<double, 2>({0.1, 0.1}));

    grid->addPBDescriptor(Direction::left);
    grid->addPBDescriptor(Direction::bottom);

    wenoAux = std::make_shared<WENOAuxiliary<2, order>>(grid);
    wenoAux->buildReconstructPatch();
    wenoAux->buildWENOPatch();
    wenoAux->updateWENOInfo();
    GridDataWENOBuilderPtr<2, order> dataBuilder = std::make_shared<GridDataWENO<2, order>::Builder>();
    dataBuilder->setWENOAuxiliary(wenoAux);

    data = std::dynamic_pointer_cast<GridDataWENO<2, order>>(dataBuilder->build(0, grid));
    for (size_t i = 0; i < grid->dataSize()[0]; ++i) {
      for (size_t j = 0; j < grid->dataSize()[1]; ++j) {
        data->operator()({i, j}) =
            std::sin(2 * M_PI * grid->BaryCenter({i, j})[0]) * std::sin(2 * M_PI * grid->BaryCenter({i, j})[1]);
      }
    }
    data->calculateBasisFunction();
  }

  static constexpr int order = 1;
  MeshPtr<2> grid;
  GridDataWENOPtr<2, order> data;
  WENOAuxiliaryPtr<2, order> wenoAux;

public:
  std::vector<typename PolyInfo<2, order>::Vec> grad_choice(int idx) { return data->recon_cache[idx].grad_choice; }

  typename PolyInfo<2, order>::Vec gradient(int idx) { return data->recon_cache[idx].Slope; }
};

TEST_F(GridData2DWENOTest, grad_choice) {
  // Because the code is from Dealii at first.
  // The loop direction is opposite so the x and y have to be changed
  const double h = 0.1;
  auto grads = grad_choice(0);
  EXPECT_EQ(grads.size(), (size_t)5);

  // center 0 1 9 10 90
  Vector2D v_b;
  v_b[0] = h * (data->GridDataBase::value({0, 1}) - data->GridDataBase::value({0, 0})) +
           -h * (data->GridDataBase::value({0, 9}) - data->GridDataBase::value({0, 0}));

  v_b[1] = h * (data->GridDataBase::value({1, 0}) - data->GridDataBase::value({0, 0})) +
           -h * (data->GridDataBase::value({9, 0}) - data->GridDataBase::value({0, 0}));
  double vb0 = v_b[0] * 50;
  double vb1 = v_b[1] * 50;
  EXPECT_NEAR(grads[0][1], vb1, 1.0e-11);
  EXPECT_NEAR(grads[0][0], vb0, 1.0e-11); // y, x

  // right 0 1 2 11 91
  v_b[0] = h * (data->GridDataBase::value({0, 1}) - data->GridDataBase::value({0, 0})) +
           h * (data->GridDataBase::value({1, 1}) - data->GridDataBase::value({0, 0})) +
           h * (data->GridDataBase::value({9, 1}) - data->GridDataBase::value({0, 0})) +
           2 * h * (data->GridDataBase::value({0, 2}) - data->GridDataBase::value({0, 0}));

  v_b[1] = h * (data->GridDataBase::value({1, 1}) - data->GridDataBase::value({0, 0})) +
           -h * (data->GridDataBase::value({9, 1}) - data->GridDataBase::value({0, 0}));
  vb0 = v_b[0] * 100 / 7.0;
  vb1 = v_b[1] * 50;
  EXPECT_NEAR(grads[1][0], vb1, 1.0e-11);
  EXPECT_NEAR(grads[1][1], vb0, 1.0e-11); // y, x

  // left 0 8 9 19 99
  v_b[0] = -h * (data->GridDataBase::value({0, 9}) - data->GridDataBase::value({0, 0})) +
           -h * (data->GridDataBase::value({1, 9}) - data->GridDataBase::value({0, 0})) +
           -h * (data->GridDataBase::value({9, 9}) - data->GridDataBase::value({0, 0})) +
           -2 * h * (data->GridDataBase::value({0, 8}) - data->GridDataBase::value({0, 0}));

  v_b[1] = h * (data->GridDataBase::value({1, 9}) - data->GridDataBase::value({0, 0})) +
           -h * (data->GridDataBase::value({9, 9}) - data->GridDataBase::value({0, 0}));
  vb0 = v_b[0] * 100 / 7.0;
  vb1 = v_b[1] * 50;
  EXPECT_NEAR(grads[2][0], vb1, 1.0e-11);
  EXPECT_NEAR(grads[2][1], vb0, 1.0e-11); // y, x

  // up 0 10 11 19 20
  v_b[0] = h * (data->GridDataBase::value({1, 1}) - data->GridDataBase::value({0, 0})) +
           -h * (data->GridDataBase::value({1, 9}) - data->GridDataBase::value({0, 0}));

  v_b[1] = h * (data->GridDataBase::value({1, 0}) - data->GridDataBase::value({0, 0})) +
           h * (data->GridDataBase::value({1, 1}) - data->GridDataBase::value({0, 0})) +
           h * (data->GridDataBase::value({1, 9}) - data->GridDataBase::value({0, 0})) +
           2 * h * (data->GridDataBase::value({2, 0}) - data->GridDataBase::value({0, 0}));
  vb0 = v_b[0] * 50;
  vb1 = v_b[1] * 100 / 7.0;
  EXPECT_NEAR(grads[3][0], vb1, 1.0e-11);
  EXPECT_DOUBLE_EQ(grads[3][1], vb0); // y, x

  // down 0 80 90 91 99
  v_b[0] = h * (data->GridDataBase::value({9, 1}) - data->GridDataBase::value({0, 0})) +
           -h * (data->GridDataBase::value({9, 9}) - data->GridDataBase::value({0, 0}));

  v_b[1] = -h * (data->GridDataBase::value({9, 0}) - data->GridDataBase::value({0, 0})) +
           -h * (data->GridDataBase::value({9, 1}) - data->GridDataBase::value({0, 0})) +
           -h * (data->GridDataBase::value({9, 9}) - data->GridDataBase::value({0, 0})) +
           -2 * h * (data->GridDataBase::value({8, 0}) - data->GridDataBase::value({0, 0}));
  vb0 = v_b[0] * 50;
  vb1 = v_b[1] * 100 / 7.0;
  EXPECT_NEAR(grads[4][0], vb1, 1.0e-11);
  EXPECT_DOUBLE_EQ(grads[4][1], vb0); // y, x
}

TEST_F(GridData2DWENOTest, gradient) {
  const double epsilon = 1.0e-04;
  auto grads = grad_choice(0);
  std::vector<double> weights(grads.size(), 0.0);

  double sum = 0;
  for (size_t j = 0; j < grads.size(); ++j) {
    weights[j] = (pow(grads[j][0], 2.) + pow(grads[j][1], 2.)) * grid->AreaOfEle();
    weights[j] = 1. / pow(epsilon + weights[j], 2.); // 2 for all previous tests
    sum += weights[j];
  }
  for (size_t j = 0; j < grads.size(); ++j) {
    weights[j] /= sum;
  }

  Vector2D result = Vector2D::makeZero();
  for (size_t j = 0; j < grads.size(); ++j) {
    result += weights[j] * grads[j];
  }

  EXPECT_DOUBLE_EQ(gradient(0)[0], result[0]);
  EXPECT_DOUBLE_EQ(gradient(0)[1], result[1]);
}

TEST_F(GridData2DWENOTest, value) {
  auto grad = gradient(0);
  Mesh2D::point_t pt = grid->BoundaryCenter({0, 0}, left);
  Mesh2D::point_t bc = grid->BaryCenter({0, 0});
  EXPECT_NEAR(data->value(grid->BoundaryCenter({0, 0}, left), {0, 0}),
              data->GridDataBase::value({0, 0}) + grad[1] * (pt[0] - bc[0]) + grad[0] * (pt[1] - bc[1]),
              1.0e-15); // y,x
}

TEST_F(GridData2DWENOTest, gradientValue) {
  EXPECT_DOUBLE_EQ(data->gradient(grid->BoundaryCenter({0, 0}, left), {0, 0})[0], gradient(0)[1]);
  EXPECT_DOUBLE_EQ(data->gradient(grid->BoundaryCenter({0, 0}, left), {0, 0})[1], gradient(0)[0]); // y, x
}

TEST_F(GridData2DWENOTest, deepCopy) {
  GridDataWENOPtr<2, order> data2 = std::dynamic_pointer_cast<GridDataWENO<2, order>>(data->deepCopy());

  auto grad = gradient(0);
  Mesh2D::point_t pt = grid->BoundaryCenter({0, 0}, left);
  Mesh2D::point_t bc = grid->BaryCenter({0, 0});
  EXPECT_NEAR(data2->value(grid->BoundaryCenter({0, 0}, left), {0, 0}),
              data->GridDataBase::value({0, 0}) + grad[1] * (pt[0] - bc[0]) + grad[0] * (pt[1] - bc[1]),
              1.0e-15); // y, x
}

} // namespace vox
