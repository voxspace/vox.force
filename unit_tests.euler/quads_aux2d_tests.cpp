//
//  quads_aux2d_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2020/5/16.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "../src.euler/mesh.h"
#include "../src.euler/quads_auxiliary.h"
#include <gtest/gtest.h>

using namespace vox;

class QuadsAux2DTest : public testing::Test {
public:
  void SetUp() override {
    grid = std::make_shared<Mesh<2>>(Vector<size_t, 2>({10, 10}), Vector<double, 2>({0.1, 0.1}));

    grid->addPBDescriptor(Direction::left);
    grid->addPBDescriptor(Direction::bottom);
  }

  const int order = 2;
  MeshPtr<2> grid;
};

TEST_F(QuadsAux2DTest, getEleInnerQuadPoint) {
  std::vector<Mesh2D::point_t> qi;
  QuadsAuxiliary<2, 2>::getEleInnerQuadPoint(grid, {0, 0}, qi);

  Mesh2D::point_t bc = grid->BaryCenter({0, 0});
  EXPECT_EQ(qi.size(), (size_t)1);
  EXPECT_DOUBLE_EQ(qi[0][0], bc[0]);
  EXPECT_DOUBLE_EQ(qi[0][1], bc[1]);
}

TEST_F(QuadsAux2DTest, getBryQuadPoint) {
  std::vector<Mesh2D::point_t> n;
  QuadsAuxiliary<2, 2>::getBryQuadPoint(grid, {0, 0}, n);

  const double h = 0.1;
  EXPECT_EQ(n.size(), (size_t)8);
  EXPECT_DOUBLE_EQ(n[0][1], (1.0 - 1.0 / std::sqrt(3)) * h / 2.0);
  EXPECT_DOUBLE_EQ(n[0][0], 0);
  EXPECT_DOUBLE_EQ(n[1][1], (1.0 + 1.0 / std::sqrt(3)) * h / 2.0);
  EXPECT_DOUBLE_EQ(n[1][0], 0);

  EXPECT_DOUBLE_EQ(n[2][1], (1.0 - 1.0 / std::sqrt(3)) * h / 2.0);
  EXPECT_NEAR(n[2][0], h, 1.0e-12);
  EXPECT_DOUBLE_EQ(n[3][1], (1.0 + 1.0 / std::sqrt(3)) * h / 2.0);
  EXPECT_NEAR(n[3][0], h, 1.0e-12);

  EXPECT_DOUBLE_EQ(n[4][1], 0);
  EXPECT_NEAR(n[4][0], (1.0 - 1.0 / std::sqrt(3)) * h / 2.0, 1.0e-13);
  EXPECT_DOUBLE_EQ(n[5][1], 0);
  EXPECT_NEAR(n[5][0], (1.0 + 1.0 / std::sqrt(3)) * h / 2.0, 1.0e-12);

  EXPECT_DOUBLE_EQ(n[6][1], h);
  EXPECT_NEAR(n[6][0], (1.0 - 1.0 / std::sqrt(3)) * h / 2.0, 1.0e-13);
  EXPECT_DOUBLE_EQ(n[7][1], h);
  EXPECT_NEAR(n[7][0], (1.0 + 1.0 / std::sqrt(3)) * h / 2.0, 1.0e-12);
}

TEST_F(QuadsAux2DTest, getEleQuadPoint) {
  std::vector<Mesh2D::point_t> n;
  QuadsAuxiliary<2, 2>::getEleQuadPoint(grid, {0, 0}, n);
  EXPECT_EQ(n.size(), (size_t)9);

  Mesh2D::point_t bc = grid->BaryCenter({0, 0});
  EXPECT_DOUBLE_EQ(n[0][0], bc[0]);
  EXPECT_DOUBLE_EQ(n[0][1], bc[1]);

  const double h = 0.1;
  EXPECT_DOUBLE_EQ(n[1][1], (1.0 - 1.0 / std::sqrt(3)) * h / 2.0);
  EXPECT_DOUBLE_EQ(n[1][0], 0);
  EXPECT_DOUBLE_EQ(n[2][1], (1.0 + 1.0 / std::sqrt(3)) * h / 2.0);
  EXPECT_DOUBLE_EQ(n[2][0], 0);

  EXPECT_DOUBLE_EQ(n[3][1], (1.0 - 1.0 / std::sqrt(3)) * h / 2.0);
  EXPECT_NEAR(n[3][0], h, 1.0e-12);
  EXPECT_DOUBLE_EQ(n[4][1], (1.0 + 1.0 / std::sqrt(3)) * h / 2.0);
  EXPECT_NEAR(n[4][0], h, 1.0e-12);

  EXPECT_DOUBLE_EQ(n[5][1], 0);
  EXPECT_NEAR(n[5][0], (1.0 - 1.0 / std::sqrt(3)) * h / 2.0, 1.0e-13);
  EXPECT_DOUBLE_EQ(n[6][1], 0);
  EXPECT_NEAR(n[6][0], (1.0 + 1.0 / std::sqrt(3)) * h / 2.0, 1.0e-12);

  EXPECT_DOUBLE_EQ(n[7][1], h);
  EXPECT_NEAR(n[7][0], (1.0 - 1.0 / std::sqrt(3)) * h / 2.0, 1.0e-13);
  EXPECT_DOUBLE_EQ(n[8][1], h);
  EXPECT_NEAR(n[8][0], (1.0 + 1.0 / std::sqrt(3)) * h / 2.0, 1.0e-12);
}