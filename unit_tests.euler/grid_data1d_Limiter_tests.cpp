//
//  gridData1dLimiter_test.cpp
//  unit_tests.flame
//
//  Created by 杨丰 on 2019/9/1.
//  Copyright © 2019 杨丰. All rights reserved.
//

#include "../src.euler/grid_data_limiter.h"
#include "../src.euler/recon_auxiliary.h"

#include <gtest/gtest.h>

namespace vox {

class GridData1DLimiterTest : public testing::Test {
public:
  void SetUp() override {
    grid = std::make_shared<Mesh<1>>(Vector<size_t, 1>({1000}), Vector<double, 1>({1.0 / 1000.0}));
    grid->addPBDescriptor(Direction::left);

    ReconAuxiliaryPtr<1, 1> aux = std::make_shared<ReconAuxiliary<1, 1>>(grid);
    aux->buildReconstructPatch();
    aux->updateLSMatrix();
    GridDataLimiterBuilderPtr<1> dataBuilder = std::make_shared<GridDataLimiter<1>::Builder>();
    dataBuilder->setLimiterAuxiliary(aux);
    dataBuilder->setLimiterType(GridDataLimiter<1>::BARTH);

    data = std::dynamic_pointer_cast<GridDataLimiter<1>>(dataBuilder->build(0, grid));
    for (size_t i = 0; i < grid->dataSize().length(); ++i) {
      data->operator[](i) = std::sin(2 * M_PI * grid->BaryCenter({i})[0]);
    }
    data->calculateBasisFunction();
  }

  MeshPtr<1> grid;
  GridDataLimiterPtr<1> data;

public:
  typename PolyInfo<1, 1>::Vec Slope(int idx) { return data->recon_cache[idx].Slope; }
};

TEST_F(GridData1DLimiterTest, BARTH) {
  auto slope = Slope(0);
  const double h = 0.001;
  double G_inv = 1.0 / (2 * h * h); // get from IQR G's first row and col
  double vb = -(data->GridDataBase::value({1}) - data->GridDataBase::value({0})) * h;
  vb += -(data->GridDataBase::value({999}) - data->GridDataBase::value({0})) * (-h);

  double phi_left = 1.0;
  double val_left = -G_inv * vb * (-h / 2.0);
  double M = std::max(data->GridDataBase::value({0}), data->GridDataBase::value({999}));
  double m = std::min(data->GridDataBase::value({0}), data->GridDataBase::value({999}));
  if (val_left > M - data->GridDataBase::value({0})) {
    phi_left = (M - data->GridDataBase::value({0})) / val_left;
  } else if (val_left < m - data->GridDataBase::value({0})) {
    phi_left = (m - data->GridDataBase::value({0})) / val_left;
  }

  double phi_right = 1.0;
  double val_right = -G_inv * vb * (h / 2.0);
  M = std::max(data->GridDataBase::value({0}), data->GridDataBase::value({1}));
  m = std::min(data->GridDataBase::value({0}), data->GridDataBase::value({1}));
  if (val_right > M - data->GridDataBase::value({0})) {
    phi_right = (M - data->GridDataBase::value({0})) / val_right;
  } else if (val_right < m - data->GridDataBase::value({0})) {
    phi_right = (m - data->GridDataBase::value({0})) / val_right;
  }

  EXPECT_NEAR(slope[0], -G_inv * vb * std::min(phi_left, phi_right), 1.0e-12);
}

TEST_F(GridData1DLimiterTest, SCALAR) {
  data->setLimiterType(GridDataLimiter<1>::SCALAR);
  data->calculateBasisFunction();

  auto slope = Slope(0);
  const double h = 0.001;
  double G_inv = 1.0 / (2 * h * h); // get from IQR G's first row and col
  double vb = -(data->GridDataBase::value({1}) - data->GridDataBase::value({0})) * h;
  vb += -(data->GridDataBase::value({999}) - data->GridDataBase::value({0})) * (-h);

  double phi_left = 1.0;
  double val_left = -G_inv * vb * (-h);
  double M = std::max(data->GridDataBase::value({0}), data->GridDataBase::value({999}));
  double m = std::min(data->GridDataBase::value({0}), data->GridDataBase::value({999}));
  if (val_left > M - data->GridDataBase::value({0})) {
    phi_left = (M - data->GridDataBase::value({0})) / val_left;
  } else if (val_left < m - data->GridDataBase::value({0})) {
    phi_left = (m - data->GridDataBase::value({0})) / val_left;
  }

  double phi_right = 1.0;
  double val_right = -G_inv * vb * (grid->BaryCenter({1})[0] - grid->BaryCenter({0})[0]);
  M = std::max(data->GridDataBase::value({0}), data->GridDataBase::value({1}));
  m = std::min(data->GridDataBase::value({0}), data->GridDataBase::value({1}));
  if (val_right > M - data->GridDataBase::value({0})) {
    phi_right = (M - data->GridDataBase::value({0})) / val_right;
  } else if (val_right < m - data->GridDataBase::value({0})) {
    phi_right = (m - data->GridDataBase::value({0})) / val_right;
  }

  EXPECT_DOUBLE_EQ(slope[0], -G_inv * vb * std::min(phi_left, phi_right));
}

TEST_F(GridData1DLimiterTest, RSCALAR) {
  data->setLimiterType(GridDataLimiter<1>::R_SCALAR);
  data->calculateBasisFunction();

  auto slope = Slope(0);
  const double h = 0.001;
  double G_inv = 1.0 / (2 * h * h); // get from IQR G's first row and col
  double vb = -(data->GridDataBase::value({1}) - data->GridDataBase::value({0})) * h;
  vb += -(data->GridDataBase::value({999}) - data->GridDataBase::value({0})) * (-h);

  double M = std::max(std::max(data->GridDataBase::value({999}), data->GridDataBase::value({0})),
                      data->GridDataBase::value({1}));
  double m = std::min(std::min(data->GridDataBase::value({999}), data->GridDataBase::value({0})),
                      data->GridDataBase::value({1}));

  double phi_left = 1.0;
  double val_left = -G_inv * vb * (-h);
  if (val_left > M - data->GridDataBase::value({0})) {
    phi_left = (M - data->GridDataBase::value({0})) / val_left;
  } else if (val_left < m - data->GridDataBase::value({0})) {
    phi_left = (m - data->GridDataBase::value({0})) / val_left;
  }

  double phi_right = 1.0;
  double val_right = -G_inv * vb * (grid->BaryCenter({1})[0] - grid->BaryCenter({0})[0]);
  if (val_right > M - data->GridDataBase::value({0})) {
    phi_right = (M - data->GridDataBase::value({0})) / val_right;
  } else if (val_right < m - data->GridDataBase::value({0})) {
    phi_right = (m - data->GridDataBase::value({0})) / val_right;
  }

  EXPECT_DOUBLE_EQ(slope[0], -G_inv * vb * std::min(phi_left, phi_right));
}

TEST_F(GridData1DLimiterTest, value) {
  auto slope = Slope(0);
  EXPECT_DOUBLE_EQ(data->value(grid->BoundaryCenter({0}, right), {0}),
                   data->GridDataBase::value({0}) + slope[0] * 0.001 / 2);
}

TEST_F(GridData1DLimiterTest, gradientValue) {
  EXPECT_EQ(data->gradient(grid->BoundaryCenter({0}, right), {0})[0], Slope(0)[0]);
}

TEST_F(GridData1DLimiterTest, deepCopy) {
  GridDataLimiterPtr<1> data2 = std::dynamic_pointer_cast<GridDataLimiter<1>>(data->deepCopy());

  auto slope = Slope(0);
  EXPECT_DOUBLE_EQ(data2->value(grid->BoundaryCenter({0}, right), {0}),
                   data->GridDataBase::value({0}) + slope[0] * 0.001 / 2);
}

} // namespace vox
