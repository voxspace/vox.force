//
//  advDiffusionBurgers2dState.cpp
//  apps
//
//  Created by 杨丰 on 2020/5/17.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "advDiffusionBurgers2dState.h"
#include "../../src.euler/error_approximator.h"
#include "../../src.euler/fluxes/flux_LF.h"
#include "../../src.euler/grid_data_unlimited.h"
#include "../../src.euler/grid_system_data_minmax.h"
#include "../../src.euler/recon_auxiliary.h"
#include "../../src.common/timer.h"
#include "../ProblemInfo/AdvectionProblem.h"
#include <fstream>

using namespace vox;

const double eps = 0.1;
void init2d(const Mesh2D::point_t &pt, Vector1D &value) {
  if ((pt[0] + 0.5) * (pt[0] + 0.5) + (pt[1] + 0.5) * (pt[1] + 0.5) < 0.16) {
    value[0] = 1.0;
  } else if ((pt[0] - 0.5) * (pt[0] - 0.5) + (pt[1] - 0.5) * (pt[1] - 0.5) < 0.16) {
    value[0] = -1.0;
  } else {
    value[0] = 0.0;
  }
}

AdvDiffusionBurgers2DState::AdvDiffusionBurgers2DState(int l) : CFL(0.1) {
  size_t N = l;
  double s = 3.0 / static_cast<double>(l);
  _grid = std::make_shared<Mesh<DIM>>(Vector<size_t, DIM>({N, N}), Vector<double, DIM>({s, s}),
                                      Vector<double, DIM>({-1.5, -1.5}));

  std::array<EquationScalar<DIM>::fluxFunc, DIM> flux = {ScalarEquationDataBase::Burgers::flux(),
                                                         ScalarEquationDataBase::Burgers::flux()};
  std::array<EquationScalar<DIM>::fluxFunc, DIM> fluxDer = {ScalarEquationDataBase::Burgers::fluxDer(),
                                                            ScalarEquationDataBase::Burgers::fluxDer()};
  std::array<EquationScalar<DIM>::viscousFunc, DIM> vfuncs;
  vfuncs[0] = ScalarEquationDataBase::ViscousTerm::deGenerate(eps);
  vfuncs[1] = ScalarEquationDataBase::ViscousTerm::deGenerate(eps);

  _eqInfo = std::make_shared<EquationScalar<DIM>>(flux, fluxDer, vfuncs);

  _advectionSolver = std::make_shared<AdvectionSolver<DIM, DOS>>(_grid, std::make_shared<FluxLF<DIM, DOS>>());
  _ssprkSolver = std::make_shared<SSPRKSolver<DIM, DOS>>();
}

void AdvDiffusionBurgers2DState::createScene() {
  // load resources and information
  _problemDef = std::make_shared<ProblemDef<DIM, DOS>>(
      _eqInfo, _grid, Advection::bd<DIM>, Advection::VisBoundaryCondition<DIM>, init2d, DIM, "2DadvdiffusionBurgers");

  ReconAuxiliaryPtr<DIM, 2> aux = std::make_shared<ReconAuxiliary<DIM, 2>>(_grid);
  aux->buildReconstructPatch();
  aux->updateLSMatrix();
  GridDataUnlimitedBuilderPtr<DIM, 2> dataBuilder = std::make_shared<GridDataUnlimited<DIM, 2>::Builder>();
  dataBuilder->setReconAuxiliary(aux);

  // set data structure
  buffer->setNewBuffer(GridSystemData<DIM, DOS>::builder().build(dataBuilder, _grid));
  buffer->setOldBuffer(buffer->newBuffer()->deepCopy());
}

void AdvDiffusionBurgers2DState::initialize() {
  Vector1D tmp;
  VolumeIntegrator<DIM, 2> integrator(_grid->origin(), _grid->gridSpacing());
  for (size_t i = 0; i < _grid->dataSize()[0]; ++i) {
    for (size_t j = 0; j < _grid->dataSize()[1]; ++j) {
      tmp = integrator.volumeIntegral<Vector1D>(
          [&](const Mesh<DIM>::point_t &pt) -> Vector1D {
            Vector1D tmp_quad;
            _problemDef->initialCondition(pt, tmp_quad);
            return tmp_quad;
          },
          {i, j});

      tmp /= _grid->AreaOfEle();

      buffer->newBuffer()->setVectorData({i, j}, tmp);
    }
  }
}

void AdvDiffusionBurgers2DState::timeStep(double &dt) {
  dt = std::numeric_limits<double>::max();
  for (size_t i = 0; i < _grid->dataSize()[0]; ++i) {
    for (size_t j = 0; j < _grid->dataSize()[1]; ++j) {
      dt = std::min(dt, _grid->SizeOfEle() * CFL / _eqInfo->MaxCharacteristicSpeed(buffer->newBuffer()->value({i, j})));
    }
  }
}

void AdvDiffusionBurgers2DState::update(double &dt) {
  // use dual buffer strategy to swap the pointer and avoid copy of all data
  _ssprkSolver->ssprk3(
      buffer->newBuffer(), dt,
      [&](const GridSystemDataPtr<DIM, DOS> &in, GridSystemDataPtr<DIM, DOS> out) -> void {
        in->calculateBasisFunction();
        _advectionSolver->advect(in, dt, _problemDef, AdvectionSolver<DIM, DOS>::Whole_flux, std::move(out));
      },
      buffer->oldBuffer());

  buffer->swapBuffer();
}

void AdvDiffusionBurgers2DState::postProcess() {
  buffer->newBuffer()->calculateBasisFunction();

  // save result
  buffer->newBuffer()->save_data("AdvResult.dat");
}
