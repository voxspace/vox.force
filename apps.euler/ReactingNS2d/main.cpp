//
//  main.cpp
//  apps
//
//  Created by 杨丰 on 2020/5/17.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "../../src.euler/physics_solver.h"
#include "ReactingNS2dState.h"

int main(int argc, char *argv[]) {
  if (argc < 2) {
    std::cerr << "Usage: " << argv[0] << "<length>" << std::endl;
    return 1;
  }

  ReactingNS2DState mySolverState(atof(argv[1]));

  vox::PhysicsSolver<2, 5> solver(&mySolverState);
  mySolverState._notifyPhysicsSystem(&solver);

  solver.createScene();

  solver.initialize();

  solver.preProcess();

  solver.Run(1000.0);

  solver.postProcess();

  return 0;
}
