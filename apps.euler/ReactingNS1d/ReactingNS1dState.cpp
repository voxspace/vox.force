//
//  ReactingNS1dState.cpp
//  apps
//
//  Created by 杨丰 on 2020/5/17.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "ReactingNS1dState.h"

#include "../../src.euler/error_approximator.h"
#include "../../src.euler/fluxes/flux_RNS_HLLC.h"
#include "../../src.euler/grid_data_unlimited.h"
#include "../../src.euler/grid_system_data.h"
#include "../../src.euler/log_manager.h"
#include "../../src.euler/recon_auxiliary.h"
#include <fstream>

using namespace vox;

struct Parameter {
  double gamma{}, q0{}, Ea{}, f{}, Ww{}, x0{}, L_half{}, k{}, K{}, A{}, L{}, s{};
  std::vector<std::vector<double>> ref_Y;

  double rho_inf_r{};
  double u_inf_r{};
  double p_inf_r{};
  double e_inf_r{};
  double f_inf_r{};

  double rho_inf_l{};
  double u_inf_l{};
  double p_inf_l{};
  double e_inf_l{};
  double f_inf_l{};
};
Parameter *para;
LogManager *mLog;

void ReadPara() {
  std::fstream file("ZND_configuration_solution.dat");

  delete para;

  para = new Parameter;

  file >> para->Ea >> para->q0 >> para->f >> para->gamma >> para->K >> para->L_half >> para->s;
  int length = 0;
  file >> length;
  para->ref_Y.resize(5);
  para->ref_Y[0].resize(length);
  para->ref_Y[1].resize(length);
  para->ref_Y[2].resize(length);
  para->ref_Y[3].resize(length);
  para->ref_Y[4].resize(length);
  for (int i = 0; i < length; ++i) {
    file >> para->ref_Y[0][i] >> para->ref_Y[1][i] >> para->ref_Y[2][i] >> para->ref_Y[3][i] >> para->ref_Y[4][i];
  }

  file.close();

  para->Ww = 1.;
  para->x0 = 480.0; // 62.0;//26.0;///96.;///210.0/// initial position of the shock
  para->k = 0.;     /// 0 means no perturbation at all.
  /// maybe useless.../// periods of the perturbation
  /// added on the initial condition. Maybe useless...
  para->A = 0.1; /// magnitude of the perturbation, maybe useless...
  para->L = 10.; /// tunnel width, caption L might not be a good name here...

  std::cout << "In this simulation, the parameters used are as follows" << std::endl;
  std::cout << "para.gamma = " << para->gamma << std::endl;
  std::cout << "para.q0 = " << para->q0 << std::endl;
  std::cout << "para.Ea = " << para->Ea << std::endl;
  std::cout << "para.f = " << para->f << std::endl;
  std::cout << "para.K = " << para->K << std::endl;

  std::cout << "para.Ww = " << para->Ww << std::endl;
  std::cout << "para.x0 = " << para->x0 << std::endl;
  std::cout << "para.L_half = " << para->L_half << std::endl;
  std::cout << "para.k = " << para->k << std::endl;
  std::cout << "para.A = " << para->A << std::endl;

  std::cout << "para.L = " << para->L << std::endl;
  std::cout << "para.s = " << para->s << std::endl;
}

void initial_values(const Vector1D &p, double &rho, double &u, double &P, double &Y) {
  const double &x0 = para->x0;
  const std::vector<std::vector<double>> &ref_Y = para->ref_Y;

  double dx = ref_Y[0][1] - ref_Y[0][0]; /// negative
  double rx = p[0] - x0;
  int index = static_cast<int>(std::floor(rx / dx));

  rho = ref_Y[1][index] + 0.5 * (ref_Y[1][index + 1] - ref_Y[1][index]);
  u = ref_Y[2][index] + 0.5 * (ref_Y[2][index + 1] - ref_Y[2][index]);
  P = ref_Y[3][index] + 0.5 * (ref_Y[3][index + 1] - ref_Y[3][index]);
  Y = ref_Y[4][index] + 0.5 * (ref_Y[4][index + 1] - ref_Y[4][index]);
}

/**
 * @brief find Y on the left and right boundaries first, then calculate the other quantities.
 *
 */
void setBoundaryCondition(const MeshPtr<1> &grid) {
  const double &gamma = para->gamma;
  const double &q0 = para->q0;
  const double &s = para->s;
  double rho = 1., u = 0., P = 1., Y = 1.;
  para->rho_inf_r = rho;
  para->u_inf_r = u - s;
  para->p_inf_r = P;
  para->f_inf_r = Y;
  para->e_inf_r = para->p_inf_r / (gamma - 1.) + 0.5 * para->rho_inf_r * para->u_inf_r * para->u_inf_r +
                  para->rho_inf_r * para->f_inf_r * q0;

  Mesh1D::point_t leftBnd_pnt = grid->origin();
  initial_values(leftBnd_pnt, rho, u, P, Y);
  para->rho_inf_l = rho;
  para->u_inf_l = u - s;
  para->p_inf_l = P;
  para->f_inf_l = Y;
  para->e_inf_l = para->p_inf_l / (gamma - 1) + 0.5 * para->rho_inf_l * para->u_inf_l * para->u_inf_l +
                  para->rho_inf_l * para->f_inf_l * para->q0;
}

void bd1d(const Vector<double, 4> &val, Vector<double, 4> &result, const std::array<double, 1> &out_normal,
          const Mesh1D::point_t &pt, int flag) {
  if (flag == 2) { // supersonic inflow
    result[0] = para->rho_inf_r;
    result[1] = para->rho_inf_r * para->u_inf_r;
    result[2] = para->p_inf_r / (para->gamma - 1.) + 0.5 * para->rho_inf_r * (para->u_inf_r * para->u_inf_r) +
                para->rho_inf_r * para->f_inf_r * para->q0; /// e_inf_r;
    result[3] = para->rho_inf_r * para->f_inf_r;
  } else if (flag == 1) { // do nothing means extrapolation boundary condition
    result = val;
  } else {
    std::cout << "something wrong in boundaryValue..." << std::endl;
    getchar();
  }
}

void VisBoundaryCondition(const Vector<Vector<double, 1>, 4> &val, Vector<Vector<double, 1>, 4> &result,
                          const std::array<double, 1> &out_normal, const Mesh1D::point_t &pt, int flag) {
  result = val;
}

void initial(const Mesh1D::point_t &pt, Vector<double, 4> &value) {
  const double &gamma = para->gamma;
  const double &q0 = para->q0;
  const double &s = para->s;
  double rho = 1., u = 0., P = 1., Y = 1.;

  if (pt[0] < para->x0 - 1.0e-08) {
    initial_values(pt, rho, u, P, Y);

    value[0] = rho;
    value[1] = rho * (u - s);
    value[2] = P / (gamma - 1.) + 0.5 * rho * ((u - s) * (u - s) + 0. * 0.) + rho * Y * q0;
    value[3] = rho * Y;
  } else {
    value[0] = rho;
    value[1] = rho * (u - s);
    value[2] = P / (gamma - 1.) + 0.5 * rho * ((u - s) * (u - s) + 0. * 0.) + rho * Y * q0;
    value[3] = rho * Y;
  }
}

void perturbInitialCondition(const Mesh1D::point_t &pt, Vector<double, 4> &value) {
  const double &Ww = para->Ww;
  const double &shock_position = para->x0;
  //    const double &magnitude = para->A;
  //    const double &tunnelWidth = para->L;
  //    const double &k = para->k;

  if (pt[0] < shock_position - 1.0e-08 && pt[0] > shock_position - Ww + 1.0e-08) {
    //        value[2] = magnitude * sin(2 * k * M_PI * pt[1] / tunnelWidth);
  }
}

//-------------------------------------------------------------------------------
double ReactingNS1DState::output_maxPressure() {
  double maxPressure = -1.0e+06;
  double press;

  for (size_t i = 0; i < buffer->newBuffer()->numberOfVectorData()[0]; ++i) {
    if (fabs(_grid->BaryCenter({i})[0] - para->x0) > 10)
      continue;

    press = _eqInfo->pressure(buffer->newBuffer()->value({i}));

    if (press > maxPressure)
      maxPressure = press;
  }
  return maxPressure;
}

ReactingNS1DState::ReactingNS1DState(int l) : CFL(0.5) {
  mLog = new LogManager;
  mLog->createLog("testLog");

  size_t N = l;
  _grid = std::make_shared<Mesh<DIM>>(Vector<size_t, DIM>({N}), Vector<double, DIM>({1.0 / static_cast<double>(N)}));

  _advectionSolver = std::make_shared<AdvectionSolver<DIM, DOS>>(_grid, std::make_shared<FluxRNSHLLC<DIM>>());
  _ssprkSolver = std::make_shared<SSPRKSolver<DIM, DOS>>();
  _precorSolver = std::make_shared<Prediction_CorrectionSolver<DIM, DOS>>();

  ReadPara();
}

ReactingNS1DState::~ReactingNS1DState() {
  delete para;
  delete mLog;
}

void ReactingNS1DState::createScene() {
  // load resources and information
  _eqInfo = std::make_shared<EquationReactingNS<1>>(para->gamma, para->q0, 0.01, 0.06, 0.01);

  _problemDef = std::make_shared<ProblemDef<DIM, DOS>>(_eqInfo, _grid, bd1d, VisBoundaryCondition, initial, 1, "1DRNS");

  ReconAuxiliaryPtr<1, 1> aux = std::make_shared<ReconAuxiliary<1, 1>>(_grid);
  aux->buildReconstructPatch();
  aux->updateLSMatrix();
  GridDataUnlimitedBuilderPtr<1, 1> dataBuilder = std::make_shared<GridDataUnlimited<1, 1>::Builder>();
  dataBuilder->setReconAuxiliary(aux);

  setBoundaryCondition(_grid);

  // set data structure for whole data
  buffer->setNewBuffer(GridSystemData<DIM, DOS>::builder().build(dataBuilder, _grid));
  buffer->setOldBuffer(buffer->newBuffer()->deepCopy());
}

void ReactingNS1DState::initialize() {
  Vector<double, DOS> tmp;
  for (size_t i = 0; i < buffer->newBuffer()->numberOfVectorData()[0]; i++) {
    _problemDef->initialCondition(_grid->BaryCenter({i}), tmp);

    buffer->newBuffer()->setVectorData({i}, tmp);
  }
}

void ReactingNS1DState::timeStep(double &dt) {
  dt = std::numeric_limits<double>::max();
  for (size_t i = 0; i < buffer->newBuffer()->numberOfVectorData()[0]; ++i) {
    dt = std::min(dt, _grid->SizeOfEle() * CFL / _eqInfo->MaxCharacteristicSpeed(buffer->newBuffer()->value({i})));
  }
}

void ReactingNS1DState::preProcess() {
  double dt = 0.0;
  _ssprkSolver->ssprk1(
      buffer->newBuffer(), dt,
      [&](const GridSystemDataPtr<DIM, DOS> &in, GridSystemDataPtr<DIM, DOS> out) -> void {
        in->calculateBasisFunction();
        _advectionSolver->advect(in, dt, _problemDef, AdvectionSolver<DIM, DOS>::Whole_flux, std::move(out));
      },
      buffer->oldBuffer());
  buffer->swapBuffer();

  for (size_t i = 0; i < buffer->newBuffer()->numberOfVectorData()[0]; i++) {
    Vector<double, DOS> tmp = buffer->newBuffer()->value({i});
    perturbInitialCondition(_grid->BaryCenter({i}), tmp);
    buffer->newBuffer()->setVectorData({i}, tmp);
  }
  std::cout << "initial condition is perturbed ..." << std::endl;
}

void ReactingNS1DState::update(double &dt) {
  reactionUpdate(0.5 * dt);

  advectionUpdate(dt);

  reactionUpdate(0.5 * dt);

  LogManager::getSingleton().logMessage(std::to_string(GlobalClock::getSingleton().getTime()) + "\t" +
                                        std::to_string(output_maxPressure()));
}

void ReactingNS1DState::advectionUpdate(double dt) {
  _ssprkSolver->ssprk2(
      buffer->newBuffer(), dt,
      [&](const GridSystemDataPtr<DIM, DOS> &in, GridSystemDataPtr<DIM, DOS> out) -> void {
        in->calculateBasisFunction();
        _advectionSolver->advect(in, dt, _problemDef, AdvectionSolver<DIM, DOS>::Whole_flux, std::move(out));
      },
      buffer->oldBuffer());

  buffer->swapBuffer();
}

void ReactingNS1DState::reactionUpdate(double dt) {
  _precorSolver->precor(
      buffer->newBuffer(), dt,
      [&](const Vector<double, DOS> &in, Vector<double, DOS> &out) -> void {
        double p = _eqInfo->pressure(in);
        double T = p / in[0];
        out[0] = 0.;
        out[1] = 0.;
        out[2] = 0.;
        out[3] = -para->K * in[DOS - 1] * exp(-para->Ea / T);
      },
      [&](const Vector<double, DOS> &initial_cellAverage) -> double {
        return 10. / (para->K * exp(-initial_cellAverage[0] * para->Ea / _eqInfo->pressure(initial_cellAverage)));
      },
      [&](const Vector<double, DOS> &left, const Vector<double, DOS> &right) -> double {
        return std::abs(left[DOS - 1] - right[DOS - 1]);
      },
      buffer->oldBuffer());

  buffer->swapBuffer();
}

void ReactingNS1DState::postProcess() {
  // save result
  buffer->newBuffer()->save_data("AdvResult.dat");
}
