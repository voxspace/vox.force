//
//  laplacian2dState.cpp
//  apps
//
//  Created by 杨丰 on 2020/5/17.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "laplacian2dState.h"
#include "../../src.euler/error_approximator.h"
#include "../../src.euler/fluxes/flux_LF.h"
#include "../../src.euler/grid_data_unlimited.h"
#include "../../src.euler/grid_system_data_minmax.h"
#include "../../src.euler/recon_auxiliary.h"
#include "../ProblemInfo/AdvectionProblem.h"
#include <fstream>

using namespace vox;

Laplacian2DState::Laplacian2DState(int l) {
  size_t N = l;
  double s = 1.0 / static_cast<double>(l);
  _grid = std::make_shared<Mesh<DIM>>(Vector<size_t, DIM>({N, N}), Vector<double, DIM>({s, s}));

  _grid->addPBDescriptor(Direction::left);
  _grid->addPBDescriptor(Direction::bottom);

  std::array<EquationScalar<DIM>::fluxFunc, DIM> flux{};
  std::array<EquationScalar<DIM>::fluxFunc, DIM> fluxDer{};
  std::array<EquationScalar<DIM>::viscousFunc, DIM> vfuncs = {ScalarEquationDataBase::ViscousTerm::constVal(1.0),
                                                              ScalarEquationDataBase::ViscousTerm::constVal(1.0)};

  _eqInfo = std::make_shared<EquationScalar<DIM>>(flux, fluxDer, vfuncs);

  _advectionSolver = std::make_shared<AdvectionSolver<DIM, DOS>>(_grid, std::make_shared<FluxLF<DIM, DOS>>());
}

void Laplacian2DState::createScene() {
  // load resources and information
  _problemDef = std::make_shared<ProblemDef<DIM, DOS>>(
      _eqInfo, _grid, Advection::bd<DIM>, Advection::VisBoundaryCondition<DIM>, Advection::init<DIM>, 2, "2DLaplacian");

  ReconAuxiliaryPtr<DIM, 2> aux = std::make_shared<ReconAuxiliary<DIM, 2>>(_grid);
  aux->buildReconstructPatch(PatchSearcher<DIM, 2>::Node);
  aux->updateLSMatrix();
  GridDataUnlimitedBuilderPtr<DIM, 2> dataBuilder = std::make_shared<GridDataUnlimited<DIM, 2>::Builder>();
  dataBuilder->setReconAuxiliary(aux);

  // set data structure
  buffer->setNewBuffer(GridSystemData<DIM, DOS>::builder().build(dataBuilder, _grid));
  buffer->setOldBuffer(buffer->newBuffer()->deepCopy());
}

void Laplacian2DState::initialize() {
  Vector1D tmp;
  VolumeIntegrator<DIM, 2> integrator(_grid->origin(), _grid->gridSpacing());
  for (size_t i = 0; i < _grid->dataSize()[0]; ++i) {
    for (size_t j = 0; j < _grid->dataSize()[1]; ++j) {
      tmp = integrator.volumeIntegral<Vector1D>(
          [&](const Mesh<DIM>::point_t &pt) -> Vector1D {
            Vector1D tmp_quad;
            _problemDef->initialCondition(pt, tmp_quad);
            return tmp_quad;
          },
          {i, j});

      tmp /= _grid->AreaOfEle();

      buffer->newBuffer()->setVectorData({i, j}, tmp);
    }
  }
}

void Laplacian2DState::timeStep(double &dt) { dt = 0.1; }

void Laplacian2DState::update(double &dt) {
  buffer->newBuffer()->calculateBasisFunction();
  _advectionSolver->advect(buffer->newBuffer(), dt, _problemDef, AdvectionSolver<DIM, DOS>::Viscous_only,
                           buffer->oldBuffer());
  buffer->swapBuffer();
}

void Laplacian2DState::postProcess() {
  // save result
  buffer->newBuffer()->save_data("AdvResult.dat");

  // output error
  std::function<double(const Mesh2D::point_t &pt)> realFunc;
  realFunc = [&](const Mesh2D::point_t &pt) -> double {
    return -2 * std::pow(2 * M_PI, 2) * std::sin(2 * M_PI * pt[0]) * std::sin(2 * M_PI * pt[1]);
  };

  double error = 0.0;
  double dx = _grid->SizeOfEle();
  for (size_t i = 0; i < _grid->dataSize()[0]; ++i) {
    for (size_t j = 0; j < _grid->dataSize()[1]; ++j) {
      dx = std::min(dx, _grid->AreaOfEle());
      error +=
          _grid->AreaOfEle() * std::abs(buffer->newBuffer()->value({i, j})[0] + realFunc(_grid->BaryCenter({i, j})));
    }
  }

  std::string filename = "L1Error.dat";
  std::ofstream output(filename.c_str(), std::ios::app);
  output << dx << "\t" << error << std::endl;
}
