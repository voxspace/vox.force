//
//  laplacian2dState.hpp
//  apps
//
//  Created by 杨丰 on 2020/5/17.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef laplacian2dState_hpp
#define laplacian2dState_hpp

#include "../../src.euler/physics_solver.h"
#include "../../src.euler/solver_state.h"
#include "../../src.euler/dual_buffer.h"
#include "../../src.euler/equations/equation_scalar.h"
#include "../../src.euler/advection_solver.h"

class Laplacian2DState : public vox::SolverState<2, 1> {
public:
  //! Default constructor
  explicit Laplacian2DState(int l);

public:
  //! Load resources and equation info
  void createScene() override;

  //! Set initial value
  void initialize() override;

  //! Time Step
  void timeStep(double &dt) override;

  //! PostProcess
  void postProcess() override;

  void update(double &dt) override;

private:
  vox::AdvectionSolverPtr<DIM, DOS> _advectionSolver;

  vox::MeshPtr<DIM> _grid;
  vox::EquationScalarPtr<DIM> _eqInfo;
  vox::ProblemDefPtr<DIM, DOS> _problemDef;
};
#endif /* laplacian2dState_hpp */
