CMAKE_MINIMUM_REQUIRED(VERSION 3.14)

# Target name
set(target apps.euler.laplacian2d)

# Sources
file(GLOB sources
        ${CMAKE_CURRENT_SOURCE_DIR}/*.cpp
        "${CMAKE_CURRENT_SOURCE_DIR}/*.cu")

add_definitions(-DJET_USE_CUDA)

# Build executable
add_executable(${target}
        ${sources})

# Project options
set_target_properties(
        ${target}
        PROPERTIES
        CUDA_SEPARABLE_COMPILATION ON)

list(APPEND CMAKE_MODULE_PATH "../../cmake")
find_package(TBB)

# Link libraries
target_link_libraries(${target}
        PUBLIC
        ${DEFAULT_LINKER_OPTIONS}
        ${DEFAULT_LIBRARIES}
        Flex_Euler TBB::tbb dl)
