function [] = plot_isosurface()
%SHOWMESH 

p = load('../mat_Node.dat');
t = load('../mat_Ele.dat');

U = load('../result.dat');
U = U(:,4);

U = U';
p = p';
t = t';

aux = ones(1, size(t, 2));
t = [t; aux];


%% 2D
pdeplot(p,[],t,'xydata',U,'xystyle','off','contour',...
'on','levels',20,'colormap','jet', 'colorbar','on');
xlim([0,1]);
xlabel('x');
ylim([0,1]);
ylabel('y');
box on

%% 3D
% pdeplot(p,[],t,'xydata',U,'xystyle','flat',...
%          'zdata',U,'zstyle','continuous',...
%          'mesh', 'on',...
%          'colormap', 'jet',...
%          'colorbar','on'); 
% view([-45,30]);
% box on;
% grid on;

%print -deps Solid_order4_20.eps

end

