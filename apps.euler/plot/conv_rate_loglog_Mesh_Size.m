function conv_rate_loglog_Mesh_Size(x1,y1,x2,y2,C)
% This function plots the convergence rate of an 'almost straight' curve
% using a triangle
% x1 : the x-coordinate of the first point
% y1 : the y-coordinate of the first point
% x2 : the x-coordinate of the second point
% y2 : the y-coordinate of the second point
% C  : lowering factor of y which makes the triangle under the curve
% Remark: Genearlly speaking, x1 < x2, y1 < y2

  y1 = y1/C;
  y2 = y2/C;
  
  loglog([x1,x2],[y1,y2]);
  loglog([x1,x2],[y1,y1]);
  loglog([x2,x2],[y1,y2]);
  text(x2*C, (y2*y1)^0.5, num2str((log(y2) - log(y1))/(log(x2) - log(x1))),...
      'HorizontalAlignment','right')


end