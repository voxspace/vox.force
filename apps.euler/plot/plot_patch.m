function [] = plot_patch()
%
%% load data
p = load('../mat_Node.dat');
t = load('../mat_Ele.dat');

U = load('../result.dat');
c = U(:,4);

%% plot by using patch
n_ele = size(t, 1);
n_vertex = size(t, 2);
for i = 1:n_ele
    Vt = [];
    Ft = [];
    for j = 1:n_vertex
        Vt = [Vt; p(t(i, j), :) U(i, 4)]; 
        Ft = [Ft, j];
    end
    S.Vertices = Vt;
    S.Faces = Ft;
    S.FaceVertexCData = c(i);
    S.FaceColor = 'flat';
    patch(S);
end
view(3);

% colorbar
% xlabel('x');
% ylabel('y');

% print -depsc file.eps
end

