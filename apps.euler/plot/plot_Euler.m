function [] = plot_Euler( )
%PLOTEULER Summary of this function goes here
%   Detailed explanation goes here
Decomp = load('../../cmake-build-debug/bin/AdvResult.dat');
% git@github.com:SeekPossibility/CFDdataset.git
exact = load('../../../CFD_Gallery/Euler1D_MSod/result.dat');

subplot(2,2,1);
plot(Decomp(:,1), Decomp(:,3), '. b');
hold on
plot(exact(:, 1), exact(:,3), '-k');
% legend('Decomp', 'NonDecomp', 'Location', 'best');
ylabel('Density');

subplot(2,2,2);
plot(Decomp(:,1), Decomp(:,4)./Decomp(:,3), '. b');
hold on
plot(exact(:, 1), exact(:,4)./exact(:,3), '-k');
% legend('Decomp', 'NonDecomp', 'Location', 'best');
ylabel('Velocity');

subplot(2,2,3);
plot(Decomp(:,1), 0.4*(Decomp(:,5) - 0.5*Decomp(:,4).*Decomp(:,4)./Decomp(:,3)), '. b');
hold on
plot(exact(:, 1), 0.4*(exact(:,5) - 0.5*exact(:,4).*exact(:,4)./exact(:,3)), '-k');
% legend('Decomp', 'NonDecomp', 'Location', 'best');
ylabel('Pressure');

subplot(2,2,4);
plot(Decomp(:,1), Decomp(:,5)./Decomp(:,3) - 0.5*(Decomp(:,4)./Decomp(:,3)).^2, '. b');
hold on
plot(exact(:, 1), exact(:,5)./exact(:,3) - 0.5*(exact(:,4)./exact(:,3)).^2 , '-k');
% legend('Decomp', 'NonDecomp', 'Location', 'best');
ylabel('Internal energy');

% print -depsc Euler_test5.eps
end

