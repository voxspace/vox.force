//
//  NS2dPositiveState.cpp
//  apps
//
//  Created by 杨丰 on 2020/5/17.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "NS2dPositiveState.h"
#include "../../src.euler/error_approximator.h"
#include "../../src.euler/fluxes/flux_LF.h"
#include "../../src.euler/fluxes/flux_NS_Positive.h"
#include "../../src.euler/grid_data_unlimited.h"
#include "../../src.euler/grid_system_data_NSPositive.h"
#include "../../src.euler/recon_auxiliary.h"
#include <fstream>

#include "../ProblemInfo/DualMach.h"
#include "../ProblemInfo/Sedov.h"

using namespace vox;

const double Re = 200;
const double Pr = 0.72;
const double Gamma = 1.4;
const double eps = 9.5;
void Init(const Mesh2D::point_t &x, Vector<double, 4> &u, double t) {
  double r2 = std::pow(x[0] - 5.0 - t, 2) + std::pow(x[1] - 5.0 - t, 2);

  u[0] =
      std::pow(1 - (Gamma - 1) / Gamma / 8.0 / std::pow(M_PI, 2) * std::pow(eps, 2) * exp(1 - r2), 1.0 / (Gamma - 1));
  u[1] = u[0] * (1 - eps / 2.0 / M_PI * std::exp(0.5) * (1 - r2) * (x[1] - 5.0 - t));
  u[2] = u[0] * (1 + eps / 2.0 / M_PI * std::exp(0.5) * (1 - r2) * (x[0] - 5.0 - t));
  u[3] = std::pow(u[0], Gamma) / 0.4 + 0.5 * (u[1] * u[1] + u[2] * u[2]) / u[0];
}
//-------------------------------------------------------------------------
NS2DPositiveState::NS2DPositiveState(int l) : CFL(0.1) {
  const double mu = 1.0 / Re;
  const double kappa = Gamma / Re / Pr;

  size_t N = l;
  double s = 2.0 / static_cast<double>(l);
  _grid = std::make_shared<Mesh<DIM>>(Vector<size_t, DIM>({N, N}), Vector<double, DIM>({s, s}));

  _eqInfo = std::make_shared<EquationNS<DIM>>(Gamma, mu, kappa);

  _advectionSolver = std::make_shared<AdvectionSolver<DIM, DOS>>(_grid, std::make_shared<FluxLF<DIM, DOS>>());
  _ssprkSolver = std::make_shared<SSPRKSolver<DIM, DOS>>();
}

void NS2DPositiveState::createScene() {
  // load resources and information
  _problemDef = std::make_shared<ProblemDef<DIM, DOS>>(_eqInfo, _grid, Sedov::BoundaryCondition,
                                                       Sedov::VisBoundaryCondition, Sedov::Init, 1, "2DNS");

  ReconAuxiliaryPtr<DIM, 1> aux = std::make_shared<ReconAuxiliary<DIM, 1>>(_grid);
  aux->buildReconstructPatch();
  aux->updateLSMatrix();
  GridDataUnlimitedBuilderPtr<DIM, 1> dataBuilder = std::make_shared<GridDataUnlimited<DIM, 1>::Builder>();
  dataBuilder->setReconAuxiliary(aux);

  // set data structure
  buffer->setNewBuffer(GridSysDataNSPositive<DIM, 1>::builder().build(dataBuilder, _grid, _eqInfo));
  buffer->setOldBuffer(buffer->newBuffer()->deepCopy());
}

void NS2DPositiveState::initialize() {
  Vector<double, DOS> tmp;
  VolumeIntegrator<DIM, 2> integrator(_grid->origin(), _grid->gridSpacing());
  for (size_t i = 0; i < _grid->dataSize()[0]; ++i) {
    for (size_t j = 0; j < _grid->dataSize()[1]; ++j) {
      tmp = integrator.volumeIntegral<Vector<double, DOS>>(
          [&](const Mesh<DIM>::point_t &pt) -> Vector<double, DOS> {
            Vector<double, DOS> tmp_quad;
            _problemDef->initialCondition(pt, tmp_quad);
            return tmp_quad;
          },
          {i, j});

      tmp /= _grid->AreaOfEle();

      buffer->newBuffer()->setVectorData({i, j}, tmp);
    }
  }
}

void NS2DPositiveState::timeStep(double &dt) {
  dt = std::numeric_limits<double>::max();
  for (size_t i = 0; i < _grid->dataSize()[0]; ++i) {
    for (size_t j = 0; j < _grid->dataSize()[1]; ++j) {
      dt = std::min(dt, _grid->SizeOfEle() * CFL / _eqInfo->MaxCharacteristicSpeed(buffer->newBuffer()->value({i, j})));
      dt = std::max(dt, _grid->SizeOfEle() * _grid->SizeOfEle() * 0.1);
    }
  }
}

void NS2DPositiveState::update(double &dt) {
  _ssprkSolver->ssprk2(
      buffer->newBuffer(), dt,
      [&](const GridSystemDataPtr<DIM, DOS> &in, GridSystemDataPtr<DIM, DOS> out) -> void {
        in->calculateBasisFunction();
        _advectionSolver->advect(in, dt, _problemDef, AdvectionSolver<DIM, DOS>::Whole_flux, std::move(out));
      },
      buffer->oldBuffer());

  while (true) {
    bool isNeg = false;
    for (size_t i = 0; i < _grid->dataSize()[0]; ++i) {
      for (size_t j = 0; j < _grid->dataSize()[1]; ++j) {
        Vector<double, DOS> vals = buffer->oldBuffer()->value({i, j});
        double P = (_eqInfo->Gamma - 1) * (vals[3] - 0.5 * (vals[1] * vals[1] + vals[2] * vals[2]) / vals[0]);
        if (P < 0.0 || std::isnan(std::abs(P)) || vals[0] < 1.0e-13) {
          isNeg = true;
        }
      }
    }

    if (!isNeg) {
      break;
    } else {
      dt *= 0.5;
      _ssprkSolver->ssprk2(
          buffer->newBuffer(), dt,
          [&](const GridSystemDataPtr<DIM, DOS> &in, GridSystemDataPtr<DIM, DOS> out) -> void {
            in->calculateBasisFunction();
            _advectionSolver->advect(in, dt, _problemDef, AdvectionSolver<DIM, DOS>::Whole_flux, out);
          },
          buffer->oldBuffer());
    }
  }

  buffer->swapBuffer();
}

void NS2DPositiveState::postProcess() {
  // save result
  buffer->newBuffer()->save_data("AdvResult.dat");
}
