//
//  Euler3dState.cpp
//  apps
//
//  Created by 杨丰 on 2020/5/17.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "Euler3dState.h"

#include "../../src.euler/error_approximator.h"
#include "../../src.euler/fluxes/flux_LF.h"
#include "../../src.euler/grid_system_data.h"
#include <fstream>

#include "../ProblemInfo/airfoil3D.h"

using namespace vox;
//TODO: Fix for BoundaryMark
Euler3DState::Euler3DState() : CFL(0.1) {
  _grid = std::make_shared<Mesh<3>>(Vector3UZ({10, 10, 10}), Vector<double, 3>({0.1, 0.1, 0.1}));

  _eqInfo = std::make_shared<EquationNS<3>>(1.4);

  _advectionSolver = std::make_shared<AdvectionSolver<DIM, DOS>>(_grid, std::make_shared<FluxLF<DIM, DOS>>());
  _ssprkSolver = std::make_shared<SSPRKSolver<DIM, DOS>>();
}

void Euler3DState::createScene() {
  // load resources and information
  _problemDef = std::make_shared<ProblemDef<DIM, DOS>>(_eqInfo, _grid, AirFoil::BoundaryCondition, nullptr,
                                                       AirFoil::Init, 1, "3DEuler");

  // set data structure
  buffer->setNewBuffer(GridSystemData<DIM, DOS>::builder().build(_grid));
  buffer->setOldBuffer(buffer->newBuffer()->deepCopy());
}

void Euler3DState::initialize() {
  Vector<double, DOS> tmp;
  for (size_t i = 0; i < _grid->dataSize()[0]; ++i) {
    for (size_t j = 0; j < _grid->dataSize()[1]; ++j) {
      for (size_t k = 0; k < _grid->dataSize()[2]; ++k) {
        _problemDef->initialCondition(_grid->BaryCenter({i, j, k}), tmp);
        buffer->newBuffer()->setVectorData({i, j, k}, tmp);
      }
    }
  }
}

void Euler3DState::timeStep(double &dt) {
  dt = std::numeric_limits<double>::max();
  for (size_t i = 0; i < _grid->dataSize()[0]; ++i) {
    for (size_t j = 0; j < _grid->dataSize()[1]; ++j) {
      for (size_t k = 0; k < _grid->dataSize()[2]; ++k) {
        dt = std::min(dt, _grid->SizeOfEle() * CFL /
                              _eqInfo->MaxCharacteristicSpeed(buffer->newBuffer()->value({i, j, k})));
      }
    }
  }
}

void Euler3DState::update(double &dt) {
  // use dual buffer strategy to swap the pointer and avoid copy of all data
  _ssprkSolver->ssprk1(
      buffer->newBuffer(), dt,
      [&](const GridSystemDataPtr<DIM, DOS> &in, GridSystemDataPtr<DIM, DOS> out) -> void {
        in->calculateBasisFunction();
        _advectionSolver->advect(in, dt, _problemDef, AdvectionSolver<DIM, DOS>::inviscid_only, std::move(out));
      },
      buffer->oldBuffer());

  buffer->swapBuffer();
}

void Euler3DState::postProcess() {
  // save result
  buffer->newBuffer()->save_data("AdvResult.dat");
}
