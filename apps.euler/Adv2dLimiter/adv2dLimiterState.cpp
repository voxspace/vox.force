//
//  adv2dLimiterState.cpp
//  apps
//
//  Created by 杨丰 on 2020/5/16.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "adv2dLimiterState.h"

#include "../../src.euler/error_approximator.h"
#include "../../src.euler/fluxes/flux_LF.h"
#include "../../src.euler/grid_data_limiter.h"
#include "../../src.euler/grid_system_data.h"
#include "../../src.euler/recon_auxiliary.h"
#include "../ProblemInfo/AdvectionProblem.h"
#include <fstream>

using namespace vox;

Adv2DLimiterState::Adv2DLimiterState(int l) : CFL(0.1) {
  size_t N = l;
  double s = 1.0 / static_cast<double>(l);
  _grid = std::make_shared<Mesh<DIM>>(Vector<size_t, DIM>({N, N}), Vector<double, DIM>({s, s}));

  _grid->addPBDescriptor(Direction::left);
  _grid->addPBDescriptor(Direction::bottom);

  std::array<EquationScalar<DIM>::fluxFunc, DIM> flux;
  flux[0] = ScalarEquationDataBase::Advection::flux(1.0);
  flux[1] = ScalarEquationDataBase::Advection::flux(2.0);
  std::array<EquationScalar<DIM>::fluxFunc, DIM> fluxDer;
  fluxDer[0] = ScalarEquationDataBase::Advection::fluxDer(1.0);
  fluxDer[1] = ScalarEquationDataBase::Advection::fluxDer(2.0);
  std::array<EquationScalar<DIM>::viscousFunc, DIM> vfuncs{};

  _eqInfo = std::make_shared<EquationScalar<DIM>>(flux, fluxDer, vfuncs);

  _advectionSolver = std::make_shared<AdvectionSolver<DIM, DOS>>(_grid, std::make_shared<FluxLF<DIM, DOS>>());
  _ssprkSolver = std::make_shared<SSPRKSolver<DIM, DOS>>();
}

void Adv2DLimiterState::createScene() {
  // load resources and information
  _problemDef = std::make_shared<ProblemDef<DIM, DOS>>(_eqInfo, _grid, Advection::bd<DIM>, nullptr,
                                                       Advection::init<DIM>, 1, "2Dadv");

  // set data structure
  ReconAuxiliaryPtr<DIM, 1> aux = std::make_shared<ReconAuxiliary<DIM, 1>>(_grid);
  aux->buildReconstructPatch();
  aux->updateLSMatrix();
  GridDataLimiterBuilderPtr<DIM> dataBuilder = std::make_shared<GridDataLimiter<DIM>::Builder>();
  dataBuilder->setLimiterAuxiliary(aux);
  dataBuilder->setLimiterType(GridDataLimiter<DIM>::BARTH);

  buffer->setNewBuffer(GridSystemData<DIM, DOS>::builder().build(dataBuilder, _grid));
  buffer->setOldBuffer(buffer->newBuffer()->deepCopy());
}

void Adv2DLimiterState::initialize() {
  Vector1D tmp;
  VolumeIntegrator<DIM, 2> integrator(_grid->origin(), _grid->gridSpacing());
  for (size_t i = 0; i < _grid->dataSize()[0]; ++i) {
    for (size_t j = 0; j < _grid->dataSize()[1]; ++j) {
      tmp = integrator.volumeIntegral<Vector1D>(
          [&](const Mesh<DIM>::point_t &pt) -> Vector1D {
            Vector1D tmp_quad;
            _problemDef->initialCondition(pt, tmp_quad);
            return tmp_quad;
          },
          {i, j});

      tmp /= _grid->AreaOfEle();

      buffer->newBuffer()->setVectorData({i, j}, tmp);
    }
  }
}

void Adv2DLimiterState::timeStep(double &dt) {
  dt = std::numeric_limits<double>::max();
  for (size_t i = 0; i < _grid->dataSize()[0]; ++i) {
    for (size_t j = 0; j < _grid->dataSize()[1]; ++j) {
      dt = std::min(dt, _grid->SizeOfEle() * CFL / _eqInfo->MaxCharacteristicSpeed(buffer->newBuffer()->value({i, j})));
    }
  }
}

void Adv2DLimiterState::update(double &dt) {
  // use dual buffer strategy to swap the pointer and avoid copy of all data
  _ssprkSolver->ssprk2(
      buffer->newBuffer(), dt,
      [&](const GridSystemDataPtr<DIM, DOS> &in, GridSystemDataPtr<DIM, DOS> out) -> void {
        in->calculateBasisFunction();
        _advectionSolver->advect(in, dt, _problemDef, AdvectionSolver<DIM, DOS>::inviscid_only, std::move(out));
      },
      buffer->oldBuffer());

  buffer->swapBuffer();
}

void Adv2DLimiterState::postProcess() {
  buffer->newBuffer()->calculateBasisFunction();

  // save result
  buffer->newBuffer()->save_data("AdvResult.dat");

  // output error
  Vector1D error;
  double dx;
  std::array<std::function<double(const Mesh2D::point_t &pt)>, 1> realFunc;
  realFunc[0] = [&](const Mesh2D::point_t &pt) -> double {
    return std::sin(2 * M_PI * (pt[0] - 1.5)) * std::sin(2 * M_PI * (pt[1] - 2 * 1.5));
  };

  ErrorApproximator<DIM, DOS> L1ErrorApprox(_grid);
  L1ErrorApprox.l1Error(buffer->newBuffer(), realFunc, _eqInfo, error, dx);

  std::string filename = "L1Error.dat";
  std::ofstream output(filename.c_str(), std::ios::app);
  output << dx << "\t";
  for (size_t i = 0; i < DOS; i++) {
    output << error[i] << std::endl;
  }
}
