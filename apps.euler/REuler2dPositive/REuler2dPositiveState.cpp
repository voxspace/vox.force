//
//  REuler2dPositiveState.cpp
//  apps
//
//  Created by 杨丰 on 2020/5/17.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "REuler2dPositiveState.h"
#include "../../src.euler/fluxes/flux_HLL.h"
#include "../../src.euler/grid_data_weno.h"
#include "../../src.euler/grid_system_data_rNSPositive.h"
#include "../../src.euler/weno_auxiliary.h"
#include <fstream>

using namespace vox;

const double GAMMA = 1.2;
const double Q = 50;
const double Ea = 50;
const double K = 2566.4;

void bd2d(const Vector<double, 5> &val, Vector<double, 5> &result, const std::array<double, 2> &out_normal,
          const Mesh2D::point_t &pt, int flag) {
  if (flag == 1 || flag == 4) {
    double Un = val[1] * out_normal[0] + val[2] * out_normal[1];
    result[0] = val[0];
    result[1] = val[1] - 2 * Un * out_normal[0];
    result[2] = val[2] - 2 * Un * out_normal[1];
    result[3] = val[3];
    result[4] = val[4];
  } else if (flag == 2 || flag == 3) { // supersonic inflow
    result[0] = 1.0;
    result[1] = 1.0 * 0.0;
    result[2] = 1.0 * 0.0;
    result[3] = 1.0e-9 / (GAMMA - 1.) + 0.5 * 1.0 * (0.0 * 0.0 + 0.0 * 0.0) + 1.0 * 1.0 * Q;
    result[4] = 1.0 * 1.0;
  } else {
    std::cout << "something wrong in boundaryValue..." << std::endl;
    getchar();
  }
}

void initial(const Mesh2D::point_t &pt, Vector<double, 5> &value) {
  if (pt.length() < 0.6) {
    value[0] = 1.0;
    value[1] = 1.0 * 0.0;
    value[2] = 1.0 * 0.0;
    value[3] = 80 / (GAMMA - 1.) + 0.5 * 1.0 * (0.0 * 0.0 + 0.0 * 0.0) + 1.0 * 0.0 * Q;
    value[4] = 1.0 * 0.0;
  } else {
    value[0] = 1.0;
    value[1] = 1.0 * 0.0;
    value[2] = 1.0 * 0.0;
    value[3] = 1.0e-9 / (GAMMA - 1.) + 0.5 * 1.0 * (0.0 * 0.0 + 0.0 * 0.0) + 1.0 * 1.0 * Q;
    value[4] = 1.0 * 1.0;
  }
}

//---------------------------------------------------------------------------

REulerPositive2DState::REulerPositive2DState() : CFL(0.2) {
  _grid = std::make_shared<Mesh<2>>(Vector<size_t, 2>({10, 10}), Vector<double, 2>({0.1, 0.1}));

  _eqInfo = std::make_shared<EquationReactingNS<2>>(GAMMA, Q);

  _advectionSolver = std::make_shared<AdvectionSolver<DIM, DOS>>(_grid, std::make_shared<FluxHLL<DIM, DOS>>());
  _ssprkSolver = std::make_shared<SSPRKSolver<DIM, DOS>>();
  _precorSolver = std::make_shared<Prediction_CorrectionSolver<DIM, DOS>>();
}

void REulerPositive2DState::createScene() {
  // load resources and information
  _problemDef = std::make_shared<ProblemDef<DIM, DOS>>(_eqInfo, _grid, bd2d, nullptr, initial, 1, "2DREulerPositive");

  WENOAuxiliaryPtr<DIM, 1> wenoAux = std::make_shared<WENOAuxiliary<DIM, 1>>(_grid);
  wenoAux->buildReconstructPatch();
  wenoAux->buildWENOPatch();
  wenoAux->updateWENOInfo();
  GridDataWENOBuilderPtr<2, 1> dataBuilder = std::make_shared<GridDataWENO<2, 1>::Builder>();
  dataBuilder->setWENOAuxiliary(wenoAux);

  // set data structure
  buffer->setNewBuffer(GridSysDataRNSPositive<DIM, 1>::builder().build(dataBuilder, _grid, _eqInfo));
  buffer->setOldBuffer(buffer->newBuffer()->deepCopy());
}

void REulerPositive2DState::initialize() {
  Vector<double, DOS> tmp;
  for (size_t i = 0; i < _grid->dataSize()[0]; ++i) {
    for (size_t j = 0; j < _grid->dataSize()[1]; ++j) {
      _problemDef->initialCondition(_grid->BaryCenter({i, j}), tmp);
      buffer->newBuffer()->setVectorData({i, j}, tmp);
    }
  }
}

void REulerPositive2DState::timeStep(double &dt) {
  dt = std::numeric_limits<double>::max();
  for (size_t i = 0; i < _grid->dataSize()[0]; ++i) {
    for (size_t j = 0; j < _grid->dataSize()[1]; ++j) {
      dt = std::min(dt, _grid->SizeOfEle() * CFL / _eqInfo->MaxCharacteristicSpeed(buffer->newBuffer()->value({i, j})));
    }
  }

  dt = std::min(dt, 1.0 / K);
}

static double output_time = 0.01;
const double output_duration = 0.01;
void REulerPositive2DState::update(double &dt) {
  // formal run
  reactionUpdate(0.5 * dt);

  advectionUpdate(dt);

  reactionUpdate(0.5 * dt);

  if (GlobalClock::getSingleton().getTime() > output_time) {
    output_time += output_duration;
  }
}

void REulerPositive2DState::advectionUpdate(double dt) {
  _ssprkSolver->ssprk2(
      buffer->newBuffer(), dt,
      [&](const GridSystemDataPtr<DIM, DOS> &in, GridSystemDataPtr<DIM, DOS> out) -> void {
        in->calculateBasisFunction();
        _advectionSolver->advect(in, dt, _problemDef, AdvectionSolver<DIM, DOS>::inviscid_only, std::move(out));
      },
      buffer->oldBuffer());

  buffer->swapBuffer();
}

void REulerPositive2DState::reactionUpdate(double dt) {
  _precorSolver->precor(
      buffer->newBuffer(), dt,
      [&](const Vector<double, DOS> &in, Vector<double, DOS> &out) -> void {
        double p = _eqInfo->pressure(in);
        double T = p / in[0];
        out[0] = 0.;
        out[1] = 0.;
        out[2] = 0.;
        out[3] = 0.;
        out[4] = -K * in[DOS - 1] * exp(-Ea / T);
      },
      [&](const Vector<double, DOS> &initial_cellAverage) -> double {
        return 10. / (K * exp(-initial_cellAverage[0] * Ea / _eqInfo->pressure(initial_cellAverage)));
      },
      [&](const Vector<double, DOS> &left, const Vector<double, DOS> &right) -> double {
        return std::abs(left[DOS - 1] - right[DOS - 1]);
      },
      buffer->oldBuffer());

  buffer->swapBuffer();
}

void REulerPositive2DState::postProcess() {
  // save result
  buffer->newBuffer()->save_data("AdvResult.dat");
}
