//
//  main.cpp
//  apps
//
//  Created by 杨丰 on 2020/5/17.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "../../src.euler/physics_solver.h"
#include "REuler2dPositiveState.h"

int main(int argc, char *argv[]) {
  if (argc < 1) {
    std::cerr << "Usage: " << argv[0] << "<length>" << std::endl;
    return 1;
  }

  REulerPositive2DState mySolverState;

  vox::PhysicsSolver<2, 5> solver(&mySolverState);
  mySolverState._notifyPhysicsSystem(&solver);

  solver.createScene();

  solver.initialize();

  solver.preProcess();

  solver.Run(0.2);

  solver.postProcess();

  return 0;
}
