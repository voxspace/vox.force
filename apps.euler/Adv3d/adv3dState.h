//
//  adv3dState.hpp
//  apps
//
//  Created by 杨丰 on 2020/5/17.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef adv3dState_hpp
#define adv3dState_hpp

#include <utility>

#include "../../src.euler/advection_solver.h"
#include "../../src.euler/dual_buffer.h"
#include "../../src.euler/equations/equation_scalar.h"
#include "../../src.euler/physics_solver.h"
#include "../../src.euler/solver_state.h"
#include "../../src.euler/ssprk_solver.h"

class Adv3DState : public vox::SolverState<3, 1> {
public:
  //! Default constructor
  explicit Adv3DState(int l);

  //! Sets the advection solver.
  void setAdvectionSolver(vox::AdvectionSolverPtr<DIM, DOS> newSolver) { _advectionSolver = std::move(newSolver); }

  //! Sets the max allowed CFL number.
  void setMaxCfl(double newCfl) { CFL = newCfl; }

public:
  //! Load resources and equation info
  void createScene() override;

  //! Set initial value
  void initialize() override;

  //! Time Step
  void timeStep(double &dt) override;

  //! PostProcess
  void postProcess() override;

  void update(double &dt) override;

private:
  vox::AdvectionSolverPtr<DIM, DOS> _advectionSolver;
  vox::SSPRKSolverPtr<DIM, DOS> _ssprkSolver;

  vox::MeshPtr<DIM> _grid;
  vox::EquationScalarPtr<DIM> _eqInfo;
  vox::ProblemDefPtr<DIM, DOS> _problemDef;

  double CFL;
};
#endif /* adv3dState_hpp */
