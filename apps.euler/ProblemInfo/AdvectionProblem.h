//
//  AdvectionProblem.h
//  DigitalFlex
//
//  Created by 杨丰 on 2019/6/25.
//  Copyright © 2019 杨丰. All rights reserved.
//

#ifndef AdvectionProblem_h
#define AdvectionProblem_h

#include "../../src.euler/mesh.h"

namespace Advection {
template <int dim> void init(const typename vox::Mesh<dim>::point_t &pt, vox::Vector1D &value) {
  value[0] = 1.0;
  for (int i = 0; i < dim; i++) {
    value[0] *= std::sin(2 * M_PI * pt[i]);
  }
}

template <int dim>
void bd(const vox::Vector1D &val, vox::Vector1D &result, const std::array<double, dim> &outNormal,
        const typename vox::Mesh<dim>::point_t &pt, vox::Direction d) {
  result = val;
}

template <int dim>
void VisBoundaryCondition(const vox::Vector<vox::Vector<double, dim>, 1> &val,
                          vox::Vector<vox::Vector<double, dim>, 1> &result, const std::array<double, dim> &outNormal,
                          const typename vox::Mesh<dim>::point_t &pt, vox::Direction d) {
  result = val;
}

double indicator(const vox::Vector1D &left, const vox::Vector1D &right) { return fabs(left[0] - right[0]); }
//---------------------------------------------------------------------------
} // namespace Advection

#endif /* AdvectionProblem_h */
