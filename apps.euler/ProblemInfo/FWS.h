//
//  FWS.h
//  DigitalFlex
//
//  Created by 杨丰 on 2019/6/25.
//  Copyright © 2019 杨丰. All rights reserved.
//

#ifndef FWS_h
#define FWS_h

#include "../../src.euler/mesh.h"

using namespace vox;
#define GAMMA 1.4

Vector<double, 4> U_i;
namespace FWS {
// t = 4
void Init(const vox::Mesh2D::point_t x, Vector<double, 4> &u) {
  static bool flag = true;
  if (flag) {
    U_i[0] = 1.4;
    U_i[1] = U_i[0] * 3.0;
    U_i[2] = U_i[0] * 0.0;
    U_i[3] = 1. / (GAMMA - 1) + 0.5 * (U_i[1] * U_i[1] + U_i[2] * U_i[2]) / U_i[0];
    flag = false;
  }
  u = U_i;
}

void BoundaryCondition(const Vector<double, 4> &val, Vector<double, 4> &result, const std::array<double, 2> &out_normal,
                       const vox::Mesh2D::point_t &pt, int flag) {
  switch (flag) {
  case 1: // Inflow
    result = U_i;
    break;
  case 2: // Outflow
    result = val;
    break;
  case 3: // Reflective in y
    result = val;
    result[2] = -result[2];
    break;
  case 4: // Reflective in x
    result = val;
    result[1] = -result[1];
    break;
  default:
    std::cerr << "Error boundary mark!" << std::endl;
    abort();
  }
}

double IteratorInfo(const Vector<double, 4> &left, const Vector<double, 4> &right) {
  double p1 = 0.4 * (left[3] - .5 * (left[1] * left[1] + left[2] * left[2]) / left[0]);
  double p2 = 0.4 * (right[3] - .5 * (right[1] * right[1] + right[2] * right[2]) / right[0]);
  return 0.1 * fabs(p1 - p2);
}
} // namespace FWS

#endif /* FWS_h */
