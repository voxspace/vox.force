//
//  ShockTube.h
//  DigitalFlex
//
//  Created by 杨丰 on 2019/6/25.
//  Copyright © 2019 杨丰. All rights reserved.
//

#ifndef ShockTube_h
#define ShockTube_h

#include "../../src.euler/mesh.h"

using namespace vox;
namespace ShockTube {
void Init(const vox::Mesh2D::point_t x, Vector<double, 4> &u) {
  if (x[0] < .5) {
    u[0] = 1.0;
    u[1] = 0;
    u[2] = 0;
    u[3] = 2.5;
    /*
     u[0] = 0.445;
     u[1] = u[0]*0.698;
     u[2] = 0;
     u[3] = 3.528/0.4+0.5*0.445*0.698*0.698;
     */
  } else {
    u[0] = 0.125;
    u[1] = 0;
    u[2] = 0;
    u[3] = 0.25;
    /*
     u[0] = 0.5;
     u[1] = 0;
     u[2] = 0;
     u[3] = 0.571/0.4;
     */
  }
}

void BoundaryCondition(const Vector<double, 4> &val, Vector<double, 4> &result, const std::array<double, 2> &out_normal,
                       const vox::Mesh2D::point_t &pt, int flag) {
  if (flag == 2)
    result = val;
  else {
    std::cerr << "Error Boundary Mark" << std::endl;
    abort();
  }
}

double IteratorInfo(Vector<double, 4> &left, const Vector<double, 4> &right) {
  double p1 = 0.4 * (left[3] - .5 * (left[1] * left[1] + left[2] * left[2]) / left[0]);
  double p2 = 0.4 * (right[3] - .5 * (right[1] * right[1] + right[2] * right[2]) / right[0]);
  return 100 * fabs(p1 - p2);
}
} // namespace ShockTube

#endif /* ShockTube_h */
