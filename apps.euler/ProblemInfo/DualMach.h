//
//  DualMach.h
//  DigitalFlex
//
//  Created by 杨丰 on 2019/6/25.
//  Copyright © 2019 杨丰. All rights reserved.
//

#ifndef DualMach_h
#define DualMach_h

#include "../../src.euler/global_clock.h"
#include "../../src.euler/mesh.h"

using namespace vox;
Vector<double, 4> U_l;
Vector<double, 4> U_r;
namespace DualMach {
// t = 0.2
void Init(const vox::Mesh2D::point_t x, Vector<double, 4> &u) {
  // mach MS = 10; toro p101(3.53)
  static bool flag = true;
  if (flag) {
    U_l[0] = 8.;
    U_l[1] = 57.1597;
    U_l[2] = -33.0012;
    U_l[3] = 563.544;
    U_r[0] = 1.4;
    U_r[1] = 0.;
    U_r[2] = 0.;
    U_r[3] = 2.5;
    flag = false;
  }
  if (x[1] - std::sqrt(3.) * (x[0] - 1. / 6.) > 0)
    u = U_l;
  else
    u = U_r;
}

void BoundaryCondition(const Vector<double, 4> &val, Vector<double, 4> &result, const std::array<double, 2> &out_normal,
                       const vox::Mesh2D::point_t &pt, int flag) {
  switch (flag) {
  case 1: /// U_l
    result = U_l;
    break;
  case 2: /// U_r
    result = U_r;
    break;
  case 3: /// reflective boundary condition
    result = val;
    result[2] = -result[2];
    break;
  case 4: /// U_l or U_r
    if (pt[0] > 1. / 6. + (1. + 20. * (vox::GlobalClock::getSingleton().getTime())) / std::sqrt(3.))
      result = U_r;
    else
      result = U_l;
    break;
  default:
    std::cerr << "Error boundary mark!" << std::endl;
    abort();
  }
}

void VisBoundaryCondition(const Vector<Vector<double, 2>, 4> &val, Vector<Vector<double, 2>, 4> &result,
                          const std::array<double, 2> &out_normal, const vox::Mesh2D::point_t &pt, int flag) {
  result = val;
  if (flag == 3) {
    /// reflective boundary condition
    // rho
    double Un = val[0][0] * out_normal[0] + val[0][1] * out_normal[1];
    result[0][0] -= 2 * Un * out_normal[0];
    result[0][1] -= 2 * Un * out_normal[1];

    // moment
    double tanVel = val[1][0] * out_normal[1] - val[1][1] * out_normal[0];
    result[1][0] -= 2 * tanVel * out_normal[1];
    result[1][1] -= -2 * tanVel * out_normal[0];
    tanVel = val[2][0] * out_normal[1] - val[2][1] * out_normal[0];
    result[2][0] -= 2 * tanVel * out_normal[1];
    result[2][1] -= -2 * tanVel * out_normal[0];

    // E
    Un = val[3][0] * out_normal[0] + val[3][1] * out_normal[1];
    result[3][0] -= 2 * Un * out_normal[0];
    result[3][1] -= 2 * Un * out_normal[1];
  }
}

double IteratorInfo(const Vector<double, 4> &left, const Vector<double, 4> &right) {
  double p1 = 0.4 * (left[3] - .5 * (left[1] * left[1] + left[2] * left[2]) / left[0]);
  double p2 = 0.4 * (right[3] - .5 * (right[1] * right[1] + right[2] * right[2]) / right[0]);
  return 0.01 * fabs(p1 - p2) + 0.01 * fabs(left[0] - right[0]);
}
} // namespace DualMach

#endif /* DualMach_h */
