//
//  Sedov.h
//  DigitalFlex
//
//  Created by 杨丰 on 2019/6/25.
//  Copyright © 2019 杨丰. All rights reserved.
//

#ifndef Sedov_h
#define Sedov_h

#include "../../src.euler/mesh.h"

using namespace vox;
namespace Sedov {
// t = 1
void Init(const vox::Mesh2D::point_t x, Vector<double, 4> &u) {
  // Spherical geometries
  const double E0 = 0.244816; // Total energy released at the origin
  const double h = 1.0 / 20;  // Size of the cell near the origin
  u[0] = 1;
  u[1] = 0;
  u[2] = 0;
  u[3] = (x.length() < h) ? (E0 / (M_PI * h * h / 4.0)) : 1e-4;
}

void BoundaryCondition(const Vector<double, 4> &val, Vector<double, 4> &result, const std::array<double, 2> &out_normal,
                       const vox::Mesh2D::point_t &pt, int flag) {
  switch (flag) {
  case 1: // Bottom edge: Reflective
    result = val;
    result[2] = -result[2];
    break;
  case 2: // Top edge
    result = val;
    break;
  case 3: // Right edge
    result = val;
    break;
  case 4: // Left edge: Reflective
    result = val;
    result[1] = -result[1];
    break;
  default:
    std::cerr << "Error boundary mark!" << std::endl;
    abort();
  }
}

void VisBoundaryCondition(const Vector<Vector<double, 2>, 4> &val, Vector<Vector<double, 2>, 4> &result,
                          const std::array<double, 2> &out_normal, const vox::Mesh2D::point_t &pt, int flag) {
  result = val;
  if (flag == 1 || flag == 4) {
    /// reflective boundary condition
    // rho
    double Un = val[0][0] * out_normal[0] + val[0][1] * out_normal[1];
    result[0][0] -= 2 * Un * out_normal[0];
    result[0][1] -= 2 * Un * out_normal[1];

    // moment
    double tanVel = val[1][0] * out_normal[1] - val[1][1] * out_normal[0];
    result[1][0] -= 2 * tanVel * out_normal[1];
    result[1][1] -= -2 * tanVel * out_normal[0];
    tanVel = val[2][0] * out_normal[1] - val[2][1] * out_normal[0];
    result[2][0] -= 2 * tanVel * out_normal[1];
    result[2][1] -= -2 * tanVel * out_normal[0];

    // E
    Un = val[3][0] * out_normal[0] + val[3][1] * out_normal[1];
    result[3][0] -= 2 * Un * out_normal[0];
    result[3][1] -= 2 * Un * out_normal[1];
  }
}

//    static double Source(const _bU& u, const Point& pt, _bU& src)
//    {
//#ifdef R_CONSERVATIVE
//        src = 0;
//        src[1] = (Euler::GAMMA - 1.)*
//        (u[3] - 0.5*(u[1]*u[1] + u[2]*u[2])/u[0]);
//#else
//        src[0] = u[1];
//        src[1] = u[1]*u[1]/u[0];
//        src[2] = u[2]*u[1]/u[0];
//        double p = (Euler::GAMMA - 1.)*
//        (u[3] - 0.5*(u[1]*u[1] + u[2]*u[2])/u[0]);
//        src[3] = u[1]/u[0]*(u[3] + p);
//        src /= (-pt[0]);
//#endif // R_CONSERVATIVE
//    }

double IteratorInfo(Vector<double, 4> &left, const Vector<double, 4> &right) {
  double p1 = (1.4 - 1.) * (left[3] - .5 * (left[1] * left[1] + left[2] * left[2]) / left[0]);
  double p2 = (1.4 - 1.) * (right[3] - .5 * (right[1] * right[1] + right[2] * right[2]) / right[0]);
  return fabs(p1 - p2);
}
} // namespace Sedov

#endif /* Sedov_h */
