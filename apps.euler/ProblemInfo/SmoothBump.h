//
//  SmoothBump.h
//  DigitalFlex
//
//  Created by 杨丰 on 2019/6/25.
//  Copyright © 2019 杨丰. All rights reserved.
//

#ifndef SmoothBump_h
#define SmoothBump_h

#include "../../src.euler/mesh.h"

using namespace vox;
Vector<double, 4> U_infty;
namespace SmoothBump {
void Init(const vox::Mesh2D::point_t x, Vector<double, 4> &u) {
  static bool initialized = false;
  if (!initialized) {
    U_infty[0] = 1.0;
    U_infty[1] = 0.5 * sqrt(1.4);
    U_infty[2] = 0.0;
    U_infty[3] = 1.0 / (1.4 - 1) + 0.5 * U_infty[1] * U_infty[1] / U_infty[0];
    initialized = true;
  }
  u = U_infty;
}

void BoundaryCondition(const Vector<double, 4> &val, Vector<double, 4> &result, const std::array<double, 2> &out_normal,
                       const vox::Mesh2D::point_t &pt, int flag) {
  vox::Mesh2D::point_t tng;
  switch (flag) {
  case 1:          // Left or right boundary
    if (pt[0] < 0) // left boundary
    {
      result = U_infty; // Inflow
      result[3] = val[3] - 0.5 * (val[1] * val[1] + val[2] * val[2]) / val[0] +
                  0.5 * (result[1] * result[1] + result[2] * result[2]) / result[0];
    } else // right boundary
    {
      result = val; // Outflow
      result[3] = 1.0 / (1.4 - 1.) + 0.5 * (result[1] * result[1] + result[2] * result[2]) / result[0];
    }
    break;
  case 2: // Bottom boundary
    tng[0] = 1.;
    tng[1] = -50. * pt[0] * pt[1];
    tng /= (tng.length());
    result[0] = val[0];
    result[1] = 2. * (val[1] * tng[0] + val[2] * tng[1]) * tng[0] - val[1];
    result[2] = 2. * (val[1] * tng[0] + val[2] * tng[1]) * tng[1] - val[2];
    result[3] = val[3];
    break;
  case 3: // Upper boundary
    result = val;
    result[2] = -result[2];
    break;
  default:
    std::cerr << "Error boundary mark!" << std::endl;
    abort();
  }
}

double IteratorInfo(Vector<double, 4> &left, const Vector<double, 4> &right) {
  double p1 = 0.4 * (left[3] - .5 * (left[1] * left[1] + left[2] * left[2]) / left[0]);
  double p2 = 0.4 * (right[3] - .5 * (right[1] * right[1] + right[2] * right[2]) / right[0]);
  return fabs(p1 - p2);
}
} // namespace SmoothBump

#endif /* SmoothBump_h */
