//
//  airfoil.h
//  DigitalFlex
//
//  Created by 杨丰 on 2020/1/5.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef airfoil_h
#define airfoil_h

#include "../../src.euler/mesh.h"

using namespace vox;
namespace AirFoil {
const double GAMMA = 1.4;
const double attack_angle = 0.0 * (atan(1.0) / 45) * 1.25;
const double mach_number = 0.38;

const double rho_inf = 1.0;
const double u_inf = cos(attack_angle);
const double v_inf = sin(attack_angle);
const double w_inf = 0.0;

const double momentum = 0.5 * rho_inf * (u_inf * u_inf + v_inf * v_inf + w_inf * w_inf);
const double p_inf = 2 * momentum / (GAMMA * mach_number * mach_number);
const double e_inf = p_inf / (GAMMA - 1) + momentum;

inline double pressure(const Vector<double, 5> &u) {
  double m = 0.0;
  for (u_int i = 1; i <= 3; i++) {
    m += u[i] * u[i];
  }
  double p = (GAMMA - 1) * (u[4] - 0.5 * m / u[0]);
  return p;
}

inline double soundSpeed(const Vector<double, 5> &u) {
  double p = pressure(u);
  p = GAMMA * p / u[0];
  if (p < 0.0)
    return 0.0;
  return sqrt(p);
}
//---------------------------------------------------------------------------------
extern const vox::Mesh3D::point_t body_center(0.0, 0.0, 0.0);
extern const double chord_length = 1.0;

inline void vortexCorrection(const double &C_l, const vox::Mesh3D::point_t &x, double &u_inf_ast, double &v_inf_ast,
                             double &p_inf_ast, double &r_inf_ast) {
  double PI = 4.0 * atan(1.0);
  ;
  double dx = x[0] - body_center[0];
  double dy = x[1] - body_center[1];
  double d = sqrt(dx * dx + dy * dy);
  double sin_theta = dy / d, cos_theta = dx / d;
  double V2_inf = u_inf * u_inf + v_inf * v_inf;
  double G = 0.5 * sqrt(V2_inf) * chord_length * C_l;
  double M2 = mach_number * mach_number;
  double phi = G * sqrt(1.0 - M2) / (2 * PI * d);
  double sint = sin_theta * cos(attack_angle) - cos_theta * sin(attack_angle);
  phi /= (1.0 - M2 * sint * sint);
  u_inf_ast = u_inf + phi * sin_theta;
  v_inf_ast = v_inf - phi * cos_theta;

  double delta = (GAMMA - 1.0) / GAMMA;
  double V2_inf_ast = u_inf_ast * u_inf_ast + v_inf_ast * v_inf_ast;
  p_inf_ast =
      pow(pow(p_inf, delta) + delta * rho_inf * (V2_inf - V2_inf_ast) / (2 * pow(p_inf, 1.0 / GAMMA)), 1.0 / delta);
  r_inf_ast = rho_inf * pow(p_inf_ast / p_inf, 1.0 / GAMMA);
}

//---------------------------------------------------------------------------------
void Init(const vox::Mesh3D::point_t x, Vector<double, 5> &u) {
  u[0] = rho_inf;
  u[1] = rho_inf * u_inf;
  u[2] = rho_inf * v_inf;
  u[3] = rho_inf * w_inf;
  u[4] = e_inf;
}

const double ZERO = 1.e-12;
void BoundaryCondition(const Vector<double, 5> &val, Vector<double, 5> &result, const std::array<double, 3> &out_normal,
                       const vox::Mesh3D::point_t &pt, int flag) {
  if (flag == 4) { /// so wall boundary condition
    double Un = val[1] * out_normal[0] + val[2] * out_normal[1] + val[3] * out_normal[2];

    result = val;
    result[1] = result[1] - 2 * Un * out_normal[0];
    result[2] = result[2] - 2 * Un * out_normal[1];
    result[3] = result[3] - 2 * Un * out_normal[2];

  } else {
    int i, j;
    double t[2][3] = {{0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}};
    double t0_l2_norm = sqrt(out_normal[0] * out_normal[0] + out_normal[1] * out_normal[1]);
    if (t0_l2_norm < ZERO) {
      if (out_normal[2] > 0) {
        t[0][0] = 1.0;
        t[1][1] = 1.0;
      } else {
        t[0][0] = 1.0;
        t[1][1] = -1.0;
      }
    } else {
      t[0][0] = 1.0 / t0_l2_norm * out_normal[1];
      t[0][1] = -1.0 / t0_l2_norm * out_normal[0];
      t[1][0] = 1.0 / t0_l2_norm * out_normal[0] * out_normal[2];
      t[1][1] = 1.0 / t0_l2_norm * out_normal[1] * out_normal[2];
      t[1][2] = 1.0 / t0_l2_norm * (-out_normal[0] * out_normal[0] - out_normal[1] * out_normal[1]);
    }
    Matrix3x3D coord_matrix = Matrix3x3D::makeZero();
    for (i = 0; i < 2; i++) {
      for (j = 0; j < 3; j++) {
        coord_matrix(i, j) = t[i][j];
      }
    }
    for (i = 0; i < 3; i++) {
      coord_matrix(2, i) = out_normal[i];
    }

    double rho_in = val[0];
    double u_in[3] = {val[1] / val[0], val[2] / val[0], val[3] / val[0]};
    double un_in = u_in[0] * out_normal[0] + u_in[1] * out_normal[1] + u_in[2] * out_normal[2];
    double ut0_in = u_in[0] * t[0][0] + u_in[1] * t[0][1] + u_in[2] * t[0][2];
    double ut1_in = u_in[0] * t[1][0] + u_in[1] * t[1][1] + u_in[2] * t[1][2];

    double un_inf = u_inf * out_normal[0] + v_inf * out_normal[1] + w_inf * out_normal[2];
    double ut0_inf = u_inf * t[0][0] + v_inf * t[0][1] + w_inf * t[0][2];
    double ut1_inf = u_inf * t[1][0] + v_inf * t[1][1] + w_inf * t[1][2];

    double c_in = soundSpeed(val);
    double c_inf = sqrt(GAMMA * p_inf / rho_inf);

    double Rm, Rp, S, ut0, ut1, un, c;
    /// farfield boundary
    if (un_in >= 0) {
      S = pressure(val) / pow(rho_in, GAMMA);
      ut0 = ut0_in;
      ut1 = ut1_in;
      Rp = fabs(un_in) + 2 * c_in / (GAMMA - 1);
      Rm = fabs(un_inf) - 2 * c_inf / (GAMMA - 1);
      un = 0.5 * (Rp + Rm);
      c = 0.25 * (GAMMA - 1) * (Rp - Rm);
    } else {
      S = p_inf / pow(rho_inf, GAMMA);
      ut0 = ut0_inf;
      ut1 = ut1_inf;
      Rp = fabs(un_inf) + 2 * c_inf / (GAMMA - 1);
      Rm = fabs(un_in) - 2 * c_in / (GAMMA - 1);

      un = -0.5 * (Rp + Rm);
      c = 0.25 * (GAMMA - 1) * (Rp - Rm);
    }
    Vector3D b = Vector3D::makeZero();
    Vector3D vel = Vector3D::makeZero();
    double r = pow((c * c) / (GAMMA * S), 1 / (GAMMA - 1));
    double p = r * c * c / GAMMA;
    b[0] = ut0;
    b[1] = ut1;
    b[2] = un;
    vel = coord_matrix.transposed() * b;
    double u = vel[0];
    double v = vel[1];
    double w = vel[2];

    result[0] = r;
    result[1] = r * u;
    result[2] = r * v;
    result[3] = r * w;
    result[4] = p / (GAMMA - 1) + 0.5 * r * (u * u + v * v + w * w);
  }
}

void VisBoundaryCondition(const Vector<Vector<double, 3>, 5> &val, Vector<Vector<double, 3>, 5> &result,
                          const std::array<double, 3> &out_normal, const vox::Mesh3D::point_t &pt, int flag) {
  result = val;
  if (flag == 3) {
    /// reflective boundary condition
    // rho
    double Un = val[0][0] * out_normal[0] + val[0][1] * out_normal[1] + val[0][2] * out_normal[2];
    result[0][0] -= 2 * Un * out_normal[0];
    result[0][1] -= 2 * Un * out_normal[1];
    result[0][2] -= 2 * Un * out_normal[2];

    // moment
    //(-y, x, 0)
    double tanVel = -val[1][0] * out_normal[1] + val[1][1] * out_normal[0];
    result[1][0] -= -2 * tanVel * out_normal[1];
    result[1][1] -= 2 * tanVel * out_normal[0];
    //(-z, 0, x)
    tanVel = -val[1][0] * out_normal[2] + val[1][2] * out_normal[0];
    result[1][0] -= -2 * tanVel * out_normal[2];
    result[1][2] -= 2 * tanVel * out_normal[0];

    tanVel = -val[2][0] * out_normal[1] + val[2][1] * out_normal[0];
    result[2][0] -= -2 * tanVel * out_normal[1];
    result[2][1] -= 2 * tanVel * out_normal[0];
    tanVel = -val[2][0] * out_normal[2] + val[2][2] * out_normal[0];
    result[2][0] -= -2 * tanVel * out_normal[2];
    result[2][1] -= 2 * tanVel * out_normal[0];

    tanVel = -val[2][0] * out_normal[1] + val[2][1] * out_normal[0];
    result[3][0] -= -2 * tanVel * out_normal[1];
    result[3][1] -= 2 * tanVel * out_normal[0];
    tanVel = -val[2][0] * out_normal[2] + val[2][2] * out_normal[0];
    result[3][0] -= -2 * tanVel * out_normal[2];
    result[3][1] -= 2 * tanVel * out_normal[0];
    // E
    Un = val[4][0] * out_normal[0] + val[4][1] * out_normal[1] + val[4][2] * out_normal[2];
    result[4][0] -= 2 * Un * out_normal[0];
    result[4][1] -= 2 * Un * out_normal[1];
    result[4][2] -= 2 * Un * out_normal[2];
  }
}

} // namespace AirFoil

#endif /* airfoil_h */
