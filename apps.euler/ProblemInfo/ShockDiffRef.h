//
//  ShockDiffRef.h
//  DigitalFlex
//
//  Created by 杨丰 on 2019/12/20.
//  Copyright © 2019 杨丰. All rights reserved.
//

#ifndef ShockDiffRef_h
#define ShockDiffRef_h

#include "../../src.flame/grid.h"
#include "../../src.flame/pch.h"

namespace ShockDiffraction_Reflection {
// mach MS = 5.09; toro p101(3.53)
const double GAMMA = 1.4;
const double rhor = 1.4;
const double Pr = 1.0;
const double cr = 1.0;
const double ur = 0.0;
const double MR = 0.0 / cr;

const double MS = 10;
const double S = MS * cr;

const double rhol = (GAMMA + 1) * std::pow(MR - MS, 2) / ((GAMMA - 1) * std::pow(MR - MS, 2) + 2) * rhor;
const double Pl = (2 * GAMMA * std::pow(MR - MS, 2) - (GAMMA - 1)) / (GAMMA + 1) * Pr;
const double vel = (1 - rhor / rhol) * S + ur * rhor / rhol;

// t = 0.23
void BoundaryCondition(const Vector<double, 4> &val, Vector<double, 4> &result, const std::array<double, 2> &outnormal,
                       const vox::Grid2D::point_t &pt, int flag) {
  if (flag == 2 || flag == 3) {
    if (pt[0] < 0.2 + vox::GlobalClock::getSingleton().getTime() * S) {
      result[0] = rhol;
      result[1] = vel * rhol;
      result[2] = 0.0;
      result[3] = Pl / 0.4 + 0.5 * (result[1] * result[1] + result[2] * result[2]) / result[0];
    } else {
      result[0] = 1.4;
      result[1] = 0.0;
      result[2] = 0.0;
      result[3] = 2.5;
    }
  } else if (flag == 1) {
    result[0] = val[0];
    result[1] = val[1];
    result[2] = val[2];
    result[3] = val[3];
  } else if (flag == 4) {
    double Un = val[1] * outnormal[0] + val[2] * outnormal[1];
    result[0] = val[0];
    result[1] = val[1] - 2 * Un * outnormal[0];
    result[2] = val[2] - 2 * Un * outnormal[1];
    result[3] = val[3];
  } else {
    std::cout << "something wrong in boundaryValue..." << std::endl;
    getchar();
  }
}

void VisBoundaryCondition(const Vector<Vector<double, 2>, 4> &val, Vector<Vector<double, 2>, 4> &result,
                          const std::array<double, 2> &outnormal, const vox::Grid2D::point_t &pt, int flag) {
  UNUSED_VARIABLE(pt);

  result = val;
  if (flag == 4) {
    /// reflective boundary condition
    // rho
    double Un = val[0][0] * outnormal[0] + val[0][1] * outnormal[1];
    result[0][0] -= 2 * Un * outnormal[0];
    result[0][1] -= 2 * Un * outnormal[1];

    // moment
    double tanVel = val[1][0] * outnormal[1] - val[1][1] * outnormal[0];
    result[1][0] -= 2 * tanVel * outnormal[1];
    result[1][1] -= -2 * tanVel * outnormal[0];
    tanVel = val[2][0] * outnormal[1] - val[2][1] * outnormal[0];
    result[2][0] -= 2 * tanVel * outnormal[1];
    result[2][1] -= -2 * tanVel * outnormal[0];

    // E
    Un = val[3][0] * outnormal[0] + val[3][1] * outnormal[1];
    result[3][0] -= 2 * Un * outnormal[0];
    result[3][1] -= 2 * Un * outnormal[1];
  }
}

void initial(const vox::Grid2D::point_t &pt, Vector<double, 4> &value) {
  if (pt[0] < 0.2) {
    value[0] = rhol;
    value[1] = vel * rhol;
    value[2] = 0.0;
    value[3] = Pl / 0.4 + 0.5 * (value[1] * value[1] + value[2] * value[2]) / value[0];
  } else {
    value[0] = 1.4;
    value[1] = 0.0;
    value[2] = 0.0;
    value[3] = 2.5;
  }
}
} // namespace ShockDiffraction_Reflection

#endif /* ShockDiffRef_h */
