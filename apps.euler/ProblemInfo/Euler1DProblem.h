//
//  Euler1DProblem.h
//  DigitalFlex
//
//  Created by 杨丰 on 2019/6/16.
//  Copyright © 2019 杨丰. All rights reserved.
//

#ifndef Euler1DProblem_h
#define Euler1DProblem_h

#include "../../src.euler/mesh.h"

namespace Euler {
// output t = 0.2
void ModifySod(const vox::Mesh1D::point_t &pt, vox::Vector3D &u) {
  if (pt[0] > 0.3) {
    u[0] = 0.125;
    u[1] = 0;
    u[2] = 0.1 / 0.4;
  } else {
    u[0] = 1.;
    u[1] = 0.75; // or 0 in Sod
    u[2] = 1. / 0.4 + 0.5 * u[1] * u[1] / u[0];
  }
}

// t = 1.0 [-5, 5]
void Lax(const vox::Mesh1D::point_t &pt, vox::Vector3D &u) {
  if (pt[0] > 0.0) {
    u[0] = 0.5;
    u[1] = 0;
    u[2] = 0.571 / 0.4;
  } else {
    u[0] = 0.445;
    u[1] = 0.698 * 0.445;
    u[2] = 3.528 / 0.4 + 0.5 * u[1] * u[1] / u[0];
  }
}

// t = 0.6
void DoubleRarefaction(const vox::Mesh1D::point_t &pt, vox::Vector3D &u) {
  if (pt[0] > 0.5) {
    u[0] = 7.0;
    u[1] = 7.0;
    u[2] = 0.2 / 0.4 + 0.5 * u[1] * u[1] / u[0];
  } else {
    u[0] = 7.0;
    u[1] = -7.0;
    u[2] = 0.2 / 0.4 + 0.5 * u[1] * u[1] / u[0];
  }
}

// output t = 0.15
void Test123(const vox::Mesh1D::point_t &pt, vox::Vector3D &u) {
  if (pt[0] > 0.5) {
    u[0] = 1.0;
    u[1] = 2.0;
    u[2] = 0.4 / 0.4 + 0.5 * u[1] * u[1] / u[0];
  } else {
    u[0] = 1.0;
    u[1] = -2.0;
    u[2] = 0.4 / 0.4 + 0.5 * u[1] * u[1] / u[0];
  }
}

// output t = 0.012
void RobustTest1(const vox::Mesh1D::point_t &pt, vox::Vector3D &u) {
  if (pt[0] > 0.5) {
    u[0] = 1.0;
    u[1] = 0.0;
    u[2] = 0.01 / 0.4;
  } else {
    u[0] = 1.0;
    u[1] = 0.0;
    u[2] = 1000. / 0.4;
  }
}

// output t = 0.035
void RobustTest2(const vox::Mesh1D::point_t &pt, vox::Vector3D &u) {
  if (pt[0] > 0.4) {
    u[0] = 5.999242;
    u[1] = -6.19633 * u[0];
    u[2] = 46.0950 / 0.4 + 0.5 * u[1] * u[1] / u[0];
  } else {
    u[0] = 5.99924;
    u[1] = 19.5975 * u[0];
    u[2] = 460.894 / 0.4 + 0.5 * u[1] * u[1] / u[0];
  }
}

// output t = 0.012
void RobustTest3(const vox::Mesh1D::point_t &pt, vox::Vector3D &u) {
  if (pt[0] > 0.8) {
    u[0] = 1.0;
    u[1] = -19.59745 * u[0];
    u[2] = 0.01 / 0.4 + 0.5 * u[1] * u[1] / u[0];
  } else {
    u[0] = 1.0;
    u[1] = -19.59745 * u[0];
    u[2] = 1000. / 0.4 + 0.5 * u[1] * u[1] / u[0];
  }
}
//-------------------------------------------------------------------------
void bd1d(const vox::Vector3D &val, vox::Vector3D &result, const std::array<double, 1> &outNormal,
          const vox::Mesh1D::point_t &pt, int flag) {
  result = val;
}

void visbd1d(const vox::Vector<vox::Vector<double, 1>, 3> &val, vox::Vector<vox::Vector<double, 1>, 3> &result,
             const std::array<double, 1> &outNormal, const vox::Mesh1D::point_t &pt, int flag) {
  result = -val;
}

} // namespace Euler

#endif /* Euler1DProblem_h */
