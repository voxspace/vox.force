//
//  Noh.h
//  DigitalFlex
//
//  Created by 杨丰 on 2019/6/25.
//  Copyright © 2019 杨丰. All rights reserved.
//

#ifndef Noh_h
#define Noh_h

#include "../../src.euler/mesh.h"

using namespace vox;
namespace Noh {
void Init(const vox::Mesh2D::point_t x, Vector<double, 4> &u) {
  const double r0 = 1.0;
  const double V = 1.0;
  double lx = x.length();
  double vr = -V * x[0] / lx;
  double vz = -V * x[1] / lx;

  u[0] = r0;
  u[1] = r0 * vr;
  u[2] = r0 * vz;
  u[3] = 0.5 * r0 * V * V;
}

void BoundaryCondition(const Vector<double, 4> &val, Vector<double, 4> &result, const std::array<double, 2> &out_normal,
                       const vox::Mesh2D::point_t &pt, int flag) {
  switch (flag) {
  case 1: // Bottom edge: Reflective
    result = val;
    result[2] = -result[2];
    break;
  case 2: // Right edge
    result = val;
    break;
  case 3: // Top edge
    result = val;
    break;
  case 4: // Left edge: Reflective
    result = val;
    result[1] = -result[1];
    break;
  default:
    std::cerr << "Error boundary mark!" << std::endl;
    abort();
  }
}

//    double Source(const _bU& u, const Point& pt, _bU& src)
//    {
//#ifdef R_CONSERVATIVE
//        src = 0;
//        src[1] = (Euler::GAMMA - 1.)*
//        (u[3] - 0.5*(u[1]*u[1] + u[2]*u[2])/u[0]);
//#else
//        src[0] = u[1];
//        src[1] = u[1]*u[1]/u[0];
//        src[2] = u[2]*u[1]/u[0];
//        double p = (Euler::GAMMA - 1.)*
//        (u[3] - 0.5*(u[1]*u[1] + u[2]*u[2])/u[0]);
//        src[3] = u[1]/u[0]*(u[3] + p);
//        src /= (-pt[0]);
//#endif
//    }

double IteratorInfo(Vector<double, 4> &left, const Vector<double, 4> &right) {
  double p1 = (1.4 - 1.) * (left[3] - .5 * (left[1] * left[1] + left[2] * left[2]) / left[0]);
  double p2 = (1.4 - 1.) * (right[3] - .5 * (right[1] * right[1] + right[2] * right[2]) / right[0]);
  return fabs(p1 - p2);
}
} // namespace Noh

#endif /* Noh_h */
