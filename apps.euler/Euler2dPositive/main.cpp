//
//  main.cpp
//  apps
//
//  Created by 杨丰 on 2020/5/17.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "../../src.euler/physics_solver.h"
#include "Euler2dPositiveState.h"

int main(int argc, char *argv[]) {
  if (argc < 1) {
    std::cerr << "Usage: " << argv[0] << "<length>" << std::endl;
    return 1;
  }

  Euler2DPositiveState mySolverState;

  vox::PhysicsSolver<2, 4> solver(&mySolverState);
  mySolverState._notifyPhysicsSystem(&solver);

  solver.createScene();

  solver.initialize();

  solver.Run(0.2);

  solver.postProcess();

  return 0;
}
