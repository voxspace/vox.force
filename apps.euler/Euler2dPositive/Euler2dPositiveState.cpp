//
//  Euler2dPositiveState.cpp
//  apps
//
//  Created by 杨丰 on 2020/5/17.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "Euler2dPositiveState.h"
#include "../../src.euler/error_approximator.h"
#include "../../src.euler/fluxes/flux_LF.h"
#include "../../src.euler/grid_data_weno.h"
#include "../../src.euler/grid_system_data_NSPositive.h"
#include "../../src.euler/weno_auxiliary.h"
#include <fstream>

#include "../ProblemInfo/DualMach.h"
#include "../ProblemInfo/FWS.h"
#include "../ProblemInfo/Noh.h"
#include "../ProblemInfo/Sedov.h"
#include "../ProblemInfo/ShockTube.h"
#include "../ProblemInfo/SmoothBump.h"

using namespace vox;
//TODO: Fix for BoundaryMark
Euler2DPositiveState::Euler2DPositiveState() : CFL(0.1) {
  _grid = std::make_shared<Mesh<2>>(Vector<size_t, 2>({10, 10}), Vector<double, 2>({0.1, 0.1}));
  _eqInfo = std::make_shared<EquationNS<2>>(1.4);

  _advectionSolver = std::make_shared<AdvectionSolver<DIM, DOS>>(_grid, std::make_shared<FluxLF<DIM, DOS>>());
  _ssprkSolver = std::make_shared<SSPRKSolver<DIM, DOS>>();
}

void Euler2DPositiveState::createScene() {
  // load resources and information
  _problemDef = std::make_shared<ProblemDef<DIM, DOS>>(_eqInfo, _grid, DualMach::BoundaryCondition, nullptr,
                                                       DualMach::Init, 1, "2DEuler");

  WENOAuxiliaryPtr<DIM, 1> wenoAux = std::make_shared<WENOAuxiliary<DIM, 1>>(_grid);
  wenoAux->buildReconstructPatch();
  wenoAux->buildWENOPatch();
  wenoAux->updateWENOInfo();
  GridDataWENOBuilderPtr<DIM, 1> dataBuilder = std::make_shared<GridDataWENO<2, 1>::Builder>();
  dataBuilder->setWENOAuxiliary(wenoAux);

  // set data structure
  buffer->setNewBuffer(GridSysDataNSPositive<DIM, 1>::builder().build(dataBuilder, _grid, _eqInfo));
  buffer->setOldBuffer(buffer->newBuffer()->deepCopy());
}

void Euler2DPositiveState::initialize() {
  Vector<double, DOS> tmp;
  VolumeIntegrator<DIM, 2> integrator(_grid->origin(), _grid->gridSpacing());
  for (size_t i = 0; i < _grid->dataSize()[0]; ++i) {
    for (size_t j = 0; j < _grid->dataSize()[1]; ++j) {
      tmp = integrator.volumeIntegral<Vector<double, DOS>>(
          [&](const Mesh<DIM>::point_t &pt) -> Vector<double, DOS> {
            Vector<double, DOS> tmp_quad;
            _problemDef->initialCondition(pt, tmp_quad);
            return tmp_quad;
          },
          {i, j});

      tmp /= _grid->AreaOfEle();

      buffer->newBuffer()->setVectorData({i, j}, tmp);
    }
  }
}

void Euler2DPositiveState::timeStep(double &dt) {
  dt = std::numeric_limits<double>::max();
  for (size_t i = 0; i < _grid->dataSize()[0]; ++i) {
    for (size_t j = 0; j < _grid->dataSize()[1]; ++j) {
      dt = std::min(dt, _grid->SizeOfEle() * CFL / _eqInfo->MaxCharacteristicSpeed(buffer->newBuffer()->value({i, j})));
    }
  }
}

void Euler2DPositiveState::update(double &dt) {
  // adaptMesh();
  _ssprkSolver->ssprk2(
      buffer->newBuffer(), dt,
      [&](const GridSystemDataPtr<DIM, DOS> &in, GridSystemDataPtr<DIM, DOS> out) -> void {
        in->calculateBasisFunction();
        _advectionSolver->advect(in, dt, _problemDef, AdvectionSolver<DIM, DOS>::inviscid_only, std::move(out));
      },
      buffer->oldBuffer());

  buffer->swapBuffer();
}

void Euler2DPositiveState::postProcess() {
  // save result
  buffer->newBuffer()->save_data("AdvResult.dat");
}
