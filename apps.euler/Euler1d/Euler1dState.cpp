//
//  Euler1dState.cpp
//  apps
//
//  Created by 杨丰 on 2020/5/16.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "Euler1dState.h"
#include "../../src.euler/error_approximator.h"
#include "../../src.euler/fluxes/flux_LF.h"
#include "../../src.euler/grid_system_data.h"
#include "../ProblemInfo/Euler1DProblem.h"
#include <fstream>

using namespace vox;

Euler1DState::Euler1DState(int l) : CFL(0.1) {
  size_t N = l;
  _grid = std::make_shared<Mesh<DIM>>(Vector<size_t, DIM>({N}), Vector<double, DIM>({1.0 / static_cast<double>(N)}));

  _eqInfo = std::make_shared<EquationNS<DIM>>(1.4);

  _advectionSolver = std::make_shared<AdvectionSolver<DIM, DOS>>(_grid, std::make_shared<FluxLF<DIM, DOS>>());
  _ssprkSolver = std::make_shared<SSPRKSolver<DIM, DOS>>();
}

void Euler1DState::createScene() {
  // load resources and information
  _problemDef =
      std::make_shared<ProblemDef<DIM, DOS>>(_eqInfo, _grid, Euler::bd1d, nullptr, Euler::ModifySod, 1, "1DEuler");

  // set data structure
  buffer->setNewBuffer(GridSystemData<DIM, DOS>::builder().build(_grid));
  buffer->setOldBuffer(buffer->newBuffer()->deepCopy());
}

void Euler1DState::initialize() {
  Vector3D tmp;
  for (size_t i = 0; i < buffer->newBuffer()->numberOfVectorData()[0]; i++) {
    _problemDef->initialCondition(_grid->BaryCenter({i}), tmp);

    buffer->newBuffer()->setVectorData({i}, tmp);
  }
}

void Euler1DState::timeStep(double &dt) {
  dt = std::numeric_limits<double>::max();
  for (size_t i = 0; i < buffer->newBuffer()->numberOfVectorData()[0]; ++i) {
    dt = std::min(dt, _grid->SizeOfEle() * CFL / _eqInfo->MaxCharacteristicSpeed(buffer->newBuffer()->value({i})));
  }
}

void Euler1DState::update(double &dt) {
  // use dual buffer strategy to swap the pointer and avoid copy of all data
  _ssprkSolver->ssprk1(
      buffer->newBuffer(), dt,
      [&](const GridSystemDataPtr<DIM, DOS> &in, GridSystemDataPtr<DIM, DOS> out) -> void {
        in->calculateBasisFunction();
        _advectionSolver->advect(in, dt, _problemDef, AdvectionSolver<DIM, DOS>::inviscid_only, std::move(out));
      },
      buffer->oldBuffer());

  buffer->swapBuffer();
}

void Euler1DState::postProcess() {
  // save result
  buffer->newBuffer()->save_data("AdvResult.dat");
}
