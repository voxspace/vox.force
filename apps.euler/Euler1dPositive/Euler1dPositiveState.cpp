//
//  Euler1dPositiveState.cpp
//  apps
//
//  Created by 杨丰 on 2020/5/16.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "Euler1dPositiveState.h"
#include "../../src.euler/error_approximator.h"
#include "../../src.euler/fluxes/flux_LF.h"
#include "../../src.euler/grid_data_weno.h"
#include "../../src.euler/grid_system_data_NSPositive.h"
#include "../../src.euler/weno_auxiliary.h"
#include "../ProblemInfo/Euler1DProblem.h"
#include <fstream>

using namespace vox;

Euler1DPositiveState::Euler1DPositiveState(int l) : CFL(0.1) {
  size_t N = l;
  _grid = std::make_shared<Mesh<DIM>>(Vector<size_t, DIM>({N}), Vector<double, DIM>({1.0 / static_cast<double>(N)}));

  _eqInfo = std::make_shared<EquationNS<DIM>>(1.4);

  _advectionSolver = std::make_shared<AdvectionSolver<DIM, DOS>>(_grid, std::make_shared<FluxLF<DIM, DOS>>());
  _ssprkSolver = std::make_shared<SSPRKSolver<DIM, DOS>>();
}

void Euler1DPositiveState::createScene() {
  // load resources and information
  _problemDef =
      std::make_shared<ProblemDef<DIM, DOS>>(_eqInfo, _grid, Euler::bd1d, nullptr, Euler::RobustTest3, 1, "1DEuler");

  WENOAuxiliaryPtr<DIM, 1> wenoAux = std::make_shared<WENOAuxiliary<DIM, 1>>(_grid);
  wenoAux->buildReconstructPatch();
  wenoAux->buildWENOPatch();
  wenoAux->updateWENOInfo();
  GridDataWENOBuilderPtr<DIM, 1> dataBuilder = std::make_shared<GridDataWENO<DIM, 1>::Builder>();
  dataBuilder->setWENOAuxiliary(wenoAux);

  // set data structure
  buffer->setNewBuffer(GridSysDataNSPositive<DIM, 1>::builder().build(dataBuilder, _grid, _eqInfo)); // Main change
  buffer->setOldBuffer(buffer->newBuffer()->deepCopy());
}

void Euler1DPositiveState::initialize() {
  Vector3D tmp;
  VolumeIntegrator<DIM, 2> integrator(_grid->origin(), _grid->gridSpacing());
  for (size_t i = 0; i < buffer->newBuffer()->numberOfVectorData()[0]; i++) {
    tmp = integrator.volumeIntegral<Vector3D>(
        [&](const Mesh1D::point_t &pt) -> Vector3D {
          Vector3D tmp_quad;
          _problemDef->initialCondition(pt, tmp_quad);
          return tmp_quad;
        },
        {i});

    tmp /= _grid->AreaOfEle();

    buffer->newBuffer()->setVectorData({i}, tmp);
  }
}

void Euler1DPositiveState::timeStep(double &dt) {
  dt = std::numeric_limits<double>::max();
  for (size_t i = 0; i < buffer->newBuffer()->numberOfVectorData()[0]; ++i) {
    dt = std::min(dt, _grid->SizeOfEle() * CFL / _eqInfo->MaxCharacteristicSpeed(buffer->newBuffer()->value({i})));
  }
}

void Euler1DPositiveState::update(double &dt) {
  _ssprkSolver->ssprk2(
      buffer->newBuffer(), dt,
      [&](const GridSystemDataPtr<DIM, DOS> &in, GridSystemDataPtr<DIM, DOS> out) -> void {
        in->calculateBasisFunction();
        _advectionSolver->advect(in, dt, _problemDef, AdvectionSolver<DIM, DOS>::inviscid_only, std::move(out));
      },
      buffer->oldBuffer());

  buffer->swapBuffer();
}

void Euler1DPositiveState::postProcess() {
  // save result
  buffer->newBuffer()->save_data("AdvResult.dat");
}
