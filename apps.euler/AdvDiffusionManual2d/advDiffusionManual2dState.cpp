//
//  advDiffusionManual2dState.cpp
//  apps
//
//  Created by 杨丰 on 2020/5/17.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "advDiffusionManual2dState.h"
#include "../../src.euler/error_approximator.h"
#include "../../src.euler/fluxes/flux_LF.h"
#include "../../src.euler/grid_data_unlimited.h"
#include "../../src.euler/grid_system_data_minmax.h"
#include "../../src.euler/recon_auxiliary.h"
#include "../../src.common/timer.h"
#include "../ProblemInfo/AdvectionProblem.h"
#include <fstream>

using namespace vox;

const double eps = 0.01;
void init2d(const Mesh2D::point_t &pt, Vector1D &value) {
  if ((pt[0] * pt[0] + pt[1] * pt[1]) < 0.5) {
    value[0] = 1.0;
  } else {
    value[0] = 0.0;
  }
}
// Buckley_Leverett
std::function<double(double u)> flux_x() {
  return [](double u) -> double { return u * u / (u * u + (1.0 - u) * (1.0 - u)); };
}

std::function<double(double u)> flux_y() {
  return [](double u) -> double { return flux_x()(u) * (1.0 - 5.0 * (1 - u) * (1 - u)); };
}

std::function<double(double u)> fluxDers() {
  return [](double u) -> double { return std::sqrt(2.0); };
}

AdvDiffusionManual2DState::AdvDiffusionManual2DState(int l) : CFL(0.1) {
  size_t N = l;
  double s = 3.0 / static_cast<double>(l);
  _grid = std::make_shared<Mesh<DIM>>(Vector<size_t, DIM>({N, N}), Vector<double, DIM>({s, s}),
                                      Vector<double, DIM>({-1.5, -1.5}));

  std::array<EquationScalar<DIM>::fluxFunc, DIM> flux;
  flux[0] = flux_x();
  flux[1] = flux_y();
  std::array<EquationScalar<DIM>::fluxFunc, DIM> fluxDer = {fluxDers(), fluxDers()};
  std::array<EquationScalar<DIM>::viscousFunc, DIM> vfuncs = {ScalarEquationDataBase::ViscousTerm::constVal(eps),
                                                              ScalarEquationDataBase::ViscousTerm::constVal(eps)};

  _eqInfo = std::make_shared<EquationScalar<DIM>>(flux, fluxDer, vfuncs);

  _advectionSolver = std::make_shared<AdvectionSolver<DIM, DOS>>(_grid, std::make_shared<FluxLF<DIM, DOS>>());
  _ssprkSolver = std::make_shared<SSPRKSolver<DIM, DOS>>();
}

void AdvDiffusionManual2DState::createScene() {
  _problemDef = std::make_shared<ProblemDef<DIM, DOS>>(
      _eqInfo, _grid, Advection::bd<DIM>, Advection::VisBoundaryCondition<DIM>, init2d, 2, "2DadvdiffusionBurgers");

  ReconAuxiliaryPtr<DIM, 2> aux = std::make_shared<ReconAuxiliary<DIM, 2>>(_grid);
  aux->buildReconstructPatch();
  aux->updateLSMatrix();
  GridDataUnlimitedBuilderPtr<DIM, 2> dataBuilder = std::make_shared<GridDataUnlimited<DIM, 2>::Builder>();
  dataBuilder->setReconAuxiliary(aux);

  // set data structure
  buffer->setNewBuffer(GridSystemData<DIM, DOS>::builder().build(dataBuilder, _grid));
  buffer->setOldBuffer(buffer->newBuffer()->deepCopy());
}

void AdvDiffusionManual2DState::initialize() {
  Vector1D tmp;
  VolumeIntegrator<DIM, 2> integrator(_grid->origin(), _grid->gridSpacing());
  for (size_t i = 0; i < _grid->dataSize()[0]; ++i) {
    for (size_t j = 0; j < _grid->dataSize()[1]; ++j) {
      tmp = integrator.volumeIntegral<Vector1D>(
          [&](const Mesh<DIM>::point_t &pt) -> Vector1D {
            Vector1D tmp_quad;
            _problemDef->initialCondition(pt, tmp_quad);
            return tmp_quad;
          },
          {i, j});

      tmp /= _grid->AreaOfEle();

      buffer->newBuffer()->setVectorData({i, j}, tmp);
    }
  }
}

void AdvDiffusionManual2DState::timeStep(double &dt) {
  dt = std::numeric_limits<double>::max();
  for (size_t i = 0; i < _grid->dataSize()[0]; ++i) {
    for (size_t j = 0; j < _grid->dataSize()[1]; ++j) {
      dt = std::min(dt, _grid->SizeOfEle() * CFL / _eqInfo->MaxCharacteristicSpeed(buffer->newBuffer()->value({i, j})));
    }
  }
}

void AdvDiffusionManual2DState::update(double &dt) {
  // use dual buffer strategy to swap the pointer and avoid copy of all data
  _ssprkSolver->ssprk3(
      buffer->newBuffer(), dt,
      [&](const GridSystemDataPtr<DIM, DOS> &in, GridSystemDataPtr<DIM, DOS> out) -> void {
        in->calculateBasisFunction();
        _advectionSolver->advect(in, dt, _problemDef, AdvectionSolver<DIM, DOS>::Whole_flux, std::move(out));
      },
      buffer->oldBuffer());

  buffer->swapBuffer();
}

void AdvDiffusionManual2DState::postProcess() {
  buffer->newBuffer()->calculateBasisFunction();

  // save result
  buffer->newBuffer()->save_data("AdvResult.dat");
}
