//
//  NS3dPositiveState.cpp
//  apps
//
//  Created by 杨丰 on 2020/5/17.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "NS3dPositiveState.h"
#include "../../src.euler/error_approximator.h"
#include "../../src.euler/fluxes/flux_LF.h"
#include "../../src.euler/fluxes/flux_NS_Positive.h"
#include "../../src.euler/grid_data_unlimited.h"
#include "../../src.euler/grid_system_data_NSPositive.h"
#include "../../src.euler/recon_auxiliary.h"
#include <fstream>

#include "../ProblemInfo/airfoil3D.h"

using namespace vox;

const double Re = 1000;
const double Pr = 0.72;
const double Gamma = 1.4;
//-------------------------------------------------------------------------
//TODO: Fix for BoundaryMark
NS3DPositiveState::NS3DPositiveState() : CFL(0.1) {
  const double mu = 1.0 / Re;
  const double kappa = Gamma / Re / Pr;

  _grid = std::make_shared<Mesh<3>>(Vector3UZ({10, 10, 10}), Vector<double, 3>({0.1, 0.1, 0.1}));

  _eqInfo = std::make_shared<EquationNS<3>>(Gamma, mu, kappa);

  _advectionSolver = std::make_shared<AdvectionSolver<DIM, DOS>>(_grid, std::make_shared<FluxLF<DIM, DOS>>());
  _ssprkSolver = std::make_shared<SSPRKSolver<DIM, DOS>>();
}

void NS3DPositiveState::createScene() {
  _problemDef = std::make_shared<ProblemDef<DIM, DOS>>(_eqInfo, _grid, AirFoil::BoundaryCondition,
                                                       AirFoil::VisBoundaryCondition, AirFoil::Init, 1, "3DNS");

  ReconAuxiliaryPtr<DIM, 1> aux = std::make_shared<ReconAuxiliary<3, 1>>(_grid);
  aux->buildReconstructPatch();
  aux->updateLSMatrix();
  GridDataUnlimitedBuilderPtr<DIM, 1> dataBuilder = std::make_shared<GridDataUnlimited<3, 1>::Builder>();
  dataBuilder->setReconAuxiliary(aux);

  // set data structure
  buffer->setNewBuffer(GridSysDataNSPositive<DIM, 1>::builder().build(dataBuilder, _grid, _eqInfo));
  buffer->setOldBuffer(buffer->newBuffer()->deepCopy());
}

void NS3DPositiveState::initialize() {
  Vector<double, DOS> tmp;
  VolumeIntegrator<DIM, 2> integrator(_grid->origin(), _grid->gridSpacing());
  for (size_t i = 0; i < _grid->dataSize()[0]; ++i) {
    for (size_t j = 0; j < _grid->dataSize()[1]; ++j) {
      for (size_t k = 0; k < _grid->dataSize()[2]; ++k) {
        tmp = integrator.volumeIntegral<Vector<double, DOS>>(
            [&](const Mesh<DIM>::point_t &pt) -> Vector<double, DOS> {
              Vector<double, DOS> tmp_quad;
              _problemDef->initialCondition(pt, tmp_quad);
              return tmp_quad;
            },
            {i, j, k});

        tmp /= _grid->AreaOfEle();

        buffer->newBuffer()->setVectorData({i, j, k}, tmp);
      }
    }
  }
}

void NS3DPositiveState::timeStep(double &dt) {
  dt = std::numeric_limits<double>::max();
  for (size_t i = 0; i < _grid->dataSize()[0]; ++i) {
    for (size_t j = 0; j < _grid->dataSize()[1]; ++j) {
      for (size_t k = 0; k < _grid->dataSize()[2]; ++k) {
        dt = std::min(dt, _grid->SizeOfEle() * CFL /
                              _eqInfo->MaxCharacteristicSpeed(buffer->newBuffer()->value({i, j, k})));
        dt = std::max(dt, _grid->SizeOfEle() * _grid->SizeOfEle() * 0.1);
      }
    }
  }
}

void NS3DPositiveState::update(double &dt) {
  _ssprkSolver->ssprk1(
      buffer->newBuffer(), dt,
      [&](const GridSystemDataPtr<DIM, DOS> &in, GridSystemDataPtr<DIM, DOS> out) -> void {
        in->calculateBasisFunction();
        _advectionSolver->advect(in, dt, _problemDef, AdvectionSolver<DIM, DOS>::Whole_flux, std::move(out));
      },
      buffer->oldBuffer());

  while (true) {
    bool isNeg = false;
    for (size_t i = 0; i < _grid->dataSize()[0]; ++i) {
      for (size_t j = 0; j < _grid->dataSize()[1]; ++j) {
        for (size_t k = 0; k < _grid->dataSize()[2]; ++k) {
          Vector<double, 5> vals = buffer->oldBuffer()->value({i, j, k});
          double P = (_eqInfo->Gamma - 1) * (vals[3] - 0.5 * (vals[1] * vals[1] + vals[2] * vals[2]) / vals[0]);
          if (P < 0.0 || std::isnan(std::abs(P)) || vals[0] < 1.0e-13) {
            isNeg = true;
          }
        }
      }
    }

    if (!isNeg) {
      break;
    } else {
      dt *= 0.5;
      _ssprkSolver->ssprk1(
          buffer->newBuffer(), dt,
          [&](const GridSystemDataPtr<DIM, DOS> &in, GridSystemDataPtr<DIM, DOS> out) -> void {
            in->calculateBasisFunction();
            _advectionSolver->advect(in, dt, _problemDef, AdvectionSolver<DIM, DOS>::Whole_flux, out);
          },
          buffer->oldBuffer());
    }
  }

  buffer->swapBuffer();
}

void NS3DPositiveState::postProcess() {
  // save result
  buffer->newBuffer()->save_data("AdvResult.dat");
}
