//
//  advDiffusion2dState.cpp
//  apps
//
//  Created by 杨丰 on 2020/5/17.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "advDiffusion2dState.h"
#include "../../src.euler/error_approximator.h"
#include "../../src.euler/fluxes/flux_LF.h"
#include "../../src.euler/grid_data_unlimited.h"
#include "../../src.euler/grid_system_data_minmax.h"
#include "../../src.euler/recon_auxiliary.h"
#include "../../src.common/timer.h"
#include "../ProblemInfo/AdvectionProblem.h"
#include <fstream>

using namespace vox;

const double eps = 1.0e-4;
void init2d(const double t, const Mesh2D::point_t &pt, Vector1D &value) {
  value[0] = std::exp(-8 * M_PI * M_PI * eps * t) * std::sin(2 * M_PI * (pt[0] + pt[1] - 2 * t));
}

AdvDiffusion2DState::AdvDiffusion2DState(int l) : CFL(0.1) {
  size_t N = l;
  double s = 1.0 / static_cast<double>(l);
  _grid = std::make_shared<Mesh<DIM>>(Vector<size_t, DIM>({N, N}), Vector<double, DIM>({s, s}));

  _grid->addPBDescriptor(Direction::left);
  _grid->addPBDescriptor(Direction::bottom);

  std::array<EquationScalar<DIM>::fluxFunc, DIM> flux;
  flux[0] = ScalarEquationDataBase::Advection::flux(1.0);
  flux[1] = ScalarEquationDataBase::Advection::flux(1.0);
  std::array<EquationScalar<DIM>::fluxFunc, DIM> fluxDer;
  fluxDer[0] = ScalarEquationDataBase::Advection::fluxDer(1.0);
  fluxDer[1] = ScalarEquationDataBase::Advection::fluxDer(1.0);
  std::array<EquationScalar<DIM>::viscousFunc, DIM> vfuncs = {ScalarEquationDataBase::ViscousTerm::constVal(eps),
                                                              ScalarEquationDataBase::ViscousTerm::constVal(eps)};

  _eqInfo = std::make_shared<EquationScalar<DIM>>(flux, fluxDer, vfuncs);

  _advectionSolver = std::make_shared<AdvectionSolver<DIM, DOS>>(_grid, std::make_shared<FluxLF<DIM, DOS>>());
  _ssprkSolver = std::make_shared<SSPRKSolver<DIM, DOS>>();
}

void AdvDiffusion2DState::createScene() {
  // load resources and information
  _problemDef = std::make_shared<ProblemDef<DIM, DOS>>(
      _eqInfo, _grid, Advection::bd<DIM>, Advection::VisBoundaryCondition<DIM>,
      [](auto &&PH1, auto &&PH2) {
        return init2d(0.0, std::forward<decltype(PH1)>(PH1), std::forward<decltype(PH2)>(PH2));
      },
      2, "2Dadvdiffusion");

  ReconAuxiliaryPtr<DIM, 2> aux = std::make_shared<ReconAuxiliary<DIM, 2>>(_grid);
  aux->buildReconstructPatch(PatchSearcher<DIM, 2>::Node);
  aux->updateLSMatrix();
  GridDataUnlimitedBuilderPtr<DIM, 2> dataBuilder = std::make_shared<GridDataUnlimited<DIM, 2>::Builder>();
  dataBuilder->setReconAuxiliary(aux);

  // set data structure
  buffer->setNewBuffer(GridSystemData<DIM, DOS>::builder().build(dataBuilder, _grid));
  buffer->setOldBuffer(buffer->newBuffer()->deepCopy());
}

void AdvDiffusion2DState::initialize() {
  Vector1D tmp;
  VolumeIntegrator<DIM, 2> integrator(_grid->origin(), _grid->gridSpacing());
  for (size_t i = 0; i < _grid->dataSize()[0]; ++i) {
    for (size_t j = 0; j < _grid->dataSize()[1]; ++j) {
      tmp = integrator.volumeIntegral<Vector1D>(
          [&](const Mesh<DIM>::point_t &pt) -> Vector1D {
            Vector1D tmp_quad;
            _problemDef->initialCondition(pt, tmp_quad);
            return tmp_quad;
          },
          {i, j});

      tmp /= _grid->AreaOfEle();

      buffer->newBuffer()->setVectorData({i, j}, tmp);
    }
  }
}

void AdvDiffusion2DState::timeStep(double &dt) {
  dt = std::numeric_limits<double>::max();
  for (size_t i = 0; i < _grid->dataSize()[0]; ++i) {
    for (size_t j = 0; j < _grid->dataSize()[1]; ++j) {
      dt = std::min(dt, _grid->SizeOfEle() * CFL / _eqInfo->MaxCharacteristicSpeed(buffer->newBuffer()->value({i, j})));
    }
  }
}

void AdvDiffusion2DState::update(double &dt) {
  // use dual buffer strategy to swap the pointer and avoid copy of all data
  _ssprkSolver->ssprk3(
      buffer->newBuffer(), dt,
      [&](const GridSystemDataPtr<DIM, DOS> &in, GridSystemDataPtr<DIM, DOS> out) -> void {
        in->calculateBasisFunction();
        _advectionSolver->advect(in, dt, _problemDef, AdvectionSolver<DIM, DOS>::Whole_flux, std::move(out));
      },
      buffer->oldBuffer());

  buffer->swapBuffer();
}

void AdvDiffusion2DState::postProcess() {
  buffer->newBuffer()->calculateBasisFunction();

  // save result
  buffer->newBuffer()->save_data("AdvResult.dat");

  // output error
  Vector1D error;
  double dx;
  std::array<std::function<double(const Mesh2D::point_t &pt)>, DOS> realFunc;
  realFunc[0] = [&](const Mesh2D::point_t &pt) -> double {
    Vector1D tmp;
    init2d(0.5, pt, tmp);
    return tmp[0];
  };

  ErrorApproximator<DIM, DOS> L1ErrorApprox(_grid);
  L1ErrorApprox.l1Error(buffer->newBuffer(), realFunc, _eqInfo, error, dx);

  std::string filename = "L1Error.dat";
  std::ofstream output(filename.c_str(), std::ios::app);
  output << dx << "\t";
  for (size_t i = 0; i < DOS; i++) {
    output << error[i] << std::endl;
  }
}
