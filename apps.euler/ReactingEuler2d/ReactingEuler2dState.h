//
//  ReactingEuler2dState.hpp
//  apps
//
//  Created by 杨丰 on 2020/5/17.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef ReactingEuler2dState_hpp
#define ReactingEuler2dState_hpp

#include <utility>

#include "../../src.euler/advection_solver.h"
#include "../../src.euler/dual_buffer.h"
#include "../../src.euler/equations/equation_reactingNS.h"
#include "../../src.euler/physics_solver.h"
#include "../../src.euler/pre_cor_solver.h"
#include "../../src.euler/solver_state.h"
#include "../../src.euler/ssprk_solver.h"

class ReactingEuler2DState : public vox::SolverState<2, 5> {
public:
  //! Default constructor
  explicit ReactingEuler2DState(double spacing);

  //! Default destructor
  ~ReactingEuler2DState() override;

  //! Sets the advection solver.
  void setAdvectionSolver(vox::AdvectionSolverPtr<DIM, DOS> newSolver) { _advectionSolver = std::move(newSolver); }

  //! Sets the max allowed CFL number.
  void setMaxCfl(double newCfl) { CFL = newCfl; }

public:
  //! Load resources and equation info
  void createScene() override;

  //! Set initial value
  void initialize() override;

  //! Time Step
  void timeStep(double &dt) override;

  //! Update every step
  void update(double &dt) override;

  //! PreProcess
  void preProcess() override;

  //! PostProcess
  void postProcess() override;

private:
  void advectionUpdate(double dt);

  void reactionUpdate(double dt);

  double output_maxPressure();

  vox::PreCorSolverPtr<DIM, DOS> _precorSolver;
  vox::SSPRKSolverPtr<DIM, DOS> _ssprkSolver;
  vox::AdvectionSolverPtr<DIM, DOS> _advectionSolver;

  vox::MeshPtr<DIM> _grid;
  vox::EquationReactingNSPtr<DIM> _eqInfo;
  vox::ProblemDefPtr<DIM, DOS> _problemDef;

  double CFL;
};
#endif /* ReactingEuler2dState_hpp */
