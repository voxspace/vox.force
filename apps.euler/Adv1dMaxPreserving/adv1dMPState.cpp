//
//  adv1dMPState.cpp
//  apps
//
//  Created by 杨丰 on 2020/5/16.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "adv1dMPState.h"
#include "../../src.euler/error_approximator.h"
#include "../../src.euler/fluxes/flux_LF.h"
#include "../../src.euler/grid_data_unlimited.h"
#include "../../src.euler/grid_system_data_minmax.h"
#include "../../src.euler/recon_auxiliary.h"
#include "../ProblemInfo/AdvectionProblem.h"
#include <fstream>

using namespace vox;

Adv1DMPState::Adv1DMPState(int l) : CFL(0.1), length(l) {
  size_t N = length;
  _grid = std::make_shared<Mesh<DIM>>(Vector<size_t, DIM>({N}), Vector<double, DIM>({1.0 / static_cast<double>(N)}));
  _grid->addPBDescriptor(Direction::left);

  std::array<EquationScalar<DIM>::fluxFunc, DIM> flux = {ScalarEquationDataBase::Advection::flux(1.0)};
  std::array<EquationScalar<DIM>::fluxFunc, DIM> fluxDer = {ScalarEquationDataBase::Advection::fluxDer(1.0)};
  std::array<EquationScalar<DIM>::viscousFunc, DIM> vfuncs = {};

  _eqInfo = std::make_shared<EquationScalar<DIM>>(flux, fluxDer, vfuncs);

  _advectionSolver = std::make_shared<AdvectionSolver<DIM, DOS>>(_grid, std::make_shared<FluxLF<DIM, DOS>>());
  _ssprkSolver = std::make_shared<SSPRKSolver<DIM, DOS>>();
}

void Adv1DMPState::createScene() {
  // load resources and information
  _problemDef = std::make_shared<ProblemDef<DIM, DOS>>(_eqInfo, _grid, Advection::bd<DIM>, nullptr,
                                                       Advection::init<DIM>, 2, "1Dadv");

  // set data structure
  ReconAuxiliaryPtr<DIM, 2> aux = std::make_shared<ReconAuxiliary<DIM, 2>>(_grid);
  aux->buildReconstructPatch();
  aux->updateLSMatrix();
  GridDataUnlimitedBuilderPtr<DIM, 2> unlimitedDataBuilder = std::make_shared<GridDataUnlimited<DIM, 2>::Builder>();
  unlimitedDataBuilder->setReconAuxiliary(aux);

  buffer->setNewBuffer(GridSysDataMaxPrinciple<DIM, 2>::builder().build(unlimitedDataBuilder, _grid, -1, 1));
  buffer->setOldBuffer(buffer->newBuffer()->deepCopy());
}

void Adv1DMPState::initialize() {
  Vector1D tmp;
  VolumeIntegrator<DIM, 2> integrator(_grid->origin(), _grid->gridSpacing());
  for (size_t i = 0; i < buffer->newBuffer()->numberOfVectorData()[0]; i++) {
    tmp = integrator.volumeIntegral<Vector1D>(
        [&](const Mesh1D::point_t &pt) -> Vector1D {
          Vector1D tmp_quad;
          _problemDef->initialCondition(pt, tmp_quad);
          return tmp_quad;
        },
        {i});

    tmp /= _grid->AreaOfEle();

    buffer->newBuffer()->setVectorData({i}, tmp);
  }
}

void Adv1DMPState::timeStep(double &dt) {
  dt = std::numeric_limits<double>::max();
  for (size_t i = 0; i < buffer->newBuffer()->numberOfVectorData()[0]; ++i) {
    dt = std::min(dt, _grid->SizeOfEle() * CFL / _eqInfo->MaxCharacteristicSpeed(buffer->newBuffer()->value({i})));
  }
}

void Adv1DMPState::update(double &dt) {
  // use dual buffer strategy to swap the pointer and avoid copy of all data
  _ssprkSolver->ssprk3(
      buffer->newBuffer(), dt,
      [&](const GridSystemDataPtr<DIM, DOS> &in, GridSystemDataPtr<DIM, DOS> out) -> void {
        in->calculateBasisFunction();
        _advectionSolver->advect(in, dt, _problemDef, AdvectionSolver<DIM, DOS>::inviscid_only, std::move(out));
      },
      buffer->oldBuffer());

  buffer->swapBuffer();
}

void Adv1DMPState::postProcess() {
  // save result
  buffer->newBuffer()->save_data("AdvResult.dat");

  // output error
  Vector<double, DOS> error;
  double dx;
  std::array<std::function<double(const Mesh1D::point_t &pt)>, DOS> realFunc;
  realFunc[0] = [&](const Mesh1D::point_t &pt) -> double { return std::sin(2 * M_PI * (pt[0] - 1.5)); };

  ErrorApproximator<DIM, DOS> L1ErrorApprox(_grid);
  L1ErrorApprox.l1Error(buffer->newBuffer(), realFunc, _eqInfo, error, dx);

  std::string filename = "L1Error.dat";
  std::ofstream output(filename.c_str(), std::ios::app);
  output << dx << "\t";
  for (size_t i = 0; i < DOS; i++) {
    output << error[i] << std::endl;
  }
}
