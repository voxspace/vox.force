//
//  odeState.hpp
//  apps
//
//  Created by 杨丰 on 2020/5/17.
//  Copyright © 2020 杨丰. All rights reserved.
//

#ifndef odeState_hpp
#define odeState_hpp

#include "../../src.euler/dual_buffer.h"
#include "../../src.euler/equations/equation_scalar.h"
#include "../../src.euler/physics_solver.h"
#include "../../src.euler/problemDef.h"
#include "../../src.euler/solver_state.h"
#include "../../src.euler/ssprk_solver.h"

class ODEState : public vox::SolverState<1, 1> {
public:
  //! Default constructor
  explicit ODEState(int l);

public:
  //! Load resources and equation info
  void createScene() override;

  //! Set initial value
  void initialize() override;

  //! Time Step
  void timeStep(double &dt) override;

  //! PostProcess
  void postProcess() override;

  void update(double &dt) override;

private:
  vox::MeshPtr<DIM> _grid;
  vox::EquationScalarPtr<DIM> _eqInfo;
  vox::ProblemDefPtr<DIM, DOS> _problemDef;

  vox::SSPRKSolverPtr<DIM, DOS> _ssprkSolver;
};

#endif /* odeState_hpp */
