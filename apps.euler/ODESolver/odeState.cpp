//
//  odeState.cpp
//  apps
//
//  Created by 杨丰 on 2020/5/17.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "odeState.h"
#include "../../src.euler/error_approximator.h"
#include "../../src.euler/grid_system_data.h"
#include <fstream>

using namespace vox;

void odeInit(const Mesh1D::point_t &pt, Vector1D &value) { value[0] = 1.0; }

ODEState::ODEState(int l) {
  size_t N = l;
  _grid = std::make_shared<Mesh<DIM>>(Vector<size_t, DIM>({N}), Vector<double, DIM>({1.0 / static_cast<double>(N)}));
  _grid->addPBDescriptor(Direction::left);

  std::array<EquationScalar<DIM>::fluxFunc, DIM> flux = {ScalarEquationDataBase::Advection::flux(1.0)};
  std::array<EquationScalar<DIM>::fluxFunc, DIM> fluxDer = {ScalarEquationDataBase::Advection::fluxDer(1.0)};
  std::array<EquationScalar<DIM>::viscousFunc, DIM> vfuncs{};

  _eqInfo = std::make_shared<EquationScalar<DIM>>(flux, fluxDer, vfuncs);

  _ssprkSolver = std::make_shared<SSPRKSolver<DIM, DOS>>();
}

void ODEState::createScene() {
  // load resources and information
  _problemDef = std::make_shared<ProblemDef<DIM, DOS>>(_eqInfo, _grid, nullptr, nullptr, odeInit, 1, "1DODE");

  // set data structure
  buffer->setNewBuffer(GridSystemData<DIM, DOS>::builder().build(_grid));
  buffer->setOldBuffer(buffer->newBuffer()->deepCopy());
}

void ODEState::initialize() {
  Vector1D tmp;
  for (size_t i = 0; i < buffer->newBuffer()->numberOfVectorData()[0]; i++) {
    _problemDef->initialCondition(_grid->BaryCenter({i}), tmp);

    buffer->newBuffer()->setVectorData({i}, tmp);
  }

  buffer->newBuffer()->calculateBasisFunction();
}

void ODEState::timeStep(double &dt) {
  dt = std::numeric_limits<double>::max();
  for (size_t i = 0; i < buffer->newBuffer()->numberOfVectorData()[0]; ++i) {
    dt = std::min(dt, _grid->SizeOfEle());
  }
}

void ODEState::update(double &dt) {
  _ssprkSolver->ssprk3(
      buffer->newBuffer(), dt,
      [&](const GridSystemDataPtr<DIM, DOS> &in, const GridSystemDataPtr<DIM, DOS> &out) -> void {
        for (size_t i = 0; i < in->numberOfVectorData()[0]; ++i) {
          out->setVectorData({i}, in->value({i}));
        }
      },
      buffer->oldBuffer());

  buffer->swapBuffer();
}

void ODEState::postProcess() {
  // save result
  buffer->newBuffer()->save_data("AdvResult.dat");

  // output error
  Vector1D error;
  double dx;
  std::array<std::function<double(const Mesh1D::point_t &pt)>, 1> realFunc;
  realFunc[0] = [&](const Mesh1D::point_t &pt) -> double { return std::exp(1.0); };

  ErrorApproximator<DIM, DOS> L1ErrorApprox(_grid);
  L1ErrorApprox.l1Error(buffer->newBuffer(), realFunc, _eqInfo, error, dx);

  std::string filename = "L1Error.dat";
  std::ofstream output(filename.c_str(), std::ios::app);
  output << dx << "\t";
  for (size_t i = 0; i < DOS; i++) {
    output << error[i] << std::endl;
  }
}
