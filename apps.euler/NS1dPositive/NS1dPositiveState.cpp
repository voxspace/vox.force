//
//  NS1dPositiveState.cpp
//  apps
//
//  Created by 杨丰 on 2020/5/16.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "NS1dPositiveState.h"
#include "../../src.euler/error_approximator.h"
#include "../../src.euler/fluxes/flux_LF.h"
#include "../../src.euler/grid_data_unlimited.h"
#include "../../src.euler/grid_system_data_NSPositive.h"
#include "../../src.euler/recon_auxiliary.h"
#include "../ProblemInfo/Euler1DProblem.h"
#include <utility>

using namespace vox;

const double Re = 1000;
const double Pr = 0.72;
const double Gamma = 1.4;
void NSInitCondition(const Point<1> &pt, Vector3D &u) {
  // t = 0.1
  u[0] = 1.0;
  u[1] = 1.0 * 0.0;
  u[2] = 12.0 / (Gamma - 1) + 0.5 * std::exp(-4.0 * std::cos(pt[0] * pt[0]));
}
//---------------------------------------------------------------------------------
NS1DPositiveState::NS1DPositiveState(int l) : CFL(0.1) {
  const double mu = 1.0 / Re;
  const double kappa = Gamma / Re / Pr;

  size_t N = l;
  _grid = std::make_shared<Mesh<DIM>>(Vector<size_t, DIM>({N}), Vector<double, DIM>({1.0 / static_cast<double>(N)}));

  _eqInfo = std::make_shared<EquationNS<DIM>>(Gamma, mu, kappa);

  _advectionSolver = std::make_shared<AdvectionSolver<DIM, DOS>>(_grid, std::make_shared<FluxLF<DIM, DOS>>());
  _ssprkSolver = std::make_shared<SSPRKSolver<DIM, DOS>>();
}

void NS1DPositiveState::createScene() {
  // load resources and information
  _problemDef =
      std::make_shared<ProblemDef<DIM, DOS>>(_eqInfo, _grid, Euler::bd1d, Euler::visbd1d, Euler::Lax, 1, "1DNS");

  ReconAuxiliaryPtr<DIM, 1> aux = std::make_shared<ReconAuxiliary<DIM, 1>>(_grid);
  aux->buildReconstructPatch();
  aux->updateLSMatrix();
  GridDataUnlimitedBuilderPtr<DIM, 1> dataBuilder = std::make_shared<GridDataUnlimited<DIM, 1>::Builder>();
  dataBuilder->setReconAuxiliary(aux);

  // set data structure
  buffer->setNewBuffer(GridSysDataNSPositive<DIM, 1>::builder().build(dataBuilder, _grid, _eqInfo)); // Main change
  buffer->setOldBuffer(buffer->newBuffer()->deepCopy());
}

void NS1DPositiveState::initialize() {
  Vector3D tmp;
  VolumeIntegrator<DIM, 2> integrator(_grid->origin(), _grid->gridSpacing());
  for (size_t i = 0; i < buffer->newBuffer()->numberOfVectorData()[0]; i++) {
    tmp = integrator.volumeIntegral<Vector3D>(
        [&](const Mesh1D::point_t &pt) -> Vector3D {
          Vector3D tmp_quad;
          _problemDef->initialCondition(pt, tmp_quad);
          return tmp_quad;
        },
        {i});

    tmp /= _grid->AreaOfEle();

    buffer->newBuffer()->setVectorData({i}, tmp);
  }
}

void NS1DPositiveState::timeStep(double &dt) {
  dt = std::numeric_limits<double>::max();
  for (size_t i = 0; i < buffer->newBuffer()->numberOfVectorData()[0]; ++i) {
    dt = std::min(dt, _grid->SizeOfEle() * CFL / _eqInfo->MaxCharacteristicSpeed(buffer->newBuffer()->value({i})));
    dt = std::min(dt, _grid->SizeOfEle() * _grid->SizeOfEle());
  }
}

void NS1DPositiveState::update(double &dt) {
  _ssprkSolver->ssprk2(
      buffer->newBuffer(), dt,
      [&](const GridSystemDataPtr<DIM, DOS> &in, GridSystemDataPtr<DIM, DOS> out) -> void {
        in->calculateBasisFunction();
        _advectionSolver->advect(in, dt, _problemDef, AdvectionSolver<DIM, DOS>::Whole_flux, std::move(out));
      },
      buffer->oldBuffer());

  buffer->swapBuffer();
}

void NS1DPositiveState::postProcess() {
  // save result
  buffer->newBuffer()->save_data("AdvResult.dat");
}
