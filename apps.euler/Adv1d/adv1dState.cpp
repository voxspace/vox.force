//
//  adv1dState.cpp
//  apps
//
//  Created by 杨丰 on 2020/5/16.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "adv1dState.h"
#include "../../src.euler/error_approximator.h"
#include "../../src.euler/fluxes/flux_LF.h"
#include "../../src.euler/grid_system_data.h"
#include "../ProblemInfo/AdvectionProblem.h"
#include <fstream>

using namespace vox;

Adv1DState::Adv1DState(int l) : CFL(0.1), length(l) {
  size_t N = length;
  _grid = std::make_shared<Mesh<DIM>>(Vector<size_t, DIM>({N}), Vector<double, DIM>({1.0 / static_cast<double>(N)}));
  _grid->addPBDescriptor(Direction::left);

  std::array<vox::EquationScalar<DIM>::fluxFunc, DIM> flux = {ScalarEquationDataBase::Advection::flux(1.0)};
  std::array<EquationScalar<DIM>::fluxFunc, DIM> fluxDer = {ScalarEquationDataBase::Advection::fluxDer(1.0)};
  std::array<EquationScalar<DIM>::viscousFunc, DIM> vfuncs{};

  _eqInfo = std::make_shared<EquationScalar<DIM>>(flux, fluxDer, vfuncs);

  _advectionSolver = std::make_shared<AdvectionSolver<DIM, DOS>>(_grid, std::make_shared<FluxLF<DIM, DOS>>());
  _ssprkSolver = std::make_shared<SSPRKSolver<DIM, DOS>>();
}

void Adv1DState::createScene() {
  // load resources and information
  _problemDef = std::make_shared<ProblemDef<DIM, DOS>>(_eqInfo, _grid, Advection::bd<DIM>, nullptr,
                                                       Advection::init<DIM>, 1, "1Dadv");

  // set data structure
  buffer->setNewBuffer(GridSystemData<DIM, DOS>::builder().build(_grid));
  buffer->setOldBuffer(buffer->newBuffer()->deepCopy());
}

void Adv1DState::initialize() {
  Vector1D tmp;
  for (size_t i = 0; i < buffer->newBuffer()->numberOfVectorData()[0]; i++) {
    _problemDef->initialCondition(_grid->BaryCenter({i}), tmp);

    buffer->newBuffer()->setVectorData({i}, tmp);
  }
}

void Adv1DState::timeStep(double &dt) {
  dt = std::numeric_limits<double>::max();
  for (size_t i = 0; i < buffer->newBuffer()->numberOfVectorData()[0]; ++i) {
    dt = std::min(dt, _grid->SizeOfEle() * CFL / _eqInfo->MaxCharacteristicSpeed(buffer->newBuffer()->value({i})));
  }
}

void Adv1DState::update(double &dt) {
  // use dual buffer strategy to swap the pointer and avoid copy of all data
  _ssprkSolver->ssprk1(
      buffer->newBuffer(), dt,
      [&](const GridSystemDataPtr<DIM, DOS> &in, GridSystemDataPtr<DIM, DOS> out) -> void {
        in->calculateBasisFunction();
        _advectionSolver->advect(in, dt, _problemDef, AdvectionSolver<DIM, DOS>::inviscid_only, std::move(out));
      },
      buffer->oldBuffer());

  buffer->swapBuffer();
}

void Adv1DState::postProcess() {
  // save result
  buffer->newBuffer()->save_data("AdvResult.dat");

  // output error
  Vector<double, DOS> error;
  double dx;
  std::array<std::function<double(const Mesh1D::point_t &pt)>, DOS> realFunc;
  realFunc[0] = [&](const Mesh1D::point_t &pt) -> double { return std::sin(2 * M_PI * (pt[0] - 1.5)); };

  ErrorApproximator<DIM, DOS> ErrorApprox(_grid);
  ErrorApprox.l1Error(buffer->newBuffer(), realFunc, _eqInfo, error, dx);

  std::string filename = "L1Error.dat";
  std::ofstream output(filename.c_str(), std::ios::app);
  output << dx << "\t";
  for (size_t i = 0; i < DOS; i++) {
    output << error[i] << std::endl;
  }
}
