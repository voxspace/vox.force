//
//  laplacian1dState.cpp
//  apps
//
//  Created by 杨丰 on 2020/5/17.
//  Copyright © 2020 杨丰. All rights reserved.
//

#include "laplacian1dState.h"
#include "../../src.euler/error_approximator.h"
#include "../../src.euler/fluxes/flux_LF.h"
#include "../../src.euler/grid_data_unlimited.h"
#include "../../src.euler/grid_system_data_minmax.h"
#include "../../src.euler/recon_auxiliary.h"
#include "../ProblemInfo/AdvectionProblem.h"
#include <fstream>

using namespace vox;

Laplacian1DState::Laplacian1DState(int l) {
  size_t N = l;
  _grid = std::make_shared<Mesh<DIM>>(Vector<size_t, DIM>({N}), Vector<double, DIM>({1.0 / static_cast<double>(N)}));

  _grid->addPBDescriptor(Direction::left);

  std::array<EquationScalar<DIM>::fluxFunc, DIM> flux{};
  std::array<EquationScalar<DIM>::fluxFunc, DIM> fluxDer{};
  std::array<EquationScalar<DIM>::viscousFunc, DIM> vfuncs = {ScalarEquationDataBase::ViscousTerm::constVal(1.0)};

  _eqInfo = std::make_shared<EquationScalar<DIM>>(flux, fluxDer, vfuncs);

  _advectionSolver = std::make_shared<AdvectionSolver<DIM, DOS>>(_grid, std::make_shared<FluxLF<DIM, DOS>>());
}

void Laplacian1DState::createScene() {
  _problemDef = std::make_shared<ProblemDef<DIM, DOS>>(
      _eqInfo, _grid, Advection::bd<DIM>, Advection::VisBoundaryCondition<DIM>, Advection::init<DIM>, 1, "1DLaplacian");

  ReconAuxiliaryPtr<DIM, 1> aux = std::make_shared<ReconAuxiliary<DIM, 1>>(_grid);
  aux->buildReconstructPatch();
  aux->updateLSMatrix();
  GridDataUnlimitedBuilderPtr<DIM, 1> dataBuilder = std::make_shared<GridDataUnlimited<DIM, 1>::Builder>();
  dataBuilder->setReconAuxiliary(aux);

  // set data structure
  buffer->setNewBuffer(GridSystemData<DIM, DOS>::builder().build(dataBuilder, _grid));
  buffer->setOldBuffer(buffer->newBuffer()->deepCopy());
}

void Laplacian1DState::initialize() {
  Vector1D tmp;
  VolumeIntegrator<DIM, 2> integrator(_grid->origin(), _grid->gridSpacing());
  for (size_t i = 0; i < buffer->newBuffer()->numberOfVectorData()[0]; i++) {
    tmp = integrator.volumeIntegral<Vector1D>(
        [&](const Mesh1D::point_t &pt) -> Vector1D {
          Vector1D tmp_quad;
          _problemDef->initialCondition(pt, tmp_quad);
          return tmp_quad;
        },
        {i});

    tmp /= _grid->AreaOfEle();

    buffer->newBuffer()->setVectorData({i}, tmp);
  }
}

void Laplacian1DState::timeStep(double &dt) { dt = 0.1; }

void Laplacian1DState::update(double &dt) {
  buffer->newBuffer()->calculateBasisFunction();
  _advectionSolver->advect(buffer->newBuffer(), dt, _problemDef, AdvectionSolver<DIM, DOS>::Viscous_only,
                           buffer->oldBuffer());
  buffer->swapBuffer();
}

void Laplacian1DState::postProcess() {
  // save result
  buffer->newBuffer()->save_data("AdvResult.dat");
  buffer->newBuffer()->calculateBasisFunction();

  // output error
  Vector1D error;
  double dx;
  std::array<std::function<double(const Mesh1D::point_t &pt)>, 1> realFunc;
  realFunc[0] = [&](const Mesh1D::point_t &pt) -> double { return std::pow(2 * M_PI, 2) * std::sin(2 * M_PI * pt[0]); };

  ErrorApproximator<DIM, DOS> L1ErrorApprox(_grid);
  L1ErrorApprox.l1Error(buffer->newBuffer(), realFunc, _eqInfo, error, dx);

  std::string filename = "L1Error.dat";
  std::ofstream output(filename.c_str(), std::ios::app);
  output << dx << "\t";
  for (size_t i = 0; i < DOS; i++) {
    output << error[i] << std::endl;
  }
}
