# Vox.Force

Vox.Force in a numerical simulation engine, which is now mainly contain the fluid simulation methods like Euler,
Lagrange and Hybrid methods. This code is build on C++17 and can be compiled with most of the commonly available
compilers such as g++, clang++.

## Key Features

- Finite Volume Methods. Including several kinds of flux and high order reconstruction methods like WENO and TVD, which
  can be used to solve compressible equations like Euler Equations and Reacting Euler Equations.
- Incompressible Solver on sparse data structure [OpenVDB](https://github.com/AcademySoftwareFoundation/openvdb) .
  Including VDB point searcher for SPH and hybrid method like PIC and FLIP, which can be save as vdb format and render
  them in Houdini.
- Incompressible Solver on dense data structure. Including mesh-based method like Stable fluid-based method, Levelset
  methods and hybrid methods.

## Acknowledge

This repos is inspired by [fluid-engine-dev](https://github.com/doyubkim/fluid-engine-dev) which teach me how to code
numerical algorithm by using C++. In this repos, files in src.common and src.lagrange folder are all forked
from [fluid-engine-dev(branches dev-v2-gpu)](https://github.com/doyubkim/fluid-engine-dev).

Thanks to [Guanghui Hu](https://www.fst.um.edu.mo/people/garyhu/) and [Ruo Li](http://dsec.pku.edu.cn/~rli/index.php)
who teach me how to make researches in the numerical simulations and [AFEPack](https://github.com/wangheyu/AFEPack)
inspires me to start this opensource project.

